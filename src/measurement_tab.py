# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets,QtGui,QtCore
import time
import traceback
import sys
import jsonpickle
from src.measurement_file import MeasurementFile
from src.widgets import APTSelectWidget
from src.python_editor import PythonEditor
from time import perf_counter as clock

class NoRequiredDevicesError(Exception):
    pass

class EndOfMeasurementException(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__("Thread cancelled", *args, **kwargs)


class Measurement(QtCore.QThread):
    measurement_progress = QtCore.pyqtSignal(float)
    estimated_time = QtCore.pyqtSignal(float)
    measurement_info = QtCore.pyqtSignal(str)
    measurement_error = QtCore.pyqtSignal(str)
    trigger_start = QtCore.pyqtSignal()

    automatic_file = True # change in subclass if you don't want automatic MeasurementFile object
    enable_kill = False # change in subclass if you don't want to create additional kill button

    def __init__(self, parent=None):
        super().__init__(parent)
        self.killed = False
        self.cancelled = False
        self.paused = False
        
    def setup_additional_widgets(self, canvas_widget):
        """ This function is supposed to create user interface """
        pass
        
    
    def check_if_cancel_requested(self):
        """ Call this function periodically to enable user to pause or cancel
        the measurement """
        if self.paused:
            self.log("Paused")
            while self.paused:
                time.sleep(0.2)
                if self.cancelled:
                    raise EndOfMeasurementException()
        else:
            if self.cancelled:
                raise EndOfMeasurementException()
    
    def sleep_checking_cancel(self, timeout):
        t1 = time.time()+timeout
        remaining = t1 - time.time()
        while remaining > 0:
            self.check_if_cancel_requested()
            self.msleep(int(1000*(min(0.5,remaining))))
            remaining = t1 - time.time()
            
    def wait_checking_cancel(self, func):
        """ Call this function to wait for certain condition (e.g., until 
        motor reaches its destination) while checking for pause or cancel """
        self.check_if_cancel_requested()
        while not func():
            self.check_if_cancel_requested()
            time.sleep(0.2)
    
    def log(self, msg):
        """ Call this function instead of print """
        self.measurement_info.emit(str(msg))
    
    def run(self):
        """ You should override this method in subclass """
        try:
            for i in range(10):
                self.check_if_cancel_requested()
                time.sleep(1)
                self.log("Step %d" %i)
                self.measurement_progress.emit(i)
        except:
            pass

    def on_start(self):
        pass
    
    def on_stop(self):
        pass
    

class MeasurementTab(QtWidgets.QWidget):
    def __init__(self, thread_class=Measurement, parent=None):
        super().__init__(parent)
        self.thread = thread_class(self)
        old_run = self.thread.run
        def secured_run():
            try:
                old_run()
            except EndOfMeasurementException:
                self.thread.measurement_error.emit("Cancelled")
            except Exception as e:
                self.thread.measurement_error.emit(traceback.format_exc())
        self.thread.run = secured_run
        self.thread.finished.connect(self.thread_stopped)
        self.thread.trigger_start.connect(self.start)

        self.pause_start = None
        self.pause_time_total = None
        self.time_estimator_ignore_until = None

        self.setup_ui()   
        
    def setup_ui(self):
        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

        if self.thread.automatic_file:
            hlayout = QtWidgets.QHBoxLayout()
            hlayout.addWidget(QtWidgets.QLabel("File name:"))

            self.file_input = QtWidgets.QLineEdit()
            hlayout.addWidget(self.file_input, 4)
            button = QtWidgets.QPushButton("Browse")
            def browse_for_file():
                file = QtWidgets.QFileDialog.getSaveFileName(self,
                                "Save file as", self.file_input.text(),
                                "HDF5 file (*.hd5)")
                if file[0]:
                    self.file_input.setText(file[0])
            button.clicked.connect(browse_for_file)
            hlayout.addWidget(button)
            layout.addLayout(hlayout)
        
        widget = QtWidgets.QWidget(self)
        layout.addWidget(widget)
        self.thread.setup_additional_widgets(widget)

        hlayout = QtWidgets.QHBoxLayout()  # bottom layout for progress bar and control buttons

        self.progress_bar = QtWidgets.QProgressBar()
        self.progress_bar.setAlignment(QtCore.Qt.AlignCenter)
        self.progress_bar.setValue(0)
        self.progress_bar.setMaximum(1000)
        self.thread.measurement_progress.connect(self.progress_updated)
        hlayout.addWidget(self.progress_bar)
        hlayout.setStretch(0, 10)

        self.log_widget = QtWidgets.QTextEdit()
        self.log_widget.setReadOnly(True)
        self.thread.measurement_info.connect(self.log_widget.append)
        self.thread.measurement_error.connect(lambda msg: self._show_msg(msg, 'red'))
        layout.addWidget(self.log_widget)
        button_box = QtWidgets.QDialogButtonBox()
        button_box.setContentsMargins(0, 0, 0, 0)
        self.start_button = QtWidgets.QPushButton("Start")
        button_box.addButton(self.start_button, QtWidgets.QDialogButtonBox.AcceptRole)
        self.start_button.clicked.connect(self.start)
        self.pause_button = QtWidgets.QPushButton("Pause")
        self.pause_button.clicked.connect(self.pause)
        self.pause_button.setEnabled(False)
        self.pause_button.setCheckable(True)
        button_box.addButton(self.pause_button, QtWidgets.QDialogButtonBox.NoRole)
        self.cancel_button = QtWidgets.QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.cancel)
        self.cancel_button.setEnabled(False)
        button_box.addButton(self.cancel_button, QtWidgets.QDialogButtonBox.RejectRole)
        self.kill_button = QtWidgets.QPushButton("Kill")
        self.kill_button.clicked.connect(self.kill_process)
        self.kill_button.setEnabled(False)
        if self.thread.enable_kill:
            button_box.addButton(self.kill_button, QtWidgets.QDialogButtonBox.RejectRole)
        hlayout.addWidget(button_box)

        layout.addLayout(hlayout)

        self.load_settings()

        self.file_input.editingFinished.connect(self.save_settings)
        def make_connect(object):
            if isinstance(object, QtWidgets.QLineEdit):
                object.editingFinished.connect(self.save_settings)
            elif isinstance(object, QtWidgets.QComboBox):
                object.currentIndexChanged.connect(self.save_settings)
            elif isinstance(object, QtWidgets.QDoubleSpinBox):
                object.valueChanged.connect(self.save_settings)
            elif isinstance(object, QtWidgets.QGroupBox):
                object.clicked.connect(self.save_settings)
            elif isinstance(object, APTSelectWidget):
               object.textChanged.connect(self.save_settings)
            elif isinstance(object, QtWidgets.QCheckBox):
                object.clicked.connect(self.save_settings)
            elif isinstance(object, PythonEditor):
                object.SCN_FOCUSOUT.connect(self.save_settings)
            elif isinstance(object, list):
                for i in range(len(object)):
                    make_connect(object[i])
        for name, object in vars(self.thread).items():
            make_connect(object)

    def grab_settings(self):
        def parse_value(object):
            if isinstance(object, QtWidgets.QLineEdit):
                return object.text()
            elif isinstance(object, QtWidgets.QComboBox):
                return object.currentText()
            elif isinstance(object, QtWidgets.QDoubleSpinBox):
                return object.value()
            elif isinstance(object, QtWidgets.QGroupBox):
                return object.isChecked()
            elif isinstance(object, APTSelectWidget):
                return object.text()
            elif isinstance(object, QtWidgets.QCheckBox):
                return object.isChecked()
            elif isinstance(object, PythonEditor):
                return object.text()
            elif isinstance(object, list):
                return [parse_value(object[i]) for i in range(len(object))]
            return None

        parsed = {}
        parsed_measurement = {}
        for k, v in vars(self.thread).items():
            p = parse_value(v)
            if p is not None and not (isinstance(p, list) and all(x is None for x in p)):
                parsed[k] = p

        for k, v in vars(self).items():
            p = parse_value(v)
            if p is not None and not (isinstance(p, list) and all(x is None for x in p)):
                parsed_measurement[k] = p
        return (parsed_measurement, parsed)

    def save_settings(self):
        filename = "config\\tab_" + type(self.thread).__name__.lower() + ".cfg"
        try:
            with open(filename, "w") as file:
                file.write(jsonpickle.encode(self.grab_settings()))
        except Exception:
            print(traceback.format_exc())

    def load_settings(self):
        filename = "config\\tab_" + type(self.thread).__name__.lower() + ".cfg"
        try:
            with open(filename, "r") as file:
                parsed_measurement, parsed = jsonpickle.decode(file.read())
                self.file_input.setText(parsed_measurement["file_input"])
                def apply_value(value, object):
                    if isinstance(object, QtWidgets.QLineEdit):
                       object.setText(value)
                    elif isinstance(object, QtWidgets.QComboBox):
                        object.setCurrentText(value)
                    elif isinstance(object, QtWidgets.QDoubleSpinBox):
                        object.setValue(value)
                    elif isinstance(object, QtWidgets.QGroupBox):
                        object.setChecked(value)
                    elif isinstance(object, APTSelectWidget):
                        object.setText(value)
                    elif isinstance(object, QtWidgets.QCheckBox):
                        object.setChecked(value)
                    elif isinstance(object, PythonEditor):
                        object.setText(value)
                    elif isinstance(object, list):
                        for i in range(len(value)):
                            apply_value(value[i], object[i])
                    return None
                for name in parsed:
                    apply_value(parsed[name], getattr(self.thread, name))
        except Exception:
            pass

    def start(self):
        self.log_widget.clear()
        if self.thread.automatic_file:
            try:
                filename = self.file_input.text()
                self.thread.measurement_file = MeasurementFile(filename)
            except:
                self._show_msg('Cannot open file: %s' % filename) , 'red'
                return
        self.file_input.setEnabled(False)
        self.start_button.setEnabled(False)
        self.pause_button.setEnabled(True)
        self.cancel_button.setEnabled(True)
        self.kill_button.setEnabled(True)
        self.thread.paused = self.pause_button.isChecked()
        self.thread.cancelled = False
        self.thread.killed = False
        self.log_widget.setTextColor(QtGui.QColor(255,0,0))
        self.log_widget.append("Starting")
        self.log_widget.setTextColor(QtGui.QColor(0,0,0))
        self.progress_bar.setValue(0)
        self.progress_bar_history = [(clock(), 0)]
        self.pause_time_total = 0
        self.time_estimator_ignore_until = 0
        self.thread.automatic_file_path = self.file_input.text()  # TODO consult with Tomek
        self.thread.on_start()
        self.thread.start()

    def cancel(self):
        self.thread.cancelled = True
        self.cancel_button.setText("Canceling")

    def kill_process(self):
        self.thread.killed = True
    
    def _show_msg(self, msg, color='red'):
        self.log_widget.setTextColor(QtGui.QColor(color))
        self.log_widget.append(msg)
        self.log_widget.setTextColor(QtGui.QColor(color))
    
    def pause(self, activate):
        self.log_widget.setTextColor(QtGui.QColor(255, 0, 0))
        self.log_widget.append("Requesting pause - please wait" if activate else "Resuming")
        self.log_widget.setTextColor(QtGui.QColor(0, 0, 0))
        self.thread.paused = activate
        if activate:
            self.pause_start = clock()
        else:
            self.pause_time_total += clock() - self.pause_start
            
    def thread_stopped(self):
        if self.thread.automatic_file:
            try:
                self.thread.measurement_file.close()
                del(self.thread.measurement_file) # close the file
            except Exception as e:
                traceback.print_exc(file=sys.stdout)
        self.thread.on_stop()
        self.file_input.setEnabled(True)
        self.start_button.setEnabled(True)
        self.pause_button.setEnabled(False)
        self.pause_button.setChecked(False)
        self.cancel_button.setEnabled(False)
        self.kill_button.setEnabled(False)
        self.cancel_button.setText("Cancel")
        self.progress_bar.setValue(0)
        self.log_widget.setTextColor(QtGui.QColor(255,0,0))
        self.log_widget.append("Finished")
        self.log_widget.setTextColor(QtGui.QColor(0,0,0))
        self.progress_bar.setValue(0)
        self.progress_bar.setFormat("")

    def progress_updated(self, x):
        self.progress_bar_history += [(clock() - self.pause_time_total, x)]
        self.progress_bar.setValue(1000 * x)

        if self.progress_bar_history[-1][1] < self.progress_bar_history[-2][1] - 0.2:
            self.time_estimator_ignore_until = len(self.progress_bar_history) - 1

        n = len(self.progress_bar_history) - 1
        n_disc = int(abs(n - 0.1 - self.time_estimator_ignore_until) ** 0.55) + self.time_estimator_ignore_until
        duration = self.progress_bar_history[n][0] - self.progress_bar_history[n_disc][0]
        progress = self.progress_bar_history[n][1] - self.progress_bar_history[n_disc][1]
        try:
            t = int(round(duration / progress * (1 - x)))
            progress_bar_text = f"{round(x * 1000) / 10:3.1f}% {t // 3600 :5d}h {t // 60 % 60 :02d}min {t % 60 :02d}s left"
            self.progress_bar.setFormat(progress_bar_text)
        except ZeroDivisionError:
            pass

