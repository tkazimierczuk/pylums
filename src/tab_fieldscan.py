# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets,QtCore
import devices
from devices.thorlabs import apt
#from devices.oxford.set
import time

from .measurement_tab import Measurement
from .tab_anisotropy import APTSelectWidget

from numpy import linspace
import numpy as np


DO_NOTHING = 0
DO_GO_ZERO = 1
DO_PERSISTENT = 2

class FieldSweepMeasurement(Measurement):
    title = 'Field sweep'

    def run(self):
        self.canvas_widget.setEnabled(False)
        try:
            # calculate vector of fields ahead of time
            field_start = self.from_widget.value()
            field_stop = self.to_widget.value()
            field_step = self.step_widget.value()
            if field_start > field_stop:
                field_step = -abs(field_step)
            else:
                field_step = abs(field_step)
            fields = np.arange(field_start, field_stop, field_step)
            if abs(fields[-1] - field_stop) > 1e-5:
                fields = np.r_[fields, field_stop]
            
            # invoke the magnet device
            self._magnet = devices.active_devices[self.magnet_device.text()]
            self._spectrometer = devices.active_devices[self.spectrometer_device.text()]
            
            # prepare apt device (if needed)
            self._angles = []
            self._angles2 = []
            
            if self.apt_group.isChecked():
                
                if self.apt_group2.isChecked():
                    
                    self._apt = devices.active_devices[self.apt_device.text()]
                    self._motornum = int(self.motor_id.text())
                    
                    self._apt2 = devices.active_devices[self.apt_device2.text()]
                    self._motornum2 = int(self.motor_id2.text())
                    for widget in self.apt_pos:
                        try:
                            self._angles.append(float(widget.text()))
                        except ValueError:
                            pass
                    for widget in self.apt_pos2:
                        try:
                            self._angles2.append(float(widget.text()))
                        except ValueError:
                            pass
                else:
                    self._apt = devices.active_devices[self.apt_device.text()]
                    self._motornum = int(self.motor_id.text())
                    for widget in self.apt_pos:
                        try:
                            self._angles.append(float(widget.text()))
                        except ValueError:
                            pass
                        
            self.log("Will measure %d magnetic fields" % len(fields))
            if self._angles:
                if self._angles2:
                    self.log("At each field, %d positions of APT will be measured" % int(len(self._angles)+len(self._angles2)))
                else:
                    self.log("At each field, %d positions of APT will be measured" % len(self._angles))
            else:
                self.log("APT will NOT be used")
            
            # now actual measurement proceeds
            
            for i,field in enumerate(fields):
                
                self.measurement_progress.emit(i/len(fields))
                
                self.check_if_cancel_requested()
                
                self._magnet.set_field(field)
                
                self._measurement_at_single_field()               
        
        except:
            self.canvas_widget.setEnabled(True)
            raise
            
        
        
        if not self.apt_group2.isChecked():
            for i,angle in enumerate(self._angles):
                filename = self.measurement_file.filename +'_pol_'+str(angle) + '.dat'
                x = self.measurement_file.file['x'][0]
                y = fields
                z = self.measurement_file.file['z'][i::len(self._angles)]
                self.measurement_file._export_txt(filename, x, y, z)
        
        
            if len(self._angles) == 2 and fields[0] == 0:
                filename = self.measurement_file.filename +'_combined.dat'
                x = self.measurement_file.file['x'][0]
                y = np.r_[-fields[::-1], fields]
                z1 = self.measurement_file.file['z'][0::2,:][::-1,:]
                z2 = self.measurement_file.file['z'][1::2,:]
                z = np.r_[z1,z2]
                self.measurement_file._export_txt(filename, x, y, z)
                
            if len(self._angles) == 2 and fields[-1] == 0:
                filename = self.measurement_file.filename +'_combined.dat'
                x = self.measurement_file.file['x'][0]
                y = np.r_[fields, -fields[::-1]]
                z1 = self.measurement_file.file['z'][0::2,:][::-1,:]
                z2 = self.measurement_file.file['z'][1::2,:]
                z = np.r_[z2,z1]
                self.measurement_file._export_txt(filename, x, y, z)
        else:
        
            for j,angle2 in enumerate(self._angles2):
                for i,angle in enumerate(self._angles):
                    filename = self.measurement_file.filename +'_pol1_'+str(angle) + '_pol2_'+str(angle2)+'.dat'
                    x = self.measurement_file.file['x'][0]
                    y = fields
                    z = self.measurement_file.file['z'][int(i+(j*len(self._angles)))::int(len(self._angles)+len(self._angles2))]
                    self.measurement_file._export_txt(filename, x, y, z)
            
            
            if len(self._angles) == 2 and len(self._angles2) == 2 and fields[0] == 0:
                filename = self.measurement_file.filename +'_pol2_'+str(self._angles2[0])+'_combined.dat'
                x = self.measurement_file.file['x'][0]
                y = np.r_[-fields[::-1], fields]
                z1 = self.measurement_file.file['z'][0::4,:][::-1,:]
                z2 = self.measurement_file.file['z'][1::4,:]
                z = np.r_[z1,z2]
                self.measurement_file._export_txt(filename, x, y, z)
                
            if len(self._angles) == 2 and len(self._angles2) == 2 and fields[-1] == 0:
                filename = self.measurement_file.filename +'_pol2_'+str(self._angles2[0])+'_combined.dat'
                x = self.measurement_file.file['x'][0]
                y = np.r_[fields, -fields[::-1]]
                z1 = self.measurement_file.file['z'][0::4,:][::-1,:]
                z2 = self.measurement_file.file['z'][1::4,:]
                z = np.r_[z2,z1]
                self.measurement_file._export_txt(filename, x, y, z)
                
            if len(self._angles) == 2 and len(self._angles2) == 2 and fields[0] == 0:
                filename = self.measurement_file.filename +'_pol2_'+str(self._angles2[1])+'_combined.dat'
                x = self.measurement_file.file['x'][0]
                y = np.r_[-fields[::-1], fields]
                z1 = self.measurement_file.file['z'][2::4,:][::-1,:]
                z2 = self.measurement_file.file['z'][3::4,:]
                z = np.r_[z1,z2]
                self.measurement_file._export_txt(filename, x, y, z)
                
            if len(self._angles) == 2 and len(self._angles2) == 2 and fields[-1] == 0:
                filename = self.measurement_file.filename +'_pol2_'+str(self._angles2[1])+'_combined.dat'
                x = self.measurement_file.file['x'][0]
                y = np.r_[fields, -fields[::-1]]
                z1 = self.measurement_file.file['z'][2::4,:][::-1,:]
                z2 = self.measurement_file.file['z'][3::4,:]
                z = np.r_[z2,z1]
                self.measurement_file._export_txt(filename, x, y, z)
                
        after = self.after_widget.currentData()
        if after == DO_GO_ZERO:
            self._magnet.set_field(0, persistent_after=True)
        elif after == DO_PERSISTENT:
            self._magnet.set_field(self._magnet.field(), persistent_after=True)
        self.canvas_widget.setEnabled(True)
    
    
    def _measurement_at_single_field(self):
        self.log("Starting single field")
        if not self._angles:
            while self._magnet.busy():
                self.check_if_cancel_requested()
                time.sleep(0.2)
            self.measurement_file.start_snapshot()
            x,y = self._spectrometer.take_single_spectrum()
            self.measurement_file.finish_snapshot(x=x, y=self._magnet.field(), z=y)
            return
        
        if not self._angles2:    
            for angle in self._angles:
                self._apt.move_absolute(self._motornum, angle)
                while self._magnet.busy() or not self._apt.is_stopped(self._motornum):
                    self.check_if_cancel_requested()
                    time.sleep(0.2)
                self.measurement_file.start_snapshot()
                x,y = self._spectrometer.take_single_spectrum()
                self.measurement_file.finish_snapshot(x=x, z=y)
            return    
            
        for angle2 in self._angles2:
            for angle in self._angles:
                self._apt2.move_absolute(self._motornum2, angle2)
                self._apt.move_absolute(self._motornum, angle)
                while self._magnet.busy() or not self._apt.is_stopped(self._motornum) or not self._apt.is_stopped(self._motornum2):
                    self.check_if_cancel_requested()
                    time.sleep(0.2)
                self.measurement_file.start_snapshot()
                x,y = self._spectrometer.take_single_spectrum()
                self.measurement_file.finish_snapshot(x=x, z=y)
                
    def setup_additional_widgets(self, canvas_widget):
        """ This function is supposed to create user interface """
        self.canvas_widget = canvas_widget
        hlayout = QtWidgets.QHBoxLayout()
        canvas_widget.setLayout(hlayout)
        
        field_group = QtWidgets.QGroupBox("Field sweep parameters")
        formlayout = QtWidgets.QFormLayout()
        self.spectrometer_device = QtWidgets.QLineEdit("spectrometer")
        formlayout.addRow("Spectrometer device id", self.spectrometer_device)
        self.magnet_device = QtWidgets.QLineEdit("magnet")
        formlayout.addRow("Magnet device id", self.magnet_device)
        self.from_widget = QtWidgets.QDoubleSpinBox()
        self.from_widget.setSuffix(" T")
        self.from_widget.setRange(-10, 10)
        self.from_widget.setDecimals(4)
        formlayout.addRow("From field:", self.from_widget)
        self.to_widget = QtWidgets.QDoubleSpinBox()
        self.to_widget.setSuffix(" T")
        self.to_widget.setRange(-10, 10)
        self.to_widget.setDecimals(4)
        formlayout.addRow("To field:", self.to_widget)
        self.step_widget = QtWidgets.QDoubleSpinBox()
        self.step_widget.setSuffix(" T")
        self.step_widget.setRange(0, 10)
        self.step_widget.setDecimals(4)
        formlayout.addRow("Step:", self.step_widget)
        
        self.after_widget = QtWidgets.QComboBox()
        self.after_widget.addItem("Do nothing", DO_NOTHING)
        self.after_widget.addItem("Go to zero", DO_GO_ZERO)
        self.after_widget.addItem("Set persistent", DO_PERSISTENT)
        formlayout.addRow("After measurement:", self.after_widget)
        field_group.setLayout(formlayout)
        hlayout.addWidget(field_group)
        
        self.apt_group = QtWidgets.QGroupBox("Motorized waveplate:detection")
        self.apt_group.setCheckable(True)
        formlayout2 = QtWidgets.QFormLayout()
        self.apt_device = QtWidgets.QLineEdit("apt")
        formlayout2.addRow("APT server id", self.apt_device)
        self.motor_id = APTSelectWidget()
        #self.motor_id.setPlaceholderText("click to select")
        formlayout2.addRow("Motor number", self.motor_id)
        self.apt_group.setLayout(formlayout2)
        
        self.apt_pos = []
        for i in range(3):
            w = QtWidgets.QLineEdit("")
            w.setPlaceholderText("[empty]")
            formlayout2.addRow("Position #%d" %i, w)
            self.apt_pos.append(w)
            
        self.apt_group2 = QtWidgets.QGroupBox("Motorized waveplate:excitation")
        self.apt_group2.setCheckable(True)
        self.apt_group2.setChecked(False)
        formlayout3 = QtWidgets.QFormLayout()
        self.apt_device2 = QtWidgets.QLineEdit("apt")
        formlayout3.addRow("APT server id", self.apt_device2)
        self.motor_id2 = APTSelectWidget()
        #self.motor_id.setPlaceholderText("click to select")
        formlayout3.addRow("Motor number", self.motor_id2)
        self.apt_group2.setLayout(formlayout3)
        
        self.apt_pos2 = []
        for i in range(3):
            w = QtWidgets.QLineEdit("")
            w.setPlaceholderText("[empty]")
            formlayout3.addRow("Position #%d" %i, w)
            self.apt_pos2.append(w)
        
        hlayout.addWidget(self.apt_group)
        hlayout.addWidget(self.apt_group2)
        hlayout.addStretch(2)