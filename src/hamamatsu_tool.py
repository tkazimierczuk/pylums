# -*- coding: utf-8 -*-
"""
@author: kacperoreszczuk
"""

from PyQt5 import QtWidgets, QtGui

try:
    from PyQt5.QtGui import QSizePolicy
except:
    from PyQt5.QtWidgets import QSizePolicy

import numpy as np
import jsonpickle
import traceback
import os
import subprocess
import math
import tempfile
import time
import sys

from src.widgets import HLine


def error_message(message):
    msg = QtWidgets.QMessageBox()
    msg.setIcon(QtWidgets.QMessageBox.Critical)
    msg.setText(message)
    msg.exec_()
    print(message)


class HamamatsuToolTab(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.file = None
        self.datasets = []
        self.datasets_names = []
        try:
            with open("config\\hamamatsu_tool_last_dir.cfg", "r") as file:
                self.last_directory = jsonpickle.decode(file.read())
        except Exception as e:
            self.last_directory = os.getcwd()

        try:
            with open("config\\pleview_path.cfg", "r") as file:
                self.pleview_path = jsonpickle.decode(file.read())
        except Exception as e:
            self.pleview_path = ""

        self.digits_count = 0
        self.filename_base = None
        self.part_numbers = []
        self.setup_ui()

    def setup_ui(self):
        hlayout = QtWidgets.QHBoxLayout()
        vlayout = QtWidgets.QVBoxLayout()

        self.button_browse_in = QtWidgets.QPushButton("Browse")
        self.button_browse_in.clicked.connect(self.browse_in)
        self.edit_filename_in = QtWidgets.QLineEdit()
        self.edit_filename_in.editingFinished.connect(self.load_file)
        self.button_browse_out = QtWidgets.QPushButton("Browse")
        self.button_browse_out.clicked.connect(self.browse_out)
        self.edit_filename_out = QtWidgets.QLineEdit()
        self.button_load = QtWidgets.QPushButton("Load")
        self.button_load.clicked.connect(self.load_file)
        self.check_autoname = QtWidgets.QCheckBox("Change only file extension")
        self.check_autoname.setChecked(True)
        self.check_autoname.toggled.connect(self.update_enable_out_edit)

        self.check_offset_correction = QtWidgets.QCheckBox("Compensate rising slope time offsets")
        self.check_offset_correction.setChecked(False)
        self.check_background = QtWidgets.QCheckBox("Subtract background")
        self.check_background.setChecked(False)
        self.check_time_zero = QtWidgets.QCheckBox("Normalize time axis to rising slope position")
        self.check_time_zero.setChecked(False)
        self.edit_background_distance = QtWidgets.QLineEdit()
        self.edit_background_distance.setText("25")
        self.edit_tilt_x = QtWidgets.QLineEdit()
        self.edit_tilt_x.setValidator(QtGui.QDoubleValidator())
        self.edit_tilt_y = QtWidgets.QLineEdit()
        self.edit_tilt_y.setValidator(QtGui.QDoubleValidator())
        self.edit_arc = QtWidgets.QLineEdit()
        self.edit_arc.setValidator(QtGui.QDoubleValidator())

        self.edit_pleview_path = QtWidgets.QLineEdit()
        self.button_pleview_path = QtWidgets.QPushButton("Browse")
        self.button_pleview_path.clicked.connect(self.browse_pleview_path)
        self.button_analyse = QtWidgets.QPushButton("Analyse")
        self.button_analyse.clicked.connect(lambda: self.convert(pleview=False, preview=True))
        self.button_convert = QtWidgets.QPushButton("Convert")
        self.button_convert.clicked.connect(lambda: self.convert(pleview=False, preview=False))
        self.button_convert_pleview = QtWidgets.QPushButton("Convert && Pleview")
        self.button_convert_pleview.clicked.connect(lambda: self.convert(pleview=True, preview=False))
        self.button_pleview = QtWidgets.QPushButton("Pleview")
        self.button_pleview.clicked.connect(lambda: self.convert(pleview=True, preview=True))

        label_next = QtWidgets.QLabel('You can enumerate through files with automatic increase of some value '
                                      'inside a filename.\n Filename will be updated with every conversion made '
                                      'with "Convert" button.\nPlease encapsulate iterated value with <> brackets.\n'
                                      'F. eg.: d002_measurement_at_voltage_<2>V.dat')
        self.check_iterate_plus_sign = QtWidgets.QCheckBox("Use '+' sign")
        self.edit_iterate_interval = QtWidgets.QLineEdit("1")
        self.edit_iterate_interval.setFixedWidth(65)
        self.edit_iterate_precision = QtWidgets.QLineEdit("0")
        self.edit_iterate_precision.setFixedWidth(35)

        self.edit_preview = QtWidgets.QTextEdit()
        self.table_parts = QtWidgets.QTableWidget(self)
        self.table_parts.verticalHeader().setVisible(False)
        self.table_parts.setSizePolicy(QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding))

        layout_filename = QtWidgets.QGridLayout()
        layout_filename.setContentsMargins(0, 0, 0, 0)
        layout_filename.addWidget(self.edit_filename_in, 0, 0)
        layout_filename.addWidget(self.button_browse_in, 0, 1)
        layout_filename.addWidget(self.edit_filename_out, 3, 0)
        layout_filename.addWidget(self.button_browse_out, 3, 1)

        layout_convert = QtWidgets.QHBoxLayout()
        layout_convert.addStretch()
        layout_convert.addWidget(self.button_analyse)
        layout_convert.addWidget(self.button_convert)
        layout_convert.addWidget(self.button_convert_pleview)
        layout_convert.addWidget(self.button_pleview)
        layout_convert.addStretch()
        layout_convert.setContentsMargins(0, 0, 0, 0)

        layout_load = QtWidgets.QHBoxLayout()
        layout_load.addStretch()
        layout_load.addWidget(self.button_load)
        layout_load.addStretch()
        layout_load.setContentsMargins(0, 0, 0, 0)
        layout_filename.addLayout(layout_load, 1, 0, 1, 2)

        layout_tilt_x = QtWidgets.QHBoxLayout()
        layout_tilt_x.addWidget(QtWidgets.QLabel("Tilt correction - move top edge to right by x pixels:"))
        layout_tilt_x.addWidget(self.edit_tilt_x)
        layout_tilt_x.addStretch()
        layout_tilt_x.setContentsMargins(0, 0, 0, 0)

        layout_tilt_y = QtWidgets.QHBoxLayout()
        layout_tilt_y.addWidget(QtWidgets.QLabel("Tilt correction - move right edge up by x pixels:"))
        layout_tilt_y.addWidget(self.edit_tilt_y)
        layout_tilt_y.addStretch()
        layout_tilt_y.setContentsMargins(0, 0, 0, 0)

        layout_arc = QtWidgets.QHBoxLayout()
        layout_arc.addWidget(QtWidgets.QLabel("Parabolic correction - move edges down by x pixels:"))
        layout_arc.addWidget(self.edit_arc)
        layout_arc.addStretch()
        layout_arc.setContentsMargins(0, 0, 0, 0)

        layout_background = QtWidgets.QHBoxLayout()
        layout_background.addWidget(self.check_background)
        layout_background.addWidget(QtWidgets.QLabel("Discard x pixels before rising slope:"))
        layout_background.addWidget(self.edit_background_distance)
        layout_background.addStretch()
        layout_background.setContentsMargins(0, 0, 0, 0)

        layout_check = QtWidgets.QHBoxLayout()
        layout_check.addWidget(QtWidgets.QLabel("Output file path:"))
        layout_check.addStretch(1)
        layout_check.addWidget(self.check_autoname)
        layout_check.setContentsMargins(0, 0, 0, 0)
        layout_filename.addLayout(layout_check, 2, 0, 1, 2)

        layout_pleview = QtWidgets.QHBoxLayout()
        layout_pleview.setContentsMargins(0, 0, 0, 0)
        layout_pleview.addWidget(QtWidgets.QLabel("Pleview path:"))
        layout_pleview.addWidget(self.edit_pleview_path)
        layout_pleview.addWidget(self.button_pleview_path)

        layout_next_file = QtWidgets.QHBoxLayout()
        layout_next_file.setContentsMargins(0, 0, 0, 0)
        layout_next_file.addWidget(QtWidgets.QLabel("Increase value by:"))
        layout_next_file.addWidget(self.edit_iterate_interval)
        layout_next_file.addWidget(QtWidgets.QLabel("Digits after decimal point:"))
        layout_next_file.addWidget(self.edit_iterate_precision)
        layout_next_file.addWidget(self.check_iterate_plus_sign)
        layout_next_file.addStretch()

        vlayout.addWidget(QtWidgets.QLabel("Input file path:"))
        vlayout.addLayout(layout_filename)
        vlayout.addWidget(HLine())
        vlayout.addWidget(self.check_time_zero)
        vlayout.addWidget(self.check_offset_correction)
        vlayout.addLayout(layout_background)
        vlayout.addLayout(layout_tilt_x)
        vlayout.addLayout(layout_tilt_y)
        vlayout.addLayout(layout_arc)
        vlayout.addWidget(HLine())
        vlayout.addLayout(layout_pleview)
        vlayout.addLayout(layout_convert)
        vlayout.addStretch(1)
        vlayout.addWidget(HLine())
        vlayout.addWidget(label_next)
        vlayout.addLayout(layout_next_file)

        hlayout.addLayout(vlayout)
        hlayout.addWidget(self.table_parts)
        self.setLayout(hlayout)

        self.edit_filename_in.setText(self.last_directory)
        self.edit_filename_out.setText(self.last_directory)
        self.edit_pleview_path.setText(self.pleview_path)
        self.edit_pleview_path.editingFinished.connect(self.save_pleview_path)
        self.load_settings()
        self.update_enable_out_edit()

    def load_settings(self):
        try:
            with open("config\\hamamatsu_tool_last_setup.cfg", "r") as file:
                time_zero, offset, background, background_neglect, tilt_y, arc, tilt_x = jsonpickle.decode(file.read())
                self.check_time_zero.setChecked(time_zero)
                self.check_offset_correction.setChecked(offset)
                self.check_background.setChecked(background)
                self.edit_background_distance.setText(background_neglect)
                self.edit_tilt_y.setText(tilt_y)
                self.edit_arc.setText(arc)
                self.edit_tilt_x.setText(tilt_x)
        except Exception as e:
            self.save_settings()

    def save_settings(self):
        try:
            with open("config\\hamamatsu_tool_last_setup.cfg", "w") as file:
                settings = (int(self.check_time_zero.checkState()), int(self.check_offset_correction.checkState()),
                            int(self.check_background.checkState()), self.edit_background_distance.text(),
                            self.edit_tilt_y.text(), self.edit_arc.text(), self.edit_tilt_x.text())
                file.write(jsonpickle.encode(settings))
        except Exception as e:
            pass

    def browse_in(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, "Load input file",
                                                         self.last_directory, "ITEX Image Files (*.img)")
        filename = filename[0]
        new_dir = '/'.join(filename.split('/')[:-1]) + '/'
        if new_dir != "":
            self.last_directory = new_dir
            if filename != "":
                try:
                    with open("config\\hamamatsu_tool_last_dir.cfg", "w") as file:
                        file.write(jsonpickle.encode(self.last_directory))
                except Exception as e:
                    error_message("Error while saving new default directory: " + traceback.format_exc())

        if filename != "":
            self.edit_filename_in.setText(filename)
        self.update_enable_out_edit()
        self.load_file()

    def browse_out(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, "Choose output file",
                                                         self.last_directory, "DAT Files (*.dat)")
        filename = filename[0]
        new_dir = '/'.join(filename.split('/')[:-1]) + '/'
        if new_dir != "":
            self.last_directory = new_dir
            if filename != "":
                try:
                    with open("config\\hamamatsu_tool_last_dir.cfg", "w") as file:
                        file.write(jsonpickle.encode(self.last_directory))
                except Exception as e:
                    error_message("Error while saving new default directory:\n" + traceback.format_exc())

        self.edit_filename_out.setText(filename)

    def browse_pleview_path(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, "Choose pleview location",
                                                         self.last_directory, "EXE Files (*.exe)")[0]
        if filename != '':
            self.edit_pleview_path.setText(filename)
            self.save_pleview_path()

    def save_pleview_path(self):
        try:
            with open("config\\pleview_path.cfg", "w") as file:
                file.write(jsonpickle.encode(self.edit_pleview_path.text()))
        except Exception as e:
            error_message("Error while saving Pleview location:\n" + traceback.format_exc())

    def update_enable_out_edit(self):
        self.button_browse_out.setEnabled(not self.check_autoname.isChecked())
        self.edit_filename_out.setEnabled(not self.check_autoname.isChecked())

        if self.check_autoname.isChecked() and self.edit_filename_in.text()[-4:] == ".img":
            base = self.edit_filename_in.text()[:-4 - self.digits_count].replace("<", "").replace(">", "")
            if base[-1] == '_':
                base = base[:-1]
            self.edit_filename_out.setText(base + ".dat")
        return

    def load_file(self):
        if self.edit_filename_in.text() == "":
            return
        try:
            filename = self.edit_filename_in.text().replace("<", "").replace(">", "")
            self.last_directory = '/'.join(filename.split('/')[:-1]) + '/'
            self.digits_count = 0
            while '0' <= filename[-self.digits_count - 1 - 4] <= '9':
                self.digits_count += 1
            self.filename_base = filename[:-4 - self.digits_count]
            if self.digits_count == 0:
                selected_part = 0
            else:
                if not all(['0' <= digit <= '9' for digit in filename[-4 - self.digits_count:-4]]):
                    selected_part = None
                else:
                    selected_part = int(filename[-4 - self.digits_count:-4])

            if filename != "":
                try:
                    with open("config\\hamamatsu_tool_last_dir.cfg", "w") as file:
                        file.write(jsonpickle.encode(self.last_directory))
                except Exception as e:
                    error_message("Error while saving new default directory: " + traceback.format_exc())

            list_of_files = [self.last_directory + file for file in os.listdir(self.last_directory)]
            preselected = [file for file in list_of_files if
                           file[:-4 - self.digits_count] == self.filename_base and file[-4:] == '.img']

            selected_part_index = 0
            if self.digits_count == 0:
                self.part_numbers = [0]
            else:
                self.part_numbers = []
                for name in preselected:
                    numbertext = name[-4 - self.digits_count: -4]
                    if not all(['0' <= digit <= '9' for digit in numbertext]):
                        continue
                    else:
                        if selected_part is not None and selected_part == int(numbertext):
                            selected_part_index = len(self.part_numbers)
                        self.part_numbers.append(int(numbertext))
            self.part_numbers.sort()

            self.table_parts.clear()
            self.table_parts.setRowCount(len(self.part_numbers))
            self.table_parts.setColumnCount(2)
            self.table_parts.setHorizontalHeaderLabels(['Detected parts', 'Time offset'])
            i = 0
            for number in self.part_numbers:
                self.table_parts.setItem(i, 0, QtWidgets.QTableWidgetItem(str(number)))
                i += 1
            if selected_part_index is not None:
                self.table_parts.setRangeSelected(
                    QtGui.QTableWidgetSelectionRange(selected_part_index, 0, selected_part_index, 0), True)
            self.update_enable_out_edit()

        except Exception as e:
            error_message("Error while parsing filename: %s" % (traceback.format_exc(),))

    def convert(self, preview=False, pleview=False):
        selected_ranges = self.table_parts.selectedRanges()
        selected_indexes_set = set([])
        for r in selected_ranges:
            for index in range(r.topRow(), r.bottomRow() + 1):
                selected_indexes_set.add(index)
        selected_indexes = list(selected_indexes_set)
        filenames = []
        for index in selected_indexes:
            filename = self.filename_base
            for i in reversed(range(self.digits_count)):
                filename += str((self.part_numbers[index] // (10 ** i)) % 10)
            filename += ".img"
            filenames.append(filename)

        if not filenames:
            error_message("No valid files selected!")
            return

        datas = []
        time_offsets = []
        wavelengths = None
        times = None

        try:
            for j in range(len(filenames)):
                filename = filenames[j]
                index = selected_indexes[j]  # row in the table widget
                file = open(filename, 'rb')

                # read file header:
                header = file.read(2)
                if header != b'IM':
                    error_message("%s - Invalid File format!" % (filename,))
                    return
                comment_length = int.from_bytes(file.read(2), byteorder='little')
                width = int.from_bytes(file.read(2), byteorder='little')
                height = int.from_bytes(file.read(2), byteorder='little')
                file.read(4)
                data_type = int.from_bytes(file.read(2), byteorder='little')
                bytes_per_value = {0: 1, 2: 2, 3: 4}[data_type]
                file.read(50 + comment_length)

                # read data:
                data_raw = file.read(bytes_per_value * width * height)
                data = np.fromstring(data_raw, dtype=('<u' + str(bytes_per_value))).reshape(height, width)
                if wavelengths is None:
                    wavelengths_raw = file.read(4 * width)
                    wavelengths = np.fromstring(wavelengths_raw, dtype='<f4')
                if times is None:
                    times_raw = file.read(4 * height)
                    times = np.fromstring(times_raw, dtype='<f4')
                datas.append(data)

                # analyse time profile of data, find time offset:
                timeprofile = np.sum(data, axis=1)
                gauss = [(lambda x: math.exp(-x ** 2))(x) for x in np.arange(-1.5, 1.5, 0.1)]
                smoothed = np.convolve(timeprofile, gauss, 'valid')
                threshold = min(smoothed) * 0.5 + max(smoothed) * 0.5
                time_offset = np.where(smoothed > threshold)[0][0] + int(len(gauss) / 2)
                self.table_parts.setItem(index, 1, QtWidgets.QTableWidgetItem(str(time_offset)))
                time_offsets.append(time_offset)
                self.repaint()

            # sum data into single array
            mean_offset = int(np.mean(time_offsets))
            if self.check_offset_correction.checkState():
                datas = [np.roll(datas[i], mean_offset - time_offsets[i], axis=0) for i in range(len(datas))]
            merged_data = np.sum(np.array(datas), axis=0)

            # apply correction to intensity to compensate nonlinear character of streak camera trigger signal
            t0 = max(times) * 0.5 + min(times) * 0.5
            merged_data = np.array(
                [merged_data[i] * math.cos((times[i] - t0) * (math.pi / 6605.)) for i in range(merged_data.shape[0])])

            # apply tilt x correction
            try:
                tilt_x = float(self.edit_tilt_x.text())
            except Exception:
                tilt_x = 0
            tilts_x = [int(round(x)) for x in (np.arange(len(times)) / len(times) - 0.5) * tilt_x]
            merged_data = np.array([np.roll(merged_data[i], -tilts_x[i]) for i in range(merged_data.shape[0])])

            # apply tilt y correction
            try:
                tilt_y = float(self.edit_tilt_y.text())
            except Exception:
                tilt_y = 0

            try:
                tilt_arc = float(self.edit_arc.text())
            except Exception:
                tilt_arc = 0
            x = np.linspace(0, 1, len(wavelengths))

            if wavelengths[-1] < wavelengths[0]:
                tilt_y = -tilt_y

            positions_y = [int(round(y)) for y in (x - 0.5) * 2 * tilt_y - (x - 0.5) * (x - 0.5) * 4 * tilt_arc]
            transposed = np.transpose(merged_data)
            transposed = np.array([np.roll(transposed[i], positions_y[i]) for i in range(transposed.shape[0])])
            merged_data = np.transpose(transposed)

            # subtract background
            if self.check_background.checkState():
                transposed = merged_data.transpose()
                discarded_first = 0
                if self.check_offset_correction.checkState():
                    discarded_first += mean_offset - min(time_offsets)
                discarded_first += max(positions_y)

                pixel_count = mean_offset - int(self.edit_background_distance.text())
                if pixel_count <= discarded_first:
                    message = "There are no pixels to take background from (mean time offset is equal "\
                              f"to {mean_offset})."
                    message += f" You chose to discard {int(self.edit_background_distance.text())} pixels "\
                               "before rising slope."
                    if mean_offset - min(time_offsets) > 0:
                        message += f" First {mean_offset - min(time_offsets)} pixels are discarded due to "\
                                   "time offset correction."
                    if max(positions_y) > 0:
                        message += f" {max(positions_y)} pixels are discarded due to vertical tilt correction."
                    message += "\n\nTry changing number of discarded pixels before rising slope."
                    if discarded_first > 0:
                        message += " You can also unselect parts, that have very early rising slope position."
                    error_message(message)
                    return
                if pixel_count - discarded_first < 5:
                    message = f"There are only {pixel_count - discarded_first} pixels to take background "\
                              f"from (mean time offset is equal to {mean_offset})."
                    message += f" You chose to discard {int(self.edit_background_distance.text())} pixels "\
                               "before rising slope."
                    if mean_offset - min(time_offsets) > 0:
                        message += f" {mean_offset - min(time_offsets)} pixels are discarded due "\
                                   "to time offset correction."
                    if max(positions_y) > 0:
                        message += f" {max(positions_y)} pixels are discarded due to vertical tilt correction."
                    message += "\n\nIf you want more precise background subtraction try changing "\
                               "number of discarded pixels before rising slope."
                    if discarded_first > 0:
                        message += " You can also unselect parts, that have very early rising slope position."
                    message += "\n\nYou can ignore this warning message."
                    error_message(message)
                means = []
                for i in range(transposed.shape[0]):
                    mean = np.mean(transposed[i][discarded_first:pixel_count])
                    means.append(mean)
                merged_data = np.array([transposed[i] - means[i] for i in range(transposed.shape[0])]).transpose()

            if self.check_time_zero.checkState():
                times = np.array(times) - times[mean_offset]

            # save to file
            z = np.array(merged_data)
            y = np.array(times)
            x = np.array(wavelengths)
            z = np.concatenate((x[np.newaxis, :], z), axis=0)

            if preview:
                filename = f'{tempfile.gettempdir()}\\pylums_preview_{time.time()}.dat'
            else:
                filename = self.edit_filename_out.text()

            txtfile = open(filename, 'wb')
            np.savetxt(txtfile, np.array([y]), fmt="%.9g")
            np.savetxt(txtfile, np.transpose(z), fmt="%.9g")
            txtfile.close()

            if pleview:
                subprocess.call(f'start "" "{self.edit_pleview_path.text()}" "{filename}"', shell=True)

            self.save_settings()
        except Exception:
            error_message(traceback.format_exc())

        if not pleview and not preview:
            try:
                fn = self.edit_filename_in.text()
                digits = int(self.edit_iterate_precision.text())
                interval = float(self.edit_iterate_interval.text())
                plus = "+" if self.check_iterate_plus_sign.isChecked() else ""
                a, b = tuple(fn.split("<"))
                b, c = tuple(b.split(">"))
                b = f"{float(b) + interval:{plus}.{digits}f}"
                self.edit_filename_in.setText(f"{a}<{b}>{c}")
                self.load_file()
            except Exception:
                print(traceback.format_exc())

