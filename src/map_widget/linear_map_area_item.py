
import math
from PyQt5 import QtWidgets, QtGui, QtCore
from collections import OrderedDict

epsilon = 0.000000001

class LinearMapAreaItem(QtWidgets.QGraphicsItem):
    def __init__(self, parent, x=-100, y=200, length=200, step=10):
        super().__init__()
        self.parent = parent
        self.setZValue(10)

        self.parent.scene.addItem(self)
        self.zoom = 1

        self.setAcceptHoverEvents(True)
        self.border_size = 15
        self.hover = None
        self.drag = None
        self.setFlags(QtWidgets.QGraphicsItem.ItemIsSelectable | QtWidgets.QGraphicsItem.ItemIsFocusable)

        layout = QtWidgets.QGridLayout()
        self.widget = QtWidgets.QFrame()
        self.widget.setFrameShape(QtWidgets.QFrame.Box)
        self.widget.setLayout(layout)
        self.widget.setSizePolicy(QtWidgets.QSizePolicy())

        self.label_map_size = QtWidgets.QLabel()
        layout.addWidget(self.label_map_size, 0, 0, 1, 2)

        self.sides = OrderedDict(
            [("x", x), ("y", y), ("length", length), ("rotation", 0), ("step", step)])
        self.edits = OrderedDict([])

        col = 1
        for s in self.sides:
            layout.addWidget(QtWidgets.QLabel(s + ":"), col, 0)
            edit = QtWidgets.QLineEdit(str(self.sides[s]))
            edit.setValidator(QtGui.QDoubleValidator())
            layout.addWidget(edit, col, 1)
            edit.setFixedWidth(70)
            edit.editingFinished.connect(self.update_transform)
            edit.editingFinished.connect(self.scene().update)
            self.edits[s] = edit
            col += 1

        for s in self.sides:
            self.edits[s].textChanged.connect(self.update_label_map_size)
            self.edits[s].textChanged.connect(self.parent.update_label_total_map_count)

        self.update_label_map_size()
        self.parent.update_label_total_map_count()
        self.update_transform()

    def setParameter(self, parameter, value):
        if value == 0:
            self.edits[parameter].setText("0")
        else:
            self.edits[parameter].setText(str(round(value, 7 - int(math.floor(math.log10(abs(value)))))))
        self.update_transform()

    def get_parameter(self, parameter):
        try:
            return float(self.edits[parameter].text())
        except Exception:
            return 0.

    def update_border_rect(self):
        rect = self.boundingRect()
        dx = QtCore.QPointF(self.border_size / self.zoom, 0)
        dy = QtCore.QPointF(0, -self.border_size / self.zoom)  # minus because y axis inversion
        self.border_rect = {}
        self.border_rect_ordered_keys = ['c', 'r']#, 'e']
        self.border_rect['r'] = QtCore.QRectF(rect.topRight(), rect.bottomRight() - dx)
        #self.border_rect['l'] = QtCore.QRectF(rect.bottomLeft(), rect.topLeft() + dx - dy)
        self.border_rect['c'] = QtCore.QRectF(rect.bottomLeft(), rect.topRight() - dx)

    def boundingRect(self):
        return QtCore.QRectF(0, -self.border_size / self.zoom, self.get_parameter("length"), self.border_size / self.zoom)

    def update_transform(self):
        transform = QtGui.QTransform()
        transform.translate(self.get_parameter("x"), self.get_parameter("y"))
        transform.rotate(self.get_parameter("rotation"))
        self.setTransform(transform)

    def hoverEnterEvent(self, event):
        self.refresh_hover_rect(event.pos())

    def hoverLeaveEvent(self, event):
        self.hover = None
        self.update()
        self.scene().update()

    def hoverMoveEvent(self, event):
        self.refresh_hover_rect(event.pos())

    def refresh_hover_rect(self, mouse_pos):
        self.hover = None
        for rect in self.border_rect_ordered_keys:  # rectangles can overlap for small zoom, ordering prefers corners for easy resize
            if self.border_rect[rect].contains(mouse_pos):
                self.hover = rect

        self.scene().update()

    def mousePressEvent(self, event):
        if event.button() != QtCore.Qt.LeftButton:
            return
        self.drag = None
        self.refresh_hover_rect(event.pos())
        self.drag = self.hover
        self.drag_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() != QtCore.Qt.LeftButton:
            return
        self.refresh_hover_rect(event.pos())
        self.drag = None

    def mouseMoveEvent(self, event):
        if self.drag is not None:
            #self.drag_pos = event.pos()
            x = 0
            y = 0
            length = self.get_parameter("length")
            rotation = math.radians(self.get_parameter("rotation"))

            if self.drag == 'c':
                v = event.pos() - self.drag_pos
                x += v.x()
                y += v.y()
                new_pos = self.transform().map(QtCore.QPointF(x, y))
                self.setParameter("x", new_pos.x())
                self.setParameter("y", new_pos.y())

            if self.drag == 'r' :
                y = self.drag_pos.y()
                dist = math.sqrt(event.pos().x() ** 2 + event.pos().y() ** 2)
                x2 = math.sqrt(max(0, dist ** 2 - y ** 2))
                alfa1 = math.atan2(y, x2)
                alfa2 = math.atan2(event.pos().y(), event.pos().x())
                length += x2 - self.drag_pos.x()
                rotation += alfa2 - alfa1
                rotation %= 360
                self.drag_pos.setX(x2)

            self.setParameter("length", length)
            self.setParameter("rotation", math.degrees(rotation))

            self.scene().update()
            self.update()

    def map_origin(self): #defines offset of grid of map points, in map area coordinates
        if self.parent.check_box_snap.isChecked():
            a = math.radians(self.get_parameter("rotation"))
            x = self.get_parameter("x")
            y = self.get_parameter("y")
            return (- x * math.cos(a) - y * math.sin(a), - y * math.cos(a) + x * math.sin(a))
        else:
            return (0, 0)

    def map_area_parameters(self):
        length = self.get_parameter("length")
        step = self.get_parameter("step")
        rotation = self.get_parameter("rotation")
        if step == 0:
            return (length, 1, 0, 0, rotation)
        end = math.floor((length) / step + epsilon + 1)
        begin = 0
        return (length, step, end, begin, rotation)

    def update_label_map_size(self):
        end, begin = self.map_area_parameters()[2:4]
        s = str(end - begin) + " points"
        self.label_map_size.setText(s)

    def paint(self, painter, option, widget=None):
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        self.zoom = option.levelOfDetailFromTransform(painter.worldTransform())

        self.update_border_rect()

        pen = QtGui.QPen()
        pen.setCosmetic(True)  # fixed width regardless of transformations
        pen.setColor(QtGui.QColor(128, 128, 128))
        pen.setWidth(2)
        painter.setPen(pen)
        painter.drawRect(self.boundingRect())
        if self.hover is not None:
            painter.drawRect(self.border_rect[self.hover])

        grid = self.zoom * self.get_parameter("step")
        if grid > 1:
            length, step, end, begin, rotation = self.map_area_parameters()

            pen = QtGui.QPen()
            pen.setCosmetic(True)  # fixed width regardless of transformations
            pen.setColor(QtGui.QColor(255, 0, 0))
            pen.setWidth(max(3, min(7, grid / 3)))
            painter.setPen(pen)

            if end - begin < 10000:
                for ix in range(begin, end):
                    painter.drawLine(QtCore.QPointF(step * ix, 0),
                        QtCore.QPointF(step * ix + 0.000001, 0))

    def get_positions_count(self):
        length, step, end, begin, rotation = self.map_area_parameters()
        n = end - begin
        return n

    def get_positions(self):
        positions = []
        length, step, end, begin, rotation = self.map_area_parameters()
        if (step > 0):
            n = end - begin
            if n <= 1000000:
                for ix in range(begin, end):
                    x = step * ix
                    y = 0
                    global_x = self.get_parameter("x") + x * math.cos(math.radians(rotation)) - y * math.sin(math.radians(rotation))
                    global_y = self.get_parameter("y") + y * math.cos(math.radians(rotation)) + x * math.sin(math.radians(rotation))
                    positions.append((global_x, global_y))
        return positions

    def larger_step(self):
        return float(self.edits["step"].text())