from scipy import optimize
from PyQt5 import QtWidgets, QtGui, QtCore, QtSvg
from collections import OrderedDict
import math
import jsonpickle
import traceback

def round_floating(number):
    return round(number, 7 - int(math.floor(math.log10(max(abs(number), 10 ** (-3))))) - 1)

class AnchorItem(QtWidgets.QGraphicsEllipseItem):
    def __init__(self, parent, real_pos):
        super().__init__(parent)
        self.setEnabled(True)
        self.setActive(True)
        self.setRect(-5, -5, 10, 10)
        self.setBrush(QtGui.QBrush(QtCore.Qt.yellow))
        self.setFlag(QtWidgets.QGraphicsItem.ItemIgnoresTransformations, True)
        self.real_pos = real_pos
        self.parent = parent

    def contextMenuEvent(self, event):
        menu = QtWidgets.QMenu()
        remove_action = menu.addAction("Remove anchor point")
        selected_action = menu.exec(event.screenPos())
        if selected_action == remove_action:
            self.parentItem().remove_anchor(self, save = True)


class FlagItem(QtWidgets.QGraphicsEllipseItem):
    def __init__(self, parent, name):
        super().__init__(parent)
        self.setEnabled(True)
        self.setActive(True)
        self.setRect(-5, -5, 10, 10)
        self.color = QtCore.Qt.cyan
        self.setBrush(QtGui.QBrush(self.color))
        self.setFlag(QtWidgets.QGraphicsItem.ItemIgnoresTransformations, True)

        text = QtWidgets.QGraphicsSimpleTextItem(name, self)
        text.setFlag(QtWidgets.QGraphicsItem.ItemIgnoresTransformations, True)
        font = QtGui.QFont()
        font.setStyleStrategy(QtGui.QFont.ForceOutline)
        text.setFont(font)
        self.name = name

    def contextMenuEvent(self, event):
        event.accept()
        menu = QtWidgets.QMenu()
        gotoAction = menu.addAction("Go to Flag")
        removeAction = menu.addAction("Remove flag")
        selectedAction = menu.exec(event.screenPos())
        if selectedAction == removeAction:
            self.parentItem().remove_flag(self, save = True)
        if selectedAction == gotoAction:
            scene_pos = self.parentItem().mapToScene(self.pos())
            self.parentItem().parent.goto((scene_pos.x(), scene_pos.y()))


class SampleImageItem(QtWidgets.QGraphicsPixmapItem):
    def __init__(self, parent):
        super().__init__()

        self.parent = parent
        self.scene = self.parent.scene
        self.loaded = False
        self.anchor_items = []
        self.flag_items = []
        self.index = 0
        self.widget = QtWidgets.QWidget()
        self.widget.setVisible(False)
        self.focus_points = []
        layout = QtWidgets.QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.widget.setLayout(layout)

        button_back = QtWidgets.QPushButton("back")
        button_back.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        button_back.setMinimumWidth(45)
        layout.addWidget(button_back, 0, 0)
        button_back.clicked.connect(self.move_back)

        button_front = QtWidgets.QPushButton("front")
        button_front.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        button_front.setMinimumWidth(45)
        layout.addWidget(button_front, 0, 1)
        button_front.clicked.connect(self.move_front)

        self.edit_name = QtWidgets.QLineEdit("")
        self.edit_name.setReadOnly(True)
        layout.addWidget(self.edit_name, 1, 0, 1, 2)

        button_load = QtWidgets.QPushButton("Replace image")
        layout.addWidget(button_load, 0, 2)
        button_load.clicked.connect(self.load_image)

        button_fit = QtWidgets.QPushButton("Fit Transform")
        layout.addWidget(button_fit, 1, 2)
        button_fit.clicked.connect(self.find_best_transform)

        self.sides = OrderedDict([("x", -100), ("y", 100), ("rotation", 0), ("width", 100), ("a. ratio", 1)])
        self.edits = OrderedDict([])
        self.checks = OrderedDict([])
        col = 0
        for s in self.sides:
            #layout.addWidget(QtWidgets.QLabel(s + ":"), 0, 2 * col + 3)
            edit = QtWidgets.QLineEdit(str(self.sides[s]))
            edit.setValidator(QtGui.QDoubleValidator())
            layout.addWidget(edit, 1, col + 3)
            #edit.setMinimumWidth(80)
            edit.editingFinished.connect(self.update_pixmap)
            edit.editingFinished.connect(self.save_settings)
            self.edits[s] = edit

            check = QtWidgets.QCheckBox("Fit " + s)
            layout.addWidget(check, 0, col + 3)
            self.checks[s] = check

            col += 1

        for i in ["x", "y", "width", "rotation"]:
            self.checks[i].setChecked(1)

        self.last_directory = ""
        try:
            with open("config\\map_widget_image_dir.cfg", "r") as file:
                self.last_directory= jsonpickle.decode(file.read())
        except Exception as e:
            print(traceback.format_exc())

    def move_front(self):
        self.parent.move_image_front(self) #todo lambda image = self: image.pare....

    def move_back(self):
        self.parent.move_image_back(self)

    def mousePressEvent(self, event):
        self.parent.set_active_image(self)

    def contextMenuEvent(self, event):
        menu = QtWidgets.QMenu()
        goto_action = menu.addAction("Go here")
        add_anchor_action = menu.addAction("Anchor this point")
        add_flag_action = menu.addAction("Add flag here")
        remove_action = menu.addAction("Remove map")
        selected_action = menu.exec(event.screenPos())
        if selected_action == remove_action:
            self.parent.remove_image(self)
        elif selected_action == goto_action:
            self.parent.goto((event.scenePos().x(),event.scenePos().y()))
        elif selected_action == add_anchor_action:
            dialog = QtWidgets.QDialog()
            dialog.setWindowTitle("Anchor this point")
            layout = QtWidgets.QFormLayout()
            dialog.setLayout(layout)
            x_input = QtWidgets.QLineEdit(str(self.parent.cursor.x()))
            layout.addRow("X coordinate", x_input)
            y_input = QtWidgets.QLineEdit(str(self.parent.cursor.y()))
            layout.addRow("Y coordinate", y_input)
            buttonBox = QtWidgets.QDialogButtonBox()
            buttonBox.setOrientation(QtCore.Qt.Horizontal)
            buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
            buttonBox.accepted.connect(dialog.accept)
            buttonBox.rejected.connect(dialog.reject)
            layout.addRow(buttonBox)

            # d.setWindowModality(Qt.ApplicationModal)
            dialog.show()
            try:
                if dialog.exec_():
                    real_pos = (float(x_input.text()), float(y_input.text()))
                    anchor = AnchorItem(self, real_pos)
                    pos = self.mapFromScene(event.scenePos())
                    anchor.setPos(pos)
                    self.anchor_items.append(anchor)
                    self.save_settings()
            except Exception:
                self.parent.error_message(traceback.format_exc())
        elif selected_action == add_flag_action:
            dialog = QtWidgets.QDialog()
            dialog.setWindowTitle("Add flag")
            layout = QtWidgets.QFormLayout()
            dialog.setLayout(layout)
            edit_name = QtWidgets.QLineEdit()
            layout.addRow("Name", edit_name)
            buttonBox = QtWidgets.QDialogButtonBox()
            buttonBox.setOrientation(QtCore.Qt.Horizontal)
            buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel | QtWidgets.QDialogButtonBox.Ok)
            buttonBox.accepted.connect(dialog.accept)
            buttonBox.rejected.connect(dialog.reject)
            layout.addRow(buttonBox)

            # d.setWindowModality(Qt.ApplicationModal)
            dialog.show()
            if dialog.exec_():
                name = edit_name.text()
                flag = FlagItem(self, name)
                pos = self.mapFromScene(event.scenePos())
                flag.setPos(pos)
                self.flag_items.append(flag)
                self.save_settings()

    def load_image(self):
        self.pixmap = QtGui.QPixmap()
        fileName = QtWidgets.QFileDialog.getOpenFileName(self.parent, "Load sample image",
                                                         self.last_directory, "Image Files (*.png *.jpg *.bmp *.svg)")
        fileName = fileName[0]
        self.last_directory = '/'.join(fileName.split('/')[:-1])
        self.edit_name.setText(fileName.split('/')[-1])
        try:
            with open("config\\map_widget_image_dir.cfg", "w") as file:
                file.write(jsonpickle.encode(self.last_directory))
        except Exception as e:
            print(traceback.format_exc())

        self.config_filename = fileName + ".cfg"

        if not fileName:
            return

        try:
            #if fileName.lower().endswith("svg"):
            #    self.bg_item = QtSvg.QGraphicsSvgItem(fileName)
            #else:
            reader = QtGui.QImageReader(fileName)
            size_of_image = reader.size()
            if size_of_image.height() * size_of_image.width() > 150000000:
                self.parent.error_message("Images larger than 150 megapixels (f. eg. 15000x10000 px) are not supported. Please reduce dimensions of the file.")
                return

            if self.loaded:
                self.scene.removeItem(self)
                del (self.pixmap)
                self.loaded = False
            pixmap = QtGui.QPixmap(fileName)
            pixmap = pixmap.transformed(QtGui.QTransform().scale(1, -1))
            self.w = pixmap.width()
            self.h = pixmap.height()
            self.setPixmap(pixmap)
            self.scene.addItem(self)
            self.setEnabled(True)
            self.setActive(True)
            self.loaded = True
            self.setAcceptedMouseButtons(QtCore.Qt.RightButton | QtCore.Qt.LeftButton)
            self.current_coordinates = (0, 0)
            self.load_settings()
            self.update_pixmap()
        except Exception as e:
            self.parent.error_message("Error while loading image.\n" + traceback.format_exc())

    def update_pixmap(self):
        if self.loaded:
            try:
                transform_params = [float(self.edits[key].text()) for key in list(self.sides)]
            except Exception:
                self.parent.error_message("Please check transformation values.\n\n" + traceback.format_exc())
                return
            transform = QtGui.QTransform()
            transform.translate(transform_params[0], transform_params[1])
            transform.rotate(transform_params[2])
            transform.scale(transform_params[3],
                            transform_params[3] / transform_params[4] / (float(self.w) / float(self.h)))
            transform.scale(1 / self.w, 1 / self.h)
            self.setTransform(transform)

    def load_settings(self):
        try:
            flag_items = list(self.flag_items)
            for flag in flag_items:
                self.remove_flag(flag, save=False)
            anchor_items = list(self.anchor_items)
            for anchor in anchor_items:
                self.remove_anchor(anchor, save = False)
            self.focus_points = []

            try:
                try:
                    transform_params, anchors, flags, focus_points = jsonpickle.decode(open(self.config_filename, "r").read())
                    self.focus_points = focus_points
                except Exception:
                    transform_params, anchors, flags = jsonpickle.decode(open(self.config_filename, "r").read())
                for a in flags:
                    name, pos = a
                    flag = FlagItem(self, name)
                    flag.setPos(QtCore.QPointF(pos[0], pos[1]))
                    self.flag_items.append(flag)
            except Exception as e:
                print(traceback.format_exc())
                transform_params, anchors = jsonpickle.decode(open(self.config_filename, "r").read())
            for a in anchors:
                real_pos, pos = a
                anchor = AnchorItem(self, real_pos)
                anchor.setPos(QtCore.QPointF(pos[0], pos[1]))
                self.anchor_items.append(anchor)


            keys = list(self.sides)
            for i in range(len(keys)):
                self.edits[keys[i]].setText(str(transform_params[i]))
        except Exception as e:
            print(traceback.format_exc())

    def save_settings(self):
        try:
            with open(self.config_filename, "w") as file:
                transform_params = [float(self.edits[key].text()) for key in list(self.sides)]
                anchors = [(a.real_pos, (a.pos().x(), a.pos().y())) for a in self.anchor_items]
                flags = [(a.name, (a.pos().x(), a.pos().y())) for a in self.flag_items]
                file.write(jsonpickle.encode([transform_params, anchors, flags, self.focus_points]))
        except Exception as e:
            print(traceback.format_exc())

    def remove_anchor(self, anchor_item, save=False):
        self.anchor_items.remove(anchor_item)
        anchor_item.setParentItem(None)
        anchor_item.scene().removeItem(anchor_item)
        if save:
            self.save_settings()

    def remove_flag(self, flag_item, save=False):
        self.flag_items.remove(flag_item)
        flag_item.setParentItem(None)
        flag_item.scene().removeItem(flag_item)
        if save:
            self.save_settings()

    def find_best_transform(self):
        """ Use least squares method to find the best transform which fits the anchor points"""
        if len(self.anchor_items) < 1:
            return

        keys = list(self.sides)
        transform_params = [float(self.edits[key].text()) for key in keys]
        free_params_index = []  # indexes of free params
        free_params_initial = []
        for i in range(len(keys)):  # take at most first 2*n free parameters when there are n anchors
            if self.checks[keys[i]].isChecked() and len(free_params_index) < 2 * len(self.anchor_items):
                free_params_index.append(i)
                free_params_initial.append(transform_params[i])
        # print("init params ", free_params_initial, " free_params_index ", free_params_index)
        data = [(a.pos(), a.real_pos) for a in self.anchor_items]

        def update_transform_params(params):
            j = 0
            for i in free_params_index:
                transform_params[i] = params[j]
                j += 1

        def build_transform(params):
            update_transform_params(params)
            transform = QtGui.QTransform()
            transform.translate(transform_params[0], transform_params[1])
            transform.rotate(transform_params[2])
            transform.scale(transform_params[3],
                            transform_params[3] / transform_params[4] / (float(self.w) / float(self.h)))
            transform.scale(1 / self.w, 1 / self.h)
            return transform

        def fitfunc(params):
            transform = build_transform(params)
            chi2 = 0.
            for point0, point1 in data:
                diff = QtCore.QPointF(point1[0], point1[1]) - transform.map(point0)
                chi2 += diff.x() ** 2 + diff.y() ** 2
            return chi2

        best = optimize.fmin(fitfunc, free_params_initial)
        update_transform_params(best)

        for i in range(len(keys)):
            transform_params[i] = round_floating(transform_params[i])
            self.edits[keys[i]].setText(str(transform_params[i]))
        self.update_pixmap()
        self.save_settings()

