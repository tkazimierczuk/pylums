import math
import numpy
import threading
import socket
from time import sleep
from time import perf_counter as clock
import jsonpickle
from PyQt5 import QtWidgets, QtGui, QtCore
from src.map_widget.linear_map_area_item import LinearMapAreaItem, epsilon
from src.map_widget.map_area_item import MapAreaItem, epsilon
from src.map_widget.sample_image_item import SampleImageItem, round_floating
import traceback
from devices.virtual.map_widget import MapWidgetWorker, MapWidgetInterface


def get_ip():
    return (([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or
             [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close())
               for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0]


class MyGraphicsScene(QtGui.QGraphicsScene):
    def __init__(self, map_widget=None):
        super(self.__class__, self).__init__()
        self.map_widget = map_widget
        self.drag = False
        self.drag_pos = None
        self.dragging_lock = False  # prevent recursive events when panning with mouse

    def contextMenuEvent(self, event):
        # Check it item exists on event position
        for item in self.items(event.scenePos(), deviceTransform=self.map_widget.view_widget.transform()):
            if not isinstance(item, MapAreaItem) and not isinstance(item, LinearMapAreaItem):
                item.contextMenuEvent(event)
                return

        menu = QtWidgets.QMenu()
        goto_action = menu.addAction("Go here")
        selected_action = menu.exec(event.screenPos())
        if selected_action == goto_action:
            self.map_widget.goto((event.scenePos().x(), event.scenePos().y()))

    def mouseDoubleClickEvent(self, event):
        self.map_widget.goto((event.scenePos().x(), event.scenePos().y()))

    def mousePressEvent(self, event):
        if event.button() != QtCore.Qt.LeftButton:
            event.ignore()
            return

        event.accept()
        items_here = self.items(event.scenePos(), QtCore.Qt.ContainsItemShape)
        map_area_items_here = [item for item in items_here if isinstance(item, (MapAreaItem, LinearMapAreaItem))]
        if not map_area_items_here:
            self.drag = True
            self.drag_pos = QtCore.QPointF(event.scenePos())
        QtGui.QGraphicsScene.mousePressEvent(self, event)  # propagate to objects in scene

    def mouseReleaseEvent(self, event):
        if event.button() != QtCore.Qt.LeftButton:
            return
        self.drag = False
        QtGui.QGraphicsScene.mouseReleaseEvent(self, event)  # propagate to objects in scene

    def mouseMoveEvent(self, event):
        try:
            position = QtCore.QPointF(event.scenePos())
            self.map_widget.xwidget.setText(str(position.x()))
            self.map_widget.ywidget.setText(str(position.y()))
            if self.drag and not self.dragging_lock:
                view = self.map_widget.view_widget
                error = self.drag_pos - position

                self.dragging_lock = True  # events caused by view frame movements will not move scene recursively
                view.setSceneRect(view.sceneRect().x() + error.x(), view.sceneRect().y() + error.y(),
                                  view.sceneRect().width(), view.sceneRect().height())
                view_center = view.mapToScene(view.width() / 2., view.height() / 2.)
                view_center += QtCore.QPointF(error.x(), error.y())
                view.centerOn(view_center)
                self.dragging_lock = False

                event.accept()
            else:
                event.ignore()
        except Exception:
            print(traceback.format_exc())
        QtGui.QGraphicsScene.mouseMoveEvent(self, event)  # propagate to objects in scene


class ZoomableGraphicsView(QtWidgets.QGraphicsView):
    def __init__(self, parent):
        super().__init__(parent)
        self.setMouseTracking(True)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setTransform(self.transform().scale(1, -1))

        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        self.list_key_on = []
        self.list_key_pressed = []
        self.list_key_released = []
        self.arrow_navigation_timer = QtCore.QTimer()
        self.arrow_navigation_timer.timeout.connect(self.loop_arrow_navigation)
        self.arrow_navigation_interval = 25
        self.arrow_navigation_timer.setInterval(self.arrow_navigation_interval)
        self.arrow_navigation_timer.start()
        self.parent = parent

    def keyPressEvent(self, event):
        self.list_key_on.append(event.key())
        self.list_key_pressed.append(event.key())
        event.accept()

    def keyReleaseEvent(self, event):
        try:
            self.list_key_on.remove(event.key())
        except ValueError:
            pass  # key press event may have not been registered, if focus was on different window
        self.list_key_released.append(event.key())
        event.accept()

    def loop_arrow_navigation(self):
        dx = dy = 0
        if QtCore.Qt.Key_Up in self.list_key_on:
            dy += 1
        if QtCore.Qt.Key_Down in self.list_key_on:
            dy -= 1
        if QtCore.Qt.Key_Left in self.list_key_on:
            dx -= 1
        if QtCore.Qt.Key_Right in self.list_key_on:
            dx += 1
        if dx != 0 or dy != 0:
            zoom = math.sqrt(abs(self.transform().determinant()))

            step = (self.sceneRect().width() +
                    self.sceneRect().height()) / 2 * self.arrow_navigation_interval / 1000 / zoom  # 1 screen/s
            self.setSceneRect(self.sceneRect().x() + dx * step, self.sceneRect().y() + dy * step,
                              self.sceneRect().width(), self.sceneRect().height())

            view_center = self.mapToScene(self.width() // 2, self.height() // 2)
            view_center += QtCore.QPointF(dx * step, dy * step)
            self.centerOn(view_center)

        self.list_key_pressed = []
        self.list_key_released = []

    def wheelEvent(self, event):
        # Zoom Factor
        zoom_in_factor = 1.25
        zoom_out_factor = 1 / zoom_in_factor

        # Save the scene pos
        old_pos = self.mapToScene(event.pos())

        # Zoom
        if event.angleDelta().y() > 0:
            zoom_factor = zoom_in_factor
        else:
            zoom_factor = zoom_out_factor
        self.scale(zoom_factor, zoom_factor)

        # Get the new position
        new_pos = self.mapToScene(event.pos())
        # Move scene to old position
        delta = new_pos - old_pos

        old_transform = self.transform()
        scene_rect = self.sceneRect()

        self.setSceneRect(scene_rect.x() - delta.x(), scene_rect.y() - delta.y(), scene_rect.width(),
                          scene_rect.height())
        self.setTransform(old_transform)


class MapWidget(QtWidgets.QWidget):
    error_signal = QtCore.pyqtSignal(str)
    progress_bar_signal = QtCore.pyqtSignal(int, str)

    def __init__(self, device_list, parent=None):
        super().__init__(parent)
        self.device_list = device_list
        self.slaves = []
        self.pools = []
        self.images = []
        self.focus_points = []
        self.focus_image_id = 0
        self.mapping_points = None
        self.map_filename = None
        self.max_grid = None
        self.mapping_thread = None
        self.connection_error_retry_count = 0
        self.worker = None

        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(20)
        self.timer.timeout.connect(self.timeout)
        self.timer.start()

        self.error_signal.connect(self.error_message_slot)

        self.last_directory = ""
        try:
            with open("config\\map_widget_data_dir.cfg", "r") as file:
                self.last_directory = jsonpickle.decode(file.read())
        except Exception:
            pass

        try:
            with open("config\\map_widget_delays.cfg", "r") as file:
                short_delay, long_delay = jsonpickle.decode(file.read())
        except Exception:
            short_delay = 1
            long_delay = 5

        layout = QtWidgets.QVBoxLayout()  # main vertical layout
        self.setLayout(layout)

        self.button_start_map = QtWidgets.QPushButton("Start")
        self.button_start_map.clicked.connect(self.start_pause_mapping)
        self.button_abort_map = QtWidgets.QPushButton("Abort")
        self.button_abort_map.setEnabled(False)
        self.button_abort_map.clicked.connect(self.abort_mapping)
        self.edit_short_delay = QtWidgets.QLineEdit()
        self.edit_short_delay.setFixedWidth(30)
        self.edit_short_delay.setValidator(QtGui.QDoubleValidator())
        self.edit_short_delay.setText(str(short_delay))
        self.edit_long_delay = QtWidgets.QLineEdit()
        self.edit_long_delay.setFixedWidth(30)
        self.edit_long_delay.setValidator(QtGui.QDoubleValidator())
        self.edit_long_delay.setText(str(long_delay))
        self.progress_bar = QtWidgets.QProgressBar()
        self.progress_bar.setAlignment(QtCore.Qt.AlignCenter)
        self.progress_bar_signal.connect(self.update_progress_bar_slot)
        self.check_show_remote_widget = QtWidgets.QCheckBox("Show remote operation panel")
        self.check_show_remote_widget.stateChanged.connect(self.update_visibility_remote_widget)

        self.hwidget1 = QtWidgets.QWidget()
        self.hwidget1.setVisible(False)
        hlayout1 = QtWidgets.QHBoxLayout(self.hwidget1)  # horizontal layout for filename and mapping control
        hlayout1.setContentsMargins(0, 0, 0, 0)
        hlayout1.addWidget(self.button_start_map)
        hlayout1.addWidget(self.button_abort_map)
        hlayout1.addWidget(QtWidgets.QLabel("Short/long step delay:"))
        hlayout1.addWidget(self.edit_short_delay)
        hlayout1.addWidget(self.edit_long_delay)
        hlayout1.addWidget(self.progress_bar)
        hlayout1.addWidget(self.check_show_remote_widget)

        layout.addWidget(self.hwidget1)

        self.edit_filename = QtWidgets.QLineEdit()
        self.edit_filename.setText(self.last_directory + '/')
        self.edit_filename_extension = QtWidgets.QLineEdit()
        self.edit_filename_extension.setVisible(False)
        self.edit_filename_extension.setEnabled(False)
        self.button_browse = QtWidgets.QPushButton("Browse")
        self.button_browse.clicked.connect(self.browse_filename)
        self.check_auto_name = QtWidgets.QCheckBox("Extend")
        self.check_auto_name.stateChanged.connect(self.edit_filename_extension.setVisible)

        self.hwidget2 = QtWidgets.QWidget()
        self.hwidget2.setVisible(False)
        hlayout2 = QtWidgets.QHBoxLayout(self.hwidget2)  # horizontal layout for filename and mapping control
        hlayout2.setContentsMargins(0, 0, 0, 0)
        hlayout2.addWidget(self.edit_filename)
        hlayout2.addWidget(self.edit_filename_extension)
        hlayout2.addWidget(self.button_browse)
        layout.addWidget(self.hwidget2)

        # mapping, graphics view, custom code, focus autocorrection
        self.view_widget = ZoomableGraphicsView(self)
        self.setup_scene()
        self.map_area_scroll = QtWidgets.QScrollArea()
        self.map_area_scroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.map_area_scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.map_area_scroll.setWidgetResizable(True)
        map_area_scroll_frame = QtWidgets.QFrame(self.map_area_scroll)
        self.map_area_scroll.setWidget(map_area_scroll_frame)
        self.map_area_scroll.setFixedWidth(175)
        self.map_area_scroll.setVisible(False)
        map_area_buttons_layout = QtWidgets.QGridLayout()
        self.map_area_layout = QtWidgets.QVBoxLayout()
        map_area_scroll_frame.setLayout(self.map_area_layout)
        self.label_total_map_count = QtWidgets.QLabel()
        self.map_area_layout.addWidget(self.label_total_map_count)
        self.check_box_snap = QtWidgets.QCheckBox("Snap to grid")
        self.check_box_snap.setChecked(True)
        self.check_box_snap.stateChanged.connect(self.update_label_total_map_count)
        self.map_area_layout.addWidget(self.check_box_snap)
        self.map_area_layout.addLayout(map_area_buttons_layout)
        self.map_area_layout.addStretch(10)
        self.map_area_items = []
        self.button_add_map_area = QtWidgets.QPushButton("Add Rect.")
        self.button_add_map_area.setFixedWidth(65)
        map_area_buttons_layout.addWidget(self.button_add_map_area, 0, 0)
        self.button_add_map_area.clicked.connect(self.add_map_area)
        self.button_add_linear_map_area = QtWidgets.QPushButton("Add Line")
        self.button_add_linear_map_area.setFixedWidth(65)
        map_area_buttons_layout.addWidget(self.button_add_linear_map_area, 0, 1)
        self.button_add_linear_map_area.clicked.connect(self.add_linear_map_area)
        self.button_delete_map_area = QtWidgets.QPushButton("Delete")
        self.button_delete_map_area.setFixedWidth(65)
        map_area_buttons_layout.addWidget(self.button_delete_map_area, 1, 0, 2, 2)
        self.button_delete_map_area.setEnabled(False)
        self.button_delete_map_area.clicked.connect(self.delete_map_area)

        self.edit_req_port = QtWidgets.QLineEdit("11020")
        self.button_start_worker = QtWidgets.QPushButton("Create Worker")
        self.widget_worker = QtWidgets.QGroupBox("Virtual Device Worker setup")
        self.widget_worker.setVisible(False)
        self.layout_worker = QtWidgets.QGridLayout(self.widget_worker)
        self.widget_worker.setToolTip("Create MapWidgetInterface object for remote operation.\n"
                                      "F. eg.:\n"
                                      "from devices.virtual.map_widget import MapWidgetInterface\n"
                                      "map_widget = MapWidgetInterface(req_port=11020)")
        self.button_start_worker.clicked.connect(self.toggle_worker)
        self.layout_worker.addWidget(QtWidgets.QLabel("req_port:"), 0, 0)
        self.layout_worker.addWidget(self.edit_req_port, 0, 1)
        self.layout_worker.addWidget(self.button_start_worker, 1, 0, 1, 3)
        self.layout_worker.setRowStretch(2, 1)

        self.check_focus = QtWidgets.QCheckBox("Enable focus correction")
        self.list_images = QtWidgets.QListWidget()
        self.list_images.currentRowChanged.connect(self.set_active_image_index)
        self.button_focus_increase = QtWidgets.QPushButton("Focus Up")
        self.button_focus_decrease = QtWidgets.QPushButton("Focus Down")
        self.edit_focus_step = QtWidgets.QLineEdit()
        self.edit_focus_step.editingFinished.connect(self.save_focus_step)
        try:
            with open("config\\map_widget_focus_step.cfg", "r") as file:
                self.edit_focus_step.setText(jsonpickle.decode(file.read()))
        except Exception:
            pass
        self.button_focus_increase.clicked.connect(self.focus_increase)
        self.button_focus_decrease.clicked.connect(lambda: self.focus_increase(backwards=True))
        self.button_focus_add = QtWidgets.QPushButton("Add new point")
        self.button_focus_add.clicked.connect(self.focus_add_point)
        self.button_focus_delete = QtWidgets.QPushButton("Delete selected point")
        self.button_focus_delete.clicked.connect(self.focus_delete_point)
        self.list_points = QtWidgets.QListWidget()

        layout_focus_buttons = QtWidgets.QGridLayout()
        layout_focus_buttons.setContentsMargins(0, 0, 0, 0)
        layout_focus_buttons.addWidget(self.button_focus_add, 0, 0)
        layout_focus_buttons.addWidget(self.button_focus_delete, 0, 1)

        layout_focus_buttons2 = QtWidgets.QGridLayout()
        layout_focus_buttons2.setContentsMargins(0, 0, 0, 0)
        layout_focus_buttons2.addWidget(QtWidgets.QLabel("Vertical correction step:"), 0, 0)
        layout_focus_buttons2.addWidget(self.edit_focus_step, 0, 1)
        layout_focus_buttons2.addWidget(self.button_focus_decrease, 1, 0)
        layout_focus_buttons2.addWidget(self.button_focus_increase, 1, 1)
        self.widget_focus = QtWidgets.QWidget()
        self.widget_focus.setFixedWidth(240)
        self.widget_focus.setVisible(False)
        self.layout_focus = QtWidgets.QVBoxLayout(self.widget_focus)
        self.layout_focus.setContentsMargins(0, 0, 0, 0)
        self.layout_focus.addWidget(self.check_focus)
        self.layout_focus.addWidget(self.list_images)
        self.layout_focus.addLayout(layout_focus_buttons)
        self.layout_focus.addWidget(self.list_points)
        self.layout_focus.addLayout(layout_focus_buttons2)

        vlayout_right = QtWidgets.QVBoxLayout()
        vlayout_right.setContentsMargins(0, 0, 0, 0)
        vlayout_right.addWidget(self.widget_worker)
        vlayout_right.addWidget(self.widget_focus)

        hlayout3 = QtWidgets.QHBoxLayout()
        hlayout3.addWidget(self.map_area_scroll)
        hlayout3.addWidget(self.view_widget)
        hlayout3.addLayout(vlayout_right)
        layout.addLayout(hlayout3)

        self.check_show_mapping = QtWidgets.QCheckBox("Mapping panel")
        self.check_show_mapping.stateChanged.connect(self.update_visibility_mapping)
        self.button_new_image = QtWidgets.QPushButton("Add new image")
        self.button_new_image.pressed.connect(self.add_new_image)
        self.xwidget = QtWidgets.QLineEdit()
        self.ywidget = QtWidgets.QLineEdit()
        self.check_show_focus = QtWidgets.QCheckBox("Show focus correction panel")

        hlayout4 = QtWidgets.QHBoxLayout()
        hlayout4.addWidget(self.check_show_mapping)
        hlayout4.addWidget(self.button_new_image)
        hlayout4.addStretch(10)
        hlayout4.addWidget(QtWidgets.QLabel("x:"))
        hlayout4.addWidget(self.xwidget)
        hlayout4.addSpacing(20)
        hlayout4.addWidget(QtWidgets.QLabel("y:"))
        hlayout4.addWidget(self.ywidget)
        hlayout4.addWidget(self.check_show_focus)
        self.check_show_focus.stateChanged.connect(self.update_visibility_focus)
        layout.addLayout(hlayout4)

        # images will be added to hlayout5 in separate routines (add_new_image and remove_image)
        self.hlayout5 = QtWidgets.QHBoxLayout()
        self.hlayout5.setContentsMargins(0, 0, 0, 0)
        layout.addLayout(self.hlayout5)

        # select axes and spectrometer
        hlayout6 = QtWidgets.QHBoxLayout()
        self.combos = {}
        for direction in ("x", "y", "z", "spectrometer"):
            hlayout6.addWidget(QtWidgets.QLabel(direction + ":"))
            combo = QtWidgets.QComboBox()
            combo.addItem("None")
            combo.setMinimumWidth(100)
            hlayout6.addWidget(combo)
            self.combos[direction] = combo
            hlayout6.addSpacing(10)
        hlayout6.addStretch()
        layout.addLayout(hlayout6)

        # status, connect, disconnect
        hlayout7 = QtWidgets.QHBoxLayout()
        self.button_refresh = QtWidgets.QPushButton("Refresh")
        self.button_refresh.clicked.connect(self.refresh_combos)
        hlayout7.addWidget(self.button_refresh)
        self.button_start = QtWidgets.QPushButton("Start control")
        self.button_start.setCheckable(True)
        self.button_start.clicked.connect(lambda: self.start(not self.active))
        self.button_start.clicked.connect(self.save_settings)
        hlayout7.addWidget(self.button_start)
        self.label_status = QtWidgets.QLabel()
        hlayout7.addStretch(1)
        hlayout7.addWidget(self.label_status)
        layout.addLayout(hlayout7)

        self.refresh_combos()
        self.refresh_images_list()

        if self.combos["x"].currentText() == "None" and self.combos["y"].currentText() == "None":
            self.start(activate=False)
        else:
            self.start(activate=True)
        self.show()

        self.mapping_state = "idle"  # "active", "pause"
        self.mapping_lock = threading.Lock()
        self.zeromq_lock = threading.Lock()

    def error_message(self, message):
        self.error_signal.emit(message)

    def update_progress_bar(self, count, text):
        self.progress_bar_signal.emit(count, text)

    def error_message_slot(self, message):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(message)
        msg.exec_()
        print(message)

    def update_progress_bar_slot(self, count, text):
        self.progress_bar.setValue(count)
        self.progress_bar.setFormat(text)
        with self.mapping_lock:
            state = self.mapping_state
        if state == 'pause':
            self.button_start_map.setText("Resume")

    def update_visibility_focus(self):
        self.widget_focus.setVisible(bool(self.check_show_focus.checkState()))

    def update_visibility_remote_widget(self):
        self.widget_worker.setVisible(bool(self.check_show_remote_widget.checkState()))

    def update_visibility_mapping(self):
        self.hwidget1.setVisible(bool(self.check_show_mapping.checkState()))
        self.hwidget2.setVisible(bool(self.check_show_mapping.checkState()))
        self.map_area_scroll.setVisible(bool(self.check_show_mapping.checkState()))
        self.widget_worker.setVisible(bool(self.check_show_remote_widget.checkState()) and
                                      bool(self.check_show_mapping.checkState()))

    def add_new_image(self):
        self.images.append(SampleImageItem(self))
        self.hlayout5.addWidget(self.images[-1].widget)
        self.images[-1].load_image()
        self.images[-1].index = len(self.images) - 1
        self.images[-1].setZValue(self.images[-1].index + 1)  # new image will be visible on top of the others
        self.set_active_image(self.images[-1])
        self.refresh_images_list()

    def remove_image(self, image):
        self.hlayout5.removeWidget(image.widget)
        self.scene.removeItem(image)
        image.widget.deleteLater()
        self.images.pop(image.index)
        if len(self.images) > 0:
            for i in range(len(self.images)):
                self.images[i].index = i
                self.images[i].setZValue(i + 1)
            self.set_active_image(self.images[-1])
        self.refresh_images_list()

    def refresh_images_list(self):
        if self.focus_image_id != 0:
            image_under_focus = self.images[self.focus_image_id - 1]
        else:
            image_under_focus = None

        self.list_images.clear()
        self.list_images.addItem("< global >")
        for im in self.images:
            self.list_images.addItem(im.edit_name.text())

        if image_under_focus is not None:
            self.list_images.setCurrentRow(image_under_focus.index + 1)
        else:
            self.list_images.setCurrentRow(0)

    def refresh_points_list(self):
        index = self.list_points.currentRow()
        self.list_points.clear()
        if self.focus_image_id != 0:
            plane_object = self.images[self.focus_image_id - 1]
        else:
            plane_object = self
        for point in plane_object.focus_points:
            self.list_points.addItem(str(point))
        self.list_points.setCurrentRow(index)

    def focus_increase(self, backwards=False):
        val = 0
        if self.edit_focus_step.text().strip() != "":
            try:
                val = float(self.edit_focus_step.text())
                if backwards:
                    val *= -1
            except Exception:
                self.error_message(traceback.format_exc())
                return
        if self.focus_image_id != 0:
            plane_object = self.images[self.focus_image_id - 1]
        else:
            plane_object = self
        n = len(plane_object.focus_points)
        plane_object.focus_points += numpy.transpose(numpy.array([numpy.zeros(n), numpy.zeros(n), numpy.ones(n) * val]))
        plane_object.focus_points = [tuple(x) for x in plane_object.focus_points.tolist()]
        self.refresh_points_list()
        plane_object.save_settings()

    def save_focus_step(self):
        try:
            with open("config\\map_widget_focus_step.cfg", "w") as file:
                file.write(jsonpickle.encode(self.edit_focus_step.text()))
        except Exception as e:
            pass

    def set_active_image(self, active_image):
        self.set_active_image_index(active_image.index + 1)

    def set_active_image_index(self, index):
        if index < 0:  # after list is cleared
            return
        if self.focus_image_id == index:
            return
        self.focus_image_id = index
        self.refresh_points_list()
        if index > 0:
            index -= 1
            for image in self.images:
                image.widget.setVisible(False)
            self.images[index].widget.setVisible(True)
            self.refresh_images_list()

    def move_image_back(self, image):
        id = image.index
        if id == 0:
            return  # already at back
        self.images[id] = self.images[id - 1]
        self.images[id - 1] = image
        for i in range(len(self.images)):
            self.images[i].index = i
            self.images[i].setZValue(i + 1)
        self.refresh_images_list()

    def move_image_front(self, image):
        id = image.index
        if id == len(self.images) - 1:
            return  # already at front
        self.images[id] = self.images[id + 1]
        self.images[id + 1] = image
        for i in range(len(self.images)):
            self.images[i].index = i
            self.images[i].setZValue(i + 1)
        self.refresh_images_list()

    def focus_delete_point(self):
        if self.focus_image_id != 0:
            plane_object = self.images[self.focus_image_id - 1]
        else:
            plane_object = self
        if len(plane_object.focus_points) < 1:
            return
        index = self.list_points.currentRow()
        plane_object.focus_points.pop(self.list_points.currentRow())
        if index >= len(plane_object.focus_points):
            index -= 1
        if index >= 0:
            self.list_points.setCurrentRow(index)
        plane_object.save_settings()
        self.refresh_points_list()

    def focus_add_point(self):
        if not self.active:
            return
        x = None
        y = None
        z = None
        try:
            for direction, combo in self.combos.items():
                if combo.currentText() == "None":
                    continue
                device_id = combo.currentIndex() - 1
                if direction == "x":
                    x = round_floating(self.pools[device_id]())
                if direction == "y":
                    y = round_floating(self.pools[device_id]())
                if direction == "z":
                    z = round_floating(self.pools[device_id]())
        except Exception:
            self.error_message(traceback.format_exc())
        if self.focus_image_id != 0:
            plane_object = self.images[self.focus_image_id - 1]
        else:
            plane_object = self
        if x is not None and y is not None and z is not None:
            plane_object.focus_points.append((x, y, z))
            plane_object.save_settings()
        self.refresh_points_list()

    def goto(self, position):
        x_slave = None
        y_slave = None
        for direction, combo in self.combos.items():
            if combo.currentText() == "None":
                continue
            device_id = combo.currentIndex() - 1
            if direction == "x":
                x_slave = self.slaves[device_id]
            if direction == "y":
                y_slave = self.slaves[device_id]
        if x_slave is not None and y_slave is not None:
            try:
                x_slave(position[0])
                y_slave(position[1])
            except Exception:
                self.error_message(traceback.format_exc())

    def start_pause_mapping(self):
        self.button_abort_map.setEnabled(True)
        with self.mapping_lock:
            if self.mapping_state == "idle":
                self.mapping_points = self.generate_map_points_list()
                if not self.mapping_points:
                    self.error_message("Please specify points to be mapped.")
                    return
                self.progress_bar.setMaximum(len(self.mapping_points))
                self.map_filename = self.edit_filename.text() + self.edit_filename_extension.text()
                if self.map_filename[-1] == '/' or self.map_filename == '':
                    self.error_message("Please specify filename.")
                    return
                self.mapping_state = "active"
                self.max_grid = max([item.larger_step() for item in self.map_area_items])
                try:
                    with open("config\\map_widget_delays.cfg", "w") as file:
                        file.write(jsonpickle.encode((self.edit_short_delay.text(), self.edit_long_delay.text())))
                except Exception:
                    self.error_message("There was an error while reading file: " + traceback.format_exc())
                self.mapping_thread = threading.Thread(target=self._mapping_loop)
                self.mapping_thread.start()
                self.button_start_map.setText("Pause")
            elif self.mapping_state == "active":
                self.button_start_map.setText("Resume")
                self.mapping_state = "pause"
            elif self.mapping_state == "pause":
                self.button_start_map.setText("Pause")
                self.mapping_state = "active"

    def abort_mapping(self):
        with self.mapping_lock:
            if self.mapping_state != "idle":
                self.mapping_state = "abort"

    def _mapping_loop(self):
        class MapAborted(Exception):
            pass

        def prepare_finish(parent, close_files=False):
            parent.button_start_map.setText("Start")
            parent.button_abort_map.setEnabled(False)
            with parent.mapping_lock:
                parent.mapping_state = 'idle'
            if close_files:
                try:
                    file.close()
                    file_log1.close()
                    file_log2.close()
                except Exception:
                    self.error_signal.emit(
                        "There was an error while closing measurement file: " + traceback.format_exc())
                    print("error closing measurement file" + traceback.format_exc())

        def wait(t):
            goal_time = clock() + t
            while clock() < goal_time:  # wait fixed time for axes to move to target point
                sleep(max(0.1, goal_time - clock()))
                while True:  # check state, wait for it not to be pause
                    with self.mapping_lock:
                        state = self.mapping_state
                    if state == "pause":
                        sleep(0.1)
                    else:
                        break
                if state == "abort":
                    prepare_finish(self, close_files=True)
                    raise MapAborted

        def wait_for_spectrometer(spectrometer):
            while spectrometer.acquisition_status() != 'idle':  # wait for spectrometer to finish acquisition
                sleep(0.02)
                while True:  # check state, wait for it not to be pause
                    with self.mapping_lock:
                        state = self.mapping_state
                    if state == "pause":
                        sleep(0.1)
                    else:
                        break
                if state == "abort":
                    prepare_finish(self, close_files=True)
                    raise MapAborted

        with self.mapping_lock:
            filename = self.map_filename
            points = self.mapping_points
            grid_treshold = self.max_grid + epsilon

        if len(points) == 0:
            self.error_message("Please specify points to be mapped.")
            prepare_finish(self, close_files=False)
            return

        try:
            long_delay = float(self.edit_long_delay.text())
            short_delay = float(self.edit_short_delay.text())
        except Exception:
            self.error_message("There was an error while reading long/short delay values: " + traceback.format_exc())
            prepare_finish(self, close_files=False)
            return

        try:
            from src.measurement_file import MeasurementFile
            file = MeasurementFile(filename)
            file_log1 = open(filename + ".log1", "a")
            file_log2 = open(filename + ".log2", "a")
        except Exception:
            self.error_message("There was an error while creating data files and log files: " + traceback.format_exc())
            prepare_finish(self, close_files=True)
            return

        point_no = 0
        last_position = (points[0][0] - grid_treshold - epsilon, 0)  # initial distance artificially set above threshold

        axis_id_x = self.combos['x'].currentIndex() - 1
        axis_id_y = self.combos['y'].currentIndex() - 1
        spectrometer_id = self.combos['spectrometer'].currentIndex() - 1
        if axis_id_x == -1 or axis_id_y == -1:
            self.error_signal.emit("Please specify both axes.")
            prepare_finish(self)
            return
        try:
            set_x = self.slaves[axis_id_x]
            set_y = self.slaves[axis_id_y]
            get_x = self.pools[axis_id_x]
            get_y = self.pools[axis_id_y]
        except Exception:
            self.error_message("There was an error while preparing handles for x and y: " + traceback.format_exc())
            prepare_finish(self, close_files=True)
            return

        try:
            spectrometer = self.spectrometers[spectrometer_id][1]
        except Exception:
            self.error_message("There was an error while preparing handle for spectrometer: " + traceback.format_exc())

        clock_begin = clock()

        try:
            set_x(points[0][0])
            set_y(points[0][1])
        except Exception:
            self.update_progress_bar(point_no, "Error")
            self.error_message("Sorry, but an unknown error occured: " + traceback.format_exc())
        try:
            while point_no < len(points):
                try:
                    distance = ((points[point_no][0] - last_position[0]) ** 2 +
                                (points[point_no][1] - last_position[1]) ** 2) ** 0.5
                    if distance > grid_treshold:  # if actor had to move more than basic grid step:
                        step_delay = long_delay
                    else:
                        step_delay = short_delay

                    if point_no + 1 < len(points):  # start going to next point as soon as possible
                        next_point = points[point_no + 1]
                    else:
                        next_point = points[point_no]

                    point = points[point_no]

                    progress_bar_text = ""
                    if point_no > 0:
                        seconds_remaining = (clock() - clock_begin) * (len(points) - point_no) / point_no
                        progress_bar_text = "%dh %02dmin %02ds left" % (
                        int(seconds_remaining / 3600), int(seconds_remaining / 60) % 60, int(seconds_remaining) % 60)
                    self.update_progress_bar(point_no, progress_bar_text)

                    wait(step_delay)

                    spectrometer.start_acquisition()
                    wait_for_spectrometer(spectrometer)

                    real_x = get_x()
                    real_y = get_y()

                    set_x(next_point[0])
                    set_y(next_point[1])

                    data_x, data_z = spectrometer.get_latest_data()
                    file.save_snapshot(x=data_x, y=point_no + 1, z=data_z)
                    file_log1.write(str(real_x) + "\n")
                    file_log2.write(str(real_y) + "\n")

                    last_position = point
                    point_no += 1

                except Exception as e:
                    if e.__class__ == MapAborted:
                        raise MapAborted
                    with self.mapping_lock:
                        self.mapping_state = "pause"
                    self.update_progress_bar(point_no, "Error")
                    self.error_message("Unknown error occured: " + traceback.format_exc() + "\n\nMap can be resumed.")
                    while True:
                        with self.mapping_lock:
                            state = self.mapping_state
                        if state == "pause":
                            sleep(0.1)
                        elif state == "abort":
                            raise MapAborted
                        else:
                            break
            self.update_progress_bar(len(points), "Finished")
        except MapAborted:
            self.update_progress_bar(point_no, "Aborted")

        prepare_finish(self, close_files=True)

    def browse_filename(self):
        filename, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save map as:", self.last_directory, "")
        if filename != "":
            self.last_directory = '/'.join(filename.split('/')[:-1])
            try:
                with open("config\\map_widget_data_dir.cfg", "w") as file:
                    file.write(jsonpickle.encode(self.last_directory))
            except Exception as e:
                print(traceback.format_exc())
            self.edit_filename.setText(filename)

    def delete_duplicated_points(self, points):
        order_function = lambda p: p[0] * 100 * numpy.pi + p[1]
        points.sort(key=order_function)  # low probability of equal points
        if len(points) == 0:
            unique = []
        else:
            unique = [points[0]]
            for i in range(1, len(points)):
                duplicate = False
                j = -1
                while j >= - len(unique) and order_function(unique[j]) > order_function(points[i]) - epsilon:
                    if math.sqrt((unique[j][0] - points[i][0]) ** 2 + (unique[j][1] - points[i][1]) ** 2) < epsilon:
                        duplicate = True
                        break
                    j -= 1
                if not duplicate:
                    unique.append(points[i])
        return unique

    def update_label_total_map_count(self):
        points = []
        for map_area in self.map_area_items:
            if map_area.get_positions_count() <= 30000:
                points += map_area.get_positions()
            else:
                self.label_total_map_count.setText(">30000 points total")
                return
        unique = self.delete_duplicated_points(points)
        self.label_total_map_count.setText(str(len(unique)) + " points total")

    def generate_map_points_list(self):
        points = []
        for map_area in self.map_area_items:
            points += map_area.get_positions()
        if points == []:
            return []
        else:
            rotation = self.map_area_items[0].get_parameter("rotation") * math.pi / 180
            return sorted(self.delete_duplicated_points(points),
                          key=lambda p: - math.sin(rotation - epsilon) * p[0]
                                        + math.cos(rotation - epsilon) * p[1])

    def add_map_area(self):
        tr = self.view_widget.mapToScene(QtCore.QPoint(self.view_widget.width(), 0))
        bl = self.view_widget.mapToScene(QtCore.QPoint(0, self.view_widget.height()))
        w = (tr.x() - bl.x()) / 3
        h = (tr.y() - bl.y()) / 3
        x = bl.x() + w
        y = bl.y() + h
        step = max(10 ** math.floor(math.log10(min(w, h) / 10)),
                   2 * 10 ** math.floor(math.log10(min(w, h) / 10 / 2)),
                   5 * 10 ** math.floor(math.log10(min(w, h) / 10 / 5)))
        x = step * math.floor(x / step)
        y = step * math.floor(y / step)
        w = step * math.floor(w / step)
        h = step * math.floor(h / step)

        map_area = MapAreaItem(self, x, y, w, h, step, step)

        self.button_delete_map_area.setEnabled(True)
        self.map_area_layout.removeItem(self.map_area_layout.itemAt(
            self.map_area_layout.count() - 1))
        self.map_area_layout.insertWidget(-3, map_area.widget)
        self.map_area_layout.addStretch(10)
        self.map_area_items.append(map_area)
        self.check_box_snap.stateChanged.connect(map_area.update)
        self.update_label_total_map_count()

    def add_linear_map_area(self):

        tr = self.view_widget.mapToScene(QtCore.QPoint(self.view_widget.width(), 0))
        bl = self.view_widget.mapToScene(QtCore.QPoint(0, self.view_widget.height()))
        w = (tr.x() - bl.x()) / 3
        h = (tr.y() - bl.y()) / 3
        x = bl.x() + w
        y = bl.y() + h / 2
        step = max(10 ** math.floor(math.log10(w / 10)),
                   2 * 10 ** math.floor(math.log10(w / 10 / 2)),
                   5 * 10 ** math.floor(math.log10(w / 10 / 5)))

        map_area = LinearMapAreaItem(self, x, y, w, step)

        self.button_delete_map_area.setEnabled(True)
        self.map_area_layout.removeItem(self.map_area_layout.itemAt(
            self.map_area_layout.count() - 1))
        self.map_area_layout.insertWidget(-3, map_area.widget)
        self.map_area_layout.addStretch(10)
        self.map_area_items.append(map_area)
        self.check_box_snap.stateChanged.connect(map_area.update)
        self.update_label_total_map_count()

    def delete_map_area(self):
        if len(self.map_area_items) == 1:
            self.button_delete_map_area.setEnabled(False)
        self.scene.removeItem(self.map_area_items[-1])
        self.map_area_items[-1].widget.setVisible(False)
        self.map_area_layout.removeWidget(self.map_area_items[-1].widget)
        self.map_area_items.pop(-1)

        self.update_label_total_map_count()

    def load_settings(self):
        try:
            with open("config\\map_widget.cfg", "r") as file:
                axes, self.focus_points = jsonpickle.decode(file.read())
                for direction in axes:
                    self.combos[direction].setCurrentText(axes[direction])
        except Exception as e:
            self.save_settings()

    def save_settings(self):
        try:
            with open("config\\map_widget.cfg", "w") as file:
                axes = {direction: self.combos[direction].currentText() for direction in self.combos}
                file.write(jsonpickle.encode((axes, self.focus_points)))
        except Exception as e:
            print(traceback.format_exc())

    def setup_scene(self):
        self.scene = MyGraphicsScene(self)
        self.view_widget.setScene(self.scene)

        self.cursor = QtWidgets.QGraphicsItemGroup()
        self.cursor.setZValue(1000)
        circle = QtWidgets.QGraphicsEllipseItem(self.cursor)
        circle.setRect(-5, -5, 10, 10)
        circle.setBrush(QtGui.QBrush(QtCore.Qt.red))
        circle.setFlag(QtWidgets.QGraphicsItem.ItemIgnoresTransformations, True)
        self.scene.addItem(self.cursor)

    def start(self, activate=True):
        self.active = activate
        if activate:

            self.button_start.setText("Stop Control")
            self.label_status.setText(
                "<span style=\" font-size:12pt; font-weight:600; color:#ff7700;\">Connecting...</span>")
            self.button_refresh.setEnabled(False)
            for direction, combo in self.combos.items():
                combo.setEnabled(False)
        else:
            self.button_start.setText("Start Control")
            self.label_status.setText(
                "<span style=\" font-size:12pt; font-weight:600; color:#aa0000;\">Not Connected</span>")
            self.button_refresh.setEnabled(True)
            for direction, combo in self.combos.items():
                combo.setEnabled(True)

    def refresh_combos(self):
        self.start(False)
        self.pools = []
        self.slaves = []
        self.axes_names = []
        self.spectrometers = []
        self.axis_info = []
        self.spectrometer_info = []
        my_ip = get_ip()

        try:
            from devices.spectro.spectrometer import Spectrometer
            for name, device in self.device_list.items():
                if isinstance(device, Spectrometer):
                    self.spectrometers.append((name, device))
                    # self.spectrometer_info.append((my_ip, device._req_port, device.__class__.__module__, device.__class__.__name__))
        except Exception as e:
            print(traceback.format_exc())

        try:
            from devices.thorlabs.apt import APT
        except:
            pass
        try:
            from devices.attocube.anc350 import ANC350
        except:
            class ANC350:
                pass
        from devices.misc.stepper import Stepper
        for name, device in {k: v for k, v in self.device_list.items()
                             if isinstance(v, APT) or isinstance(v, ANC350) or isinstance(v, Stepper)}.items():
            try:
                for axis in device.axes():
                    self.axes_names.append(self._create_name(name, axis))
                    self.pools.append(self._create_poll(device, axis))
                    self.slaves.append(self._create_slave(device, axis))
                    self.axis_info.append(
                        (my_ip, device._req_port, device.__class__.__module__, device.__class__.__name__, axis))
            except Exception as e:
                print(traceback.format_exc())

        for direction, combo in self.combos.items():
            n = combo.currentIndex()
            combo.clear()
            combo.addItem("None", lambda: None)
            if direction == "spectrometer":
                for name, _ in self.spectrometers:
                    combo.addItem(name)
            else:
                for name in self.axes_names:
                    combo.addItem(name)
            combo.setCurrentIndex(n)

        self.load_settings()

    def timeout(self):
        if self.active:
            try:
                last_x = None
                last_y = None
                z_slave = None
                for direction, combo in self.combos.items():
                    if combo.currentText() == "None":
                        continue
                    device_id = combo.currentIndex() - 1
                    if direction == "x":
                        last_x = self.pools[device_id]()
                        self.cursor.setX(last_x)
                    if direction == "y":
                        last_y = self.pools[device_id]()
                        self.cursor.setY(last_y)
                    if direction == "z":
                        z_slave = self.slaves[device_id]
                if self.check_focus.checkState() and last_x is not None and last_y is not None and z_slave is not None:
                    if self.focus_image_id != 0:
                        plane_object = self.images[self.focus_image_id - 1]
                    else:
                        plane_object = self
                    points = numpy.array(plane_object.focus_points)
                    if len(points) >= 3:
                        x, y, z = points.transpose()
                        mean_x = numpy.mean(x)
                        mean_y = numpy.mean(y)
                        mean_z = numpy.mean(z)
                        x = x - mean_x
                        y = y - mean_y
                        z = z - mean_z
                        # test if points are not too linear
                        n = len(x)
                        mean_square_dist = numpy.mean(x * x + y * y)
                        scalar_sum = numpy.sum(numpy.abs(x * numpy.roll(y, 1) - y * numpy.roll(x, 1)))

                        if mean_square_dist == 0:
                            self.error_message("Selected Points are identical! Plane fitting is not possible.")
                            self.check_focus.setChecked(False)
                        elif scalar_sum * 10 < mean_square_dist:
                            self.error_message("Selected Points are nearly colinear! Please spread them more evenly on "
                                               "a plane to avoid unstable fitting.")
                            self.check_focus.setChecked(False)
                        else:
                            xx = numpy.sum(x * x)
                            yy = numpy.sum(y * y)
                            xy = numpy.sum(x * y)
                            xz = numpy.sum(x * z)
                            yz = numpy.sum(y * z)
                            x = numpy.sum(x)
                            y = numpy.sum(y)
                            z = numpy.sum(z)
                            d = -xy ** 2 + 2 * x * xy * y - xx * y ** 2 - x ** 2 * yy + xx * yy
                            if d != 0:
                                z_result = mean_z + (
                                            xy * xz * y - x * xz * yy + x * xy * yz - xx * y * yz - xy ** 2 * z + xx * yy * z) / d \
                                           + (last_y - mean_y) / d * (
                                                       -xy * xz + x * xz * y - x ** 2 * yz + xx * yz + x * xy * z - xx * y * z) \
                                           + (last_x - mean_x) / d * (xz * (
                                            -y ** 2 + yy) - xy * yz + x * y * yz + xy * y * z - x * yy * z)
                                z_slave(z_result)

                self.label_status.setText(
                    "<span style=\" font-size:12pt; font-weight:600; color:#00cc00;\">Connected</span>")
                self.connection_error_retry_count = 2
            except Exception as e:
                if self.connection_error_retry_count > 0:
                    self.connection_error_retry_count -= 1
                    self.label_status.setText(
                        "<span style=\" font-size:12pt; font-weight:600; color:#ff7700;\">Axes not responding!</span>")
                else:
                    self.start(activate=False)
                    self.label_status.setText(
                        "<span style=\" font-size:12pt; font-weight:600; color:#aa0000;\">Axes not responding - disconnected</span>")
                print(traceback.format_exc())
            if self.combos["x"].currentText() == "None" or self.combos["y"].currentText() == "None":
                self.label_status.setText(
                    "<span style=\" font-size:12pt; font-weight:600; color:#ff7700;\">Some axes not selected!</span>")

    def _create_name(self, name, axis):
        return ("%s axis: %d" % (name, axis))

    def _create_poll(self, device, axis):
        def f():
            return device.get_position(axis)

        return f

    def _create_slave(self, device, axis):
        def f(position):
            return device.move_absolute(axis, position)

        return f

    def toggle_worker(self):
        if self.worker is None:
            try:
                self.worker = MapWidgetWorker(parent=self, req_port=int(self.edit_req_port.text()), pub_port=0)
                self.worker.init_device()
                self.worker.start_server()
                self.button_start_worker.setText("Disable Worker")
                self.button_start_worker.setEnabled(False)  # TODO find out how to properly delete worker
            except Exception:
                self.worker = None
                print("Worker could not be started.\n" + str(traceback.format_exc()))
        else:
            self.worker = None
            self.button_start_worker.setText("Create Worker")


def fullname(o):
    # o.__module__ + "." + o.__class__.__qualname__ is an example in
    # this context of H.L. Mencken's "neat, plausible, and wrong."
    # Python makes no guarantees as to whether the __module__ special
    # attribute is defined, so we take a more circumspect approach.
    # Alas, the module name is explicitly excluded from __qualname__
    # in Python 3.

    module = o.__class__.__module__
    if module is None or module == str.__class__.__module__:
        return o.__class__.__name__  # Avoid reporting __builtin__
    else:
        return module + '.' + o.__class__.__name__


if __name__ == '__main__':
    import sys

    app = QtWidgets.QApplication(sys.argv)
    okno = MapWidget([])
    sys.exit(app.exec_())
