import PyQt5
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.Qsci import QsciScintilla, QsciLexerPython, QsciAPIs
import os
import sys

class PythonEditor(QsciScintilla):
    run_requested = QtCore.pyqtSignal()
    ARROW_MARKER_NUM = 8

    def __init__(self, parent=None, font_size=10, line_numbers=True):
        super(PythonEditor, self).__init__(parent)

        # Set the default font
        font = QtGui.QFont()
        font.setFamily('Courier')
        font.setFixedPitch(True)
        font.setPointSize(font_size)
        self.setFont(font)

        # Margin 0 is used for line numbers
        if line_numbers:
            self.setMarginsFont(font)
            fontmetrics = QtGui.QFontMetrics(font)
            self.setMarginsFont(font)
            self.setMarginWidth(0, fontmetrics.width("0000"))
            self.setMarginLineNumbers(0, True)
            self.setMarginsBackgroundColor(QtGui.QColor("#eeeeee"))

        # Brace matching: enable for a brace immediately before or after
        # the current position
        #
        self.setBraceMatching(QsciScintilla.SloppyBraceMatch)

        # Current line visible with special background color
        self.setCaretLineVisible(True)
        self.setCaretLineBackgroundColor(QtGui.QColor("#ffe4e4"))

        # Set Python lexer
        lexer = QsciLexerPython(self)
        lexer.setDefaultFont(font)
        self.setLexer(lexer)



        ## setup autocompletion
        api = QsciAPIs(lexer)

        # import the desired api file
        pyqt_path = os.path.dirname(PyQt5.__file__)
        python3_version = int(sys.version.split(".")[1])
        api.load(os.path.join(pyqt_path, f"Qt/qsci/api/python/Python-3.{python3_version}.api"))

        api.prepare()
        self.setAutoCompletionThreshold(1)
        self.setAutoCompletionSource(QsciScintilla.AcsAll)



        # set python style tabs
        self.setIndentationsUseTabs(False)
        self.setTabWidth(4)
        self.setAutoIndent(True)
        # Don't want to see the horizontal scrollbar at all
        # Use raw message to Scintilla here (all messages are documented
        # here: http://www.scintilla.org/ScintillaDoc.html)
        self.SendScintilla(QsciScintilla.SCI_SETHSCROLLBAR, 0)

    def keyPressEvent(self, event: QtGui.QKeyEvent):
        if event.key() == QtCore.Qt.Key_F5:
            self.run_requested.emit()
        else:
            super(PythonEditor, self).keyPressEvent(event)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    editor = PythonEditor(font_size=10, line_numbers=False)
    editor.show()
    editor.setText(open(sys.argv[0]).read())
    app.exec_()