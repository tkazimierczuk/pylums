# -*- coding: utf-8 -*-
"""
Created on Tue Feb 05 2019

@author: ABogucki
"""

from PyQt5 import QtWidgets,QtGui,QtCore
import pyqtgraph as pg
import numpy as np
import datetime
from src.measurement_file import MeasurementFile
import os
import devices
import time
import traceback

class PlotTab(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.data_x = []
        self.data_y = []
        self.data_dict = {}
        self.timer = QtCore.QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.update_plot_periodically)
        self.file = None
        
        self.setup_ui()
        
    def setup_plot(self):
        pass
        
    def setup_ui(self):   
        self.button_start = QtWidgets.QPushButton("Start recording")
        self.button_start.setToolTip('Creates new temporary measurement file with status values')
        self.button_start.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
                                        QtWidgets.QSizePolicy.Fixed)

        self.button_stop = QtWidgets.QPushButton("Stop recording")
        self.button_stop.setToolTip('Stops recording to the last measurement file')
        self.button_stop.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
                                        QtWidgets.QSizePolicy.Fixed)

        self.edit_interval = QtWidgets.QLineEdit("2.0")
        self.update_interval()

        self.combo_x=QtWidgets.QComboBox()
        self.combo_x.setToolTip('Horizontal axis: Avaliable parameters for plotting')
        self.combo_x.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Fixed)
        
        self.combo_y = QtWidgets.QComboBox()
        self.combo_y.setToolTip('Vertical axis: Avaliable parameters for plotting')
        self.combo_y.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Fixed)
        
        self.plot_widget = pg.PlotWidget()
        self.plot_widget.setLabel('bottom', str(self.data_x))
        self.plot_curve = pg.PlotCurveItem()
        self.plot_widget.addItem(self.plot_curve)

        layout = QtWidgets.QVBoxLayout()
        layout_h = QtWidgets.QHBoxLayout()
        layout_h.addWidget(self.button_start)
        layout_h.addWidget(self.button_stop)
        layout_h.addWidget(QtWidgets.QLabel("Interval (s)"))
        layout_h.addWidget(self.edit_interval)
        layout_h.addWidget(QtWidgets.QLabel("x:"))
        layout_h.addWidget(self.combo_x)
        layout_h.addWidget(QtWidgets.QLabel("y:"))
        layout_h.addWidget(self.combo_y)
        layout.addLayout(layout_h)
        layout.addWidget(self.plot_widget)
        self.setLayout(layout)
        
        self.button_start.clicked.connect(self.start_recording)
        self.button_stop.clicked.connect(self.stop_recording)
        self.edit_interval.editingFinished.connect(self.update_interval)

        self.combo_x.currentTextChanged.connect(self.redraw_plot)
        self.combo_y.currentTextChanged.connect(self.redraw_plot)

    def timestamp(self):
        n = datetime.datetime.now()
        return str('{0:%Y}{0:%m}{0:%d}_{0:%H}{0:%M}{0:%S}'.format(n))

    def start_recording(self):
        timestamp = self.timestamp()
        directory = "parameter_plots"
        if not os.path.exists(directory):
            os.makedirs(directory)
        self.file = MeasurementFile(f'{directory}/Parameters_file_{timestamp}.h5', cached_only=True)
        self.time_start = time.time()
        self.file.save_snapshot()
        self.redraw_plot()
        self.data_dict = get_1D_data(self.file.file)
        print(self.data_dict)
        text_x = self.combo_x.currentText()
        text_y = self.combo_y.currentText()

        for combo in [self.combo_x, self.combo_y]:
            combo.clear()
            for v in self.data_dict:
                combo.addItem(v)

        self.combo_x.setCurrentText('elapsed_time' if text_x == "" else text_x)
        self.combo_y.setCurrentText('data_full/itc/temperature' if text_y == "" else text_y)

        print("Recording started at", timestamp)

        self.timer.start(self.interval)

    def stop_recording(self):
        self.timer.stop()
        timestamp = self.timestamp()
        try:
            self.file.file.flush()
            print("Recording closed at", timestamp)
        except Exception:
            print("Can't save file,  ", timestamp)

    def update_interval(self):
        try:
            self.interval = round(1000 * float(self.edit_interval.text()))
        except Exception:
            self.interval = 2000

    def update_plot_periodically(self):
        self.timer.start(self.interval)
        try:
            self.add_single_point()
        except Exception:
            print(traceback.format_exc())

    def get_data_point(self, dataset):
        if dataset == "elapsed_time":
            data = time.time() - self.time_start
        else:
            keys = dataset.split('/')[1:]
            data = devices.active_devices[keys[0]].status_cached()
            for key in keys[1:]:
                data = data[key]
        return data

    def add_single_point(self):
        self.file.save_snapshot()
        self.data_x.append(self.get_data_point(self.combo_x.currentText()))
        self.data_y.append(self.get_data_point(self.combo_y.currentText()))
        if not self.visibleRegion().isEmpty():
            self.refresh_plot()

    def paintEvent(self, event):
        self.refresh_plot()

    def refresh_plot(self):
        common_len = min(len(self.data_x), len(self.data_y))
        self.plot_curve.setData(self.data_x[:common_len], self.data_y[:common_len], pen="r")

    def redraw_plot(self):
        try:
            self.data_dict = get_1D_data(self.file.file)
            self.data_x = list(self.data_dict[self.combo_x.currentText()])
            self.data_y = list(self.data_dict[self.combo_y.currentText()])
            self.refresh_plot()
        except Exception:
            print("Missing data")
        
    
def getTree(hdf5file):
    datasets = []
    hdf5file.visit(datasets.append)
    return datasets


# Boolean, unsigned integer, signed integer, float, complex.
# _NUMERIC_KINDS = set('buifc')
_NUMERIC_KINDS = set('uif')


def is_numeric(array):
    """Determine whether the argument has a numeric datatype, when
    converted to a NumPy array.

    Booleans, unsigned integers, signed integers, floats and complex
    numbers are the kinds of numeric datatype.

    Parameters
    ----------
    array : array-like
        The array to check.

    Returns
    -------
    is_numeric : `bool`
        True if the array has a numeric datatype, False if not.

    """
    return np.asarray(array).dtype.kind in _NUMERIC_KINDS


def get_1D_data(hdf5file):
    data_dict = {}
    t = getTree(hdf5file)
    for b in t:
        try:
            n = np.array(hdf5file.get(b))
            if is_numeric(n) and len(n)>0:
                if len(n.shape) == 1:
                    data_dict[b] = n
        except Exception:
            pass
    return data_dict