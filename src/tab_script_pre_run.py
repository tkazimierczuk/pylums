def progress(val):
    __progress_value.value = float(val)

import devices
_ = devices.load_devices()
globals().update(devices.active_devices)
code = __script_tab_code
import h5py

import src.measurement_file
src.measurement_file.__OldMeasurementFile = src.measurement_file.MeasurementFile
class MeasurementFile(src.measurement_file.__OldMeasurementFile):
    global __script_tab_code
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        dset = self.file.create_dataset('script', (1,), dtype=h5py.special_dtype(vlen=str))
        dset[0] = code
src.measurement_file.MeasurementFile = MeasurementFile

if __filename != '':
    file = MeasurementFile(__filename)
import time
import numpy
import sys
import traceback
time.old_sleep = time.sleep

def __close_all(object):
    if object.__class__.__name__ in ['TextIOWrapper', 'BufferedWriter', 'MeasurementFile']:
        object.close()
    elif isinstance(object, list):
        for o in object:
            __close_all(o)
    elif isinstance(object, dict):
        for o in object:
            __close_all(object[o])

def __cleanup_and_finish():
    for obj in globals():
        object = globals()[obj]
        __close_all(object)
    raise EndOfMeasurementException('')

def check_if_cancel_requested():
    if __cancel_requested.value:
        __cleanup_and_finish()
    if __pause_requested.value:
        print('Paused')
        while __pause_requested.value:
            if __cancel_requested.value:
                __cleanup_and_finish()
            time.old_sleep(0.1)

def __sleep_checking_pause(timeout):
    t1 = time.time() + timeout
    check_if_cancel_requested()
    remaining = t1 - time.time()
    while remaining > 0:
        time.old_sleep(min(0.5, remaining))
        check_if_cancel_requested()
        remaining = t1 - time.time()
time.sleep = __sleep_checking_pause
from time import sleep

try:
    __slack_token = open("config/slack_token.cfg").read()
except Exception:
    __slack_token = None

__slack_client = None

def slack_message(username, text):
    try:
        global __slack_client, __user_ids
        if __slack_client is None:
            try:
                from slack import WebClient as __WebClient
            except:
                print("Could not import slack or slackclient modules!")
                return
            assert __slack_token, 'Slack token not specified in "config/slack_token.cfg"!'
            __slack_client = __WebClient(token=__slack_token)
            response = __slack_client.users_list()
            users = response["members"]
            __user_ids = {user["real_name"]: user["id"] for user in users if "real_name" in user}
            __user_ids.update({user["name"]: user["id"] for user in users if "name" in user})
        if username in __user_ids:
            username = __user_ids[username]
        __slack_client.chat_postMessage(channel=username, text=text)
    except Exception:
        print("Error sending Slack Message:")
        print(traceback.format_exc())