# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets, QtCore, QtGui
from src.python_editor import PythonEditor
import time
import traceback

from src.measurement_tab import Measurement, EndOfMeasurementException
from multiprocessing import Process, Value, Pipe
from DeviceServer import StreamToPipe, PipeListener
import sys
import os


def parse_traceback(tb, stack_lines):
    # find and replace 'File "<string>"' with script names and replace line numbers with correct ones in given file
    def get_real_line_no(line_no):
        # search stack to find out in which file and line error occured
        lines_counted = 0
        for filename, lines, lines_total, lines_includes in stack_lines:
            lines_counted += lines
            if lines_counted + lines_includes >= line_no:
                return (lines_total + lines_includes) - (lines_counted - line_no), filename

    def parse_line(line):
        line = line.replace(", in <module>", "") # cleanup unnecessary text
        try:
            spl = line.split('File "<string>", line ')
            assert len(spl) == 2, ""
            spl_coma = spl[1].split(",", 1)
            line_no = int(spl_coma[0].strip())
            line_no_file, filename = get_real_line_no(line_no - 1)
            line = f'File "{filename}", line {line_no_file}'
            line += f',{spl_coma[1]}'  # added in separate operation, as element 1 does not necessarily exist
        except (AssertionError, IndexError):
            pass

        # remove script_tab related stages from traceback
        if ('tab_script.py", line ' in line and "in process_func" in line) or line.strip() == "exec(code, dict)":
            return ""
        return line + "\n"

    return "".join([parse_line(line) for line in tb.split("\n")]).strip("\n")


def process_func(pout, perr, code, file_path, stack_lines, progress_value, pause_requested, cancel_requested):
    sys.stdout = StreamToPipe(pout)
    sys.stderr = StreamToPipe(perr)
    code_pre = open("src/tab_script_pre_run.py").read()

    dict = {"__progress_value": progress_value,
            "__filename": file_path,
            "__pause_requested": pause_requested,
            "__cancel_requested": cancel_requested,
            "__script_tab_code": code,
            "EndOfMeasurementException": EndOfMeasurementException}
    exec(code_pre, dict)
    code = "try:\n" + \
           "\n".join(["    " + line for line in code.split("\n")]) + "\n" + \
           "except:\n" + \
           "    __traceback = traceback.format_exc()\n" + \
           "    raise\n"

    try:
        exec(code, dict)
    except EndOfMeasurementException:
        dict = {}
    except:
        print(parse_traceback(traceback.format_exc(), stack_lines))


help_text = "You can use all imported devices. Several special functions are enabled in Script Tab:\n" + \
            "\n" + \
            "\n" + \
            "progress(x) - updates progress bar (x in range from 0 to 1)\n" + \
            "\n" + \
            "check_if_cancel_requested() - rarely needed because cancel checking " + \
            "is performed automatically when sleep() function is used.\n" + \
            "\n" + \
            "file - You can use variable 'file' to access file created automatically\n" + \
            "\n" + \
            'include "script_name" - you can place some common functions in separate script.\n' + \
            "\n" + \
            'slack_message("user name", "Message text") - you can notify yourself when ' + \
            "the measurement is finished or when something is wrong."

class HelpWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Help")

        label = QtWidgets.QLabel(help_text)
        label.setWordWrap(True)

        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(label)


class ScriptMeasurement(Measurement):
    enable_kill = True

    def run(self):
        try:
            code, stack_lines = self.parse_includes(self.edit_code.text(),
                                                    filename=self.edit_name.text(),
                                                    stack_files=[self.edit_name.text()])

            last_progress = 0.0
            progress_value = Value("f", last_progress)
            pause_requested = Value("i", 0)
            cancel_requested = Value("i", 0)

            if self.check_file.checkState():
                file_path = self.automatic_file_path
            else:
                file_path = ""

            pout_r, pout_w = Pipe()
            perr_r, perr_w = Pipe()

            self.threads = []
            for pipe, slot in [(pout_r, self.log), (perr_r, self.log)]:
                thread = PipeListener(None, pipe)
                thread.message_received.connect(slot, QtCore.Qt.QueuedConnection)
                thread.start()
                self.threads.append(thread)

            p = Process(target=process_func,
                      name='pyLUMS - script',
                      args=(pout_w, perr_w, code, file_path, stack_lines,
                            progress_value, pause_requested, cancel_requested))
            p.start()
            while p.is_alive():
                time.sleep(0.05)
                pause_requested.value = self.paused
                cancel_requested.value = self.cancelled
                if self.killed:
                    p.terminate()
                    raise EndOfMeasurementException()
                if last_progress != progress_value.value:
                    last_progress = progress_value.value
                    self.measurement_progress.emit(last_progress)
        except:
            time.sleep(0.1)
            raise


    def setup_additional_widgets(self, canvas_widget):
        self.block_count = 0
        """ This function is supposed to create user interface """
        self.automatic_file = False
        self.canvas_widget = canvas_widget
        hlayout = QtWidgets.QHBoxLayout()
        hlayout.setContentsMargins(0, 0, 0, 0)

        self.edit_code = PythonEditor()
        self.edit_code.run_requested.connect(lambda: self.trigger_start.emit())
        self.check_file = QtWidgets.QCheckBox("Create measurement file at the start of\n"
                                              "the script, using filename chosen above")
        self.list_files = QtWidgets.QListWidget()
        self.list_files.currentItemChanged.connect(self.load_file)
        self.edit_name = QtWidgets.QLineEdit()
        self.edit_name.editingFinished.connect(lambda: self.save_file(update_filenames=True))

        self.help_widget = HelpWidget()
        self.button_help = QtWidgets.QPushButton("Help")
        self.button_help.clicked.connect(self.help_widget.show)

        self.button_create_new = QtWidgets.QPushButton("Create empty script")
        self.button_create_new.clicked.connect(lambda:self.create_file(new=True))

        self.button_create_copy = QtWidgets.QPushButton("Edit a copy")
        self.button_create_copy.clicked.connect(lambda: self.create_file(new=False))

        self.button_delete_script = QtWidgets.QPushButton("Delete script")
        self.button_delete_script.clicked.connect(self.delete_script)

        gridlayout = QtWidgets.QGridLayout()
        gridlayout.setContentsMargins(0, 0, 0, 0)
        gridlayout.addWidget(self.check_file, 0, 0, 1, 2)
        gridlayout.addWidget(self.button_create_new, 1, 0, 1, 1)
        gridlayout.addWidget(self.button_create_copy, 1, 1, 1, 1)
        gridlayout.addWidget(self.button_delete_script, 2, 0, 1, 1)
        gridlayout.addWidget(self.button_help, 2, 1, 1, 1)
        gridlayout.addWidget(self.list_files, 3, 0, 1, 2)
        gridlayout.addWidget(self.edit_name, 4, 0, 1, 2)
        self.check_file.stateChanged.connect(self.save_file)
        self.edit_code.SCN_FOCUSOUT.connect(self.save_file)

        hlayout.addWidget(self.edit_code)
        hlayout.setStretch(0, 1)
        hlayout.addLayout(gridlayout)
        canvas_widget.setLayout(hlayout)
        canvas_widget.parent().layout().setStretch(1, 1)

        self.load_filenames()

    def parse_includes(self, code, filename, stack_files):
        # This function scans consecutive includes and merges source code into one long string. At the same time,
        # the database is build, allowing to recreate which line belongs to which file, and what is its real line number.
        # To prevent looped includes, stack of used files is tracked through the recursive scan.
        stack_lines = [] # list of tuples (script name, number of lines, total number of lines)
                         # number of lines: uninterrupted consecutive lines before or after include
                         # total number of lines: total parsed lines so far in this file, except include lines
                         # total number of includes: total include lines so far in this file
                         # example: [("main file", 2, 2), ("included file", 3, 3), ("main file", 7, 9)]
        line_counter = 0
        line_counter_total = 0
        line_counter_includes = 0
        result = ""
        for line in code.split("\n"):
            try:
                # get included filename:
                splitted = line.split('"')
                assert splitted[0].strip() == "include"
                if len(splitted) != 3:  # if no matching " parentheses, try '
                    splitted = line.split("'")
                    if len(splitted) != 3:
                        raise SyntaxError("Syntax error when including file.")
                if splitted[2].strip() != "":
                    raise SyntaxError("Syntax error when including file.")

                filename_incl = splitted[1].strip()

                # check stack_files - prevent recursive inclusion
                if filename_incl in stack_files:
                    raise RecursionError(f"Recursively nested inclusion: {stack_files + [filename_incl]}.")

                # read included script and parse it
                with open("scripts/" + filename_incl + ".py", 'r') as file:
                    _ = file.readline()
                    included_code = file.read()

                stack_lines += [(filename, line_counter, line_counter_total, line_counter_includes)]
                line_counter_includes += 1
                line_counter = 0
                parsed_code, parsed_stack = self.parse_includes(included_code, filename_incl, stack_files)
                stack_lines += parsed_stack
                result += parsed_code
            except (AssertionError, IndexError):  # include statement search not succesful, return line as it is
                line_counter += 1
                line_counter_total += 1
                result += line + "\n"
            except FileNotFoundError as e:
                raise FileNotFoundError(f'Script "{filename_incl}" loaded from '
                                        f'script "{filename}" has not been found.') from e

        stack_lines += [(filename, line_counter, line_counter_total, line_counter_includes)]
        return result, stack_lines

    def load_file(self):
        self.block_signals(True)
        name = self.files[self.list_files.currentIndex().row()]
        self.edit_name.setText(name)
        try:
            filename = "scripts/" + name + ".py"
            with open(filename, 'r') as file:
                header = file.readline()
                code = file.read()
            params = header.split("#")
            try:
                autofile = int(params[2])
                locked = int(params[4])
                if (autofile != 2 and autofile != 0) or (locked != 1 and locked != 0):
                    raise Exception("invalid header")
            except Exception:
                autofile = 0
                locked = 0
                code = header + code
            self.check_file.setChecked(autofile)
            if locked != 0:
                self.edit_name.setText("!UNSAVED! rename me to save changes")
            self.edit_code.setText(code)
        except:
            print(traceback.format_exc())
        self.block_signals(False)

    def save_file(self, update_filenames=False):
        self.block_signals(True)
        name = self.edit_name.text()
        if name == "":
            self.block_signals(False)
            return
        try:
            filename = "scripts/" + name + ".py"
            header = "#use_autocreated_file:#%i#locked:#0#\n" % int(self.check_file.checkState())
            code = self.edit_code.text().replace("\r", "")
            with open(filename, 'w') as file:
                file.write(header + code)
        except:
            print(traceback.format_exc())
        if update_filenames:
            self.load_filenames()
        self.block_signals(False)

    def load_filenames(self):
        self.block_signals(True)
        last_item_index = self.list_files.currentRow()
        last_name = self.edit_name.text()
        self.list_files.clear()
        datadir = QtCore.QDir.current()
        datadir.cd("scripts")
        file_list = datadir.entryList()
        self.files = []
        self.indexes = {}
        for file in file_list:
            if file[-3:] != '.py':
                continue
            name = file[0:-3]
            if name == last_name:
                last_item_index = len(self.files)
            self.indexes[name] = len(self.files)
            self.files.append(name)
            self.list_files.addItem(name)
        self.list_files.setCurrentRow(last_item_index)
        self.block_signals(False)

    def create_file(self, new=True):
        text, ok = QtWidgets.QInputDialog.getText(self.canvas_widget, f'New {"file" if new else "copy"}',
                                                  f'Enter the name for the new {"" if new else "copy of this "}file:',
                                                  text=self.edit_name.text())
        if ok:
            if new:
                self.edit_code.setText("")
            self.edit_name.setText(text)
            self.save_file(update_filenames=True)

    def delete_script(self):
        name = self.edit_name.text()

        # noinspection PyTypeChecker
        reply = QtWidgets.QMessageBox.question(self.canvas_widget, 'Delete script',
                                               f'Are you sure you want to permanently delete script {name}?',
                                               QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.Cancel,
                                               QtWidgets.QMessageBox.Cancel)
        if reply == QtWidgets.QMessageBox.Yes:
            self.edit_name.setText("!UNSAVED! rename me to save changes")
            filename = "scripts/" + name + ".py"
            try:
                os.remove(filename)
            except Exception:
                pass
            self.save_file(update_filenames=True)

    def block_signals(self, block):
        if block:
            self.block_count += 1
        else:
            self.block_count -= 1
        block = (self.block_count > 0)
        self.list_files.blockSignals(block)
        self.edit_name.blockSignals(block)
        self.edit_code.blockSignals(block)
        self.check_file.blockSignals(block)
