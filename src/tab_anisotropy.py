# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets,QtCore
import devices
from devices.thorlabs import apt
#from devices.oxford.set
import time

from src.measurement_tab import Measurement
import numpy as np
from src.widgets import APTSelectWidget

class AnisotropyMeasurement(Measurement):
    def run(self):
        self.canvas_widget.setEnabled(False)
        try:            
            spectrometer = devices.active_devices[self.spectrometer_device.text()]
            apt = devices.active_devices[self.apt_device.text()]
            
            pos0,pos1 = self.range_combo.currentData()
            angles = np.linspace(pos0, pos1, self.steps_input.value()+1)
            
            self.log("Will measure %d positions (including both limits)" % len(angles))
            motornum = int(self.motor_id.text())
            
            for i,angle in enumerate(angles):
                self.measurement_progress.emit(i/len(angles))
                apt.move_absolute(motornum, angle)
                self.check_if_cancel_requested()
                while not apt.is_stopped(motornum):
                    self.check_if_cancel_requested()
                    time.sleep(0.2)
                self.measurement_file.start_snapshot()
                x,y = spectrometer.take_single_spectrum()
                self.measurement_file.finish_snapshot(x=x, y=angle, z=y)
        
        except:
            self.canvas_widget.setEnabled(True)
            raise   
        self.canvas_widget.setEnabled(True)
    
            
    def setup_additional_widgets(self, canvas_widget):
        """ This function is supposed to create user interface """
        self.canvas_widget = canvas_widget
        formlayout = QtWidgets.QFormLayout()
               
        self.range_combo = QtWidgets.QComboBox()
        self.range_combo.addItem("0°-90°", (0,90))
        self.range_combo.addItem("0°-180°", (0,180))
        self.range_combo.addItem("0°-360°", (0,360))
        self.range_combo.setCurrentIndex(1)
        formlayout.addRow("Range of angles:", self.range_combo)
        
        self.steps_input = QtWidgets.QSpinBox()
        self.steps_input.setMinimum(2)
        self.steps_input.setValue(45)
        self.steps_input.setMaximum(10000)
        formlayout.addRow("Steps:", self.steps_input)
        
        self.apt_device = QtWidgets.QLineEdit("apt")
        formlayout.addRow("APT server id", self.apt_device)
        
        self.motor_id = APTSelectWidget()
        formlayout.addRow("Motor number", self.motor_id)
        
        self.spectrometer_device = QtWidgets.QLineEdit("spectrometer")
        formlayout.addRow("Spectrometer device id", self.spectrometer_device)
        
        canvas_widget.setLayout(formlayout)