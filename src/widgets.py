from PyQt5 import QtWidgets,QtCore,QtGui
import devices
import random


class ColoredLineEdit(QtWidgets.QLineEdit):
    """CSS string corresponding to the bar color"""
    bar_color = "gray"
    
    def set_value(self, text, value):
        self.setText(text)
        if value < 0:
            value = 0
        if value > 0.99:
            value = 0.99
        self.setStyleSheet("""background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,
                                      stop: 0 %s, stop: %f %s,
                                      stop: %f white, stop: 1 white);""" % (self.bar_color, value, self.bar_color, value+0.01))


class VerticalScrollArea(QtWidgets.QScrollArea):
    def __init__(self, parent=None):
        super(VerticalScrollArea, self).__init__(parent)
        self.setWidgetResizable(True)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

    def eventFilter(self, object, event):
        if event.type() == QtCore.QEvent.Resize:
            self.setMinimumWidth(self.widget().minimumSizeHint().width() + self.verticalScrollBar().width())
        return super(VerticalScrollArea, self).eventFilter(object, event)


class ResizableLabel(QtWidgets.QLabel):
    def __init__(self, parent=None):
        super(ResizableLabel, self).__init__(parent)
        self.str = ""
        self.h = self.height()
        self.installEventFilter(self)
        policy = QtWidgets.QSizePolicy()
        policy.setVerticalStretch(1)
        policy.setVerticalPolicy(QtWidgets.QSizePolicy.Expanding)
        self.setSizePolicy(policy)

    def set_text(self, text):
        self.str = text
        self.update_text()

    def resizeEvent(self, *args, **kwargs):
        self.h = self.sizeHint().height()
        self.setMinimumHeight(self.h)
        self.update_text()

    def update_text(self):
        size = min(self.h - 6, 30)
        self.setText("<span style=\" font-size:%spt; font-weight:600; color:#ff7700;\">%s</span>" % (size, self.str))


class HLine(QtWidgets.QFrame):
    def __init__(self):
        super(HLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)


class APTSelectWidget(QtWidgets.QLineEdit):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setPlaceholderText("click to select")

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            dialog = QtWidgets.QDialog(self)
            layout = QtWidgets.QFormLayout()
            motor_combo = QtWidgets.QComboBox()

            layout.addRow("Select motor:", motor_combo)
            for name, device in devices.active_devices.items():
                try:
                    for axis in device.devices():
                        motor_combo.addItem(str(axis))
                except:
                    pass
            button_box = QtWidgets.QDialogButtonBox(dialog)
            button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Ok
                                          | QtWidgets.QDialogButtonBox.Cancel)
            button_box.accepted.connect(dialog.accept)
            button_box.rejected.connect(dialog.reject)
            layout.addRow(button_box)

            dialog.setLayout(layout)
            if dialog.exec_():
                self.setText(str(motor_combo.currentText()))

if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout

    app = QtWidgets.QApplication([])
    window = QtWidgets.QWidget()
    layout = QtWidgets.QVBoxLayout()
    labels = [ResizableLabel() for i in range(5)]
    for i in range(len(labels)):
        labels[i].set_text("asdfasdfsadfsadfg " + str(i))
        layout.addWidget(labels[i])
    window.setLayout(layout)
    window.show()
    app.exec_()