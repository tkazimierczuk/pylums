# -*- coding: utf-8 -*-
"""
@author: kacperoreszczuk
"""

from PyQt5 import QtWidgets, QtGui, QtCore
import numpy as np
import jsonpickle
import traceback
import os
import h5py
import subprocess
import tempfile
import time

from src.widgets import HLine


class DroppableLineEdit(QtWidgets.QLineEdit):
    def __init__(self):
        super(DroppableLineEdit, self).__init__()


class HDF5ToolTab(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.file = None
        self.datasets = []
        self.datasets_names = []
        try:
            with open("config\\hdf5_tool_last_dir.cfg", "r") as file:
                self.last_directory = jsonpickle.decode(file.read())
        except Exception as e:
            self.last_directory = os.getcwd()

        try:
            with open("config\\pleview_path.cfg", "r") as file:
                self.pleview_path = jsonpickle.decode(file.read())
        except Exception as e:
            self.pleview_path = ""

        try:
            with open("config\\easyplot_path.cfg", "r") as file:
                self.easyplot_path = jsonpickle.decode(file.read())
        except Exception as e:
            self.easyplot_path = ""

        try:
            with open("config\\hdf5_tool_shape.cfg", "r") as file:
                self.export_shape = jsonpickle.decode(file.read())
        except Exception as e:
            self.export_shape = 0

        self.setup_ui()

    def setup_ui(self):
        self.setAcceptDrops(True)
        vlayout = QtWidgets.QVBoxLayout()

        self.button_browse_in = QtWidgets.QPushButton("Browse")
        self.button_browse_in.clicked.connect(self.browse_in)
        self.edit_filename_in = QtWidgets.QLineEdit()
        self.button_browse_out = QtWidgets.QPushButton("Browse")
        self.button_browse_out.clicked.connect(self.browse_out)
        self.edit_filename_out = QtWidgets.QLineEdit()
        self.button_load = QtWidgets.QPushButton("Load")
        self.button_load.clicked.connect(self.load_file)
        self.check_autoname = QtWidgets.QCheckBox("Change only the file extension")
        self.check_autoname.setChecked(True)
        self.check_autoname.toggled.connect(self.update_enable_out_edit)
        self.dataset_list = ['x', 'y', 'z']
        label_dataset_list = {dataset: QtWidgets.QLabel(dataset) for dataset in self.dataset_list}
        self.last_dataset = {dataset: dataset for dataset in self.dataset_list}
        self.before_first_load = True
        self.combo_dataset_list = {dataset: QtWidgets.QComboBox() for dataset in self.dataset_list}
        self.button_preview = {dataset: QtWidgets.QPushButton("Preview") for dataset in self.dataset_list}

        self.radio_matrix = QtWidgets.QRadioButton()
        pixmap_matrix = QtGui.QPixmap("img/matrix_shape.png")
        self.radio_matrix.setIcon(QtGui.QIcon(pixmap_matrix))
        self.radio_matrix.setIconSize(pixmap_matrix.size())
        self.radio_xyz = QtWidgets.QRadioButton()
        pixmap_xyz = QtGui.QPixmap("img/xyz_shape.png")
        self.radio_xyz.setIcon(QtGui.QIcon(pixmap_xyz))
        self.radio_xyz.setIconSize(pixmap_xyz.size())

        if self.export_shape == 0:
            self.radio_matrix.setChecked(True)
        if self.export_shape == 1:
            self.radio_xyz.setChecked(True)
        self.radio_xyz.clicked.connect(lambda state: self.set_shape(1))
        self.radio_matrix.clicked.connect(lambda state: self.set_shape(0))

        self.combo_pick_snapshot = QtWidgets.QComboBox()

        self.edit_multitrack = QtWidgets.QSpinBox()
        self.edit_multitrack.setMaximum(9999)

        self.edit_pleview_path = QtWidgets.QLineEdit()
        self.button_pleview_path = QtWidgets.QPushButton("Browse")
        self.button_pleview_path.clicked.connect(self.browse_pleview_path)
        self.edit_easyplot_path = QtWidgets.QLineEdit()
        self.button_easyplot_path = QtWidgets.QPushButton("Browse")
        self.button_easyplot_path.clicked.connect(self.browse_easyplot_path)
        self.button_convert = QtWidgets.QPushButton("Convert")
        self.button_convert.clicked.connect(lambda: self.convert(pleview=False, easyplot=False, preview=False))
        self.button_convert_pleview = QtWidgets.QPushButton("Convert && Pleview")
        self.button_convert_pleview.clicked.connect(lambda: self.convert(pleview=True, easyplot=False, preview=False))
        self.button_convert_easyplot = QtWidgets.QPushButton("Convert && EasyPlot")
        self.button_convert_easyplot.clicked.connect(lambda: self.convert(pleview=False, easyplot=True, preview=False))
        self.button_pleview = QtWidgets.QPushButton("Pleview")
        self.button_pleview.clicked.connect(lambda: self.convert(pleview=True, easyplot=False, preview=True))
        self.button_easyplot = QtWidgets.QPushButton("EasyPlot")
        self.button_easyplot.clicked.connect(lambda: self.convert(pleview=False, easyplot=True, preview=True))
        self.edit_preview = QtWidgets.QTextEdit()

        layout_filename = QtWidgets.QGridLayout()
        layout_filename.setContentsMargins(0, 0, 0, 0)
        layout_filename.addWidget(self.edit_filename_in, 0, 0)
        layout_filename.addWidget(self.button_browse_in, 0, 1)
        layout_filename.addWidget(self.edit_filename_out, 3, 0)
        layout_filename.addWidget(self.button_browse_out, 3, 1)

        layout_dataset_choose = QtWidgets.QGridLayout()
        layout_dataset_choose.setContentsMargins(0, 0, 0, 0)
        layout_dataset_choose.setColumnStretch(1, 1)
        layout_dataset_choose.addWidget(QtWidgets.QLabel('Please select datasets x, y, z (some may be left "None"):'), 0, 0, 1, 4)
        label_output_shape = QtWidgets.QLabel('Please select the shape of the output data file:')
        label_output_shape.setAlignment(QtCore.Qt.AlignCenter)
        layout_dataset_choose.addWidget(label_output_shape, 0, 4, 1, 2)

        for i in range(len(self.dataset_list)):
            layout_dataset_choose.addWidget(label_dataset_list[self.dataset_list[i]], i+1, 0)
            layout_dataset_choose.addWidget(self.combo_dataset_list[self.dataset_list[i]], i+1, 1)
            layout_dataset_choose.addWidget(self.button_preview[self.dataset_list[i]], i+1, 3)
            if self.dataset_list[i] == 'y':
                self.combo_dataset_list[self.dataset_list[i]].currentIndexChanged.connect(
                    self.refresh_pick_snapshot_combo)
            self.combo_dataset_list[self.dataset_list[i]].setMaxVisibleItems(40)

            # below: checked_state catching boolean value from signal, so it wont overwrite it
            self.button_preview[self.dataset_list[i]].clicked.connect(lambda checked_state, i=i:
                self.edit_preview.setText(self.generate_preview(self.dataset_list[i])))
        layout_dataset_choose.addWidget(QtWidgets.QLabel("Pick single snapshot:"), 1, 2, 1, 1)
        layout_dataset_choose.addWidget(self.combo_pick_snapshot, 2, 2, 1, 1)
        layout_dataset_choose.addWidget(self.radio_matrix, 1, 4, len(self.dataset_list) - 1, 1)
        layout_dataset_choose.addWidget(self.radio_xyz, 1, 5, len(self.dataset_list) - 1, 1)
        layout_dataset_choose.addWidget(QtWidgets.QLabel("Multitrack row no. (0, 1, ...):"),
                                        len(self.dataset_list), 4, 1, 1)
        layout_dataset_choose.addWidget(self.edit_multitrack, len(self.dataset_list), 5, 1, 1)



        layout_convert = QtWidgets.QHBoxLayout()
        layout_convert.addStretch()
        layout_convert.addWidget(self.button_convert)
        layout_convert.addWidget(self.button_convert_pleview)
        layout_convert.addWidget(self.button_convert_easyplot)
        layout_convert.addWidget(self.button_pleview)
        layout_convert.addWidget(self.button_easyplot)
        layout_convert.addStretch()
        layout_convert.setContentsMargins(0, 0, 0, 0)

        layout_load = QtWidgets.QHBoxLayout()
        layout_load.addStretch()
        layout_load.addWidget(self.button_load)
        layout_load.addStretch()
        layout_load.setContentsMargins(0, 0, 0, 0)
        layout_filename.addLayout(layout_load, 1, 0, 1, 2)

        layout_check = QtWidgets.QHBoxLayout()
        layout_check.addWidget(QtWidgets.QLabel("Output file path:"))
        layout_check.addStretch(1)
        layout_check.addWidget(self.check_autoname)
        layout_check.setContentsMargins(0, 0, 0, 0)
        layout_filename.addLayout(layout_check, 2, 0, 1, 2)

        layout_pleview = QtWidgets.QHBoxLayout()
        layout_pleview.setContentsMargins(0, 0, 0, 0)
        layout_pleview.addWidget(QtWidgets.QLabel("Pleview path:"))
        layout_pleview.addWidget(self.edit_pleview_path)
        layout_pleview.addWidget(self.button_pleview_path)

        layout_easyplot = QtWidgets.QHBoxLayout()
        layout_easyplot.setContentsMargins(0, 0, 0, 0)
        layout_easyplot.addWidget(QtWidgets.QLabel("Easyplot path:"))
        layout_easyplot.addWidget(self.edit_easyplot_path)
        layout_easyplot.addWidget(self.button_easyplot_path)

        vlayout.addWidget(QtWidgets.QLabel("Input file path:"))
        vlayout.addLayout(layout_filename)
        vlayout.addWidget(HLine())
        vlayout.addLayout(layout_dataset_choose)
        vlayout.addWidget(HLine())
        vlayout.addLayout(layout_convert)
        vlayout.addWidget(self.edit_preview)
        vlayout.addLayout(layout_pleview)
        vlayout.addLayout(layout_easyplot)

        self.setLayout(vlayout)

        self.edit_filename_in.setText(self.last_directory)
        self.edit_filename_out.setText(self.last_directory)
        self.edit_pleview_path.setText(self.pleview_path)
        self.edit_pleview_path.editingFinished.connect(self.save_pleview_path)
        self.edit_easyplot_path.setText(self.easyplot_path)
        self.edit_easyplot_path.editingFinished.connect(self.save_easyplot_path)
        self.update_enable_out_edit()

    def dragEnterEvent(self, e):
        if e.mimeData().text()[:8] == "file:///":
            e.accept()

    def dropEvent(self, e):
        self.edit_filename_in.setText(e.mimeData().text()[8:])
        self.update_enable_out_edit()
        self.load_file()

    def error_message(self, message):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(message)
        msg.exec_()
        print(message)

    def browse_in(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, "Load input file",
                                                             self.last_directory, "HDF5 Files (*.h5)")
        filename = filename[0]
        new_dir = '/'.join(filename.split('/')[:-1]) + '/'
        if new_dir != "":
            self.last_directory = new_dir
            if filename != "":
                try:
                    with open("config\\hdf5_tool_last_dir.cfg", "w") as file:
                        file.write(jsonpickle.encode(self.last_directory))
                except Exception as e:
                    self.error_message("Error while saving new default directory: " + traceback.format_exc())

                self.edit_filename_in.setText(filename)
                self.load_file()
        self.update_enable_out_edit()

    def browse_out(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, "Choose output file",
                                                         self.last_directory, "DAT Files (*.dat)")
        filename = filename[0]
        new_dir = '/'.join(filename.split('/')[:-1]) + '/'
        if new_dir != "":
            self.last_directory = new_dir
            if filename != "":
                try:
                    with open("config\\hdf5_tool_last_dir.cfg", "w") as file:
                        file.write(jsonpickle.encode(self.last_directory))
                except Exception as e:
                    self.error_message("Error while saving new default directory:\n" + traceback.format_exc())

        self.edit_filename_out.setText(filename)

    def browse_pleview_path(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, "Choose pleview location",
                                                         self.last_directory, "EXE Files (*.exe)")[0]
        if filename != '':
            self.edit_pleview_path.setText(filename)
            self.save_pleview_path()

    def save_pleview_path(self):
        try:
            with open("config\\pleview_path.cfg", "w") as file:
                file.write(jsonpickle.encode(self.edit_pleview_path.text()))
        except Exception as e:
            self.error_message("Error while saving Pleview location:\n" + traceback.format_exc())

    def browse_easyplot_path(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, "Choose EasyPlot location",
                                                         self.last_directory, "EXE Files (*.exe)")[0]
        if filename != '':
            self.edit_easyplot_path.setText(filename)
            self.save_easyplot_path()

    def save_easyplot_path(self):
        try:
            with open("config\\easyplot_path.cfg", "w") as file:
                file.write(jsonpickle.encode(self.edit_easyplot_path.text()))
        except Exception as e:
            self.error_message("Error while saving easyplot location:\n" + traceback.format_exc())

    def update_enable_out_edit(self):
        self.button_browse_out.setEnabled(not self.check_autoname.isChecked())
        self.edit_filename_out.setEnabled(not self.check_autoname.isChecked())

        if self.check_autoname.isChecked() and self.edit_filename_in.text()[-3:] == ".h5":
            self.edit_filename_out.setText(self.edit_filename_in.text()[:-3] + ".dat")

    def set_shape(self, shape):
        self.export_shape = shape
        try:
            with open("config\\hdf5_tool_shape.cfg", "w") as file:
                file.write(jsonpickle.encode(shape))
        except Exception as e:
            self.error_message("Error while saving easyplot location:\n" + traceback.format_exc())

    def load_file(self):
        if not self.before_first_load:
            for dataset_name in self.dataset_list:
                self.last_dataset[dataset_name] = self.combo_dataset_list[dataset_name].currentText()
        if self.edit_filename_in.text() == "":
            return
        try:
            self.file = h5py.File(self.edit_filename_in.text(), 'r')
            data = []

            def visitor_func(name, node):
                if isinstance(node, h5py.Dataset):
                    data.append((name, node))

            self.file.visititems(visitor_func)

            data = sorted(data, key=(lambda d: d[0][:10] == "data_full/"))
            data = [(name if name[:10] != "data_full/" else name[10:], node) for name, node in data]

            self.datasets_names = [name for name, node in data]
            self.datasets = [node for name, node in data]

            for i in range(len(self.dataset_list)):
                self.combo_dataset_list[self.dataset_list[i]].clear()
                self.combo_dataset_list[self.dataset_list[i]].addItem("None", None)
                for dataset, name in zip(self.datasets, self.datasets_names):
                    self.combo_dataset_list[self.dataset_list[i]].addItem(name, dataset)

            for dataset_name in self.dataset_list:
                self.combo_dataset_list[dataset_name].setCurrentText(self.last_dataset[dataset_name])
            self.before_first_load = False
            self.refresh_pick_snapshot_combo()
        except Exception as e:
            self.error_message("Error while loading HDF5 file:\n" + traceback.format_exc())

    def refresh_pick_snapshot_combo(self):
        index = self.combo_pick_snapshot.currentIndex()
        self.combo_pick_snapshot.blockSignals(True)
        self.combo_pick_snapshot.clear()
        self.combo_pick_snapshot.addItem("ALL")

        if self.combo_dataset_list['y'].currentData() is not None:
            for i, dat in enumerate(self.combo_dataset_list['y'].currentData()):
                try:
                    s = str(list(dat))
                except TypeError:
                    s = str(dat)
                s = (s[:8] + '..') if len(s) > 10 else s

                self.combo_pick_snapshot.addItem(f"{i}: {s}")

            if self.combo_pick_snapshot.count() > index:
                self.combo_pick_snapshot.setCurrentIndex(index)
        self.combo_pick_snapshot.blockSignals(False)

    def generate_preview(self, axis):
        combo = self.combo_dataset_list[axis]
        try:
            if combo.currentText() == "script":
                script = combo.currentData()[0]
                if isinstance(script, bytes):
                    script = script.decode()
                return script.replace("\\n", "\n")
            else:
                if self.combo_pick_snapshot.currentIndex() == 0:
                    return str(np.array(combo.currentData()))
                else:
                    return str(np.array(combo.currentData()[self.combo_pick_snapshot.currentIndex() - 1]))
        except Exception:
            return traceback.format_exc()

    def convert(self, preview=False, easyplot=False, pleview=False):
        try:
            z = None
            if self.combo_dataset_list['z'].currentData() is not None:
                z = self.combo_dataset_list['z'].currentData()
                if self.combo_pick_snapshot.currentIndex() > 0:
                    z = z[self.combo_pick_snapshot.currentIndex() - 1]

            # if array id 3D, assume it is a multitrack
            if len(z.shape) == 3:
                z = z[:, self.edit_multitrack.value(), :]

            x = None
            if self.combo_dataset_list['x'].currentData() is not None:
                x = np.array(self.combo_dataset_list['x'].currentData())
                if self.combo_pick_snapshot.currentIndex() > 0:
                    x = x[self.combo_pick_snapshot.currentIndex() - 1]
                if len(x.shape) == 0:
                    x = None

            y = None
            if self.combo_dataset_list['y'].currentData() is not None:
                y = np.array(self.combo_dataset_list['y'].currentData())
                if len(y.shape) == 0:
                    y = None
                if self.combo_pick_snapshot.currentIndex() > 0:
                    if len(z.shape) > 1:
                        y = np.array(range(z.shape[0])) + 1
                    else:
                        y = None
            if preview:
                filename = tempfile.gettempdir() + '\\pylums_preview_' + str(time.time()) + '.dat'
            else:
                filename = self.edit_filename_out.text()

            if self.export_shape == 0:  # matrix format
                if z is None:
                    self.error_message("Please provide 'z' data.")
                    return

                if x is not None and y is not None and len(z.shape) == 1 and x.shape == y.shape == z.shape:
                    y1 = np.unique(y)
                    if len(z) % len(y1) != 0 or y[len(z) // len(y1)] == y[len(z) // len(y1) - 1]:
                        y = y1[:-1]
                        z = z[:len(y) * (len(z) // len(y))]
                    else:
                        y = y1
                    x = x[:len(z) // len(y)]
                    z = np.reshape(z, (len(y), len(x)))
                else:
                    if len(z.shape) == 1:
                        z = [z]
                    if x is not None and len(x.shape) == 2:
                        x = x[0]
                    if y is not None and len(y.shape) == 2:
                        y = y[0]
                        
                offset = 0
                if x is not None:
                    z = np.concatenate((x[np.newaxis, :], z), axis=0)
                    offset = 1

                txtfile = open(filename, 'wb')
                if y is not None:
                    if len(y.shape) != 1 or len(y) != z.shape[0] - offset:
                        print("Mismatch between the size of y (%d) and z (%d)" % (len(x), z.shape[0] - offset))
                        return
                    np.savetxt(txtfile, np.array([y]), fmt="%.9g")
                np.savetxt(txtfile, np.transpose(z), fmt="%.9g")
                txtfile.close()

            if self.export_shape == 1:  # xyz format
                datasets = [d for d in [x, y, z] if d is not None]
                if len(datasets) == 0:
                    print("No datasets provided!")
                    return
                lengths = [len(d) for d in datasets]
                if len(np.unique(lengths)) != 1:
                    print("Mismatch between the sizes of provided x, y, z datasets!")
                    return

                txtfile = open(filename, 'wb')
                np.savetxt(txtfile, np.transpose(np.array(datasets)), fmt="%.9g")
                txtfile.close()

            if pleview:
                path = self.edit_pleview_path.text()
                if os.path.isfile(path):
                    subprocess.call('start "" "%s" "%s"' % (path, filename), shell=True)
                else:
                    self.error_message("Please select correct path to Pleview app!")
            if easyplot:
                path = self.edit_easyplot_path.text()
                if os.path.isfile(path):
                    subprocess.call('start "" "%s" "%s"' % (path, filename), shell=True)
                else:
                    self.error_message("Please select correct path to EasyPlot app!")
            return True
        except Exception:
            self.error_message(traceback.format_exc())
