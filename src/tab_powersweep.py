
import math
import scipy
from PyQt5 import QtWidgets, QtGui, QtCore
import pyqtgraph as pg
import devices
#from devices.spectrum_tab import SpectrumTabWorker
import numpy as np
import re
import os
import jsonpickle
import time
import datetime
from numpy import arccos, arctan2, cosh, exp, sqrt, arccosh, arctanh, degrees, log2, radians, tan, arcsin, ceil, e, \
    tanh, arcsinh, log, sin, nan, arctan, cos, floor, log10, pi, sinh
from numpy import pi as Pi
from numpy import pi as PI
from scipy.special import gamma, erf, erfc
import traceback
from src.widgets import VerticalScrollArea
from src.measurement_tab import Measurement
from devices.virtual.power_tab import PowerTabWorker, PowerTabInterface

class SettingPowerError(Exception):
    pass

class NegativePowerMeasuredError(SettingPowerError):
    pass

class DeviceNotFoundError(Exception):
    pass

class PowerSweepMeasurement(Measurement):
    def setup_additional_widgets(self, canvas_widget):
        self.device_list = devices.active_devices
        self.axis = None
        self.powermeter = None
        self.spectrometer = None
        self.measurement_type = "default"
        self.running = False
        self.target_power_remote = None
        self.tolerance_remote = None
        self.worker = None

        self.canvas_widget = canvas_widget
        #self.canvas_widget.setVerticalSizePolicy(QtWidgets.QSizePolicy.Minimum)
        # TODO ogarnąć żeby był najmniejszy w pionie
        hlayout = QtWidgets.QHBoxLayout()

        vlayout1 = QtWidgets.QVBoxLayout()
        vlayout1.setContentsMargins(0, 0, 0, 0)
        hlayout.addLayout(vlayout1)

        measurement_group = QtWidgets.QGroupBox("Measurement settings")
        measurement_layout = QtWidgets.QGridLayout()
        measurement_group.setLayout(measurement_layout)
        vlayout1.addWidget(measurement_group)
        vlayout1.addStretch(1)

        vlayout2 = QtWidgets.QVBoxLayout()
        vlayout2.setContentsMargins(0, 0, 0, 0)
        hlayout.addLayout(vlayout2)

        control_group = QtWidgets.QGroupBox("Direct control")
        control_layout = QtWidgets.QGridLayout()
        control_group.setLayout(control_layout)
        vlayout2.addWidget(control_group)

        hardware_group = QtWidgets.QGroupBox("Hardware setup")
        hardware_layout = QtWidgets.QGridLayout()
        hardware_group.setLayout(hardware_layout)
        vlayout2.addWidget(hardware_group)
        vlayout2.addStretch(1)

        vlayout3 = QtWidgets.QVBoxLayout()
        vlayout3.setContentsMargins(0, 0, 0, 0)
        hlayout.addLayout(vlayout3)

        calibration_group = QtWidgets.QGroupBox("Rotational filter calibration")
        calibration_layout = QtWidgets.QGridLayout()
        calibration_group.setLayout(calibration_layout)
        vlayout3.addWidget(calibration_group)

        worker_group = QtWidgets.QGroupBox("Virtual Device Worker setup")
        worker_layout = QtWidgets.QGridLayout()
        worker_group.setLayout(worker_layout)
        vlayout3.addWidget(worker_group)
        vlayout3.addStretch(1)

        self.edit_from = QtWidgets.QLineEdit()
        self.edit_to = QtWidgets.QLineEdit()
        self.edit_points_per_decade = QtWidgets.QLineEdit()
        self.combo_rounding = QtWidgets.QComboBox()
        self.combo_rounding.addItem("No rounding")
        self.combo_rounding.addItem("1 digit")
        self.combo_rounding.addItem("1.5 digits")
        self.combo_rounding.addItem("2 digits")
        self.edit_accumulations_power_threshold = QtWidgets.QLineEdit()
        self.edit_max_accumulations = QtWidgets.QLineEdit()
        self.button_print_points = QtWidgets.QPushButton("Show generated points")
        self.button_print_points.clicked.connect(self.start_print_points)
        self.check_divide_accumulations = QtWidgets.QCheckBox("Divide data by number of accumulations")
        measurement_layout.addWidget(QtWidgets.QLabel("Power from (μW):"), 0, 0)
        measurement_layout.addWidget(self.edit_from, 0, 1)
        measurement_layout.addWidget(QtWidgets.QLabel("Power to (μW):"), 1, 0)
        measurement_layout.addWidget(self.edit_to, 1, 1)
        measurement_layout.addWidget(QtWidgets.QLabel("Points per decade:"), 2, 0)
        measurement_layout.addWidget(self.edit_points_per_decade, 2, 1)
        measurement_layout.addWidget(QtWidgets.QLabel("Target power rounding:"), 3, 0)
        measurement_layout.addWidget(self.combo_rounding, 3, 1)
        measurement_layout.addWidget(QtWidgets.QLabel("Multiple accumulations\nif below power (μW):"), 4, 0)
        measurement_layout.addWidget(self.edit_accumulations_power_threshold, 4, 1)
        measurement_layout.addWidget(QtWidgets.QLabel("Max no. of accumulations:"), 5, 0)
        measurement_layout.addWidget(self.edit_max_accumulations, 5, 1)
        measurement_layout.addWidget(self.button_print_points, 6, 0, 1, 2)
        measurement_layout.addWidget(self.check_divide_accumulations, 7, 0, 1, 2)
        measurement_layout.setRowStretch(8, 1)

        self.edit_spectrometer = QtWidgets.QLineEdit("spectrometer")
        self.edit_driver = QtWidgets.QLineEdit()
        self.edit_driver_axis = QtWidgets.QLineEdit()
        self.edit_powermeter = QtWidgets.QLineEdit("powermeter")
        hardware_layout.addWidget(QtWidgets.QLabel("Spectrometer id:"), 0, 0)
        hardware_layout.addWidget(self.edit_spectrometer, 0, 1)
        hardware_layout.addWidget(QtWidgets.QLabel("Driver id:"), 1, 0)
        hardware_layout.addWidget(self.edit_driver, 1, 1)
        hardware_layout.addWidget(QtWidgets.QLabel("Driver axis number:"), 2, 0)
        hardware_layout.addWidget(self.edit_driver_axis, 2, 1)
        hardware_layout.addWidget(QtWidgets.QLabel("Powermeter id:"), 3, 0)
        hardware_layout.addWidget(self.edit_powermeter, 3, 1)
        hardware_layout.setRowStretch(4, 1)

        self.button_calibration = QtWidgets.QPushButton("Perform calibration")
        self.button_calibration.clicked.connect(self.start_calibration)
        self.edit_calibration_steps = QtWidgets.QLineEdit("36")
        self.edit_calibration_steps.setEnabled(False)
        self.edit_characteristic_angle = QtWidgets.QLineEdit()
        self.edit_characteristic_angle.setEnabled(False)
        self.edit_calibration_powers = QtWidgets.QLineEdit()
        self.edit_calibration_powers.setEnabled(False)
        self.check_reversed = QtWidgets.QCheckBox("Reversed")
        self.check_reversed.setEnabled(False)
        calibration_layout.addWidget(self.button_calibration, 0, 0, 1, 2)
        calibration_layout.addWidget(QtWidgets.QLabel("Steps:"), 1, 0)
        calibration_layout.addWidget(self.edit_calibration_steps, 1, 1)
        calibration_layout.addWidget(QtWidgets.QLabel("Slope:"), 2, 0)
        calibration_layout.addWidget(self.edit_characteristic_angle, 2, 1)
        calibration_layout.addWidget(QtWidgets.QLabel("Powers:"), 3, 0)
        calibration_layout.addWidget(self.edit_calibration_powers, 3, 1)
        calibration_layout.addWidget(self.check_reversed, 4, 0, 1, 2)

        self.edit_set_power = QtWidgets.QLineEdit("100")
        self.edit_tolerance = QtWidgets.QLineEdit("1")
        self.button_set_power = QtWidgets.QPushButton("Set Power")
        self.button_set_power.clicked.connect(self.start_set_power)
        self.edit_set_power.returnPressed.connect(self.start_set_power)
        control_layout.addWidget(QtWidgets.QLabel("Target Power (μW):"), 0, 0)
        control_layout.addWidget(self.edit_set_power, 0, 1)
        control_layout.addWidget(QtWidgets.QLabel("Tolerance (%):"), 1, 0)
        control_layout.addWidget(self.edit_tolerance, 1, 1)
        control_layout.addWidget(self.button_set_power, 2, 0, 1, 2)
        control_layout.addWidget(self.button_set_power, 2, 0, 1, 2)

        self.edit_req_port = QtWidgets.QLineEdit("11010")
        self.button_start_worker = QtWidgets.QPushButton("Create Worker")
        worker_group.setToolTip("Create PowerTabInterface object for remote operation.\n"
                                "F. eg.:\n"
                                "from devices.virtual.power_tab import PowerTabInterface\n"
                                "power_tab = PowerTabInterface(req_port=11010)")
        self.button_start_worker.clicked.connect(self.toggle_worker)
        worker_layout.addWidget(QtWidgets.QLabel("req_port:"), 0, 0)
        worker_layout.addWidget(self.edit_req_port, 0, 1)
        worker_layout.addWidget(self.button_start_worker, 1, 0, 1, 3)
        worker_layout.setRowStretch(2, 1)

        canvas_widget.setLayout(hlayout)
        canvas_widget.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        #canvas_widget.parent().layout().setStretch(2, 1)

    def toggle_worker(self):
        if self.worker is None:
            try:
                self.worker = PowerTabWorker(parent=self, req_port=int(self.edit_req_port.text()), pub_port=0)
                self.worker.init_device()
                self.worker.start_server()
                self.button_start_worker.setText("Disable Worker")
                #self.button_start_worker.setEnabled(False) # TODO find out how to properly delete worker
            except:
                PowerTabInterface(req_port=int(self.edit_req_port.text()))._kill_worker()
                self.worker = None
                print("Worker could not be started.\n" + str(traceback.format_exc()))
        else:
            PowerTabInterface(req_port=int(self.edit_req_port.text()))._kill_worker()
            #self.worker = None
            self.button_start_worker.setText("Create Worker")

    def on_start(self):
        self.canvas_widget.setEnabled(False)
        self.running = True

    def on_stop(self):
        self.axis = None
        self.spectrometer = None
        self.powermeter = None
        self.measurement_type = "default"
        self.automatic_file = True
        self.canvas_widget.setEnabled(True)
        self.running = False

    def is_stopped(self):
        return not self.running

    def get_axis(self):
        name = self.edit_driver.text()
        try:
            device = self.device_list[name]
        except KeyError:
            raise DeviceNotFoundError('Motor driver with name "%s" is not on the list of active devices!' % name)
        axis = self.edit_driver_axis.text()
        self.axis = device.get_axis(axis)

    def get_powermeter(self):
        name = self.edit_powermeter.text()
        try:
            self.powermeter = self.device_list[name]
        except KeyError:
            raise DeviceNotFoundError('Powermeter with name "%s" is not on the list of active devices!' % name)

    def get_spectrometer(self):
        name = self.edit_spectrometer.text()
        try:
            self.spectrometer = self.device_list[name]
        except KeyError:
            raise DeviceNotFoundError('Spectrometer with name "%s" is not on the list of active devices!' % name)

    def set_position(self, pos):
        act = self.axis.get_position()
        if (pos - act) % 360.0 < 180:
            self.axis.move_absolute(act + (pos - act) % 360.0)
        else:
            self.axis.move_absolute(act - (act - pos) % 360.0)
        while not self.axis.is_stopped():
            self.sleep_checking_cancel(0.1)

    def get_position(self):
        return self.axis.get_position() % 360.0

    def get_power(self, ignore_error_negative=False):
        if self.powermeter is None:
            self.get_powermeter()
        is_ok = False
        time.sleep(0.25)
        while not is_ok:
            p = np.float(self.powermeter.get_power())
            if not np.isnan(p):
                is_ok = True
        if p <= 0 and not ignore_error_negative:
            raise NegativePowerMeasuredError("%s uW" % (p * 10 ** 6,))
        #self.log("## %f" % (p * 10**6))
        return p

    def start_print_points(self):
        self.measurement_type = "print_points"
        self.automatic_file = False
        self.trigger_start.emit()
        return

    def start_set_power(self):
        self.measurement_type = "set_power"
        self.automatic_file = False
        self.trigger_start.emit()
        return

    def start_set_power_remote(self, power, tolerance):
        self.measurement_type = "set_power_remote"
        self.automatic_file = False
        self.target_power_remote = power
        self.tolerance_remote = tolerance
        self.trigger_start.emit()
        return

    def start_calibration(self):
        n, okPressed = QtWidgets.QInputDialog.getInt(self.button_calibration, "Steps", "Number of steps:", 36)
        if not okPressed:
            return
        self.edit_calibration_steps.setText(str(n))
        self.measurement_type = "autotune"
        self.automatic_file = False
        self.trigger_start.emit()

    def run(self):
        try:
            if self.measurement_type == "autotune":
                self.get_powermeter()
                self.get_axis()
                self.procedure_calibrate()
            elif self.measurement_type == "set_power":
                self.get_powermeter()
                self.get_axis()
                self.procedure_set_power()
            elif self.measurement_type == "set_power_remote":
                self.get_powermeter()
                self.get_axis()
                self.procedure_set_power(self.target_power_remote, self.tolerance_remote)
            elif self.measurement_type == "print_points":
                self.procedure_print_points()
            elif self.measurement_type == "default":
                self.get_powermeter()
                self.get_axis()
                self.get_spectrometer()
                self.procedure_default()
        except DeviceNotFoundError as e:
            self.log(str(e))

    def get_points(self):
        begin = float(self.edit_from.text()) / 10**6
        end = float(self.edit_to.text()) / 10**6
        per_decade = int(self.edit_points_per_decade.text())
        rounding_select = self.combo_rounding.currentIndex()
        max_accumulations = int(self.edit_max_accumulations.text())
        edit_power_number_constant = float(self.edit_accumulations_power_threshold.text()) / 10 ** 6

        def round_to_first(number, multiplier):
            div = 10 ** int(math.floor(math.log10(number))) / multiplier
            f = div * floor(number / div)
            c = div * ceil(number / div)
            if number / f < c / number:
                return f
            else:
                return c

        if rounding_select == 1 :
            my_round = lambda number: round_to_first(number, 1)
        elif rounding_select == 2:
            my_round = lambda number: round_to_first(number, 2)
        elif rounding_select == 3 :
            my_round = lambda number: round_to_first(number, 10)
        else:
            my_round = lambda number: number

        low_exp = floor(math.log10(begin))
        high_exp = ceil(math.log10(end))
        candidates = np.linspace(low_exp, high_exp, round((high_exp - low_exp) * per_decade) + 1)
        candidates = [my_round(c) for c in 10**candidates]
        candidates = [c for c in candidates if c >= my_round(begin) and c <= my_round(end)]
        len_prev = len(candidates)
        candidates = np.unique(candidates)
        if len_prev != len(candidates):
            self.log("Warning! Some points were duplicated due to power rounding!")
        accumulations = [max(1, min(int(ceil(edit_power_number_constant / c)), max_accumulations)) for c in candidates]
        return [(p, a) for p, a in zip(candidates, accumulations)]

    def procedure_print_points(self):
        try:
            points = self.get_points()
            self.log("\n".join(["%i accumulation%s, %s μW" %
                                (i, "" if i == 1 else "s", ('%f' % (p * 10**6)).rstrip('0').rstrip('.')) for p, i in points]))
            total_accumulations = np.sum([a for _, a in points])
            self.log(f"Measurement will consist of {total_accumulations} accumulations in total.")
        except:
            raise

    def procedure_set_power(self, target=None, tolerance_perc=None):
        try:
            # read required parameters from edit boxes
            if target is None:
                target = float(self.edit_set_power.text()) / 10 ** 6
            if tolerance_perc is None:
                tolerance_perc = float(self.edit_tolerance.text()) / 100
            try:
                steps = int(self.edit_calibration_steps.text())
                extinction = float(self.edit_characteristic_angle.text())
                is_reversed = self.check_reversed.checkState()
                calibration_powers = np.array([float(s) for s in self.edit_calibration_powers.text().split(" ")])
            except ValueError:
                self.log("Invalid calibration data, please retry autocalibration procedure!")
                return
            self.log(f"Power will be set to {target * 10 ** 6} μW...")

            # we will start looking for initial guess (and the position where we will go to if something goes wrong)
            # finds segment between calibration points, for which its ends are closest to target.
            # target will be between its edges, if not (f. eg. end of filter) it will take better edge as init guess
            closest_segment = np.argmin((calibration_powers - target) ** 2 + \
                                        (np.roll(calibration_powers, -1) - target) ** 2)
            # index of the end of the segment. Can be outside of the range [0, steps)! Take modulo steps when needed
            closest_segment_end = closest_segment + 1
            # checks slope, remembers where is the lower and higher power end of segment
            if calibration_powers[closest_segment] < calibration_powers[closest_segment_end % steps]:
                lower_index = closest_segment
                higher_index = closest_segment_end
            else:
                higher_index = closest_segment
                lower_index = closest_segment_end
            # notes down expected low and high power
            lower = calibration_powers[lower_index % steps]
            higher = calibration_powers[higher_index % steps]

            # first two cases: if unfortunately target power is not inside found segment
            # can happen when target is too high, too low or there was a power reading spike in read characteristics
            if target < lower:
                base_position = (360.0 / steps * lower_index) % 360.0
            elif target > higher:
                base_position = (360.0 / steps * higher_index) % 360.0
            # default case, will interpolate
            else:
                base_position = (360.0 / steps * (
                            higher_index * (np.log(target) - np.log(lower)) / (np.log(higher) - np.log(lower)) + \
                            lower_index * (np.log(higher) - np.log(target)) / (np.log(higher) - np.log(lower)))) % 360.0

            iteration = 1
            pos = base_position
            self.set_position(base_position)
            self.sleep_checking_cancel(0.75)
            p = self.get_power()
            init_value = p
            last_value = p
            max_deg_step = 15
            min_deg_step = 0.05
            is_not_ok = True
            n = 0
            set_time = datetime.datetime.now()
            while is_not_ok:
                # if we are not inside desired tolerance:
                if abs(p - target) >= target * tolerance_perc:
                    is_not_ok = True
                    # estimate how fr do we need to move assuming ideal logarythmic filter
                    deg_step = abs(extinction * np.log(target / p))
                    # trim it to reasonable range
                    if deg_step < min_deg_step:
                        deg_step = min_deg_step
                    if deg_step > max_deg_step:
                        deg_step = max_deg_step
                    # find the direction where we need to move
                    if p - target > 0:
                        deg_step = -deg_step
                    if is_reversed:
                        deg_step = -deg_step
                    # failsafe: if the filter is not ideal and it is more than twice too steep at some range
                    # we should decrease steps because finding will not converge
                    if iteration > 4:
                        deg_step /= 2
                    self.set_position(pos + deg_step)
                    self.sleep_checking_cancel(0.7)
                    #self.log(str((extinction, target, p, deg_step, pos)))
                    p = self.get_power()
                    pos = self.get_position()
                else:
                    if n < 1:
                        is_not_ok = True
                        n += 1
                        self.sleep_checking_cancel(0.3)
                    else:
                        is_not_ok = False

                cur_time = datetime.datetime.now()
                difference = cur_time - set_time
                wait_time = difference.seconds
                # timeout or sudden change (end of range on filter in either direction) or negagtive p
                if wait_time > 40 or \
                        target < init_value and last_value / p < 0.1 or \
                        target > init_value and p / last_value < 0.1:
                    raise SettingPowerError()
                last_value = p
                iteration += 1
        except SettingPowerError as e:
            self.log("Can't set desired power!!!")
            if isinstance(e, NegativePowerMeasuredError):
                self.log("Negative laser power measured: %s" % str(e))
            self.set_position(base_position)
            self.sleep_checking_cancel(0.5)
        except:
            raise
        try:
            self.log(f"Power is set to {round(self.get_power() * 10 ** 6, 4)} μW.")
        except NegativePowerMeasuredError:
            pass

    def procedure_calibrate(self):
        try:
            self.log("Starting filter autocalibration..")
            steps = int(self.edit_calibration_steps.text())
            self.log(f"Will measure {steps} positions.")
            positions = np.array([i / steps * 360 for i in range(steps)])
            powers = []
            for p in positions:
                self.measurement_progress.emit(p / 360.0)
                self.set_position(p)
                self.sleep_checking_cancel(0.7)
                powers.append(self.get_power())
            self.powers = np.array(powers)
            powers_smooth = (powers + np.roll(powers, 1) + np.roll(powers, -1)) / 3
            median_slope = np.median(powers_smooth / np.roll(powers_smooth, 1))
            if median_slope < 1:
                self.check_reversed.setChecked(True)
                median_slope = 1 / median_slope
            else:
                self.check_reversed.setChecked(False)
            self.edit_characteristic_angle.setText(str(round(360 / steps / np.log(median_slope), 6)))
            self.edit_calibration_powers.setText(" ".join([str(v) for v in powers]))
            self.log("Finished succesfully.")
        except:
            raise

    def procedure_default(self):
        points = self.get_points()
        divide_accumulations = self.check_divide_accumulations.checkState()
        total_accumulations = np.sum([a for _,a in points])
        self.log("Will measure %d powers in %d accumulations." % (len(points), total_accumulations))
        i = 0
        for power, accumulations in points:
            self.procedure_set_power(power)
            self.log("Accumulating...")
            #self.measurement_file.start_snapshot()
            y = 0
            powers = []
            for j in range(accumulations):
                x, yj = self.spectrometer.take_single_spectrum()
                y += yj / (accumulations if divide_accumulations else 1)
                i += 1
                self.measurement_progress.emit(i / total_accumulations)
                powers.append(self.get_power(ignore_error_negative=True))
                self.check_if_cancel_requested()
            real_power = np.mean(powers)
            self.log(f"Done. Mean ower measured after accumulations is equal to {real_power * 10**6} μW")
            self.measurement_file.save_snapshot(x=x, y=real_power, z=y, accumulations=accumulations,
                                                  real_power=real_power, target_power=power, real_power_log10=np.log10(real_power))
        self.procedure_set_power(points[len(points) // 2][0], 0.1)
