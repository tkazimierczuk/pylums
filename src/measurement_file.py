# -*- coding: utf-8 -*-

import h5py
import devices
from numpy import ndarray,array
import numpy as np
import time
import traceback

##### map_parallel heavily based on https://stackoverflow.com/questions/2562757
import threading
import logging,sys
def start_map_parallel(f, iter, max_parallel = 4):
    """Just like map(f, iter) but each is done in a separate thread."""
    # Put all of the items in the queue, keep track of order.
    from queue import Queue, Empty
    total_items = 0
    queue = Queue()
    for i, arg in enumerate(iter):
        queue.put((i, arg))
        total_items += 1
    # No point in creating more thread objects than necessary.
    if max_parallel > total_items:
        max_parallel = total_items

    # The worker thread.
    res = {}
    errors = {}
    class Worker(threading.Thread):
        def run(self):
            while not errors:
                try:
                    num, arg = queue.get(block = False)
                    try:
                        res[num] = f(arg)
                    except ConnectionError:
                        pass
                    except Exception as e:
                        errors[num] = sys.exc_info()
                except Empty:
                    break

    # Create the threads.
    threads = [Worker() for _ in range(max_parallel)]
    return (threads, errors, total_items, res)
    
def finish_map_parallel(data):
    threads, errors, total_items, res = data
    # Start the threads.
    [t.start() for t in threads]
    # Wait for the threads to finish.
    [t.join() for t in threads]

    if errors:
        if len(errors) > 1:
            logging.warning("map_parallel multiple errors: %d:\n%s"%(
                len(errors), errors))
        # Just raise the first one.
        item_i = min(errors.keys())
        type, value, tb = errors[item_i]
        # Print the original traceback
        logging.info("map_parallel exception on item %s/%s:\n%s"%(
            item_i, total_items, "\n".join(traceback.format_tb(tb))))
        raise value
    return [res[i] for i in range(total_items) if i in res]
#######

def map_parallel(f, iter, max_parallel = 4):
    data = start_map_parallel(f, iter, max_parallel)
    return finish_map_parallel(data)




class MeasurementFile:
    def __init__(self, filename, append=True, x_data=None, y_data=None, z_data=None, cached_only=False):
        """{x,y,z}_data indicates how the data is optional indication of the 
        *main* data. There are two possibilities:
            - it can be a function which will be called at each data point
            - it can be a list of strings to specify a data obtained anyway 
            by querying devices for status, e.g., ['apt','apt_60242834','position']
            """
        self.n = 0
        open_mode = 'a' if append else 'w'
        if not (filename.endswith('.h5') or filename.endswith('.hdf5')):
            filename += ".h5"
        self.file = h5py.File(filename, open_mode)
        
        self.filename = filename
        self.cached_only = cached_only
        
        self.main_data = {}
        for axis,source in (('x',x_data), ('y',y_data), ('z',z_data)):
            if not source:
                continue
            if callable(source):
                self.main_data[axis] = source
            else:
                try:
                    path = '/data_full/' + '/'.join(source)
                    self.file[axis] = h5py.SoftLink(path)
                except:
                    print("Error - data link not recognized in MeasurementFile class ")
        self.excluded_devices = set()
    
    def __del__(self):
        self.close()
            
    def flush(self):
        self.file.flush()
        self.save_txt()
        
    def save_txt(self):
        """ Try to write ASCII file with the main data """
        print("Attempting to export txt file")
        if 'z' not in self.file:
            print("No Z data in the file")
            return
        z = array(self.file['z'])
        
        x = None
        if 'x' in self.file:
            x = array(self.file['x'])[0]
            if len(x.shape)!=1 or len(x) != z.shape[1]:
                print("Mismatch between the size of x (%d) and z (%d)" %(len(x), z.shape[1]))
                return
                
        y = None
        if 'y' in self.file:
            y = array(self.file['y'])
        
        filename = self.filename + ".dat"
        self._export_txt(filename, x, y, z)
        
    
    def _export_txt(self, filename, x=None, y=None, z=None):
        """ Export provided data to ASCII file """
        if z is None:
            return

        if len(z.shape) != 2:
            print("Z data is not 2D array")
            return
        
        offset=0
        if x is not None:
            z = np.concatenate((x[np.newaxis,:],z),axis=0)
            offset=1

        txtfile = open(filename, 'wb')
        if y is not None:
            if len(y.shape)!=1 or len(y) != z.shape[0]-offset:
                print("Mismatch between the size of y (%d) and z (%d)" %(len(x), z.shape[0]-offset))
                return
            np.savetxt(txtfile, np.array([y]), fmt="%.9g")
        np.savetxt(txtfile, np.transpose(z), fmt="%.9g")
        txtfile.close()
        
    
    def close(self):
        self.save_txt()
        self.file.close()
        
    def start_snapshot(self):
        """ Start saving snapshot of all devices in background """
        self.n += 1
        for key,func in self.main_data.items():
            self._save_data(self.file, key, func())        
                
        self._devices_to_measure = list(filter(lambda x: x[0] not in self.excluded_devices, devices.active_devices.items()))

        if self.cached_only:
            self._map_data = start_map_parallel(lambda desc: (desc[0],desc[1].status_cached(ask_if_not_available=False)), self._devices_to_measure)
        else:
            self._map_data = start_map_parallel(lambda desc: (desc[0],desc[1].status_cached(timeout=1)), self._devices_to_measure)
        
    def finish_snapshot(self, **user_data):
        for key,data in user_data.items():
            self._save_data(self.file, key, data)
        group = self.file.require_group('data_full')
            
        data = finish_map_parallel(self._map_data)
        failed_devices = set([desc[0] for desc in self._devices_to_measure]) - set([res[0] for res in data])
        self.excluded_devices |= failed_devices
        if len(failed_devices) > 0:
            print("WARNING! Following devices will not save any more: ", failed_devices)
        
        for name,status in data:
            try:
                self._save_subgroup(group, name, status)
            except:
                print("Error saving device %s" % name)
        del(data)
        
        current_time = time.time()
        if 'elapsed_time' not in self.file:
            time_dataset = self.file.create_dataset('elapsed_time', shape=(1,), maxshape=(None,), dtype=float)
            time_dataset.attrs['initial_timestamp'] = current_time
            time_dataset.attrs['initial_time_string'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(current_time))
        else:
            time_dataset  = self.file['elapsed_time']
            time_dataset.resize( (self.n,) )
            time_dataset[-1] = current_time - time_dataset.attrs['initial_timestamp']
        
        self.file.flush()
        
    def save_snapshot(self, **user_data):
        """ We save the state of all active devices """
        self.start_snapshot()
        self.finish_snapshot(**user_data)
        
    def _save_subgroup(self, parent, name, data):
        """ parent - the parent subgroup of HDF file
            name - id (string) of the current group
            data - dictionary of the data with string as keys"""
        group = parent.require_group(name)
        for key,value in data.items():
            self._save_data(group, key, value)
                
                
    def _save_data(self, group, name, value):
        if type(value) == list:
            try:
                value=array(value)
            except:
                pass
        #now we start actual saving:
        if type(value) == dict:
            self._save_subgroup(group, name, value)
        elif type(value) in (int, float, bool, np.float, np.float16, np.float32, 
                             np.float64, np.int, np.int8, np.int16, np.int32, 
                             np.int64, np.uint, np.uint8, np.uint16, np.uint32, 
                             np.uint64, np.ndarray):
            value = array(value)
            newshape = (self.n,) + value.shape
            if name not in group:
                compr_kwargs = {}
                if value.size > 100:
                    compr_kwargs['compression'] = 'gzip'
                dset = group.create_dataset(name, shape=newshape, maxshape=(None,)+value.shape, dtype=value.dtype, **compr_kwargs)
            else:
                dset = group[name]
                dset.resize( newshape )
            dset[-1] = value
