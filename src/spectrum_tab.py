# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 18:23:21 2018

@author: tkaz
"""
import math
import scipy
from PyQt5 import QtWidgets, QtGui, QtCore
import pyqtgraph as pg
import devices
from devices.laser.opo import OPO
from devices.spectro.spectrometer import Spectrometer
from devices.virtual.spectrum_tab import SpectrumTabWorker
import numpy as np
import re
import os
import jsonpickle
import time
import traceback
from src.measurement_file import MeasurementFile
from queue import Queue
from src.widgets import VerticalScrollArea
from numpy import arccos, arctan2, cosh, exp, sqrt, arccosh, arctanh, degrees, log2, radians, tan, arcsin, ceil, e
from numpy import tanh, arcsinh, log, sin, nan, arctan, cos, floor, log10, pi, sinh
from numpy import pi as Pi
from numpy import pi as PI
from scipy.special import gamma, erf, erfc
import threading

class TextEdit(QtGui.QTextEdit):
    """
    A TextEdit editor that sends editingFinished events
    when the text was changed and focus is lost.
    """

    editingFinished = QtCore.pyqtSignal()
    receivedFocus = QtCore.pyqtSignal()

    def __init__(self, parent):
        super(TextEdit, self).__init__(parent)
        self._changed = False
        self.setTabChangesFocus(True)
        self.textChanged.connect(self._handle_text_changed)

    def focusInEvent(self, event):
        super(TextEdit, self).focusInEvent(event)
        self.receivedFocus.emit()

    def focusOutEvent(self, event):
        if self._changed:
            self.editingFinished.emit()
        super(TextEdit, self).focusOutEvent(event)

    def _handle_text_changed(self):
        self._changed = True

    def setTextChanged(self, state=True):
        self._changed = state

    def setHtml(self, html):
        QtGui.QTextEdit.setHtml(self, html)
        self._changed = False

class Variable(QtCore.QObject):
    def __init__(self, name="", parent=None):
        super().__init__(parent)
        self.label_name = QtWidgets.QLabel(name, parent=parent)
        self.edit_init = QtWidgets.QLineEdit(parent=parent)
        self.edit_init.setText('1')
        self.check_fix = QtWidgets.QCheckBox("Fix", parent=parent)
        self.edit_best = QtWidgets.QLineEdit(parent=parent)
        self.check_show = QtWidgets.QCheckBox("Show", parent=parent)
        self.check_show.setChecked(True)
        self.parent = parent
        self.edit_init.editingFinished.connect(lambda: self.parent.fit(event="change_model"))
        self.edit_init.editingFinished.connect(self.parent.save_settings)

    def visible(self, vis=True):
        self.label_name.setVisible(vis)
        self.edit_init.setVisible(vis)
        self.check_fix.setVisible(vis)
        self.edit_best.setVisible(vis)
        self.edit_init.setFixedWidth(100)
        self.edit_best.setFixedWidth(100)
        self.check_show.setVisible(vis)


class CustomVariable():
    def __init__(self, name=""):
        self.edit_definition = QtWidgets.QLineEdit()
        self.edit_result = QtWidgets.QLineEdit()
        self.check_show = QtWidgets.QCheckBox("Show")
        self.check_show.setChecked(True)

class SpectrumTab(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.fitter_enabled = False
        self.fitting_panel_active = False
        self.variables_all = {}
        self.variables_active = []
        self.fit_data_last = []
        self.ydata_unprocessed = None
        self.data_bg = None
        self.data_ref = None
        self.files = []
        self.indexes = {}
        self.custom_variables_gui_count = 0
        try:
            with open("config\\spectrum_tab_last_dir.cfg", "r") as file:
                self.last_directory = jsonpickle.decode(file.read())
        except Exception as e:
            self.last_directory = os.getcwd()

        self.setup_ui()
        self.check_devices()
        self.update_custom_variables_gui(save_after=False)
        self.worker = None
        self.load_settings("!UNSAVED! rename me to save changes")

    def setup_plot(self):
        pass

    def setup_ui(self):

        self.radio_spectrum = QtWidgets.QRadioButton()
        pixmap_spectrum = QtGui.QPixmap("img/spectrum.png")
        self.radio_spectrum.setIcon(QtGui.QIcon(pixmap_spectrum))
        self.radio_spectrum.setIconSize(pixmap_spectrum.size())
        self.radio_spectrum.setChecked(True)
        self.radio_spectrum.clicked.connect(lambda state: self.set_image_view(0, 0))

        self.radio_spectrum_fit = QtWidgets.QRadioButton()
        pixmap_spectrum_fit = QtGui.QPixmap("img/spectrum_fit.png")
        self.radio_spectrum_fit.setIcon(QtGui.QIcon(pixmap_spectrum_fit))
        self.radio_spectrum_fit.setIconSize(pixmap_spectrum_fit.size())
        self.radio_spectrum_fit.clicked.connect(lambda state: self.set_image_view(0, 1))

        self.radio_image = QtWidgets.QRadioButton()
        pixmap_image = QtGui.QPixmap("img/image.png")
        self.radio_image.setIcon(QtGui.QIcon(pixmap_image))
        self.radio_image.setIconSize(pixmap_image.size())
        self.radio_image.clicked.connect(lambda state: self.set_image_view(1, 0))

        self.label_track = QtWidgets.QLabel("Track no.:")
        self.spin_track = QtWidgets.QSpinBox()
        self.spin_track.setToolTip("In case of multiple row spectra, selects spectrum number")
        self.spin_track.setMinimum(1)
        self.spin_track.setMaximum(9999)
        self.spin_track.setMinimumWidth(47)
        self.spin_track.valueChanged.connect(lambda: self.fit(event="change_spectrum"))
        self.edit_binning = QtWidgets.QLineEdit("1")
        self.check_average = QtWidgets.QCheckBox("Avg. over bins")
        self.check_average.setToolTip("Average values inside bin, instead of summing them")
        self.check_average.stateChanged.connect(self.save_settings_tab)
        self.edit_binning.editingFinished.connect(self.save_settings_tab)

        self.button_take_bg = QtWidgets.QPushButton("Take background")
        self.button_take_bg.clicked.connect(self.take_bg)
        self.button_take_ref = QtWidgets.QPushButton("Take reference")
        self.button_take_ref.clicked.connect(self.take_ref)
        self.check_bg = QtWidgets.QCheckBox("Substract bg.")
        self.check_ref = QtWidgets.QCheckBox("Calculate refl. coef.")

        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')

        self.plot_item_image = pg.PlotItem()
        self.plot_image = pg.ImageView(view=self.plot_item_image)
        self.plot_item_image.setAspectLocked(False)
        self.first_image_has_been_plotted = False
        # self.plot_image.setLabel('bottom', "Wavelength (nm)")

        self.plot = pg.PlotWidget()
        self.plot.setLabel('bottom', "Wavelength (nm)")
        self.curve_init = pg.PlotCurveItem()
        self.plot.addItem(self.curve_init)
        self.curve = pg.PlotCurveItem()
        self.plot.addItem(self.curve)
        self.curve_region = pg.PlotCurveItem()
        self.plot.addItem(self.curve_region)
        self.curve_fit = pg.PlotCurveItem()
        self.plot.addItem(self.curve_fit)
        self.plot_text = pg.TextItem()
        self.plot_text.setFont(QtGui.QFont("Arial", 16))
        self.plot_text.setParentItem(self.plot.getViewBox())
        self.plot_text.setPos(30, 30)
        self.pen_main = pg.mkPen(color=(0, 0, 0))
        self.pen_region = pg.mkPen(width=2, color=(0, 200, 0))
        self.pen_init = pg.mkPen(color=(128, 128, 128), style=QtCore.Qt.DashLine)
        self.pen_fit = pg.mkPen(color=(0, 0, 255))
        self.pen_blank = pg.mkPen(color=(0, 0, 0, 0))

        self.edit_filename = QtWidgets.QLineEdit(self.last_directory)
        self.edit_filename.editingFinished.connect(self.save_settings_tab)
        self.edit_spectrum_number = QtWidgets.QLineEdit("1")
        self.edit_spectrum_number.setFixedWidth(40)
        self.edit_spectrum_number.editingFinished.connect(self.save_settings_tab)
        self.button_export = QtWidgets.QPushButton("Export")
        self.button_export.clicked.connect(self.export)
        self.button_browse = QtWidgets.QPushButton("Browse")
        self.button_browse.clicked.connect(self.browse)
        self.check_export_all = QtWidgets.QCheckBox("Export all tracks")
        self.check_export_asc = QtWidgets.QCheckBox("Export raw data to .asc")
        self.check_export_h5 = QtWidgets.QCheckBox("Export .h5 file")
        self.check_export_ep = QtWidgets.QCheckBox("Export EasyPlot file")
        self.check_export_png = QtWidgets.QCheckBox("Export graph as .png")
        for check in [self.check_export_all, self.check_export_asc, self.check_export_h5, \
                      self.check_export_ep, self.check_export_png]:
            check.stateChanged.connect(self.save_settings_tab)

        self.label_status = QtWidgets.QLabel()
        self.list_files = QtWidgets.QListWidget()
        self.list_files.currentItemChanged.connect(self.load_file)
        self.list_files.setMinimumHeight(120)
        self.button_start = QtWidgets.QPushButton("Start")
        self.button_start.setMinimumWidth(55)
        self.button_start.clicked.connect(self.enable_fit)
        self.button_stop = QtWidgets.QPushButton("Stop")
        self.button_stop.setMinimumWidth(55)
        self.button_stop.clicked.connect(self.disable_fit)
        self.button_stop.setEnabled(False)
        self.button_fit_once = QtWidgets.QPushButton("Fit once")
        self.button_fit_once.setMinimumWidth(55)
        self.button_fit_once.clicked.connect(lambda: self.fit(event="fit_once"))
        self.edit_name = QtWidgets.QLineEdit()
        self.edit_name.editingFinished.connect(self.load_filenames)
        self.edit_arguments = QtWidgets.QLineEdit()
        self.edit_arguments.editingFinished.connect(lambda: self.parse_variables(save_after=True))
        self.edit_code = TextEdit(self)
        self.edit_code.editingFinished.connect(self.save_settings)
        self.edit_code.editingFinished.connect(lambda: self.fit(event="change_model"))
        self.edit_range = QtWidgets.QLineEdit()
        self.edit_range.editingFinished.connect(self.save_settings)
        self.edit_range.editingFinished.connect(lambda: self.fit(event="change_range"))
        self.custom_variables_widgets = []
        self.custom_variables_count = 0 #different from len(self.custom_variables) - widgets can be hidden and not used
        self.layout_custom_variables = QtWidgets.QGridLayout()
        self.button_take_initial = QtWidgets.QPushButton("Use last fit as initial parameters")
        self.button_take_initial.clicked.connect(self.take_initial_parameters)
        self.load_filenames()
        self.layout_variables = QtWidgets.QGridLayout()
        self.layout_variables.setMargin(0)

        layout_header_inframe = QtWidgets.QHBoxLayout()
        layout_header_inframe.setContentsMargins(0, 0, 0, 0)
        self.frame_header = QtWidgets.QFrame()
        self.frame_header.setLayout(layout_header_inframe)

        layout_header = QtWidgets.QHBoxLayout()
        layout_header.setContentsMargins(0, 0, 0, 0)
        layout_header.addWidget(self.radio_spectrum)
        layout_header.addWidget(self.radio_spectrum_fit)
        layout_header.addWidget(self.radio_image)
        layout_header.addWidget(self.frame_header)
        layout_header.addStretch()

        layout_header_inframe.addWidget(self.label_track)
        layout_header_inframe.addWidget(self.spin_track)
        layout_header_inframe.addWidget(QtWidgets.QLabel("Bin width:"))
        layout_header_inframe.addWidget(self.edit_binning)
        layout_header_inframe.addWidget(self.check_average)
        layout_header_inframe.addWidget(self.button_take_bg)
        layout_header_inframe.addWidget(self.check_bg)
        layout_header_inframe.addWidget(self.button_take_ref)
        layout_header_inframe.addWidget(self.check_ref)

        hlayout0 = QtWidgets.QHBoxLayout()
        #hlayout1.addStretch()
        hlayout0.addWidget(
            QtWidgets.QLabel("<span align=center style=\" font-size:12pt; font-weight:600; \">Real Time Fitter</span>"))

        hlayout1 = QtWidgets.QHBoxLayout()
        hlayout1.addWidget(QtWidgets.QLabel("Last status:"))
        hlayout1.addWidget(self.label_status)
        hlayout1.addStretch()

        hlayout2 = QtWidgets.QHBoxLayout()
        hlayout2.addWidget(self.button_start)
        hlayout2.addWidget(self.button_stop)
        hlayout2.addWidget(self.button_fit_once)

        self.edit_req_port = QtWidgets.QLineEdit("11030")
        self.button_start_worker = QtWidgets.QPushButton("Create Worker")
        self.widget_worker = QtWidgets.QGroupBox("Remote access setup")
        self.layout_worker = QtWidgets.QHBoxLayout(self.widget_worker)
        self.layout_worker.setContentsMargins(4, 2, 4, 4)
        self.widget_worker.setToolTip("Create SpectrumTabInterface object for remote operation.\n"
                                "F. eg.:\n"
                                "from devices.virtual.spectrum_tab import SpectrumTabInterface\n"
                                "spectrum_tab = SpectrumTabInterface(req_port=11030)")
        self.button_start_worker.clicked.connect(self.toggle_worker)
        self.layout_worker.addWidget(QtWidgets.QLabel("req_port:"))
        self.layout_worker.addWidget(self.edit_req_port)
        self.layout_worker.addWidget(self.button_start_worker)

        hlayout3 = QtWidgets.QHBoxLayout()
        hlayout3.addWidget(QtWidgets.QLabel("Name:"))
        hlayout3.addWidget(self.edit_name)

        hlayout4 = QtWidgets.QHBoxLayout()
        hlayout4.setSpacing(2)
        hlayout4.addWidget(QtWidgets.QLabel("def f(x,"))
        hlayout4.addWidget(self.edit_arguments)
        hlayout4.addWidget(QtWidgets.QLabel("):"))


        layout_fit_top = QtWidgets.QVBoxLayout()
        layout_fit_top.setContentsMargins(0, 0, 0, 0)
        layout_fit_top.addLayout(hlayout0)
        layout_fit_top.addLayout(hlayout1)
        layout_fit_top.addLayout(hlayout2)
        layout_fit_top.addWidget(self.widget_worker)
        layout_fit = QtWidgets.QVBoxLayout()
        layout_fit.addWidget(QtWidgets.QLabel("Load fit function:"))
        layout_fit.addWidget(self.list_files)
        layout_fit.addLayout(hlayout3)
        layout_fit.addLayout(hlayout4)
        layout_fit.addWidget(self.edit_code)
        layout_fit.addWidget(QtWidgets.QLabel("Condition for x coordinate:"))
        layout_fit.addWidget(self.edit_range)
        layout_fit.addWidget(QtWidgets.QLabel("Variables - init values and results:"))
        layout_fit.addLayout(self.layout_variables)
        layout_fit.addWidget(self.button_take_initial)
        layout_fit.addWidget(QtWidgets.QLabel("Calculate variables from fit parameters:"))
        layout_fit.addLayout(self.layout_custom_variables)
        layout_fit.addStretch()


        fitter_panel = QtWidgets.QWidget()
        fitter_panel.setLayout(layout_fit)

        layout_export1 = QtWidgets.QHBoxLayout()
        layout_export1.setContentsMargins(0, 0, 0, 0)
        layout_export1.addWidget(QtWidgets.QLabel("Specify filename for exported file. You can use [count] macro "
                                                  "inside the filename. Spectrum number will be inserted in leading "
                                                  "zeros / three digit format."))

        layout_export2 = QtWidgets.QHBoxLayout()
        layout_export2.setContentsMargins(0, 0, 0, 0)
        layout_export2.addWidget(self.edit_filename)
        layout_export2.addWidget(self.button_browse)
        layout_export2.addWidget(QtWidgets.QLabel("[count] ="))
        layout_export2.addWidget(self.edit_spectrum_number)
        layout_export2.addWidget(self.button_export)

        layout_export3 = QtWidgets.QHBoxLayout()
        layout_export3.setContentsMargins(0, 0, 0, 0)
        layout_export3.addWidget(self.check_export_all)
        layout_export3.addWidget(self.check_export_asc)
        layout_export3.addWidget(self.check_export_h5)
        layout_export3.addWidget(self.check_export_ep)
        #layout_export3.addWidget(self.check_export_png)
        layout_export3.addStretch()

        widget_fit_top = QtWidgets.QWidget()
        widget_fit_top.setLayout(layout_fit_top)

        self.splitter = QtWidgets.QSplitter()
        self.splitter.addWidget(self.plot)
        scroll_area_fit = VerticalScrollArea()
        #scroll_area_fit.setStyleSheet("background-color: rgb(255, 255, 255);")
        scroll_area_fit.setWidget(fitter_panel)
        self.widget_fit_all = QtWidgets.QWidget()
        layout_fit_all = QtWidgets.QVBoxLayout(self.widget_fit_all)
        layout_fit_all.setContentsMargins(6, 0, 0, 0)
        layout_fit_all.addWidget(widget_fit_top)
        layout_fit_all.addWidget(scroll_area_fit)
        self.splitter.addWidget(self.widget_fit_all)

        layout_export = QtWidgets.QVBoxLayout()
        layout_export.setContentsMargins(0, 0, 0, 0)
        layout_export.addLayout(layout_export1)
        layout_export.addLayout(layout_export2)
        layout_export.addLayout(layout_export3)
        self.frame_export = QtWidgets.QFrame()
        self.frame_export.setLayout(layout_export)

        layout = QtWidgets.QVBoxLayout()
        layout.addLayout(layout_header)
        layout.addWidget(self.plot_image)
        layout.addWidget(self.splitter)
        layout.addWidget(self.frame_export)
        layout.setStretch(1, 100)
        layout.setStretch(2, 100)

        self.setLayout(layout)
        self.set_image_view(on=False, fit=False)
        self.load_settings_export()

    def error_message(self, message):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(message)
        msg.exec_()
        print(message)

    def set_image_view(self, on=True, fit=False):
        self.fitting_panel_active = fit
        self.widget_fit_all.setVisible(fit)
        self.splitter.setVisible(not on)
        self.frame_export.setVisible(not on)
        self.frame_header.setVisible(not on)
        self.plot_image.setVisible(on)
        if not on:
            self.fit("change_spectrum")

    def toggle_worker(self):
        try:
            self.worker = SpectrumTabWorker(parent=self, req_port=self.edit_req_port.text())
            self.worker.remote_fit_signal.connect(self.remote_fit)
            self.worker.init_device()
            self.worker.start_server(blocking=False)
            self.worker.data_bg = self.data_bg
            self.worker.data_ref = self.data_ref
            self.button_start_worker.setText("Disable Worker")
            self.button_start_worker.setEnabled(False)
        except:
            self.worker = None
            print(traceback.format_exc())

    def browse(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, "Load output file",
                                                             self.last_directory, "Output file (*)")
        filename = filename[0]
        new_dir = '/'.join(filename.split('/')[:-1]) + '/'
        if new_dir != "":
            self.last_directory = new_dir
            if filename != "":
                try:
                    with open("config\\spectrum_tab_last_dir.cfg", "w") as file:
                        file.write(jsonpickle.encode(self.last_directory))
                except Exception as e:
                    self.error_message("Error while saving new default directory: " + traceback.format_exc())
        if filename != "":
            self.edit_filename.setText(filename)

    def export(self):
        try:
            assert self.track_count
        except: # no data received yet
            return

        try:
            count = int(self.edit_spectrum_number.text())
        except:
            count = 1

        filename_base = self.edit_filename.text().replace("[count]", f"{count:03d}")
        if filename_base.strip() == "":
            self.error_message("Filename must not be empty!")
        all = self.check_export_all.isChecked()
        asc = self.check_export_asc.isChecked()
        h5 = self.check_export_h5.isChecked()
        ep = self.check_export_ep.isChecked()
        # png = self.check_export_png.isChecked()

        success = False # changed if there is any data to save

        self.spin_track.blockSignals(True)
        preivous_track = self.spin_track.value() - 1
        tracks = [preivous_track] if not all else range(self.track_count)
        dataseries = []
        for track in tracks:
            self.spin_track.setValue(track + 1)
            self.fit(event="change_spectrum")
            if all:
                filename_track = filename_base + f"_tr{track:03d}"
            else:
                filename_track = filename_base

            if asc or h5 or ep:  # if raw data will be exported
                data = self.curve.getData()
                datafit = self.curve_fit.getData()
                if len(data[0]) == 0:
                    data = None
                    continue
                success = True
                if len(datafit[0]) == 0:
                    datafit = None
                if asc and data:
                    filename = filename_track + ".asc"
                    string = "\n".join(f"{x}\t{y}" for x, y in zip(data[0], data[1]))
                    try:
                        with open(filename, "w") as file:
                            file.write(string)
                    except:
                        self.error_message("Error saving raw data snapshot\n\n" + traceback.format_exc())
                if ep or h5:
                    dataseries.append((data, f"track {track}", False, track))
                    if datafit:
                        dataseries.append((datafit, self.export_fit_text, True, track))

        if ep:
            try:
                text = '/oll 0 0\n'
                text += '/fpo  lt  15.00\n'
                for data, legend, _, _ in reversed(dataseries):
                    text += '/sa l "'
                    text += legend.replace("\\", "\\\\").replace("_", "\\_")
                    text += '" 2\n/td "xy"\n'
                    text += "\n".join(f"{x} {y}" for x, y in zip(data[0], data[1]))
                    text += "\n//nc\n"
                open(filename_base + ".ep", "w").write(text)
            except:
                self.error_message("Error saving easyplot snapshot\n\n" + traceback.format_exc())
        if h5:
            try:
                file = MeasurementFile(filename_base + ".h5", cached_only=True)
                for data, _, is_fit, track in dataseries:
                    if not is_fit:
                        file.save_snapshot(x=data[0], y=track, z=data[1],
                                           raw=self.ydata_unprocessed, bg=self.data_bg, ref=self.data_ref)
                file.close()
            except:
                self.error_message("Error saving hdf5 snapshot\n\n" + traceback.format_exc())
        self.spin_track.setValue(preivous_track + 1)
        self.fit(event="change_spectrum")
        self.spin_track.blockSignals(False)

        if success:
            self.edit_spectrum_number.setText(str(count + 1))
        self.save_settings_tab()

    def take_bg(self):
        self.data_bg = self.ydata_unprocessed
        if self.worker is not None:
            self.worker.data_bg = self.data_bg

    def take_ref(self):
        self.data_ref = self.ydata_unprocessed
        if self.worker is not None:
            self.worker.data_ref = self.data_ref

    def fit(self, event):
        assert event in ["change_spectrum", "change_range", "new_data", "fit_remote", "change_model", "fit_once"], \
            "Invalid event type"
        auto_fit = self.fitter_enabled
        error_box_forbidden = (event in ["fit_remote"]) or \
                              (not auto_fit and event in ["new_data", "change_range", "change_spectrum"])

        update_data = event in ["change_spectrum", "new_data", "fit_remote"]
        update_highlight = (event in ["change_spectrum", "change_range", "new_data", "fit_remote"]) and self.fitting_panel_active
        update_init = (event in ["change_model", "fit_remote"]) and self.fitting_panel_active
        update_fit = (event in ["fit_once", "fit_remote"]) or \
            (auto_fit and self.fitting_panel_active and event in ["change_spectrum", "new_data", "change_model", "change_range"])

        self.label_status.setText(
            f"<span align=center style=\" font-size:8pt; color:green; \">Fit not requested.</span>")

        if self.fitting_panel_active:
            self.curve_fit.setPen(self.pen_fit)
            self.curve_init.setPen(self.pen_init)
            self.curve_region.setPen(self.pen_region)
        else:
            self.plot_text.setText("")
            self.curve_fit.setPen(self.pen_blank)
            self.curve_init.setPen(self.pen_blank)
            self.curve_region.setPen(self.pen_blank)

        if update_data:
            try:
                assert self.data_all[1][0][0]
                image_plot_data = self.data_all[1].T
                self.track_count = len(self.data_all[1])
                self.xdata = self.data_all[0]
                try:
                    self.ydata = self.data_all[1][self.spin_track.value() - 1]
                except:
                    self.label_status.setText(
                        f"<span align=center style=\" font-size:8pt; color:red; \">Spectrum no. {self.spin_track.value()} does not exist in last data.</span>")
                    return
            except (TypeError, IndexError):
                image_plot_data = np.array([self.data_all[1]]).T
                self.xdata, self.ydata = self.data_all
                self.track_count = 1
            except AttributeError:
                self.label_status.setText(
                    "<span align=center style=\" font-size:8pt; color:red; \">No data to fit!</span>")
                return

            # setup image plot
            x0, x1 = (self.xdata[0], self.xdata[-1])
            auto = not self.first_image_has_been_plotted
            xscale = (x1 - x0) / image_plot_data.shape[0]
            self.plot_image.setImage(image_plot_data,
                                          autoLevels = auto, autoRange = auto, autoHistogramRange = auto,
                                          pos = [x0, 0.5], scale = [xscale, 1])
            self.first_image_has_been_plotted = True

            self.ydata_unprocessed = self.ydata

            if self.check_bg.isChecked():
                try:
                    self.ydata = self.ydata - self.data_bg
                except:
                    print(traceback.format_exc())

            if self.check_ref.isChecked():
                try:
                    if self.check_bg.isChecked():
                        self.ydata = (self.ydata - (self.data_ref - self.data_bg)) / \
                                     (self.ydata + (self.data_ref - self.data_bg))
                    else:
                        self.ydata = (self.ydata - (self.data_ref)) / \
                                     (self.ydata + (self.data_ref))
                except:
                    print(self.ydata, self.data_ref, self.data_bg)
                    print(traceback.format_exc())

            try:
                binning = int(self.edit_binning.text())
                assert binning >= 1 and binning <= len(self.xdata), "Invalid bin width"
            except:
                binning = 1

            c = len(self.xdata)
            c -= c % binning
            self.xdata = np.array(self.xdata)[0:c].reshape(-1, binning).mean(axis=1)
            if self.check_average.isChecked():
                self.ydata = np.array(self.ydata)[0:c].reshape(-1, binning).mean(axis=1)
            else:
                self.ydata = np.array(self.ydata)[0:c].reshape(-1, binning).sum(axis=1)

            self.curve.setData(*[self.xdata, self.ydata], pen=self.pen_main)
        if update_highlight:
            self.curve_region.setData([], [])
        if update_init:
            self.curve_init.setData([], [])
        if update_fit:
            self.plot_text.setText("", color=(0, 0, 0))
            self.curve_fit.setData([], [])


        try:
            if update_fit or update_init or update_highlight:  # parse all
                for v in self.variables_active:
                    self.variables_all[v].edit_best.setText("")
                variables = [v for v in self.variables_active if not self.variables_all[v].check_fix.checkState()]
                if len(variables) == 0:
                    self.disable_fit()
                    if not error_box_forbidden:
                        self.error_message("Please check if valid fit function arguments are specified.")
                    return
                variables_fixed = [v for v in self.variables_active if self.variables_all[v].check_fix.checkState()]

                body = self.edit_code.toPlainText()
                if body.strip() == "":
                    body = "0"
                if body.find('return') == -1:
                    body = "    return(" + body.replace("\n", " ") + ")"
                else:
                    body = "    " + body.replace("\n", "\n    ")

                body_range = self.edit_range.text()
                if body_range.strip() == "":
                    body_range = "True"
                if body_range.find('return') == -1:
                    body_range = "    return(" + body_range.replace("\n", " ") + ")"
                else:
                    body_range = "    " + body_range.replace("\n", "\n    ") + " "

                for v in self.variables_active:
                    while True:
                        result = re.search('(?=([^a-z^A-Z^0-9^_]' + v + '[^a-z^A-Z^0-9^_]))', body_range)
                        if not result:
                            break
                        pos = result.start() + 1
                        body_range = body_range[:pos] + self.variables_all[v].edit_init.text() + body_range[pos + len(v):]

                function = "def fit_function(x," + ','.join(variables) + "):\n" + \
                           body + "\n" + \
                           "def fit_function_v(x_v," + ','.join(variables) + "):\n" + \
                           "    return np.array([fit_function(x," + ','.join(variables) + ") for x in x_v])\n" + \
                           "def is_in_range(x):\n" + \
                           body_range + " "
                # function.replace("^", "**")

                for v in variables_fixed:
                    while True:
                        result = re.search('(?=([^a-z^A-Z^0-9^_]' + v + '[^a-z^A-Z^0-9^_]))', function)
                        if not result:
                            break
                        pos = result.start() + 1
                        function = function[:pos] + self.variables_all[v].edit_init.text() + function[pos + len(v):]
                exec(function, globals())
            if update_init or update_fit:
                init_parameters_text = [self.variables_all[v].edit_init.text().strip() for v in variables]
                init_parameters = [float(p) if p != "" else 1 for p in init_parameters_text]
                xdatafit = np.linspace(min(self.xdata), max(self.xdata), 1000)  # init values visualisation
            data = list(zip(self.xdata, self.ydata))
            if update_highlight or update_fit:
                xdata = [x for x, y in data if is_in_range(x)]
                ydata = [y for x, y in data if is_in_range(x)]

            if update_highlight:
                self.curve_region.setData(xdata, ydata, pen=self.pen_region)
            if update_init:
                ydatafit_init = fit_function_v(xdatafit, *init_parameters)
                self.curve_init.setData(xdatafit, ydatafit_init,
                    pen=self.pen_init)
            if update_fit:
                if len(xdata) < len(init_parameters):
                    self.disable_fit()
                    self.label_status.setText("<span align=center style=\" font-size:8pt; color:red; \">Too few points in defined x range!</span>")
                    return
                try:
                    best_fit_parameters, pcov = scipy.optimize.curve_fit(fit_function, xdata, ydata, p0=init_parameters,
                                                                         maxfev=200)
                except RuntimeError:
                    self.label_status.setText(
                        "<span align=center style=\" font-size:8pt; color:rgb(188, 188, 0); \">Fit did not converge.</span>")
                    return
                except:
                    best_fit_parameters, pcov = scipy.optimize.curve_fit(fit_function_v, xdata, ydata, p0=init_parameters,
                                                                         maxfev=100)
                for i in range(len(variables)):
                    best_fit_parameters[i] = round(best_fit_parameters[i],
                                                   11 - int(math.floor(
                                                       math.log10(max(abs(best_fit_parameters[i]), 10 ** (-13))))) - 1)
                    self.variables_all[variables[i]].edit_best.setText(str(best_fit_parameters[i]))

                ydatafit = fit_function_v(xdatafit, *best_fit_parameters)
                self.curve_fit.setData(xdatafit, ydatafit, pen=self.pen_fit)

                for v in variables_fixed:
                    self.variables_all[v].edit_best.setText(self.variables_all[v].edit_init.text())

                for i in range(self.custom_variables_count):
                    self.custom_variables_widgets[i].edit_result.setText("")
                    try:
                        function = " " + self.custom_variables_widgets[i].edit_definition.text().split("=")[-1] + " "
                        for v in self.variables_active:
                            while True:
                                result = re.search('(?=([^a-z^A-Z^0-9^_]' + v + '[^a-z^A-Z^0-9^_]))', function)
                                if not result:
                                    break
                                pos = result.start() + 1
                                function = function[:pos] + self.variables_all[v].edit_best.text() + function[pos + len(v):]
                        # function.replace("^", "**")
                        value = eval(function)
                        value = round(value, 11 - int(math.floor(math.log10(max(abs(value), 10 ** (-13))))) - 1)
                        self.custom_variables_widgets[i].edit_result.setText(str(value))
                    except:
                        pass

                variables_dict = {}
                legend = ""
                for v in self.variables_active:
                    if self.variables_all[v].check_show.checkState():
                        legend += v + " = " + self.variables_all[v].edit_best.text() + "\n"
                        try:
                            variables_dict[v] = float(self.variables_all[v].edit_best.text())
                        except:
                            pass

                for i in range(self.custom_variables_count):
                    if self.custom_variables_widgets[i].check_show.checkState() and \
                            self.custom_variables_widgets[i].edit_definition.text().strip() != "":
                        legend += self.custom_variables_widgets[i].edit_definition.text().split('=')[0].strip() + " = " + \
                                  self.custom_variables_widgets[i].edit_result.text() + "\n"
                        try:
                            variables_dict[self.custom_variables_widgets[i].edit_definition.text().split('=')[0].strip()] \
                                = float(self.custom_variables_widgets[i].edit_result.text())
                        except:
                            pass
                if event == "fit_remote" and self.worker is not None:
                    with self.worker.lock:
                        self.worker.timestamp = time.time()
                        self.worker.parameters = variables_dict
                        self.worker.pcov = pcov
                self.plot_text.setText(legend, color=(0, 0, 0))
                self.export_fit_text = self.edit_code.toPlainText().strip() + "\n" + legend.strip().replace("\n", ", \t")
                self.label_status.setText(
                    "<span align=center style=\" font-size:8pt; color:green; \">Fitting performed correctly.</span>")
        except RuntimeError:
            if not error_box_forbidden:
                self.error_message("asdddddd" + traceback.format_exc())
            return
        except Exception:
            self.disable_fit()
            print(traceback.format_exc())
            if not error_box_forbidden:
                self.error_message(function + "\n\n\n" + traceback.format_exc())

    def disable_fit(self):
        self.fitter_enabled = False
        self.button_start.setEnabled(True)
        self.button_stop.setEnabled(False)

    def block_signals(self, block):
        self.edit_name.blockSignals(block)
        self.edit_code.blockSignals(block)
        self.edit_range.blockSignals(block)
        self.edit_arguments.blockSignals(block)
        for k in self.variables_all:
            self.variables_all[k].edit_init.blockSignals(block)
        for v in self.custom_variables_widgets:
            v.edit_definition.blockSignals(block)

    def parse_variables(self, save_after):
        self.block_signals(True)
        str = self.edit_arguments.text()
        variables = [v.strip() for v in str.split(',')]
        variables = [v for v in variables if v != "" and bool(re.match('^[a-zA-Z0-9_]+$', v))]
        variables = list(dict.fromkeys(variables))

        for v in variables:
            if v not in self.variables_all:
                self.variables_all[v] = Variable(v, parent=self)

        for v in self.variables_all:
            self.variables_all[v].visible(False)

        for i in range(len(self.variables_active)):
            for j in range(4):
                self.layout_variables.removeWidget(self.layout_variables.itemAtPosition(i, j).widget())

        row = 0
        for v in variables:
            self.layout_variables.addWidget(self.variables_all[v].label_name, row, 0)
            self.variables_all[v].label_name.setVisible(True)
            self.layout_variables.addWidget(self.variables_all[v].edit_init, row, 1)
            self.variables_all[v].edit_init.setVisible(True)
            self.layout_variables.addWidget(self.variables_all[v].check_fix, row, 2)
            self.variables_all[v].check_fix.setVisible(True)
            self.layout_variables.addWidget(self.variables_all[v].edit_best, row, 3)
            self.variables_all[v].edit_best.setVisible(True)
            self.layout_variables.addWidget(self.variables_all[v].check_show, row, 4)
            self.variables_all[v].check_show.setVisible(True)
            row += 1

        self.variables_active = variables
        if save_after:
            self.save_settings()
        self.block_signals(False)

    def load_filenames(self):
        self.block_signals(True)
        last_item_index = self.list_files.currentRow()
        last_name = self.edit_name.text()
        self.save_settings()
        if len(self.files) > 0:
            last_file = self.edit_name.text().strip()#self.files[self.list_files.currentIndex().row()]
        else:
            last_file = None
        self.list_files.clear()
        datadir = QtCore.QDir.current()
        datadir.cd("config/fitter")
        file_list = datadir.entryList()
        self.files = []
        self.indexes = {}
        for file in file_list:
            if file[-4:] != '.txt':
                continue
            name = file[0:-4]
            if name == last_name:
                last_item_index = len(self.files)
            self.indexes[name] = len(self.files)
            self.files.append(name)
            self.list_files.addItem(name)
        self.list_files.setCurrentRow(last_item_index)
        self.block_signals(False)

    def save_settings(self):
        self.block_signals(True)
        if self.edit_name.text().strip() == "":
            return
        data = [False, self.edit_name.text().strip(), self.edit_arguments.text(), self.edit_code.toPlainText(), self.edit_range.text(),
                [(v, self.variables_all[v].edit_init.text(), self.variables_all[v].check_show.checkState() != 0,
                 self.variables_all[v].check_fix.checkState() != 0) for v in self.variables_all],
                [(v.edit_definition.text(), v.check_show.checkState() != 0)
                 for v in self.custom_variables_widgets[:self.custom_variables_count]]]

        if data != self.fit_data_last:
            self.fit_data_last = data
            filename = "config/fitter/" + self.edit_name.text().strip() + ".txt"
            try:
                with open(filename, "w") as file:
                    file.write(jsonpickle.encode(data))
            except Exception:
                print(traceback.format_exc())
        self.block_signals(False)

    def save_settings_tab(self):
        data = [self.edit_filename.text(), self.edit_spectrum_number.text(), self.check_export_all.isChecked(), \
                self.check_export_asc.isChecked(), self.check_export_h5.isChecked(), \
                self.check_export_ep.isChecked(), self.check_export_png.isChecked(), \
                self.edit_binning.text(), self.check_average.isChecked()]

        try:
            with open("config/spectrum_tab.cfg", "w") as file:
                file.write(jsonpickle.encode(data))
        except Exception:
            print(traceback.format_exc())

    def load_settings_export(self):
        self.block_signals_export(True)
        try:
            filename, count, all, asc, h5, ep, png, bin, avg = jsonpickle.decode(open("config/spectrum_tab.cfg", "r").read())
            self.edit_filename.setText(filename)
            self.edit_spectrum_number.setText(count)
            self.check_export_all.setChecked(all)
            self.check_export_asc.setChecked(asc)
            self.check_export_h5.setChecked(h5)
            self.check_export_ep.setChecked(ep)
            self.check_export_png.setChecked(png)
            self.edit_binning.setText(bin)
            self.check_average.setChecked(avg)
        except:
            print(traceback.format_exc())
        self.block_signals_export(False)

    def block_signals_export(self, block):
        self.edit_filename.blockSignals(block)
        self.edit_spectrum_number.blockSignals(block)
        self.check_export_all.blockSignals(block)
        self.check_export_asc.blockSignals(block)
        self.check_export_h5.blockSignals(block)
        self.check_export_ep.blockSignals(block)
        self.check_export_png.blockSignals(block)

    def load_file(self):
        name = self.files[self.list_files.currentIndex().row()]
        self.load_settings(name)

    def push_new_variable_widget(self):
        var = CustomVariable()
        i = len(self.custom_variables_widgets)
        self.custom_variables_widgets.append(var)
        self.layout_custom_variables.addWidget(var.edit_definition, i, 0)
        self.layout_custom_variables.addWidget(var.edit_result, i, 1)
        self.layout_custom_variables.addWidget(var.check_show, i, 2)
        var.edit_definition.editingFinished.connect(lambda: self.update_custom_variables_gui(save_after=True))
        var.edit_definition.editingFinished.connect(lambda: self.fit(event="fit_once"))

    def refresh_visibility(self):
        # show only necessary variable widgets:
        for i in range(len(self.custom_variables_widgets)):
            visible = i < self.custom_variables_count
            self.custom_variables_widgets[i].edit_definition.setVisible(visible)
            self.custom_variables_widgets[i].edit_result.setVisible(visible)
            self.custom_variables_widgets[i].check_show.setVisible(visible)

    def load_settings(self, name):
        self.block_signals(True)
        self.save_settings()
        self.edit_name.setText(name)
        try:
            filename = "config/fitter/" + name + ".txt"
            is_predefined, _, arguments, code, range_def, variables, custom_variables = \
                jsonpickle.decode(open(filename, "r").read())
            if is_predefined:
                self.edit_name.setText("!UNSAVED! rename me to save changes")
            self.edit_arguments.setText(arguments)
            self.edit_code.setText(code)
            self.edit_range.setText(range_def)
            self.custom_variables_count = len(custom_variables)

            # create missing widgets for new number of variables
            for i in range(len(self.custom_variables_widgets), self.custom_variables_count):
                self.push_new_variable_widget()

            self.refresh_visibility()

            for i in range(len(custom_variables)):
                definition, show = custom_variables[i]
                self.custom_variables_widgets[i].edit_definition.setText(definition)
                self.custom_variables_widgets[i].check_show.setChecked(show)
                self.custom_variables_widgets[i].edit_result.setText("")

            self.update_custom_variables_gui(save_after=False)
            self.parse_variables(save_after=False)

            for v, init, show, fix in variables:
                try:
                    self.variables_all[v].edit_init.setText(init)
                    self.variables_all[v].check_show.setChecked(show)
                    self.variables_all[v].check_fix.setChecked(fix)
                except:
                    pass
        except:
            print(traceback.format_exc())
        self.block_signals(False)

    def update_custom_variables_gui(self, save_after):
        for i in range(self.custom_variables_count - 1):
            if self.custom_variables_widgets[i].edit_definition.text().strip() == "":
                for j in range(i, self.custom_variables_count - 1):
                    self.custom_variables_widgets[j].edit_definition.setText(
                        self.custom_variables_widgets[j+1].edit_definition.text())
                    self.custom_variables_widgets[j].edit_result.setText(
                        self.custom_variables_widgets[j+1].edit_result.text())
                    self.custom_variables_widgets[j].check_show.setChecked(
                        self.custom_variables_widgets[j+1].check_show.checkState())
                self.custom_variables_count -= 1

        if len(self.custom_variables_widgets) == 0 or \
               self.custom_variables_widgets[self.custom_variables_count - 1].edit_definition.text().strip() != "":
            self.custom_variables_count += 1
            if self.custom_variables_count > len(self.custom_variables_widgets):
                self.push_new_variable_widget()
            self.custom_variables_widgets[self.custom_variables_count - 1].edit_definition.setText("")
            self.custom_variables_widgets[self.custom_variables_count - 1].edit_result.setText("")
            self.custom_variables_widgets[self.custom_variables_count - 1].check_show.setChecked(True)

        self.refresh_visibility()
        if save_after:
            self.save_settings()

    def take_initial_parameters(self):
        for v in self.variables_active:
            if self.variables_all[v].edit_best.text().strip() == "":
                return
            self.variables_all[v].edit_init.setText(self.variables_all[v].edit_best.text())
        self.fit(event="change_model")
        self.save_settings()

    def remote_fit(self):
        self.spin_track.blockSignals(True)
        self.spin_track.setValue(self.worker.track + 1)
        self.spin_track.blockSignals(False)
        for name, value in self.worker.init_values_dict.items():
            try:
                self.variables_all[name].edit_init.blockSignals(True)
                self.variables_all[name].edit_init.setText(str(value))
                self.variables_all[name].edit_init.blockSignals(False)
            except KeyError:
                pass
            except:
                print(traceback.format_exc())
                raise
        try:
            self.fit(event="fit_remote")
        except:
            self.worker.queue.put(False)
        else:
            self.worker.queue.put(True)



    def enable_fit(self):
        self.fitter_enabled = True
        self.button_start.setEnabled(False)
        self.button_stop.setEnabled(True)
        self.fit(event="fit_once")

    def update_plot(self, data):
        try:
            x = np.array(data[0])
            y = np.array(data[1])
            if len(y.shape) == 2:
                y[0][1]  # assert
                if y.shape[0] == 1:
                    y = y[0]
            else:
                y[1]  # assert
            x[1]  # assert
            self.data_all = (x, y)
        except (IndexError, TypeError):
            print(traceback.format_exc())
            self.label_status.setText(
                "<span align=center style=\" font-size:8pt; color:rgb(188, 188, 0); \">Incorrect data received.</span>")
        except Exception:
            print(traceback.format_exc())
        else:
            try:
                self.fit(event = "new_data")
            except:
                print(traceback.format_exc())

    def check_devices(self):
        for name, dev in devices.active_devices.items():
            if isinstance(dev, Spectrometer) or isinstance(dev, OPO):
                dev.createListenerThread(self.update_plot, b'spectrum')
                break
