#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 26 09:54:02 2018

@author: tkaz
"""

from PyQt5 import Qt,QtCore,QtGui,QtWidgets
from devices.demo.demo import WorkerForDummyDevice
import devices
import sys,traceback
import multiprocessing
import zmq
import time
import re
import psutil

class StreamToPipe():
    def __init__(self, pipe):
        self.pipe = pipe

    def write(self, buf):
        self.pipe.send(buf)

    def flush(self):
        pass   

class PipeListener(QtCore.QThread):
    """ A class to implement a thread listening for stdout/stderr 
    from other process and converting the messages to Qt signals.
    Due to limitations of QTextEdit we want to send a full line at a time."""
    message_received = QtCore.pyqtSignal(str)

    def __init__(self, parent, pipe):
        super().__init__(parent)
        self.pipe = pipe
        self.buffer = ''
         
    def run(self):
        try:
            while True: 
                line = self.pipe.recv()
                if line.endswith('\n'):
                    self.message_received.emit(self.buffer + line[:-1])
                    self.buffer = ''
                else:
                    self.buffer += line
        except:
            pass


class PersistentProcessInfo:
    _settings = QtCore.QSettings('LUMS','PyLUMS_DeviceServer')
    
    def _sanitize(s):
        return re.sub('\W+', '_', s) # substitute non-alphanumerics with underscore

    @staticmethod
    def get_pid(name):
        """ Fetch the PID of the Worker of given name.
        Returns None if no such Worker is currently registered. """
        PersistentProcessInfo._settings.sync()
        return PersistentProcessInfo._settings.value(PersistentProcessInfo._sanitize(name))
    
    @staticmethod
    def save_pid(name, pid):
        """ Register the PID of a Worker of a given name """
        PersistentProcessInfo._settings.setValue(PersistentProcessInfo._sanitize(name), pid)
        PersistentProcessInfo._settings.sync()

    @staticmethod
    def remove_pid(name):
        """ Unregister the given Worker (if registered) """
        PersistentProcessInfo._settings.remove(PersistentProcessInfo._sanitize(name))
        PersistentProcessInfo._settings.sync()



def process_for_device(pout, perr, WorkerClass, kwargs): #no ** missing!
    sys.stdout = StreamToPipe(pout)
    sys.stderr = StreamToPipe(perr)
    w = WorkerClass(**kwargs)
    w.init_device()
    w.start_server(blocking=True)


class WidgetForProcess(QtWidgets.QWidget):
    NO_STATE = 0
    OK_STATE = 1
    FAILED_STATE = 2
    ALERT_STATE = 3

    newIcon = QtCore.pyqtSignal(int)
    newAutostartSetting = QtCore.pyqtSignal(bool)
    

    def __init__(self, name, process_class = WorkerForDummyDevice, **kwargs):
        super().__init__()
        self.name = name
        self.process_class = process_class
        
        self.kwargs = kwargs        
        self.process = None
        
        layout = QtWidgets.QVBoxLayout()
        layout2 = QtWidgets.QHBoxLayout()
        label1 = QtWidgets.QLabel("REQ port:")
        edit1 = QtWidgets.QLineEdit()
        if 'req_port' in kwargs:
            edit1.setText(str(kwargs['req_port']))
        edit1.setEnabled(False)
        layout2.addWidget(label1)
        layout2.addWidget(edit1)
        layout2.addSpacing(10)
        label2 = QtWidgets.QLabel("PUB port:")
        edit2 = QtWidgets.QLineEdit()
        if 'pub_port' in kwargs:
            edit2.setText(str(kwargs['pub_port']))
            context = zmq.Context()
            self._subscriber = context.socket(zmq.SUB)
            self._subscriber.setsockopt(zmq.CONFLATE, 1) # remember only 1 last message
            self._subscriber.connect("tcp://localhost:{}".format(kwargs['pub_port']))
            self._subscriber.setsockopt(zmq.SUBSCRIBE, b"")
            
        edit2.setEnabled(False)
        layout2.addWidget(label2)
        layout2.addWidget(edit2)
        layout2.addSpacing(10)
        self.startbutton = QtWidgets.QPushButton("Start process")
        self.startbutton.setCheckable(True)
        self.startbutton.clicked.connect(self.startProcess)
        layout2.addWidget(self.startbutton)
        layout2.addStretch(5)
        layout.addLayout(layout2)
        self.textedit = QtWidgets.QTextEdit()
        self.textedit.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        layout.addWidget(self.textedit)
        self.setLayout(layout)  

        pout_r,self.pout_w = multiprocessing.Pipe()
        perr_r,self.perr_w = multiprocessing.Pipe()
               
        self.threads = []
        for pipe,slot in [(pout_r,self._appendInfo), (perr_r,self._appendInfo)]:
            thread = PipeListener(self, pipe)
            thread.message_received.connect(slot, QtCore.Qt.QueuedConnection)
            thread.start()
            self.threads.append(thread)
               
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.checkOnProcess)
        self.timer.start(5000)
                
    def _appendErr(self, text):
        self.textedit.setTextColor(QtGui.QColor(255,0,0))
        self.textedit.append(text)
        self.newIcon.emit(self.ALERT_STATE)
        
    def _appendInfo(self, text):
        self.textedit.setTextColor(QtGui.QColor(0,0,64))
        self.textedit.append(text)
        
    def __del__(self):
        if type(self.process) == multiprocessing.Process:
            self.process.terminate()
            self.process.join()
            PersistentProcessInfo.remove_pid(self.name)
            self.process = None

    def find_if_already_exists(self):
        pid = PersistentProcessInfo.get_pid(self.name)
        if pid:
            try:
                self.process = psutil.Process(pid)
            except psutil.NoSuchProcess:
                pass
            else:
                # disguise psutil.Process as multiprocessing.Process:
                self.process.join = self.process.wait
                self.process.is_alive = self.process.is_running
                self.process.exitcode = None
                self._appendErr(f"Process {pid} recognized as existing Worker.")
                self._appendErr(f"Warning: stdout from Worker is NOT be shown in this window.")
                self.process_start_time = time.time()
                self.missed_updates = 0
                self.startbutton.setChecked(True)
                self.newIcon.emit(self.OK_STATE)

    def startProcess(self, start = True):
        self.newAutostartSetting.emit(start)
        if start:
            if self.process:
                return
            self.find_if_already_exists()
            if self.process:
                return
            prev_contents = self.textedit.toHtml()
            prev_contents = prev_contents.replace('color:#000040', 'color:#8080d0')
            prev_contents = prev_contents.replace('color:#ff0000', 'color:#ff4040')
            self.textedit.clear()
            self.textedit.append(prev_contents)

            self.process = multiprocessing.Process(None, process_for_device, 
                        'pyLUMS - %s' % self.name,
                        (self.pout_w,self.perr_w,self.process_class, self.kwargs))
            self.process.daemon = True
            self.process.start()
            PersistentProcessInfo.save_pid(self.name, self.process.pid)
            self.process_start_time = time.time()
            self.missed_updates = 0 # the process will restart when this counter reaches value of 3
            self.startbutton.setChecked(True)
            self.newIcon.emit(self.OK_STATE)
        else:
            if self.process:
                self.process.terminate()
                self.process.join()
                self.process = None
                PersistentProcessInfo.remove_pid(self.name)
            self.newIcon.emit(self.NO_STATE)

    def checkOnProcess(self):
        if self.process is None:
            self.find_if_already_exists()
        if self.process is None:
            self.startbutton.setChecked(False)
            return
        if self.process.is_alive():
            self.startbutton.setChecked(True)
            self.newIcon.emit(self.OK_STATE)
            try:
                self._subscriber.poll(1) # check for events, e.g. reconnecting the socket
                if time.time() - self.process_start_time > 10: # grace period for initialization
                    self._subscriber.recv(flags=zmq.NOBLOCK)
                    self.missed_updates = 0
            except zmq.ZMQError as err:
                self.missed_updates += 1
            except:
                pass
            
            if self.missed_updates >= 3:
                self.missed_updates = 0
                self._appendErr("Restarting process due to lack of status updates")
                self.startProcess(False)
                self.startProcess(True)
                
        elif self.process.exitcode is None: # Not finished and not running
            # Do your error handling and restarting here assigning the new process to processes[n]
            self.startbutton.setChecked(False)
            self.process = None
            self.newIcon.emit(self.FAILED_STATE)
        elif self.process.exitcode < 0:
            self.startbutton.setChecked(False)
            self.process = None
            self.newIcon.emit(self.FAILED_STATE)
        else:
            print ('finished')
            self.process.join()
            self.process = None
            self.newIcon.emit(self.FAILED_STATE)



class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent = None):
        super().__init__(parent)
        self.createCentralWidget()
        self.setupIcons()

        quitAct = QtWidgets.QAction("&Quit", self)
        quitAct.setShortcuts(QtGui.QKeySequence.Quit)
        quitAct.setStatusTip("Quit the application")
        quitAct.triggered.connect(self.close)
        fileMenu = self.menuBar().addMenu("&File")
        fileMenu.addAction(quitAct)
        self.createTabs()

    def createCentralWidget(self):
        self.toolBox = QtWidgets.QToolBox()
        self.setCentralWidget(self.toolBox)

    def setupIcons(self):
        self.trayIconMenu = QtWidgets.QMenu(self)
        showAction = self.trayIconMenu.addAction("Show main window")
        showAction.triggered.connect(self.show)

        icon = QtGui.QIcon("img/icon_server.svg")
        self.setWindowIcon(icon)
        self.setWindowTitle("pyLUMS - Device server")


    def createTabs(self):
        workers_desc = devices.load_workers()
        
        for name,cls,kwargs in workers_desc:
            try:
            
                try:
                    autostart = kwargs['pylums_autostart']
                    if type(autostart) == str:
                        autostart = autostart.lower() in ['true','yes','1']
                    del(kwargs['pylums_autostart'])
                except KeyError:
                    autostart = False
                    
                widget = WidgetForProcess(name, process_class=cls, **kwargs)
                idx = self.toolBox.addItem(widget, name)
                def make_slot(idx):
                    def my_slot(state):
                        if state == WidgetForProcess.OK_STATE:
                            self.toolBox.setItemIcon(idx, QtGui.QIcon("img/ok.png"))
                        elif state == WidgetForProcess.FAILED_STATE:
                            self.toolBox.setItemIcon(idx, QtGui.QIcon("img/stop.png"))
                        elif state == WidgetForProcess.ALERT_STATE:
                            self.toolBox.setItemIcon(idx, QtGui.QIcon("img/alert.png"))
                        elif state == WidgetForProcess.NO_STATE:
                            self.toolBox.setItemIcon(idx, QtGui.QIcon())
                    return my_slot  
                widget.newIcon.connect(make_slot(idx))
                def make_slot2(name):
                    return lambda state : devices.set_worker_autostart(name, state)
                widget.newAutostartSetting.connect(make_slot2(name))
                
                if autostart:
                    widget.startProcess()
                
            except Exception as e:
                traceback.print_exc(file=sys.stdout)        


if __name__ == '__main__':
    import sys
    def run_app():
        app = QtWidgets.QApplication(sys.argv)
        icon = QtGui.QIcon("img/icon_server.svg")
        app.setWindowIcon(icon)
        
        # following lines are needed in Win7 to have separate taskbar icon
        try:
            import ctypes
            myappid = 'LUMS.pylums.DeviceServer.10' # arbitrary string
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
        except:
            pass
        
        window = MainWindow()
        
        window.show()
        app.exec_()
        
        
    run_app()
