# -*- coding: utf-8 -*-
"""
Created on Sat May 26 09:54:02 2018

@author: tkaz
"""

from PyQt5 import Qt,QtCore,QtGui,QtWidgets
import time
import devices
import traceback,sys

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.createCentralWidget()
        self.setupIcons()
        
        quitAct = QtWidgets.QAction("&Quit", self);
        quitAct.setShortcuts(QtGui.QKeySequence.Quit)
        quitAct.setStatusTip("Quit the application")
        quitAct.triggered.connect(self.close)
        
        devicesAct = QtWidgets.QAction("&Device manager", self)
        devicesAct.setStatusTip("Manage active devices")
        devicesAct.triggered.connect(self.show_devices_dialog)
        
        action_load_script = QtWidgets.QAction("Load &script", self)
        action_load_script.setStatusTip("Load script to manage the measurement")
        action_load_script.triggered.connect(self._load_script_dialog)
        
        
        fileMenu = self.menuBar().addMenu("&File")
        fileMenu.addAction(devicesAct)
        fileMenu.addSeparator()
        fileMenu.addAction(action_load_script)
        fileMenu.addSeparator()
        fileMenu.addAction(quitAct)
        self.controlMenu = self.menuBar().addMenu("&View")
        self.resize(self.width(), 800)
        
        self._tabs = []
        
    
    def createConsoleTab(self):
        """ Initialize a python console and return its widget """
        from qtconsole.rich_jupyter_widget import RichJupyterWidget
        from qtconsole.manager import QtKernelManager

        kernel_manager = QtKernelManager(kernel_name='python3')
        kernel_manager.start_kernel()
        kernel = kernel_manager.kernel
        kernel.gui = 'qt'

        kernel_client = kernel_manager.client()
        kernel_client.start_channels()
        kernel_client.namespace  = self

        def stop():
            kernel_client.stop_channels()
            kernel_manager.shutdown_kernel()

        widget = RichJupyterWidget(parent=self)
        widget.kernel_manager = kernel_manager
        widget.kernel_client = kernel_client
        widget.exit_requested.connect(stop)
        ipython_widget = widget
        ipython_widget.show()
        self.kernel_client = kernel_client
        return widget
    
    #def createProcessControlTab(self):
        #""" Creates and returns the widget to control slave processes """
        #scrollArea = QtWidgets.QScrollArea()
        #scrollArea.setWidget(widget)
        
    
    
    def addPage(self, widget, name):
        self.tabWidget.addTab(widget, name)
    
    def createCentralWidget(self):
        self.tabWidget = QtWidgets.QTabWidget()
        self.setCentralWidget(self.tabWidget)
        self.tabWidget.addTab(self.createConsoleTab(), "Console")
        #self.tabWidget.addTab(self.createProcessControlTab, "Devices")
        

    def setupIcons(self):
        self.trayIconMenu = QtWidgets.QMenu(self);
        showAction = self.trayIconMenu.addAction("Show main window")
        showAction.triggered.connect(self.show)

        icon = QtGui.QIcon("img/icon_control.svg")
        self.setWindowIcon(icon)
        self.setWindowTitle("pyLUMS - Interactive control")

        
    def _load_script_dialog(self):
        fname,fmt = QtWidgets.QFileDialog.getOpenFileName( \
                self, "Open script (GUI)", "", "Python scripts (*.py)")
        
        if not fname:
            return
            
        try:
            import types
            import importlib.machinery
            from src.measurement_tab import MeasurementTab,Measurement
            loader = importlib.machinery.SourceFileLoader(fname, fname)
            mod = types.ModuleType(loader.name)
            loader.exec_module(mod)
            for name,obj in mod.__dict__.items():
                try:
                    if issubclass(obj, Measurement) and obj!=Measurement:
                        w = MeasurementTab(obj)
                        self.addPage(w, obj.title)
                        self._tabs.append(w)
                except TypeError:
                    pass
                except:
                    traceback.print_exc(file=sys.stdout)
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
        
        
    def show_devices_dialog(self):
        already_loaded = list(devices.active_devices.keys()) # deep copy
        if devices.load_devices(use_gui=True, parent=self):
            self.kernel_client.execute('_ = devices.load_devices()')
            self.kernel_client.execute('globals().update(devices.active_devices)')
            self.kernel_client.execute('print(list(devices.active_devices))')
            
            for name,device in devices.active_devices.items():
                try:
                    if name not in already_loaded:
                        device.createDock(self, self.controlMenu)
                except Exception as e:
                    traceback.print_exc(file=sys.stdout)

if __name__ == '__main__':
    import sys,traceback
    def run_app():
        app = QtWidgets.QApplication(sys.argv)
        
        # following lines are needed in Win7 to have separate taskbar icon
        import ctypes
        try:
            myappid = 'LUMS.pylums.InteractiveControl.10' # arbitrary string
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
        except:
            pass
        
        window = MainWindow()
        window.show()
        window.kernel_client.execute('import devices')
        window.kernel_client.execute('import time')
        window.kernel_client.execute('import numpy as np')
        window.kernel_client.execute('from src.measurement_file import MeasurementFile')
        window.show_devices_dialog()

        try:
            from src import tab_script
        except ModuleNotFoundError:
            dialog = QtWidgets.QTextEdit()
            dialog.setPlainText("Qscintilla is not installed. Please install it via:\n"
                                "pip install --upgrade --user -I PyQt5-sip qscintilla")
            dialog.show()
            app.exec_()

        from src.measurement_tab import NoRequiredDevicesError
        

        try:
            from devices.misc import joystick_control
            w = joystick_control.JoystickControlWidget(devices.active_devices)
            window.addPage(w, "Pad control")
        except NoRequiredDevicesError:
            pass
        except Exception as e:
            traceback.print_exc(file=sys.stdout)

        try:
            from src import spectrum_tab
            w = spectrum_tab.SpectrumTab(window)
            window.addPage(w, "Spectrum Tab")
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            
        try:
            from src import plot_tab
            w = plot_tab.PlotTab(window)
            window.addPage(w, "Parameter Plot Tab")
        except Exception as e:
            traceback.print_exc(file=sys.stdout)

        try:
            from src.map_widget import map_widget
            w = map_widget.MapWidget(devices.active_devices)
            window.addPage(w, "Map")
        except NoRequiredDevicesError:
            pass
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
        
        from src.measurement_tab import MeasurementTab
        
        try:
            from src import tab_anisotropy
            w = MeasurementTab(tab_anisotropy.AnisotropyMeasurement)
            window.addPage(w, "Anisotropy")
        except NoRequiredDevicesError:
            pass
        except Exception as e:
            traceback.print_exc(file=sys.stdout)

        try:
            from src import tab_powersweep
            w = MeasurementTab(tab_powersweep.PowerSweepMeasurement)
            window.addPage(w, "Power Tab")
        except NoRequiredDevicesError:
            pass
        except Exception as e:
            traceback.print_exc(file=sys.stdout)

        try:
            from src import tab_fieldscan
            w = MeasurementTab(tab_fieldscan.FieldSweepMeasurement)
            window.addPage(w, "Field sweep")
        except NoRequiredDevicesError:
            pass
        except Exception as e:
            traceback.print_exc(file=sys.stdout)

        try:
            from src import tab_script
            w = MeasurementTab(tab_script.ScriptMeasurement)
            window.addPage(w, "Script Tab")
        except NoRequiredDevicesError:
            pass
        except Exception as e:
            traceback.print_exc(file=sys.stdout)

        try:
            from src import hdf5_tool
            w = hdf5_tool.HDF5ToolTab(window)
            window.addPage(w, "HDF5 Tool")
        except Exception as e:
            traceback.print_exc(file=sys.stdout)

        try:
            from src import hamamatsu_tool
            w = hamamatsu_tool.HamamatsuToolTab(window)
            window.addPage(w, "Hamamatsu Tool")
        except Exception as e:
            traceback.print_exc(file=sys.stdout)

        window.show()
        app.exec_()
                
    run_app()
