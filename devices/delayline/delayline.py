# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 10:54:18 2018

@author: Turing
"""




import ctypes as ct
from mss import mss

from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets, QtCore, QtGui
import time
import threading

import os

try:
    os.add_dll_directory(pathlib.Path().absolute().as_posix() + "/devices/delayline/") # Python 3.8+
except AttributeError:
    os.environ["PATH"] = pathlib.Path().absolute().as_posix() + "/devices/delayline/" + os.pathsep + os.environ["PATH"]


default_req_port = 9502
default_pub_port = 9503

class DelayLineWorker(DeviceWorker):    
    def __init__(self, req_port = default_req_port, pub_port = default_pub_port, **kwargs):
        super().__init__(req_port = req_port, pub_port = pub_port, **kwargs)
    def init_device(self):
        try:
            self.dll = ct.windll.LoadLibrary("EnsembleC64.dll")
        except OSError:
            self.dll = ct.windll.LoadLibrary("EnsembleC.dll")
        except:
            print("Loading library failed")
        handles = (ct.POINTER(ct.c_void_p))()
        handle_count = ct.c_int()
        
        if self.dll.EnsembleConnect(ct.byref(handles), ct.byref(handle_count) ):
            print("Delay Line connected")
            
            self.handle=handles[0]
            self.handle_count = handle_count.value
            print("handle:",self.handle," ","number of axis:",self.handle_count)
        self.dll.EnsembleMotionEnable(self.handle,ct.c_int(self.handle_count))
        self.dll.EnsembleMotionWaitMode(self.handle, ct.c_int(0))
    
    @remote
    def home(self, in_background=False):
        if not in_background:
            self.dll.EnsembleMotionHome(self.handle,ct.c_int(self.handle_count))
        else:
            try:
                if self._homing_thread.isAlive():
                    return
                else:
                    self._homing_thread.join()
            except:
                pass
            self._homing_thread = threading.Thread(target = lambda : self.home(in_background=False) )
            self._homing_thread.start()
         
    @remote   
    def moveInc(self,distance):
        self.cdistance=ct.c_double *2
        self.cspeed=ct.c_double *2
        
        self.dll.EnsembleMotionMoveInc(self.handle, ct.c_int(self.handle_count), self.cdistance(distance,0), self.cspeed(10,0))
        
        #if wait:
        #    self.dll.EnsembleMotionWaitForMotionDone(self.handle, ct.c_int(self.handle_count) ,ct.c_int(0), ct.c_int(-1), None)
        
    @remote
    def move(self,distance):
        self.cdistance=ct.c_double *2
        self.cspeed=ct.c_double *2
        
        self.dll.EnsembleMotionMoveAbs(self.handle, ct.c_int(self.handle_count), self.cdistance(distance,0), self.cspeed(10,0))
        
        #if wait:
        #    self.dll.EnsembleMotionWaitForMotionDone(self.handle, ct.c_int(self.handle_count) ,ct.c_int(0), ct.c_int(-1), None)
          
    @remote 
    def position(self):
        position=ct.c_double()
        self.dll.EnsembleStatusGetItem(self.handle, ct.c_int(0), ct.c_int(1), ct.byref(position))
        return position.value
       
    def status(self):
        d={}
        d["position"]=self.position()
        return d
    


@include_remote_methods(DelayLineWorker)
class DelayLine(DeviceOverZeroMQ):
    def __init__(self, req_port = default_req_port, pub_port = default_pub_port, **kwargs):
        super().__init__(req_port = req_port, pub_port = pub_port, **kwargs)
            
    def createDock(self, parentWidget, menu = None):
        """ Function for integration in GUI app.  """
        dock = QtWidgets.QDockWidget("Delay Line Aerotech", parentWidget)
        widget = QtWidgets.QWidget(dock)
        self.layout = QtWidgets.QVBoxLayout(parentWidget)
        
        btn=QtWidgets.QPushButton(str(round(self.position(), 3)), widget)
        
        grid = QtWidgets.QGridLayout()
        widget.setLayout(grid)
        grid.addWidget(btn, 0, 0)
        
        
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        
        
        grid2 = QtWidgets.QGridLayout()
        delayline = QtWidgets.QWidget(dock)
        delayline.setLayout(grid2)
        btn1=QtWidgets.QPushButton(">>", delayline)
        btn2=QtWidgets.QPushButton("<<", delayline)
        btn3=QtWidgets.QLineEdit("5", delayline)
        btn4=QtWidgets.QPushButton("Minimalize", delayline)
        btn6=QtWidgets.QPushButton("Home", delayline)
        btn1.setFixedWidth(38)
        btn2.setFixedWidth(38)
        btn3.setFixedWidth(28)
        btn6.setFixedWidth(38)
        btn4.setFixedWidth(74)
        positionBox=QtWidgets.QLineEdit(delayline)
        positionBox.setText("Pos.: " + str(self.position()))
        positionBox.setFixedWidth(116)
        grid2.addWidget(positionBox, 0, 0, 1, 3)
        grid2.addWidget(btn1, 1, 0)
        grid2.addWidget(btn2, 1, 1)
        grid2.addWidget(btn3, 1, 2)
        grid2.addWidget(btn4, 2, 1, 1, 2)
        grid2.addWidget(btn6, 2, 0)
        
        delayline.hide()
        
        def hideDialog():
            dock.setWidget(widget)
            
        def skipp():
            try:
                self.moveInc(float(btn3.text()))
                
            except:
                print("Wrong value")
                
        def refresh(status):
            try:
                positionBox.setText("Pos.: %.6f" % status["position"])
                btn.setText("%.6f" % status["position"])
            except:
                pass
        def skipm():
            try:
                self.moveInc(-float(btn3.text()))
                
            except:
                print("Wrong value")
        
        def home():
            self.home(in_background=True)
            
        def showDialog():
            dock.setWidget(delayline)
        
        btn.clicked.connect(showDialog)
        btn1.clicked.connect(skipp)
        btn2.clicked.connect(skipm)
        btn4.clicked.connect(hideDialog)
        #btn5.clicked.connect(refresh)
        btn6.clicked.connect(home)
        
        def goToDialog(this):
            pos,ok = QtWidgets.QInputDialog.getDouble(None, "Go to position", "New position:", 0, -500, 500, 3)
            if ok:
                    QtWidgets.QApplication.processEvents()
                    self.move(pos)
                    
        positionBox.mousePressEvent = goToDialog
        if menu:
            menu.addAction(dock.toggleViewAction())
        
        self.createListenerThread(refresh)
        
             
        
   
        
    
   
    
    
   

    

