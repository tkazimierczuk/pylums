# -*- coding: utf-8 -*-
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets,QtCore
from PyQt5.QtWidgets import (QPushButton, QMessageBox, QDialog)
from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow)
from PyQt5.QtWidgets import (QVBoxLayout, QHBoxLayout, QLineEdit)
from PyQt5.QtWidgets import (QLabel, QInputDialog)
from PyQt5.QtGui import (QFont, QColor)
import scipy.optimize
import numpy as np


class PiDelayLineWorker(DeviceWorker):
    def __init__(self, port, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.port = port
        
    def init_device(self):
        import serial
        self.ser = serial.Serial(self.port, 9600, timeout=2)  #opens serial port COM3
        print("Checking communication: " + str(self.check_communication()))
        
    def __del__(self):
        self.ser.close() #serial port close
        
    def status(self):
        d = super().status()
        d["position"] = self.position
        d["status"] = self.get_stage_status()
        return d
        
    def check_communication(self):
        pass # TODO in future

    @remote
    def _send_command(self, command, reply_expected=False, retry_attempts = 5):
        bcommand = command.encode('ascii') + b'\n'
        self.ser.write(bcommand)
        echo_reply = self.ser.readline()
        if echo_reply != bcommand:
            if retry_attempts > 0:
                self.ser.flushInput()
                return _send_command(command,  reply_expected, retry_attempts-1)
            else:
                raise IOError("Wrong echo!")
        
        if  reply_expected:
            return self.ser.readline().strip()
        

        
    @property
    def position(self):
        pos_str = self._send_command('1tp', reply_expected=True)
        return int(pos_str[4:]) / 1e4

    @position.setter
    def position(self, value):
        self._send_command('1ma'+str(round(value*1e4)))
    
    
    @remote
    def find_edge(self):
        self._send_command('1fe2')
    
    @remote
    def define_home(self):
        self._send_command('1dh')
        
    @remote
    def stop(self):
        self._send_command('1st')
        
    @remote
    def is_moving(self):
        return int(self._send_command('TV'))!=0

    @remote
    def move_position(self, value, relative=False):
        if relative:
            self._send_command('1mr'+str(round(value*1e4)))
        else:
            self.position = value
        
    @remote
    def get_position(self):
        return self.position

    @remote
    def get_stage_status(self):
        status_str = self._send_command('1ts', reply_expected=True)
        n = int(status_str[4:])
        return {'moving': bool(n&1), 'pos_limit': bool(n&4), 'neg_limit': bool(n&8)}

    @remote
    def is_moving(self):
        return self.get_stage_status()['moving']
    
    

    
@include_remote_methods(PiDelayLineWorker)
class PiDelayLine(DeviceOverZeroMQ):
    def __init__(self, req_port, pub_port, 
                 display_decimal_places=4, **kwargs):
        super().__init__(req_port = req_port, pub_port = pub_port, **kwargs)
        # custom initialization here
        self.widgets = {}
        try:
            self._display_fmt = "%."+str(display_decimal_places)+"f"
            self._display_fmt % 0.5 # raises exception if the format is incorrect
        except:
            self._display_fmt = "%.5f"
        

    def createDock(self, parentWidget, menu = None):
        """ Function for integration in GUI app.  """
        dock = QtWidgets.QDockWidget("PI delay line", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        self.layout = QtWidgets.QVBoxLayout(parentWidget)
        widget.setLayout(self.layout)
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

        hlayout = QtWidgets.QHBoxLayout()
        display = QtWidgets.QLCDNumber()
        display.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        display.setDigitCount( 9) 
        display.display("-")
        hlayout.addWidget(display)

        stop_button = QtWidgets.QPushButton("Stop")
        stop_button.setFixedWidth(50)
        def stop_clicked():
            self.stop()
        stop_button.clicked.connect(stop_clicked)
        hlayout.addWidget(stop_button)
        
        edge_button = QtWidgets.QPushButton("Find edge")
        edge_button.setFixedWidth(70)
        def edge_clicked():
            self.find_edge()
        edge_button.clicked.connect(edge_clicked)
        hlayout.addWidget(edge_button)
        
        home_button = QtWidgets.QPushButton("Define home")
        home_button.setFixedWidth(70)
        def home_clicked():
            self.define_home()
        home_button.clicked.connect(home_clicked)
        hlayout.addWidget(home_button)

        hlayout.addStretch(3)
        self.layout.addLayout(hlayout)
        self.display = display

        def on_click(event):
            if event.button() == 1:
                current = self.get_position()
                d, okPressed = QtWidgets.QInputDialog.getDouble(display, "Go to", "Target:", current, -300, 300)
                if okPressed:
                    self.move_position(d)
        display.mousePressEvent  = on_click            
        
        
    def updateSlot(self, status):
        try:
            self.display.display(self._display_fmt % status["position"])
        except:
            pass
