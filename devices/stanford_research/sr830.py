# Based on the code:
# SR830.py, Stanford Research 830 DSP lock-in driver
# Katja Nowack, Stevan Nadj-Perge, Arjan Beukman, Reinier Heeres
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import pyvisa as visa
import types
import logging
import time
from datetime import datetime
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from devices import Parameter
from PyQt5 import QtWidgets,QtCore
import time
import threading

default_req_port = 37123
default_pub_port = 37124

class SR830Worker(DeviceWorker):
    def __init__(self, req_port = default_req_port, pub_port = default_pub_port, address="GPIB8", reset=False, **kwargs):
        super().__init__(req_port = req_port, pub_port = pub_port, **kwargs)
        self._address = address
        
    IGPIB = 0
    IRS232 = 1
    IETHERNET = 2
        
    def init_device(self):
        print(str(datetime.now()))
        print("Initialization...")
        self._visa_lock = threading.Lock()
        self._connect()
        print("Communication handler created")
        
        print("End of initialization")
       
    def _connect(self):
        print("Connecting...")
        rm = visa.ResourceManager()
        if self._address[0:3] == 'COM':
            self._interface = SR830Worker.IRS232
            self.visa_handle = rm.open_resource("ASRL%s::INSTR" % self._address[3:])
            self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_STOP_BITS,
                                                visa.constants.VI_ASRL_STOP_TWO)
        elif self._address[0:4] == 'GPIB':
            self._interface = SR830Worker.IGPIB
            self.visa_handle = rm.open_resource("GPIB::%s" % self._address[4:])
            self.visa_handle.clear()
            time.sleep(0.2)
        else:
            self._interface = SR830Worker.IETHERNET
            self.visa_handle = rm.open_resource("TCPIP::%s::7020::SOCKET" % self._address)
            self.visa_handle.write_termination = '\n'
            
        self.visa_handle.read_termination = '\n'
        self.visa_handle.timeout = 1000
    
    def status(self):
        d = super().status()

        d['sensitivity'] = self.get_sensitivity()
        d['tau'] = self.get_tau()
        d['frequency'] = self.get_frequency()
        d['amplitude'] = self.get_amplitude()
        d['phase'] = self.get_phase()
        d['X'] = self.get_X()
        d['Y'] = self.get_Y()
        d['R'] = self.get_R()
        d['P'] = self.get_P()
        d['ref_input'] = self.get_ref_input()
        d['ext_trigger'] = self.get_ext_trigger()
        d['sync_filer'] = self.get_sync_filter()
        d['harmonic'] = self.get_harmonic()
        d['input_config'] = self.get_input_config()
        d['input_shield'] = self.get_input_shield()
        d['input_coupling'] = self.get_input_coupling()
        d['notch_filter'] = self.get_notch_filter()
        d['reserve'] = self.get_reserve()
        d['filter_slope'] = self.get_filter_slope()
        d['unlocked'] = self.get_unlocked()
        d['input_overload'] = self.get_input_overload()
        d['time_constant_overload'] = self.get_time_constant_overload()
        d['output_overload'] = self.get_output_overload()

        return d

    @remote
    def _execute(self, message, responseExpected=False, reply_pattern=None, allowed_retries=5):
        try:
            with self._visa_lock:
                self.visa_handle.write(message)
                if not responseExpected:
                    return None
                res = self.visa_handle.read_raw().decode('ascii')
                self.is_offline = False
        except visa.VisaIOError:
            print(traceback.format_exc())
            if allowed_retries > 0:
                with self._visa_lock:
                    self._connect()
                return self._execute(message, reply_pattern, allowed_retries - 1)
            else:
                self.is_offline = True
                res = ''
                warnings.warn("Communication error occured on message: %s" % message, ResourceWarning)

        res = res.strip()
        if reply_pattern:
            if not re.match(pattern=reply_pattern, string=res):
                if allowed_retries > 0:
                    print("Warning! Wrong reply (Q:%s, R:%s)" % (message, res))
                    return self._execute(message, reply_pattern, allowed_retries - 1)
                else:
                    raise Exception("Wrong reply (Q:%s, R:%s)" % (message, res))

        return res
    
            
    # Functions
    @remote
    def reset(self):
        '''
        Resets the instrument to default values

        Input:
            None

        Output:
            None
        '''
        logging.info(__name__ + ' : Resetting instrument')
        self._execute('*RST')

    @remote
    def disable_front_panel(self):
        '''
        enables the front panel of the lock-in
        while being in remote control
        '''
        self._execute('OVRM 0')

    @remote
    def enable_front_panel(self):
        '''
        enables the front panel of the lock-in
        while being in remote control
        '''
        self._execute('OVRM 1')

    @remote
    def auto_phase(self):
        '''
        offsets the phase so that
        the Y component is zero
        '''
        self._execute('APHS')

    @remote
    def direct_output(self):
        '''
        select GPIB as interface
        '''
        self._execute('OUTX 1')

    @remote
    def read_output(self,output, ovl):
        '''
        Read out R,X,Y or phase (P) of the Lock-In

        Input:
            mode (int) :
            1 : "X",
            2 : "Y",
            3 : "R"
            4 : "P"

        '''
        parameters = {
        1 : "X",
        2 : "Y",
        3 : "R",
        4 : "P"
        }
        self.direct_output()
        if parameters.__contains__(output):
            logging.info(__name__ + ' : Reading parameter from instrument: %s ' %parameters.get(output))
            if ovl:
                self.get_input_overload()
                self.get_time_constant_overload()
                self.get_output_overload()
            readvalue = float(self._execute('OUTP?%s' %output, responseExpected=True))
        else:
            print ('Wrong output requested.')
        return readvalue

    @remote
    def get_X(self, ovl=False):
        '''
        Read out X of the Lock In
        Check for overloads if ovl is True
        '''
        return self.read_output(1, ovl)

    @remote
    def get_Y(self, ovl=False):
        '''
        Read out Y of the Lock In
        Check for overloads if ovl is True
        '''
        return self.read_output(2, ovl)

    @remote
    def get_R(self, ovl=False):
        '''
        Read out R of the Lock In
        Check for overloads if ovl is True
        '''
        return self.read_output(3, ovl)

    @remote
    def get_P(self, ovl=False):
        '''
        Read out P of the Lock In
        Check for overloads if ovl is True
        '''
        return self.read_output(4, ovl)

    @remote
    def set_frequency(self, frequency):
        '''
        Set frequency of the local oscillator

        Input:
            frequency (float) : frequency in Hz

        Output:
            None
        '''
        self._execute('FREQ %e' % frequency)


    @remote
    def get_frequency(self):
        '''
        Get the frequency of the local oscillator

        Input:
            None

        Output:
            frequency (float) : frequency in Hz
        '''
        self.direct_output()
        return float(self._execute('FREQ?', responseExpected=True))

    @remote
    def get_amplitude(self):
        '''
        Get the frequency of the local oscillator

        Input:
            None

        Output:
            frequency (float) : frequency in Hz
        '''
        self.direct_output()
        return float(self._execute('SLVL?', responseExpected=True))

    @remote
    def set_mode(self,val):
        self._execute('FMOD %d' %val)


    @remote
    def set_amplitude(self, amplitude):
        '''
        Set frequency of the local oscillator

        Input:
            frequency (float) : frequency in Hz

        Output:
            None
        '''
        self._execute('SLVL %e' % amplitude)


    @remote
    def set_tau(self,timeconstant):
        '''
        Set the time constant of the LockIn

        Input:
            time constant (integer) : integer from 0 to 19

        Output:
            None
        '''

        self.direct_output()
        self._execute('OFLT %s' % timeconstant)

    @remote
    def get_tau(self):
        '''
        Get the time constant of the LockIn

        Input:
            None
        Output:
            time constant (integer) : integer from 0 to 19
        '''

        self.direct_output()
        return float(self._execute('OFLT?', responseExpected=True))

    @remote
    def set_sensitivity(self, sens):
        '''
        Set the sensitivity of the LockIn

        Input:
            sensitivity (integer) : integer from 0 to 26

        Output:
            None
        '''

        self.direct_output()
        self._execute('SENS %d' % sens)

    @remote
    def get_sensitivity(self):
        '''
        Get the sensitivity
            Output:
            sensitivity (integer) : integer from 0 to 26
        '''
        self.direct_output()
        return float(self._execute('SENS?', responseExpected=True))

    
    @remote
    def get_phase(self):
        '''
        Get the reference phase shift

        Input:
            None

        Output:
            phase (float) : reference phase shit in degree
        '''
        self.direct_output()
        return float(self._execute('PHAS?', responseExpected=True))


    @remote
    def set_phase(self, phase):
        '''
        Set the reference phase shift

        Input:
            phase (float) : reference phase shit in degree

        Output:
            None
        '''
        self._execute('PHAS %e' % phase)

    @remote
    def set_aux(self, output, value):
        '''
        Set the voltage on the aux output
        Input:
            output - number 1-4 (defining which output you are addressing)
            value  - the voltage in Volts
        Output:
            None
        '''
        self._execute('AUXV %(out)i, %(val).3f' % {'out':output,'val':value})

    @remote
    def read_aux(self, output):
        '''
        Query the voltage on the aux output
        Input:
            output - number 1-4 (defining which output you are addressing)
        Output:
            voltage on the output D/A converter
        '''
        return float(self._execute('AUXV? %i' %output, responseExpected=True))

    @remote
    def get_oaux(self, value):
        '''
        Query the voltage on the aux output
        Input:
            output - number 1-4 (defining which output you are adressing)
        Output:
            voltage on the input A/D converter
        '''
        return float(self._execute('OAUX? %i' %value, responseExpected=True))

    @remote
    def set_out(self, value, channel):
        '''
        Set output voltage, rounded to nearest mV.
        '''
        self.set_aux(channel, value)

    @remote
    def get_out(self, channel):
        '''
        Read output voltage.
        '''
        return self.read_aux(channel)

    @remote
    def get_in(self, channel):
        '''
        Read input voltage, resolution is 1/3 mV.
        '''
        return self.get_oaux(channel)

    @remote
    def get_ref_input(self):
        '''
        Query reference input: internal (true,1) or external (false,0)
        '''
        return int(self._execute('FMOD?', responseExpected=True))==1
        
    @remote
    def set_ref_input(self, value):
        '''
        Set reference input: internal (true,1) or external (false,0)
        '''
        if value:
            self._execute('FMOD 1')
        else:
            self._execute('FMOD 0')

    @remote
    def get_ext_trigger(self):
        '''
        Query trigger source for external reference: sine (0), TTL rising edge (1), TTL falling edge (2)
        '''
        return int(self._execute('RSLP?', responseExpected=True))

    @remote
    def set_ext_trigger(self, value):
        '''
        Set trigger source for external reference: sine (0), TTL rising edge (1), TTL falling edge (2)
        '''
        self._execute('RSLP '+ str(value))

    @remote
    def get_sync_filter(self):
        '''
        Query sync filter. Note: only available below 200Hz
        '''
        return int(self._execute('SYNC?', responseExpected=True))==1

    @remote
    def set_sync_filter(self, value):
        '''
        Set sync filter. Note: only available below 200Hz
        '''
        if value:
            self._execute('SYNC 1')
        else:
            self._execute('SYNC 0')

    @remote
    def get_harmonic(self):
        '''
        Query detection harmonic in the range of 1..19999.
        Note: frequency*harmonic<102kHz
        '''
        return int(self._execute('HARM?', responseExpected=True))

    @remote
    def set_harmonic(self, value):
        '''
        Set detection harmonic in the range of 1..19999.
        Note: frequency*harmonic<102kHz
        '''
        self._execute('HARM '+ str(value))

    @remote
    def get_input_config(self):
        '''
        Query input configuration: A (0), A-B (1), CVC 1MOhm (2), CVC 100MOhm (3)
        '''
        return int(self._execute('ISRC?', responseExpected=True))

    @remote
    def set_input_config(self, value):
        '''
        Set input configuration: A (0), A-B (1), CVC 1MOhm (2), CVC 100MOhm (3)
        '''
        self._execute('ISRC '+ str(value))

    @remote
    def get_input_shield(self):
        '''
        Query input shield: float (false,0), gnd (true,1)
        '''
        return int(self._execute('IGND?', responseExpected=True))==1

    @remote
    def set_input_shield(self, value):
        '''
        Set input shield: float (false,0), gnd (true,1)
        '''
        if value:
            self._execute('IGND 1')
        else:
            self._execute('IGND 0')

    @remote
    def get_input_coupling(self):
        '''
        Query input coupling: AC (false,0), DC (true,1)
        '''
        return int(self._execute('ICPL?', responseExpected=True))==1

    @remote
    def set_input_coupling(self, value):
        '''
        Set input coupling: AC (false,0), DC (true,1)
        '''
        if value:
            self._execute('ICPL 1')
        else:
            self._execute('ICPL 0')

    @remote
    def get_notch_filter(self):
        '''
        Query notch filter: none (0), 1xline (1), 2xline(2), both (3)
        '''
        return int(self._execute('ILIN?', responseExpected=True))

    @remote
    def set_notch_filter(self, value):
        '''
        Set notch filter: none (0), 1xline (1), 2xline(2), both (3)
        '''
        self._execute('ILIN ' + str(value))

    @remote
    def get_reserve(self):
        '''
        Query reserve: High reserve (0), Normal (1), Low noise (2)
        '''
        return int(self._execute('RMOD?', responseExpected=True))

    @remote
    def set_reserve(self, value):
        '''
        Set reserve: High reserve (0), Normal (1), Low noise (2)
        '''
        self._execute('RMOD ' + str(value))

    @remote
    def get_filter_slope(self):
        '''
        Query filter slope: 6dB/oct. (0), 12dB/oct. (1), 18dB/oct. (2), 24dB/oct. (3)
        '''
        return int(self._execute('OFSL?', responseExpected=True))

    @remote
    def set_filter_slope(self, value):
        '''
        Set filter slope: 6dB/oct. (0), 12dB/oct. (1), 18dB/oct. (2), 24dB/oct. (3)
        '''
        self._execute('OFSL ' + str(value))
    
    @remote
    def get_unlocked(self, update=True):
        '''
        Query if PLL is locked.
        Note: the status bit will be cleared after readout!
        Set update to True for querying present unlock situation, False for querying past events
        '''
        if update:
           self._execute('LIAS? 3', responseExpected=True)     #for realtime detection we clear the bit by reading it
           time.sleep(0.02)                        #and wait for a little while so that it can be set
        return int(self._execute('LIAS? 3', responseExpected=True))==1

    @remote
    def get_input_overload(self, update=True):
        '''
        Query if input or amplifier is in overload.
        Note: the status bit will be cleared after readout!
        Set update to True for querying present overload, False for querying past events
        '''
        if update:
            self._execute('LIAS? 0', responseExpected=True)     #for realtime detection we clear the bit by reading it
            time.sleep(0.02)                        #and wait for a little while so that it can be set again
        return int(self._execute('LIAS? 0', responseExpected=True))==1

    @remote
    def get_time_constant_overload(self, update=True):
        '''
        Query if filter is in overload.
        Note: the status bit will be cleared after readout!
        Set update to True for querying present overload, False for querying past events
        '''
        if update:
            self._execute('LIAS? 1', responseExpected=True)     #for realtime detection we clear the bit by reading it
            time.sleep(0.02)                        #and wait for a little while so that it can be set again
        return int(self._execute('LIAS? 1', responseExpected=True))==1

    @remote
    def get_output_overload(self, update=True):
        '''
        Query if output (also main display) is in overload.
        Note: the status bit will be cleared after readout!
        Set update to True for querying present overload, False for querying past events
        '''
        if update:
            self._execute('LIAS? 2', responseExpected=True)     #for realtime detection we clear the bit by reading it
            time.sleep(0.02)                        #and wait for a little while so that it can be set again
        return int(self._execute('LIAS? 2', responseExpected=True))==1


@include_remote_methods(SR830Worker)
class SR830(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def createDock(self, parentWidget, menu=None):
        pass