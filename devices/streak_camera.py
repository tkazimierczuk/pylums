from datetime import datetime
import time
from time import sleep
from time import perf_counter as clock
import pyvisa as visa
import threading
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5.QtWidgets import (QWidget, QPushButton, 
    QHBoxLayout, QVBoxLayout, QApplication)
from PyQt5 import QtWidgets, QtCore, QtGui
import functools
import socket

default_req_port = 11000
default_pub_port = 11001

class StreakCameraWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, address="127.1.1.1", comm_port="1001", data_port="1002", **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

        self._address = address
        self._comm_port = int(comm_port)
        self._data_port = int(data_port)
   
    def status(self):
        d = super().status()
        return d
    
    def init_device(self):
        """
        Opens the communication 
        """
        print("=========")
        self._connect()

    def _connect(self, allowed_retries=1):
        try:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.settimeout(1.5)
            # self._socket_data = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._socket.connect((self._address, self._comm_port))
            # self._socket_data.connect((self._address, self._data_port))
            print("Socket connected...")
            reply = self._socket.recv(4096)
            if reply == b'RemoteEx Ready\r':
                print("Handshake received.")
                return
        except Exception as e:
            if allowed_retries == 0:
                raise e
            else:
                print(e)
        print("Error - retrying...")
        return self._connect(allowed_retries - 1)

    @remote
    def _execute(self, command, allowed_retries=10):
        if type(command) is str:
            command = command.encode('utf-8')
        try:
            self._socket.sendall(command)
            reply = self._socket.recv(65000)
            for part in reply.split(b'\r'):
                #print([part, command.split(b'(')[0], -1 != part.find(command.split(b'(')[0])])
                if -1 != part.find(command.split(b'(')[0]):
                    return str(part)
        except Exception as e:
            if allowed_retries == 0:
                raise e
            else:
                print(e)
            self._socket.close()
            sleep(0.2)
            self._connect(allowed_retries=0)
        sleep(0.1)
        if allowed_retries == 0:
            raise Exception("Could not get answer for command %s !", command.decode())
        return self._execute(command, allowed_retries - 1)

    @remote
    def is_stopped(self):
        result = self._execute("AcqStatus()\r")
        if result.find("idle") != -1:
            return True
        else:
            return False

    @remote
    def start_photon_counting(self):
        self.stop()
        self._execute("AcqStart(PC)\r")

    @remote
    def start_analog_integration(self):
        self.stop()
        self._execute("AcqStart(AI)\r")

    @remote
    def stop(self):
        if not self.is_stopped():
            self._execute("AcqStop()\r")

    @remote
    def save(self, filename):
        self.stop()
        result = self._execute("ImgSave(Current,Img,%s,1)\r" % filename)
        if -1 != result.find("invalid file name"):
            raise Exception(result)


@include_remote_methods(StreakCameraWorker)
class StreakCamera(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def createDock(self, parentWidget, menu=None):
        pass

    def updateSlot(self,status):
        pass
