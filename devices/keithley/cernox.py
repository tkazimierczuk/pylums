from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from devices.keithley.keithley2400 import Keithley2400Worker
from PyQt5 import QtWidgets,QtCore
import traceback
import threading
import jsonpickle
import numpy as np
from time import sleep

default_req_port = 13150
default_pub_port = 13151

class CernoxWorker(Keithley2400Worker):
    def __init__(self, calibration_file, req_port=default_req_port, pub_port=default_pub_port,
                 refresh_rate=0.25, port="COM1", **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, refresh_rate=refresh_rate, port=port, **kwargs)
        self.lock = threading.Lock()

        parameters = jsonpickle.decode(open("config/" + calibration_file).read())
        X1, X2, coeffs = parameters[0], parameters[1], parameters[2:]

        def calc(R):
            return 10 ** np.polynomial.chebyshev.chebval((np.log10(R) - X1) / (X2 - X1), coeffs)
        self.calculator = calc
        self.calculator(100)
        print("Calibration file loaded: " + calibration_file)

        self.resistance = 0
        self.measure_current = 2e-6

    def loop(self):
        print("ON/OFF differential measurement thread started")
        while True:
            self.set_current(0)
            sleep(0.5)
            status0 = super().status()
            self.set_current(self.measure_current)
            sleep(0.5)
            status1 = super().status()
            volt = status1["voltage"] - status0["voltage"]
            resistance = volt / self.measure_current

            if volt > 3e-3 and self.measure_current > 1e-7:
                measure_current = self.measure_current / 2
            elif volt < 1e-3 and self.measure_current < 20e-6:
                measure_current = self.measure_current * 2
            else:
                measure_current = self.measure_current

            with self.lock:
                self.resistance = resistance
                self.measure_current = measure_current


    def init_device(self):
        super().init_device()

        thread = threading.Thread(target=self.loop, args=())
        thread.start()
        
    def status(self):
        d = {}
        with self.lock:
            d['resistance'] = self.resistance
            d['current'] = self.measure_current
        if d['resistance'] > 0:
            d['temperature'] = self.calculator(d['resistance'])
        else:
            d['temperature'] = 999.9
        return d
     
@include_remote_methods(CernoxWorker)
class Cernox(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self._mode = None


    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("Keithley 2400", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QGridLayout()
        widget.setLayout(layout)

        self.edit_curr = QtWidgets.QLineEdit()
        self.edit_res = QtWidgets.QLineEdit()
        self.edit_temp = QtWidgets.QLineEdit()
        self.edit_curr.setMinimumWidth(100)

        layout.addWidget(self.edit_curr, 0, 0, 1, 1)
        layout.addWidget(self.edit_res, 1, 0, 1, 1)
        layout.addWidget(self.edit_temp, 2, 0, 1, 1)

        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

    def updateSlot(self, status):
        self.edit_curr.setText(f"{1e6 * status['current'] : .3f} uA")
        self.edit_res.setText(f"{status['resistance'] : .2f} Ω")
        self.edit_temp.setText(f"{status['temperature'] : .3f} K")




