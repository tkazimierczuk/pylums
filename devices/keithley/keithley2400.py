from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets,QtCore
import traceback

default_req_port = 12020
default_pub_port = 12021

class Keithley2400Worker(DeviceWorker):

    def __init__(self, address, req_port=default_req_port, pub_port=default_pub_port,
                 refresh_rate=0.25, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, refresh_rate=refresh_rate, **kwargs)
        #
        # def __init__(self, address, **kwargs):
        #     filtered_kwargs = {}
        #     for name in ('pub_port', 'req_port'):
        #         if name in kwargs:
        #             filtered_kwargs[name] = kwargs[name]
        # super().__init__(**filtered_kwargs)
        self.active = True
        self.address = address
        if self.address[:3] == "COM":
            self.address = f"ASRL{self.address[3:]}::INSTR"
        if self.address[:4] == "GPIB":
            self.address = f"GPIB::{self.address[4:]}::INSTR"
        
    def init_device(self):
        import qcodes.instrument_drivers.tektronix.Keithley_2400
        print("Starting")
        self.dev = qcodes.instrument_drivers.tektronix.Keithley_2400.Keithley_2400( \
                     name='keithley', address=self.address, timeout=0.5)
        try:
            self.dev.visa_handle.baud = 57600  # in case it is serial device
        except:
            pass
        print("...")
        self.dev.timeout.set(2)
        print("OK")
        
        
    def status(self):
        d = super().status()
        if self.active:
            d['mode'] = self.dev.mode()
            msg = self.dev._get_read_output_protected()
            fields = [float(x) for x in msg.split(',')]
            d['voltage'] = fields[0]
            d['current'] = fields[1]
            d['resistance'] = d['voltage'] / d['current'] if d['current'] != 0 else 0
        return d
        
    @remote
    def set_current(self, value):
        self.dev.curr(float(value))
       
    @remote
    def set_voltage(self, value):
        self.dev.volt(float(value))
       
    @remote
    def set_local(self, local=True):
        self.active = not local

    @remote
    def _step(self, step):
        mode = self.dev.mode()
        if mode == 'VOLT':
            axis = self.dev.volt
        elif mode == 'CURR':
            axis = self.dev.curr
            step = step / 1000
        else:
            return

        axis(axis() + step)
     
@include_remote_methods(Keithley2400Worker)
class Keithley2400(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self._mode = None


    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("Keithley 2400", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QGridLayout()
        widget.setLayout(layout)

        self.edit_vol = QtWidgets.QLineEdit()
        self.edit_cur = QtWidgets.QLineEdit()
        self.edit_res = QtWidgets.QLineEdit()
        self.edit_vol.setMinimumWidth(100)
        self.edit_vol.setEnabled(False)
        self.edit_cur.setEnabled(False)
        self.edit_res.setEnabled(False)

        def safe_execute(function):
            def fun(*args, **kwargs):
                try:
                    function(*args, **kwargs)
                except Exception:
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Critical)
                    msg.setText(traceback.format_exc())
                    msg.exec_()
            return fun

        self.edit_cur.mousePressEvent = safe_execute(self._current_dialog)
        self.edit_vol.mousePressEvent = safe_execute(self._voltage_dialog)


        self.edit_step = QtWidgets.QLineEdit("0.001")
        self.edit_step.setFixedWidth(60)
        self.button_up = QtWidgets.QPushButton("▲")
        self.button_down = QtWidgets.QPushButton("▼")
        self.button_up.clicked.connect(safe_execute(lambda state: self._step(float(self.edit_step.text()))))
        self.button_down.clicked.connect(safe_execute(lambda state: self._step(-float(self.edit_step.text()))))
        self.button_up.setFixedWidth(60)
        self.button_down.setFixedWidth(60)

        layout.addWidget(self.edit_vol, 0, 0, 1, 1)
        layout.addWidget(self.edit_cur, 1, 0, 1, 1)
        layout.addWidget(self.edit_res, 2, 0, 1, 1)
        layout.addWidget(self.button_up, 0, 1, 1, 1)
        layout.addWidget(self.edit_step, 1, 1, 1, 1)
        layout.addWidget(self.button_down, 2, 1, 1, 1)

        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

    def updateSlot(self, status):
        self._mode = status['mode']
        if self._mode == 'VOLT':
            self.edit_vol.setEnabled(True)
            self.edit_cur.setEnabled(False)
        if self._mode == 'CURR':
            self.edit_vol.setEnabled(False)
            self.edit_cur.setEnabled(True)
        self.edit_vol.setText(f"{status['voltage'] : .7f} V")
        self.edit_cur.setText(f"{1000 * status['current'] : .7f} mA")
        self.edit_res.setText(f"{status['resistance'] : .5f} Ω")

    def _voltage_dialog(self, event):
        if event.button() == 1 and self.edit_vol.isEnabled():
            value = float(self.edit_vol.text().split()[0].strip())
            d, okPressed = QtWidgets.QInputDialog.getDouble(self.edit_vol, "Set Voltage", "Target (V):",
                                                            value, decimals=7)
            if okPressed:
                self.set_voltage(d)

    def _current_dialog(self, event):
        if event.button() == 1 and self.edit_cur.isEnabled():
            value = float(self.edit_cur.text().split()[0].strip())
            d, okPressed = QtWidgets.QInputDialog.getDouble(self.edit_cur, "Set Current", "Target (mA):",
                                                            value, decimals=7)
            if okPressed:
                self.set_current(d / 1000)






