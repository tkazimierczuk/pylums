from time import sleep
import pyvisa as visa
from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
from PyQt5 import QtWidgets, QtCore, QtGui
import warnings
import traceback
import re

default_req_port = 17100
default_pub_port = 17101


class Keithley2450Worker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, address="COM1",
                 refresh_rate=0.25, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, refresh_rate=refresh_rate, **kwargs)

        self._address = address

        self.is_offline = False

    IGPIB = 0
    IRS232 = 1
    IETHERNET = 2
    IUSB = 3
    
    def _connect(self):
        print(f"Connecting to device at {self._address}")
        rm = visa.ResourceManager()

        if self._address[0:4] == 'GPIB':
            self._interface = Keithley2450Worker.IGPIB
            if "::" not in self._address:
                self.visa_handle = rm.open_resource("GPIB::%s" % self._address[4:])
            else:
                self.visa_handle = rm.open_resource(self._address)
            self.visa_handle.clear()
            sleep(0.2)
        elif self._address[0:3] == 'USB':
            self._interface = Keithley2450Worker.IUSB
            self.visa_handle = rm.open_resource(self._address)
            self.visa_handle.clear()
            sleep(0.2)
        else:
            self._interface = Keithley2450Worker.IETHERNET
            self.visa_handle = rm.open_resource("TCPIP::%s::7020::SOCKET" % self._address)
            self.visa_handle.write_termination = '\r'
            
        self.visa_handle.read_termination = '\r'
        self.visa_handle.timeout = 1000

    def init_device(self):
        self._connect()
        self.execute("smu.source.readback = smu.ON\nprint('')")
        model = self.execute("print(localnode.model)")
        version = self.execute("print(localnode.version)")
        serial = self.execute("print(localnode.serialno)")
        print(f"Succesfully connected to the Keithley {model}, v.{version}, serial no.: {serial}.")

    @remote
    def get_all(self):
        sense_mode, source_mode = self.execute("print(smu.measure.func, smu.source.func)").split()
        values = self.execute(
            "buff = buffer.make(10)\n"
            "smu.measure.count = 1\n"
            "smu.measure.read(buff)\n"
            "printbuffer(1, 1, buff.sourcevalues, buff)\n"
            ).split(',')
        source_val, sense_val = float(values[0]), float(values[1])

        current, voltage = None, None
        if 'CURRENT' in source_mode:
            current = source_val
        if 'VOLTAGE' in source_mode:
            voltage = source_val
        if 'CURRENT' in sense_mode:
            current = sense_val
        if 'VOLTAGE' in sense_mode:
            voltage = sense_val
        if 'RESISTANCE' in sense_mode:
            if current is not None:
                voltage = current * sense_val
            else:
                current = voltage / sense_val
        if voltage is None:
            raise(ValueError("Device is sourcing constant current and measuring current - "
                             "no voltage reading is performed!"))
        if current is None:
            raise(ValueError("Device is sourcing constant voltage and measuring voltage - "
                             "no current reading is performed!"))
        return voltage, current, 'VOLT' if 'VOLTAGE' in source_mode else 'CURR'

    def status(self):
        d = super().status()
        voltage, current, mode = self.get_all()
        d['mode'] = mode
        d['voltage'] = voltage
        d['current'] = current
        d['resistance'] = voltage / current if current != 0 else 0
        d['power'] = voltage * current
        d['target_value'] = self.execute("print(smu.source.level)")
        d['output'] = 'ON' if 'ON' in self.execute("print(smu.source.output)") else 'OFF'
        return d

    @remote
    def set_current(self, value):
        self.execute(
            "on = smu.source.output\n"
            "if on and smu.source.func != smu.FUNC_DC_CURRENT then smu.source.output = smu.OFF end\n"
            "smu.source.func = smu.FUNC_DC_CURRENT\n"
            "smu.measure.func = smu.FUNC_DC_VOLTAGE\n"
            "if on then smu.source.output = smu.ON end\n"
            f"smu.source.level = {float(value)}\n"
            "print('')\n"
        )

    @remote
    def set_voltage(self, value):
        self.execute(
            "on = smu.source.output\n"
            "if on and smu.source.func != smu.FUNC_DC_VOLTAGE then smu.source.output = smu.OFF end\n"
            "smu.source.func = smu.FUNC_DC_VOLTAGE\n"
            "smu.measure.func = smu.FUNC_DC_CURRENT\n"
            "if on then smu.source.output = smu.ON end\n"
            f"smu.source.level = {float(value)}\n"
            "print('')\n"
        )

    @remote
    def _step(self, value):
        self.execute(
            f"smu.source.level = smu.source.level + {float(value)}\n"
            "print('')\n"
        )

    @remote
    def execute(self, message, reply_pattern=None, allowed_retries=5):
        try:
            self.visa_handle.write(message)
            if self._interface == Keithley2450Worker.IGPIB:
                while not (self.visa_handle.read_stb() & 0x10):  # checking MAV bit
                    sleep(0.01)
            res = self.visa_handle.read_raw().decode('ascii')
            self.is_offline = False
        except visa.VisaIOError:
            if allowed_retries > 0:
                self._connect()
                return self.execute(message, reply_pattern, allowed_retries - 1)
            else:
                self.is_offline = True
                res = ''
                warnings.warn("Communication error occured on message: %s" % message, ResourceWarning)

        res = res.strip()
        if reply_pattern:
            if not re.match(pattern=reply_pattern, string=res):
                if allowed_retries > 0:
                    print("Warning! Wrong reply (Q:%s, R:%s)" % (message, res))
                    return self.execute(message, reply_pattern, allowed_retries - 1)
                else:
                    raise Exception("Wrong reply (Q:%s, R:%s)" % (message, res))
        return res


@include_remote_methods(Keithley2450Worker)
class Keithley2450(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self._mode = None

    def createDock(self, parent, menu=None):
        dock = QtWidgets.QDockWidget("Keithley 2450", parent)
        widget = QtWidgets.QWidget(parent)
        layout = QtWidgets.QGridLayout()
        widget.setLayout(layout)

        self.edit_vol = QtWidgets.QLineEdit()
        self.edit_cur = QtWidgets.QLineEdit()
        self.edit_res = QtWidgets.QLineEdit()
        self.edit_vol.setMinimumWidth(100)
        self.edit_vol.setEnabled(False)
        self.edit_cur.setEnabled(False)
        self.edit_res.setEnabled(False)

        def safe_execute(function):
            def fun(*args, **kwargs):
                try:
                    function(*args, **kwargs)
                except Exception:
                    msg = QtWidgets.QMessageBox()
                    msg.setIcon(QtWidgets.QMessageBox.Critical)
                    msg.setText(traceback.format_exc())
                    msg.exec_()
            return fun

        self.edit_cur.mousePressEvent = safe_execute(self._current_dialog)
        self.edit_vol.mousePressEvent = safe_execute(self._voltage_dialog)


        self.edit_step = QtWidgets.QLineEdit("0.001")
        self.edit_step.setFixedWidth(60)
        self.button_up = QtWidgets.QPushButton("▲")
        self.button_down = QtWidgets.QPushButton("▼")
        self.button_up.clicked.connect(safe_execute(lambda state: self._step(float(self.edit_step.text()))))
        self.button_down.clicked.connect(safe_execute(lambda state: self._step(-float(self.edit_step.text()))))
        self.button_up.setFixedWidth(60)
        self.button_down.setFixedWidth(60)

        layout.addWidget(self.edit_vol, 0, 0, 1, 1)
        layout.addWidget(self.edit_cur, 1, 0, 1, 1)
        layout.addWidget(self.edit_res, 2, 0, 1, 1)
        layout.addWidget(self.button_up, 0, 1, 1, 1)
        layout.addWidget(self.edit_step, 1, 1, 1, 1)
        layout.addWidget(self.button_down, 2, 1, 1, 1)

        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parent.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

    def updateSlot(self, status):
        self._mode = status['mode']
        if self._mode == 'VOLT':
            self.edit_vol.setEnabled(True)
            self.edit_cur.setEnabled(False)
        if self._mode == 'CURR':
            self.edit_vol.setEnabled(False)
            self.edit_cur.setEnabled(True)
        self.edit_vol.setText(f"{status['voltage'] : .7f} V")
        self.edit_cur.setText(f"{1000 * status['current'] : .7f} mA")
        self.edit_res.setText(f"{status['resistance'] : .5f} Ω")

    def _voltage_dialog(self, event):
        if event.button() == 1 and self.edit_vol.isEnabled():
            value = float(self.edit_vol.text().split()[0].strip())
            d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.edit_vol, "Set Voltage", "Target (V):",
                                                            value, decimals=7)
            if ok_pressed:
                self.set_voltage(d)

    def _current_dialog(self, event):
        if event.button() == 1 and self.edit_cur.isEnabled():
            value = float(self.edit_cur.text().split()[0].strip())
            d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.edit_cur, "Set Current", "Target (mA):",
                                                            value, decimals=7)
            if ok_pressed:
                self.set_current(d / 1000)
