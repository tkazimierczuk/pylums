# -*- coding: utf-8 -*-
"""
Driver for The Imaging Source camera
"""

from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets,QtCore,QtGui
import time
import cv2

import numpy as np

import os
os.environ["PATH"] = os.path.dirname(__file__)+"\\win32"+ os.pathsep + os.environ["PATH"]
from devices.tis.pyicic.IC_ImagingControl import IC_ImagingControl
from devices.tis.pyicic.IC_Camera import C_FRAME_READY_CALLBACK
from devices.tis.pyicic.IC_GrabberDLL import IC_GrabberDLL
import ctypes as ct


class TISWorker(DeviceWorker):
    _device_name = "TIS driver"
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._compression = False
            
            
    def init_device(self):        
        self.ic = IC_ImagingControl()
        self.ic.init_library()
        
        cam_names = self.ic.get_unique_device_names()
        print('{} cameras found: {}'.format(len(cam_names), cam_names))
        b_device_name = cam_names[0]
        self._device_name = b_device_name.decode('ascii')
        print("Opening device: ", self._device_name)
        self.cam = self.ic.get_device(b_device_name)
        self.cam.open()
        
        #self.cam.reset_properties()
        self.cam.exposure.auto = False
        self.cam.gain.auto = False
        
        """
        formats = self.cam.list_video_formats()
        
        if any([fmt.startswith(b'RGB') for fmt in formats]):
            self.is_color = True
            self.cam.set_video_format(formats[-1])
        else:
            self.is_color = False
            gray_formats = [fmt for fmt in formats if fmt.startswith(b'Y16')]
            if not gray_formats:
                gray_formats = [fmt for fmt in formats if fmt.startswith(b'Y800')]
            self.cam.set_video_format(gray_formats[-1])
        """
                    
        self._n = 0
        self._ntime = time.time()
                    
        def handle_frame(handle_ptr, p_data, frame_num, data):
            self._num = frame_num
            
            img_width,img_height,img_depth,img_fmt = self.cam.get_image_description()
            
            buffer_size = img_width * img_height * (img_depth//8) * ct.sizeof(ct.c_uint8)

            img_ptr = p_data
            img_data = ct.cast(img_ptr, ct.POINTER(ct.c_ubyte * buffer_size))
            temp_img = np.ndarray(buffer = img_data.contents,
                      dtype = np.uint8,
                      shape = (img_height, img_width, img_depth//8))
            self.broadcast_image(temp_img)
            
            self._prev_frame_time = time.time()
            self._n += 1
            if self._n % 10 == 0:
                print("Timing for 10: ", time.time() - self._ntime)
                self._ntime = time.time()
      
        self.cam.register_frame_ready_callback(C_FRAME_READY_CALLBACK(handle_frame))
        self.cam.enable_continuous_mode(True)
        self.cam.enable_trigger(False)
        self.cam.set_frame_rate(1)
        self.cam.prepare_live()
        self._prepare_image_info()
        self.cam.start_live()

    @remote
    def serial_number(self):
        return self._device_name
        
    def close_device(self):
        self.cam.stop_live()
        self.cam.close()    
        self.ic.close_library()
        
    def status(self):
        d = super().status()
        d["serialnumber"] = self._device_name
        d["live"] = True
        d["image_info"] = self._image_info
        d["exposure"] = self.get_exposure()
        d["format"] = self.get_video_format()
        d["frame_rate"] = self.get_frame_rate()
        try:
            d["gain"] = self.get_parameter_value('gain')
        except:
            pass
        return d

    def _prepare_image_info(self):
        img_width,img_height,img_depth,img_fmt = self.cam.get_image_description()
        print(self.cam.get_image_description())
        self._image_info = {"format": "rgb",
                            "width": img_width,
                            "height": img_height,
                            "format_int": img_fmt}
        
        
    def broadcast_image(self, image):
        self.send_via_pubchannel(b'image', self._image_info, image.tobytes())
        
        
    @remote
    def set_exposure(self, value):
        """ Returns exposure time in seconds """
        IC_GrabberDLL.set_exp_abs_val(self.cam._handle, value)
        
    @remote
    def get_exposure(self):
        """ Returns exposure time in seconds """
        f1 = ct.c_float()
        IC_GrabberDLL.get_exp_abs_val(self.cam._handle, f1)
        return f1.value
        
    @remote
    def list_video_formats(self):
        return [s.decode('ascii') for s in self.cam.list_video_formats()]
        
    @remote
    def list_property_names(self):
        return self.cam.list_property_names()
        
    @remote
    def set_video_format(self, video_format):
        """ video_format (string) - one of supported formats """
        try:
            self.cam.stop_live()
            self.cam.set_video_format(video_format.encode('ascii'))
            self._prepare_image_info()
            self.cam.start_live()
        except:
            msg = "Error setting the video format %s" %video_format
            print(msg)
            return msg

            
    @remote
    def get_video_format(self):
        """ returns current video format as string """
        return self.list_video_formats()[self.cam.get_format()]
        
    @remote
    def set_parameter_value(self, parameter, value):
        """Sets the property time using parameter interface (i.e. as an int).
        """
        try:
            self.cam.__getattr__(parameter).value = value
        except:
            msg = "Error setting the parameter %s" % parameter
            print(msg)
            return msg
                
    @remote
    def get_parameter_value(self,  parameter):
        """ Returns an int corresponding to the paramter value. 
        """
        try:
            return self.cam.__getattr__(parameter).value
        except:
            msg = "Error getting the parameter %s value" % parameter
            print(msg)
            return msg
        
    @remote
    def set_parameter_auto(self, parameter, on=True):
        try:
            self.cam.__getattr__(parameter).auto = on
        except:
            msg = "Error setting the parameter %s as auto" % parameter
            print(msg)
            return msg
            
    @remote
    def get_parameter_auto(self, parameter):
        try:
            return self.cam.__getattr__(parameter).auto
        except:
            msg = "Error checking if parameter %s is auto" % parameter
            print(msg)
            return msg
        
    @remote
    def get_parameter_range(self, parameter):
        try:
            return self.cam.__getattr__(parameter).range
        except:
            msg = "Error getting parameter %s range" % parameter
            print(msg)
            return msg    
    
    @remote
    def set_frame_rate(self, rate):
        self.cam.stop_live()
        self.cam.set_frame_rate(rate)
        self.cam.start_live()
        
    @remote
    def get_frame_rate(self):
        return self.cam.get_frame_rate()
    
    
    
    @remote
    def snap_image(self):
        self.cam.snap_image(500)
        time.sleep(0.5)
        data, width, height, depth = self.cam.get_image_data()
        frame = np.ndarray(buffer=data, dtype=np.uint8, shape=(height, width, depth))
        return frame
        
    @remote
    def show_property_dialog(self):
        self.cam.show_property_dialog()
        
    
@include_remote_methods(TISWorker)
class TIS(DeviceOverZeroMQ):
   
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._n = 0
        self._ntime = time.time()
        self._serial_number = ''
        
        
    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("TIS camera", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QHBoxLayout(parentWidget)
        widget.setLayout(layout)
        
        formlayout = QtWidgets.QFormLayout()
        layout.addLayout(formlayout)
        
        self.label = QtWidgets.QLabel()
        layout.addWidget(self.label)
        
        self._fmts_combo = QtWidgets.QComboBox()
        self._fmts_combo.setEnabled(False)
        formlayout.addRow("Format", self._fmts_combo)
        
        self._exposure_edit = QtWidgets.QLineEdit()
        self._exposure_edit.setEnabled(False)
        formlayout.addRow("Exposure time", self._exposure_edit)
        
        self._gain_edit = QtWidgets.QLineEdit()
        self._gain_edit.setEnabled(False)
        formlayout.addRow("Gain", self._gain_edit)
        
        self._frame_rate_edit = QtWidgets.QLineEdit()
        self._frame_rate_edit.setEnabled(False)
        formlayout.addRow("Frame rate", self._frame_rate_edit)
        
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        
        if menu:
            menu.addAction(dock.toggleViewAction())
            
        # Following lines "turn on" the widget operation
        self.createListenerThread(self.updateSlot)
        self.createListenerThread(self.updateImage, topic=b'image')

        
    def updateSlot(self, status):
        try:
            if status["serialnumber"] != self._serial_number:
                self._serial_number = status["serialnumber"]
                
                self._fmts_combo.setEnabled(True)
                self._fmts_combo.clear()
                for fmt in self.list_video_formats():
                    self._fmts_combo.addItem(fmt)
                width = self._fmts_combo.minimumSizeHint().width()
                self._fmts_combo.setMinimumWidth(300)
                #self._fmts_combo.view().setMinimumWidth(width)
                
                self._frame_rate_edit.setEnabled(True)
                self._gain_edit.setEnabled(True)
                self._exposure_edit.setEnabled(True)
                
            self._fmts_combo.setCurrentIndex(status["image_info"]["format_int"])
            self._exposure_edit.setText("%f ms" % (1000*status["exposure"]))
            self._gain_edit.setText(str(status["gain"]))
        except IOError:
            pass
        
    def updateImage(self, image_obj):
        try:
            info,data = image_obj
            height = info["height"]
            width = info["width"]
            if info["format"] == "rgb":
                fmt = QtGui.QImage.Format_RGB888
                bytesPerLine = 3 * width
            else:
                #TODO
                pass
            self._n += 1
            if self._n % 10 == 0:
                print("Timing for 10: ", time.time() - self._ntime)
                self._ntime = time.time()
            if len(data) < height * bytesPerLine:
                print("Something is wrong: ", len(data), info)
                return
            t1 = time.time()
            qImg = QtGui.QImage(data, width, height, bytesPerLine, fmt)
            t2 = time.time()
            pixmap = QtGui.QPixmap.fromImage(qImg).scaled(500,500, QtCore.Qt.KeepAspectRatio)
            t3 = time.time()
            self.label.setPixmap(pixmap)
            t4 = time.time()
            if self._n % 10 == 3:
                print("Timings: %f %f %f" % (t2-t1, t3-t2, t4-t3))
        except IOError:
            pass