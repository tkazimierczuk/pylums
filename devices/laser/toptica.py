# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods

from PyQt5 import QtWidgets,QtCore
from .wavemeter import Wavemeter
from time import sleep
import scipy

default_req_port = 7123
default_pub_port = 7124


import threading
import queue

volt_min = 50
volt_max = 90

class Calibration:
    def __init__(self):
        self.old_offset_nm = 517.295
        self.old_offset_steps = 600000
        self.old_slope = 0.00013

    def motor_to_nm(self, motor_step):
        x = self.old_slope * (motor_step - self.old_offset_steps) + self.old_offset_nm
        x = x-515
        y = 0.32591 + 0.94483*x + 0.00111*x^2 + 0.00711*x^3  -3.91738E-4*x^4 -1.74898E-4*x^5
        y = y+515
        if y < 510:
            return 510
        if y > 521:
            return 521
        return y
    
    def motor_from_nm(self, nm):
        if nm < 510:
            nm = 510
        if nm > 520:
            nm = 520
        try:
            return round(scipy.optimize.bisect(lambda w: self.motor_to_nm(w)-nm, 545000, 625000))
        except:
            return 545000
            
    _anchor = (1/515., 70) # pair [wavelength, piezo] to predict best next piezo
    fsr =  2.25291E-7 # in nm^-1 (i.e. 0.06036 nm @ 517 nm)
    inv_fsr = 0.06036
    piezo_slope = 1.5541e-8 # in nm^-1 / V
    
    def save_point(nm, piezo, nm):
        """ Call this function to inform that current piezo setting corresponds to given wavelength """
        self._anchor = (1/nm, piezo)
    
    def nm_to_piezo(self, nm):
        diff = (1/nm - self._anchor[0]) % self.fsr
        pdV = diff / self.piezo_slope
        mdV = (diff - self.fsr) / self.piezo_slope
        
        if abs(pdV) < abs(mdV) and self._anchor[1]+pdV < volt_max:
            return self._anchor[1]+pdV
        else:
            return self._anchor[1]+mdV

R_STOP = 1
R_CALIB_PIEZO = 2
R_MOVE_TO = 3
              

class DLProWorker(DeviceWorker):
    def __init__(self, *args, wm_req_port=8123, wm_pub_port=8124, wm_host='localhost', 
                       comport='COM16', dlpro_host='10.96.1.90', **kwargs):
        super().__init__(*args, **kwargs)       
        
        self._dlpro_host = dlpro_host       
        self._wm_kwargs = {'req_port': wm_req_port,
                           'pub_port': wm_pub_port,
                           'host': wm_host}
        self._comport = comport

        self.calibration = Calibration()        
        
    def init_device(self):
        import toptica.lasersdk.client as top
        self.client = top.Client(top.NetworkConnection(self._dlpro_host))
        self.client.open()
        self._dlc_mutex = threading.Lock()
        
        import serial
        self.ser = serial.Serial(self._comport, 9600, timeout=2) 
        self.wavemeter = Wavemeter(**(self._wm_kwargs))
        try:
            import pyTMCL
        except ImportError:
            print("### Import error: Run pip install pytmcl")
            raise
        self.bus = pyTMCL.connect(self.ser)
        self.motor = self.bus.get_motor(motor_id=2)       
        self._mot_mutex = threading.Lock()
        
        # facilities for background thread
        self._queue = queue.Queue()
        self.busy = False
        self._thread = threading.Thread(target = self._background_loop)

    def close_device(self):
        self.toptica_thread.should_terminate = True
        self.toptica_thread.join()
        self.client.close()
        self.ser.close()
        
    @remote
    def is_busy(self):
        return self.busy or self.motor.axis.actual_speed != 0
        
    def motor_is_busy(self):
        return self.motor.axis.actual_speed != 0
        
    def motor_status(self):
        with self._mot_mutex:
            pos = self.motor.axis.actual_position
            speed = self.motor.axis.actual_speed
        return {"position" : pos, "wavelength": self.calibration.motor_to_nm(pos), "speed": speed}
        
    def initialize_motor_for_dlpro(self):
        """ Checks if the settins are correct. If they are not correct (e.g. after power-down)
            then initializes all the parameters according to the Toptica Labview subVIs.
        """
        if self.motor.axis.max_current == 250:
            return
        
        self.motor.axis.max_current = 250
        self.motor.axis.max_acceleration = 250
        self.motor.axis.max_positioning_speed = 1000
        standby_current = 0
        self.home_motor()
        
    def home_motor(self):
        with self._mot_mutex:
            self.motor.reference_search(0)
    
    @property    
    def motor_position(self):
        with self._mot_mutex:
            return self.motor.axis.actual_position
            
    @motor_position.setter
    def motor_position(self, target):
        target = int(target)
        if target < 0:
            target = 0
        if target > 650000:
            target = 650000
        with self._mot_mutex:
            return self.motor.move_absolute(target)
    
    def is_motor_stopped(self):
        with self._mot_mutex:
            return self.motor.axis.actual_speed == 0
    
    def motor_get_wavelength(self):
        return self.calibration.motor_to_nm(self.motor_position)
    
    def motor_set_wavelength(self, wavelength):
        self.motor_position = int(self.calibration.motor_from_nm(wavelength))
                
    def status(self):
        """ This function will be called periodically to monitor the state 
        of the device. It should return a dictionary describing the current
        state of the device. This dictionary will be delivered to the 
        front-end class."""
        d = super().status()
        d["connected"] = True
        d["motor"] = self.motor_status()
        d["current"] = self.current
        d["piezo"] = self.piezo
        d["feedforward"] = self.feedforward
        return d
        
    @property
    def serial(self):
        return self.client.get('serial-number',str)
    
    @property
    def current(self):
        return self.client.get('laser1:dl:cc:current-set',float)
    
    @current.setter
    def current(self, value):
        self.client.set('laser1:dl:cc:current-set',float(value))
        
    @property
    def current_offset(self):
        return self.client.get('laser1:dl:cc:current-offset',float)
        
    @current_offset.setter
    def current_offset(self,value):
        return self.client.set('laser1:dl:cc:current-offset',float(value))

    @property
    def piezo(self):
        return self.client.get('laser1:dl:pc:voltage-set',float)
    
    @piezo.setter
    def piezo(self, value):
        self.client.set('laser1:dl:pc:voltage-set',float(value))
        
    @property
    def feedforward(self):
        return self.client.get('laser1:dl:cc:feedforward-factor',float)
    
    @feedforward.setter
    def feedforward(self, value):
        self.client.set('laser1:dl:cc:feedforward-factor',float(value))
        
    @property
    def motor_wavelength(self):
        return self.motor_get_wavelength()
    
    @motor_wavelength.setter
    def motor_wavelength(self, wavelength):
        self.motor_set_wavelength(wavelength)
        
    @property
    def output_enabled(self):
        return self.client.get('laser1:dl:cc:enabled',bool)
    
    @output_enabled.setter
    def output_enabled(self, value):
        return self.client.set('laser1:dl:cc:enabled',bool(value))
        
    def stop(self):
        """ Stops any background routine such as setting the wavelength or calibrating """
        self._queue.put( (R_STOP,None) )
   
    def _background_loop(self):
        """ Function that runs in another thread and performs time-consuming tasks """
        while True:
            request, arg = self._queue.get()
            if request == R_STOP:
                self.busy = False
                continue
            self.busy = True
            if request == R_CALIB_PIEZO:
                self._calibrate_piezo()
            elif request == R_MOVE_TO:
                self._move_to(arg)
            self.busy = False
    
    def _stop_requested(self):
        """ Helper function that checks if there is a new task to be done """
        return not self._queue.empty()
    
    def _move_to(self, target):
        """ Sets desired wavelength
            This function is time-consuming and therefore should be always called
            from another thread """
        self.motor_wavelength = target
        self.piezo = self.calibration.nm_to_piezo(target)
        while self.motor_is_busy():
            time.sleep(0.25)
        time.sleep(0.25)
        self._stabilize(target)
        
    def _stabilize(self, target):
        """ Finds a value of current corresponding to the stable setting """
        thr = 0.5*self.calibration.invfsr
        if self.wavemeter.is_single_mode() and abs(target-self.wavemeter.get_wavelength()) < thr:
            return True
        starting_offset = self.current_offset
        if starting_offset > 160:
            cs = np.arange(starting_offset-0.1, starting_offset-5, -0.2)
        else:
            cs = np.arange(starting_offset+0.1, starting_offset+5, 0.2)
        cnt = 0
        
        for i,c in enumerate(cs):
            self.current_offset = c
            time.sleep(0.25)
            if self.wavemeter.is_single_mode() and abs(target-self.wavemeter.get_wavelength()) < thr:
                cnt += 1
            #else:
            #    if cnt > 4:
            #        self.current_offset = 
    
    def _calibrate_piezo(self):
        """ Performs calibration of the piezo actuator.
            This function is time-consuming and therefore should be always called
            from another thread """
        from devices import N_AIR
        results = []
        for p in np.linspace(50, 90, 30):
            if self._stop_requested():
                return False
            self.piezo = p
            time.sleep(0.5)
            wv = self.wavemeter.get_wavelength()
            ok = self.wavemeter.is_single_mode()
            if ok and wv > 508 and wv < 522:
                results.append( (p, 299792458/N_AIR/wv) )
        print("Piezo calibration: %d points" % len(results))
        res = np.array(results)
        def chi2(pars):
            slope,offset,fsr = pars
            dy = res[:,1] - slope*res[:,0]
            (np.floor(dy/fsr + offset + 0.5)-0.5)*fsr
        best = scipy.optimize.minimize(chi2, (4.68, 0.4, 60), bounds=((4.5,4.8),(0,1),(55,65)) )
        print(best)
        return res, best
        
        
        
@include_remote_methods(DLProWorker)
class DLPro(DeviceOverZeroMQ):  
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)    
        
    def createDock(self, parentWidget, menu = None):
        """ Function for integration in GUI app.  """
        dock = QtWidgets.QDockWidget("DLPro Laser", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        self.layout = QtWidgets.QFormLayout(parentWidget)
        widget.setLayout(self.layout)
        
        self.display_piezo = QtWidgets.QLineEdit()
        self.layout.addRow("Piezo", self.display_piezo)
        
        self.display_current = QtWidgets.QLineEdit()
        self.layout.addRow("Current", self.display_current)
        
        self.display_motor = QtWidgets.QLineEdit()
        self.layout.addRow("Motor", self.display_motor)
        
        
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
        
        self.createListenerThread(self.updateSlot)

    def updateSlot(self, status):
        try:
            self.display_piezo.setText("{:.1f} V".format(status["piezo"]))
            self.display_current.setText("{:.1f} mA".format(status["current"]))
            self.display_motor.setText('{:,}'.format(status["motor"]["position"]).replace(',', ' ')) # space as separator
        except:
            pass