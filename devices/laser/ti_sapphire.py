# -*- coding: utf-8 -*-
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets,QtCore
from PyQt5.QtWidgets import (QPushButton, QMessageBox, QDialog)
from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow)
from PyQt5.QtWidgets import (QVBoxLayout, QHBoxLayout, QLineEdit)
from PyQt5.QtWidgets import (QLabel, QInputDialog)
from PyQt5.QtGui import (QFont, QColor)
import scipy.optimize
import numpy as np
from devices import H_C, N_AIR

class TiSapphireWorker(DeviceWorker):
    '''The class contains every methods needed to talk to the motor'''
    
    _poly_center = 750 # center of the tuning range
    
    def __init__(self, port, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.port = port
        
        self.calibration_polynomial = [4.46e4, 1885, 6.68, 0.096]
        self._offset = 0 
        
        lower_limit = -100000 # in motor units (steps)
        upper_limit =  100000 # in motor units (steps)
        
    def init_device(self):
        import serial
        self.ser = serial.Serial(self.port, 9600, timeout=5)  #opens serial port COM3
        print("Checking communication: " + str(self.check_communication()))
        self._send_command('PC2') # reduce the current
        self.set_backlash_dist(0)
        
    def __del__(self):
        self.ser.close() #serial port close
        
    def status(self):
        d = super().status()
        pos = self.get_position()
        d["wavelength"] = self.position_to_wavelength(pos)
        d["motor_position"] = pos
        return d

    @remote
    def _send_command(self, command):
        reply_expected = (command[:2].lower() in ('tp', 'tv', 'ty', 'hl', 'tf'))
        if reply_expected:
            self.ser.flushInput()
        self.ser.write(command.encode('ascii') + b'\n')
        if reply_expected:
            response = self.ser.readline().decode('ascii')
            if response[:2].lower() != command[:2].lower():
                raise Exception("Communication error! Got %s after command %s" % (response, command))
            return response[2:].strip()
        
    def set_backlash_dist(self, dist=0):
        '''Set how many steps the motor should move back and forth to account for the backlash'''
        self._send_command('sf%d' % dist)
        
    @remote
    def get_backlash_dist(self):
        '''Get how many steps the motor should move back and forth to account for the backlash'''
        return int(self._send_command('tf'))

    @remote
    def check_communication(self):
        '''Sends a handshake. Returns True if successful reply is received'''
        try:
            self._send_command('HL')
            return True
        except:
            return False

    @remote
    def is_moving(self):
        return int(self._send_command('TV'))!=0

    @remote
    def move_position(self, steps, relative=False):
        '''Go to position specified in steps'''
        steps = int(steps)
        if relative:
            self._send_command('mr%d' % steps)
        self._send_command('ma%d' % steps)
        
    @remote
    def get_position(self):
        '''Get current position of the motor'''
        return int(self._send_command('tp'))

    @remote
    def set_wavelength(self, nm):
        wpos = self.wavelength_to_position(nm)
        self.move_position(wpos)

    @remote
    def get_wavelength(self):
        return self.position_to_wavelength(self.get_position())

    def position_to_wavelength(self, pos):
        pos -= self._offset
        wv_min = 680
        wv_max = 850
        pos_min = self.wavelength_to_position(wv_min)
        pos_max = self.wavelength_to_position(wv_max)
        try:
            out = scipy.optimize.bisect(lambda w: self.wavelength_to_position(w)-pos, wv_min, wv_max)
        except ValueError:
            if abs(pos - pos_min) < abs(pos-pos_max):
                out = wv_min
            else:
                out = wv_max
        return float(out)

    def wavelength_to_position(self, nm):
        ''' Calibration function. The formula has to be monotonic in range (680nm,850nm) '''
        pos = 0
        x = 1
        for coefficient in self.calibration_polynomial:
            pos += coefficient * x
            x *= (nm - self._poly_center)
        return pos + self._offset
    
    def adjust_offset(self, nm):
        """ Shift the calibration curve so that current position will
            correspond to the given wavelength """
        expected_pos = self.wavelength_to_position(nm)
        self._offset += expected_pos-self.get_position()
        return "New offset: " + str(self._offset)
    
    def calibrate(self, points):
        ''' Adjust calibration function to fit the points
            points - a collection of pairs (motor_position, wavelength) 
                     or a single number interpreted as the current wavelength '''
        try:
            w1 = float(points)
            w0 = self.get_wavelength()
            self.calibration_polynomial[0] += w1-w0
            return
        except:
            pass
        
        try:
            if len(points) < 2:
                return
            deg = min(len(points)-1, 3)
            pp = array(points)
            cfs = np.polyfit(pp[:,0], pp[:,2], deg)
            self.calibration_polynomial = cfs[::-1]
        except:
            pass
    
    
@include_remote_methods(TiSapphireWorker)
class TiSapphire(DeviceOverZeroMQ):  
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
                             
    def createDock(self, parentWidget, menu=None):
        """ Function for integration in GUI app. Implementation below 
        creates a button and a display """
        dock = QtWidgets.QDockWidget("Ti:Sapphire laser", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        
        layout_form = QtWidgets.QFormLayout()
        self.display_nm = QtWidgets.QLineEdit()
        self.display_nm.setReadOnly(True)
        layout_form.addRow("Wavelength (air)", self.display_nm)
        self.display_energy = QtWidgets.QLineEdit()
        self.display_energy.setReadOnly(True)
        layout_form.addRow("Photon energy", self.display_energy)
        
        def on_click_nm(event):
            try:
                if event.button() == 1:
                    current = self.get_wavelength()
                    d, okPressed = QtWidgets.QInputDialog.getDouble(parentWidget, "Set wavelength", "Target wavelength (air) in nm:", current, 680, 900)
                    if okPressed:
                        self.set_wavelength(d)
            except:
                pass
        self.display_nm.mousePressEvent  = on_click_nm
        
        def on_click_energy(event):
            try:
                if event.button() == 1:
                    current = H_C*N_AIR / self.get_wavelength()*1000
                    print(current)
                    d, okPressed = QtWidgets.QInputDialog.getDouble(parentWidget, "Set energy", "Target energy in meV:", current, 1300, 1900)
                    if okPressed:
                        self.set_wavelength(H_C*N_AIR*1000/ d)
            except:
                pass
        self.display_energy.mousePressEvent  = on_click_energy
        
        button = QtWidgets.QPushButton("Calibrate")
        button.clicked.connect(self.show_calibration_window)
        layout_form.addRow(button)
        
        widget.setLayout(layout_form)
        
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
            
        # Following lines "turn on" the widget operation
        self.createListenerThread(self.updateSlot)

        
    def updateSlot(self, status):
        try:
            self.display_nm.setText("%.3f nm" % status["wavelength"])
            self.display_energy.setText("%.3f meV" % (H_C*N_AIR*1000/status["wavelength"]))
        except:
            pass

            
    def show_calibration_window(self):
        ''' Displays a window with several calibration options '''
        dialog = QtWidgets.QDialog()
        dialog.setWindowTitle("Calibrate Ti:Sapphire laser")
        layout = QtWidgets.QFormLayout(dialog)
        dialog.setLayout(layout)
        
        option1 = QtWidgets.QRadioButton("Auto-calibration using wavemeter")
        layout.addRow(option1)
        
        edit_wavemeter = QtWidgets.QLineEdit("wavemeter")
        label = QtWidgets.QLabel("Wavemeter id")
        layout.addRow(label, edit_wavemeter)
        option1.toggled.connect(edit_wavemeter.setEnabled)
        option1.toggled.connect(label.setEnabled)
        option1.setChecked(True)
        
        option2 = QtWidgets.QRadioButton("Specify current wavelength")
        layout.addRow(option2)
        edit_wavelength = QtWidgets.QLineEdit()
        edit_wavelength.setPlaceholderText("Enter wavelength in nm")
        label = QtWidgets.QLabel("Current wavelength")
        layout.addRow(label, edit_wavelength)
        option2.toggled.connect(edit_wavelength.setEnabled)
        option2.toggled.connect(label.setEnabled)
        edit_wavelength.setEnabled(False)
        label.setEnabled(False)
        
        buttonBox = QtWidgets.QDialogButtonBox(dialog)
        buttonBox.setOrientation(QtCore.Qt.Horizontal)
        buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel)
        buttonBox.accepted.connect(dialog.accept)
        buttonBox.rejected.connect(dialog.reject)
        layout.addRow(buttonBox)
        dialog.setModal(True)
        dialog.show()
        
        if dialog.exec_():
            if option1.isChecked():
                try:
                    self.perform_wavemeter_calibration(edit_wavemeter.text())
                except:
                    pass
            elif option2.isChecked():
                try:
                    self.calibrate(float(edit_wavelength.text()))
                except:
                    pass
        
    def perform_wavemeter_calibration(self, wavemeter_id):
        ''' Display a dialog with wavemeter calibration '''
        wavemeter = devices.active_devices[wavemeter_id]
        