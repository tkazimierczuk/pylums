# -*- coding: utf-8 -*-

import ctypes as ct
import threading

def get_dll():
    dll = ct.WinDLL("C:\Windows\System32\FieldMax2Lib.dll")
    dll.fm2LibOpenDriver.argtypes = [ct.c_int16]
    dll.fm2LibOpenDriver.restype =  ct.c_int32

    dll.fm2LibGetSerialNumber.argtypes = [ct.c_int32, ct.c_char_p, ct.POINTER(ct.c_int16)]
    dll.fm2LibGetSerialNumber.restype = ct.c_uint16

    dll.fm2LibSync.argtypes = [ct.c_int32]
    dll.fm2LibSync.restype = ct.c_uint16

    dll.fm2LibGetData.argtypes = [ct.c_int32, ct.c_char_p, ct.POINTER(ct.c_int16)]
    dll.fm2LibGetData.restype = ct.c_uint16

    dll.fm2LibPackagedSendReply.argtypes = [ct.c_int32, ct.c_char_p, ct.c_char_p, ct.POINTER(ct.c_int16)]
    dll.fm2LibPackagedSendReply.restype = ct.c_uint16
    return dll


#INSTR_TYPE is defined as:
#    typedef struct
#{
#	VB_INTEGER probeType;							// 0
#	VB_INTEGER meterType;							// 2
#	VB_INTEGER lastFault;							// 4
#	VB_INTEGER statisticsMode;						// 6
#	VB_INTEGER batchRestartMode;					// 8
#	VB_INTEGER fill1;								// 10
#	VB_LONG batchSizeSeconds;						// 12
#	VB_LONG batchSizePulses;						// 16
#	VB_LONG wavelength;								// 20
#	VB_LONG minWavelength;							// 24
#	VB_LONG maxWavelength;							// 28
#	VB_BOOLEAN areaCorrectionEnabled;				// 32
#	VB_INTEGER fill2;								// 34
#	VB_SINGLE areaCorrectionDiameter;				// 36
#	VB_BOOLEAN averageModeEnabled;					// 40
#	VB_INTEGER averageWindowSizeSeconds;			// 42
#	VB_INTEGER averageWindowSizePulses;				// 44
#	VB_BOOLEAN attenuationCorrectionModeEnabled;	// 46
#	VB_SINGLE attenuationCorrectionFactor;			// 48
#	VB_INTEGER triggerLevel;						// 52
#	VB_INTEGER measurementMode;						// 54
#	VB_BOOLEAN autoRangingEnabled;					// 56
#	VB_INTEGER fill3;								// 58
#	VB_SINGLE range;								// 60
#	VB_SINGLE minRange;								// 64
#	VB_SINGLE maxRange;								// 68
#	VB_BOOLEAN speedupDigitalDisplay;				// 72
#	VB_BOOLEAN speedupHostData;						// 74
#	VB_BOOLEAN speedupAnalogOutput;					// 76
#	VB_BOOLEAN speedupMeter;						// 78
#	VB_INTEGER analogOutFullscaleVoltage;			// 80
#	VB_BOOLEAN powerState;							// 82
#	VB_BOOLEAN backlight;							// 84
#	VB_BOOLEAN hertzMode;							// 86
#	VB_BOOLEAN holdMode;							// 88
#} InstrumentStateType;
#
#dll.fm2LibGetInstrumentState.argtypes = [ct.c_int32, ct.POINTER(INSTR_TYPE)]
#dll.fm2LibGetInstrumentState.restype = ct.c_bool

from  .powermeter import PowermeterWorker
import struct
import time

class FieldMaxWorker(PowermeterWorker):   
    def init_device(self):
        self._device_lock = threading.Lock()
        self.dll = get_dll()
        self.handle = self.dll.fm2LibOpenDriver(0)
        if self.handle == -1:
            raise Exception("No FieldMax II meter connected")
        
        buf = b'..........................'
        bufsize = ct.c_int16(len(buf))
        self.dll.fm2LibGetSerialNumber(self.handle, buf, ct.byref(bufsize))
        self.serial_number = buf[:bufsize.value].decode('ascii')
        self._sync_communication()
        print("Connected to device with S/N: %s" % self.serial_number)
        

    unit = 'W'    
       
    def _sync_communication(self):
        with self._device_lock:
            self.dll.fm2LibSync(self.handle)
        
    def get_power(self):
        retries = 10
        while 1+retries>0:
            res = self._get_data()
            if res is None:
                time.sleep(0.1)
                retries -= 1
            else:
                return res
        return None
        
    def _get_data(self, buffer_length=32):
        count = ct.c_int16(buffer_length)
        buf = bytes(buffer_length*8)
        with self._device_lock:
            self.dll.fm2LibGetData(self.handle, buf, ct.byref(count))
        count = count.value
        if buffer_length == count: # apparently there is a lot of old data
            self._sync_communication()
        if count == 0:
            return None
        else:
            return struct.unpack('<f',buf[8*count-8:8*count-4])[0]
        
    def _get_settings(self, command):
        bufsize = 32
        count = ct.c_int16(bufsize)
        buf = bytes(bufsize)
        with self._device_lock:
            self.dll.fm2LibPackagedSendReply(self.handle, command.encode('ascii'), buf, ct.byref(count))
        return buf[:count.value].decode('ascii')
    
    def get_wavelength(self):
        reply = self._get_settings('WOO')
        try:
            res = float(reply.split(',')[0])
        except:
            res = np.nan
        return res
    
    def get_measurement_mode(self):
        return self._get_settings('JWM')
    
    def get_range(self):
        return self._get_settings('RNG')