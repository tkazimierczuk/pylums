from datetime import datetime
import time
from time import sleep
from time import perf_counter as clock
import threading
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from devices.spectro.spectrometer import SpectrometerWorker, Spectrometer
from PyQt5.QtWidgets import (QWidget, QPushButton, 
    QHBoxLayout, QVBoxLayout, QApplication)
from PyQt5 import QtWidgets, QtCore, QtGui
import functools
import numpy
from time import perf_counter as clock
import serial
import traceback
import pyqtgraph as pg

default_req_port = 12020
default_pub_port = 12021
from scipy.optimize import curve_fit


def to_int(b):
    return int.from_bytes(b, "big")

def normalize_row(data):
    vals = numpy.sort(data)
    low = vals[vals.shape[0] // 5]
    high = vals[-1]
    return (data - low) / (high - low)

def find(data, ref):
    diffs = numpy.sum((data.astype(float) - ref.astype(float))**2, axis=1)
    i = numpy.argmin(diffs[1:-1]) + 1
    d1 = diffs[i - 1] - diffs[i]
    d2 = diffs[i + 1] - diffs[i]
    result = i + 0.5 * (d1 - d2) / (d1 + d2)
    return result


class OPOWorker(DeviceWorker):
    MODE_MANUAL = 0
    MODE_SIMPLE = 1
    MODE_ADVANCED = 2
    MODE_PREPARING = 3
    
    #POSITIONS = numpy.array([-1000, -850, -700, -550, -400, -300, -200, -100, -50, -35, -25, -17, -11, -7, -4, -2, -1, 0,
    #             1, 2, 4, 7, 11, 17, 25, 35, 50, 100, 200, 300, 400, 550, 700, 850, 1000])
    POSITIONS = numpy.array([-100, -50, -35, -25, -17, -11, -7, -4, -2, -1, 0,
                 1, 2, 4, 7, 11, 17, 25, 35, 50, 100])

    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, port="COM1", **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, refresh_rate = 0.5, **kwargs)

        self._port = port
        self._auto_mode = self.MODE_MANUAL
        self._preparation_iterator = None
        self._preparation_positions = None
        self._target_wavelength = 650
        self._target_dataset = None  # desired and neighbouring spectra
        
    def init_device(self):
        """
        Opens the communication 
        """
        print("=========")
        print(str(datetime.now()))
        print("OPO: Initialization...")
        
        self.serial = serial.Serial(self._port, 9600, timeout=1.5)
        reply = self._execute(b"SB115200")
        self.serial.close()
        self.serial = serial.Serial(self._port, 115200, timeout=1.5)
        
        self.spectrum_points = 256 # default
        
        self.get_piezo()  # test connection
        self._execute(b'W256') # setting different spectrum data size not working?
        
        print("OPO: Success")
        
    
    def _execute(self, command, reply_len = 0, allowed_retries=3):
        self.serial.flushInput()
        self.serial.write(command + b',')
        
        if reply_len == 0:
            return
            
        reply = self.serial.read(reply_len)
        if len(reply) != reply_len:
            if allowed_retries <= 0:
                raise Exception("Failed to communicate with OPO")
            print(f"Got {len(reply)} bytes, expected {reply_len}. Retrying...")
            return self._execute(command, reply_len, allowed_retries=allowed_retries-1)
        return reply
        
    def status(self):
        d = super().status()
        self.x, self.y, wl, wl_peak, wl_fwhm = self.get_spectrum(all=True)
        piezo = self.get_piezo()
        
        B = numpy.median(self.y)
        x0 = self.x[numpy.argmax(self.y)]
        A0 = (numpy.max(self.y) - B) / 2
        p0 = [A0, x0]

        def func(x, A, x0):
            return A / (1 + ((x - x0) / 8) ** 2) + B
        try:
            best, pcov = curve_fit(func, self.x, self.y, p0=p0)
            wl = best[1]
        except Exception:
            wl = x0
        
        if self._auto_mode == self.MODE_SIMPLE:
            if wl < self._target_wavelength:
                piezo += 1
            else:
                piezo -= 1
            self.set_piezo(piezo)
        elif self._auto_mode == self.MODE_ADVANCED:
            position = find(self._target_dataset, self.y)
            print(position)
            if position < len(self.POSITIONS) // 2:
                piezo += 1
            else:
                piezo -= 1
            self.set_piezo(piezo)
        elif self._auto_mode == self.MODE_PREPARING:
            if self._preparation_iterator > 0:
                self._target_dataset.append(self.y)
            if self._preparation_iterator < len(self.POSITIONS):
                self.set_piezo(self._preparation_positions[self._preparation_iterator])
                self._preparation_iterator += 1
            else:
                self._target_dataset = numpy.array(self._target_dataset)
                self._auto_mode = self.MODE_ADVANCED
                self.set_piezo(self._preparation_positions[len(self._preparation_positions) // 2])

        d["target_wavelength"] = self._target_wavelength
        d["auto"] = self._auto_mode
        d["wavelength"] = wl
        d["FWHM"] = wl_fwhm
        d["spectrum_x"] = self.x
        d["spectrum_y"] = self.y
        d["piezo"] = piezo

        self.send_via_pubchannel(b'spectrum', (self.x, self.y))
        return d

    @remote
    def toggle_auto(self):
        self._auto_mode = self.MODE_MANUAL if self._auto_mode != self.MODE_MANUAL else self.MODE_SIMPLE

    @remote
    def set_auto_simple(self, auto):
        self._auto_mode = auto

    @remote
    def set_auto_advanced(self):
        self._auto_mode = self.MODE_PREPARING
        self._preparation_iterator = 0
        self._target_dataset = []
        piezo = self.get_piezo()
        self._preparation_positions = self.POSITIONS + piezo

    @remote
    def set_target_wavelength(self, wavelength):
        self._target_wavelength = wavelength

    @remote
    def get_temperature(self):
        return to_int(self._execute(b"GTS", 2)) / 10

    @remote
    def set_temperature(self, val):
        val_b = bytes(f"STS{round(val * 10):04}", 'utf-8')
        self._execute(val_b)
        
    @remote
    def get_piezo(self):
        return to_int(self._execute(b"GZ", 2))

    @remote
    def set_piezo(self, val):
        val_b = bytes(f"P{round(val):04}", 'utf-8')
        self._execute(val_b)

    @remote
    def get_power_ir(self):
        return to_int(self._execute(b"GPI", 2))
        
    @remote
    def get_power_vis(self):
        return to_int(self._execute(b"GPV", 2))

    @remote
    def get_humidity(self):
        return to_int(self._execute(b"GH", 1))
        
    @remote
    def get_gain(self):
        return to_int(self._execute(b"GG", 1))
        
    @remote
    def set_gain(self, val):
        """Set gain from 1 to 6"""
        val_b = bytes(f"GN{round(val):1}", 'utf-8')
        self._execute(val_b)
        
    @remote
    def set_preview_wavelength(self, val):
        val_b = bytes(f"LZ{round(val * 20):05}", 'utf-8')
        self._execute(val_b)

    @remote
    def zoom_in(self):
        self._execute(b"SZI")
        
    @remote
    def zoom_out(self):
        self._execute(b"SZO")
        
    @remote
    def set_zoom(self, val):
        """Set spectrum zoom: 1: 5 nm, 2: 10 nm, 3: 20 nm, 4: 50 nm, 5: 100 nm, 6: 200 nm, 7: 500 nm"""
        val_b = bytes(f"F{round(val):1}", 'utf-8')
        self._execute(val_b)
        
    #@remote
    def get_wavelength(self):
        data = self._execute(b"GSV", 8)
        wl = to_int(data[4:6]) / 10 / 2
        return wl
        
    #@remote
    def get_wavelength_peak(self):
        data = self._execute(b"GSV", 8)
        wl = to_int(data[0:2]) / 10 / 2
        
    #@remote
    def get_FWHM(self):
        data = self._execute(b"GSV", 8)
        wl = to_int(data[2:4]) / 10 / 2
        
    @remote
    def get_last_spectrum(self):
        return self.x, self.y
    
    @remote
    def get_spectrum(self, all=False):
        data = self._execute(b"GSH", 2 * self.spectrum_points + 12)
        y = numpy.frombuffer(data, dtype=numpy.dtype('>u2'), count=self.spectrum_points)
        d1 = data[ 2 * self.spectrum_points:]
        x1 = to_int(d1[0:2]) / 10 / 2
        x2 = to_int(d1[2:4]) / 10 / 2
        x = numpy.linspace(x1, x2, self.spectrum_points)
        if not all:
            return x, y
        wl = to_int(d1[8:10]) / 10 / 2
        wl_peak = to_int(d1[4:6]) / 10 / 2
        wl_fwhm = to_int(d1[6:8]) / 10 / 2
        return x, y, wl, wl_peak, wl_fwhm

@include_remote_methods(OPOWorker)
class OPO(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self._auto = False

    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("OPO", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        vlayout = QtWidgets.QVBoxLayout(widget)
        #vlayout.setContentsMargins(0, 0, 0, 0)
        layout1 = QtWidgets.QHBoxLayout()
        layout2 = QtWidgets.QHBoxLayout()
        layout3 = QtWidgets.QHBoxLayout()
        vlayout.addLayout(layout1)
        vlayout.addLayout(layout2)
        vlayout.addLayout(layout3)
        widget.setLayout(vlayout)
        #widget.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        widget.setFixedWidth(106)

        self.button_man = QtWidgets.QPushButton("Man.")
        self.button_man.setFixedWidth(46)
        layout1.addWidget(self.button_man)

        self.button_adv = QtWidgets.QPushButton("more…")
        self.button_adv.setFixedWidth(46)
        layout1.addWidget(self.button_adv)

        self.label_actual = QtWidgets.QLabel("650.0 nm")
        self.label_actual.setFixedWidth(52)
        layout2.addWidget(self.label_actual)

        self.edit_piezo = QtWidgets.QLineEdit("1000")
        self.edit_piezo.setFixedWidth(45)
        layout2.addWidget(self.edit_piezo)


        self.button_ll = QtWidgets.QPushButton("≪")
        self.button_l = QtWidgets.QPushButton("<")
        self.button_m = QtWidgets.QPushButton(">")
        self.button_mm = QtWidgets.QPushButton("≫")
        self.button_ll.setFixedWidth(22)
        self.button_l.setFixedWidth(22)
        self.button_m.setFixedWidth(22)
        self.button_mm.setFixedWidth(22)
        self.button_ll.clicked.connect(lambda: self._step_edited(-10))
        self.button_l.clicked.connect(lambda: self._step_edited(-1))
        self.button_m.clicked.connect(lambda: self._step_edited(1))
        self.button_mm.clicked.connect(lambda: self._step_edited(10))
        layout3.addWidget(self.button_ll)
        layout3.addWidget(self.button_l)
        layout3.addWidget(self.button_m)
        layout3.addWidget(self.button_mm)

        self.widget_adv = QtWidgets.QWidget()
        self._setup_adv_widget(self.widget_adv)

        self.button_man.clicked.connect(lambda: self._try_manual())
        self.button_adv.clicked.connect(self._show_adv_widget)
        self.edit_piezo.mousePressEvent = lambda event: self._set_piezo_dialog(event, widget=False)

        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
            
        self.createListenerThread(self.updateSlot)

    def _setup_adv_widget(self, widget):
        layout = QtWidgets.QGridLayout()
        widget.setLayout(layout)

        icon = QtGui.QIcon("img/icon_control.svg")
        widget.setWindowIcon(icon)
        widget.setWindowTitle("OPO control widget")

        advanced_group = QtWidgets.QGroupBox("Spectrum locking")
        advanced_layout = QtWidgets.QGridLayout()
        advanced_group.setLayout(advanced_layout)
        layout.addWidget(advanced_group, 0, 0)

        advanced_layout.addWidget(QtWidgets.QLabel("In this mode, the spectrum of the laser will be locked to\n"
                                                   "its initial shape. First, several spectra will be acquired in\n"
                                                   "the vicinity of the initial piezo position. Collected spectra\n"
                                                   "will be compared with the laser spectrum, providing feed-\n"
                                                   "back for the drift correction.\n"), 0, 0)

        advanced_layout.addWidget(QtWidgets.QLabel("Try locking the spectrum at point, where it changes\n"
                                                   "continuously over the small range of the piezo positions.\n"), 1, 0)
        advanced_layout.setRowStretch(5, 1)

        self.button_lock = QtWidgets.QPushButton("Lock laser at current spectrum")
        self.button_lock.clicked.connect(self._try_set_auto_advanced)
        advanced_layout.addWidget(self.button_lock, 2, 0)

        simple_group = QtWidgets.QGroupBox("Simple λ stabilization (unadvised)")
        simple_layout = QtWidgets.QGridLayout()
        simple_group.setLayout(simple_layout)
        layout.addWidget(simple_group, 0, 1)

        simple_layout.addWidget(QtWidgets.QLabel("This automated mode will continuously calculate\n"
                                                 "the center wavelength of the spectrum and move\n"
                                                 "the piezo to achieve target wavelength.\n"))
        simple_layout.addWidget(QtWidgets.QLabel("Set wavelength:"))

        self.edit_wl = QtWidgets.QLineEdit("650.0")
        self.edit_wl.mousePressEvent = self._set_wl_dialog
        simple_layout.addWidget(self.edit_wl)
        simple_layout.setRowStretch(5, 1)


        manual_group = QtWidgets.QGroupBox("Manual piezoelement control")
        manual_layout = QtWidgets.QVBoxLayout()
        manual_layout1 = QtWidgets.QHBoxLayout()
        manual_layout1.setContentsMargins(0, 0, 0, 0)
        manual_layout.addLayout(manual_layout1)
        manual_layout2 = QtWidgets.QHBoxLayout()
        manual_layout2.setContentsMargins(0, 0, 0, 0)
        manual_layout.addLayout(manual_layout2)
        manual_group.setLayout(manual_layout)
        layout.addWidget(manual_group, 1, 0, 1, 2)

        self.button_llll_widget = QtWidgets.QPushButton("-100")
        self.button_lll_widget = QtWidgets.QPushButton("-25")
        self.button_ll_widget = QtWidgets.QPushButton("-5")
        self.button_l_widget = QtWidgets.QPushButton("-1")
        self.button_m_widget = QtWidgets.QPushButton("+1")
        self.button_mm_widget = QtWidgets.QPushButton("+5")
        self.button_mmm_widget = QtWidgets.QPushButton("+25")
        self.button_mmmm_widget = QtWidgets.QPushButton("+100")
        self.button_llll_widget.setFixedWidth(51)
        self.button_lll_widget.setFixedWidth(43)
        self.button_ll_widget.setFixedWidth(35)
        self.button_l_widget.setFixedWidth(27)
        self.button_m_widget.setFixedWidth(27)
        self.button_mm_widget.setFixedWidth(35)
        self.button_mmm_widget.setFixedWidth(43)
        self.button_mmmm_widget.setFixedWidth(51)
        self.button_llll_widget.clicked.connect(lambda: self._step_edited(-100))
        self.button_lll_widget.clicked.connect(lambda: self._step_edited(-25))
        self.button_ll_widget.clicked.connect(lambda: self._step_edited(-5))
        self.button_l_widget.clicked.connect(lambda: self._step_edited(-1))
        self.button_m_widget.clicked.connect(lambda: self._step_edited(1))
        self.button_mm_widget.clicked.connect(lambda: self._step_edited(5))
        self.button_mmm_widget.clicked.connect(lambda: self._step_edited(25))
        self.button_mmmm_widget.clicked.connect(lambda: self._step_edited(100))
        manual_layout1.addWidget(self.button_llll_widget)
        manual_layout1.addWidget(self.button_lll_widget)
        manual_layout1.addWidget(self.button_ll_widget)
        manual_layout1.addWidget(self.button_l_widget)
        manual_layout1.addWidget(self.button_m_widget)
        manual_layout1.addWidget(self.button_mm_widget)
        manual_layout1.addWidget(self.button_mmm_widget)
        manual_layout1.addWidget(self.button_mmmm_widget)
        manual_layout1.addStretch(1)

        self.button_manual = QtWidgets.QPushButton("Abort automatic stabilization")
        self.button_manual.clicked.connect(lambda: self._try_manual())
        manual_layout2.addWidget(self.button_manual)
        manual_layout2.addWidget(QtWidgets.QLabel("Actual:"))
        self.edit_piezo_widget = QtWidgets.QLineEdit("1000")
        self.edit_piezo_widget.setFixedWidth(40)
        manual_layout2.addWidget(self.edit_piezo_widget)
        self.edit_piezo_widget.mousePressEvent = lambda event: self._set_piezo_dialog(event, widget=True)

        self.label_actual_widget = QtWidgets.QLabel("Peak center: 650.0 nm")
        manual_layout2.addWidget(self.label_actual_widget)
        manual_layout2.addStretch(1)
        manual_layout.addStretch(1)

        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')
        self.plot_widget = pg.PlotWidget()
        self.plot_widget.setLabel('bottom', "Wavelength (nm)")
        self.plot_widget.setLabel('left', "Counts")
        self.plot_curve = pg.PlotCurveItem()
        self.plot_widget.addItem(self.plot_curve)
        self.plot_widget.setYRange(0, 1024)
        layout.addWidget(self.plot_widget, 2, 0, 1, 2)

    def _show_adv_widget(self):
        self.widget_adv.show()
        print("dudua")

    def _try_manual(self):
        try:
            self.set_auto_simple(OPOWorker.MODE_MANUAL)
        except Exception:
            pass

    def _try_set_auto_advanced(self):
        try:
            self.set_auto_advanced()
        except Exception:
            pass

    def _set_wl_dialog(self, event):
        if event.button() == 1:
            try:
                current = float(self.edit_wl.text())
            except Exception:
                current = 650
            d, okPressed = QtWidgets.QInputDialog.getDouble(self.edit_wl, "Target wavelength input",
                                                            "Target wavelength (nm):", current, decimals=4)
            if okPressed:
                try:
                    self.set_target_wavelength(d)
                    self.set_auto_simple(True)
                except Exception:
                    pass

    def _set_piezo_dialog(self, event, widget):
        if event.button() == 1:
            try:
                current = int(float(self.edit_piezo.text()))
            except Exception:
                current = 1000
            d, okPressed = QtWidgets.QInputDialog.getInt(self.edit_piezo if not widget else self.edit_piezo_widget,
                                                         "Target piezo position input", "Target position:", current)
            if okPressed:
                try:
                    self.set_auto_simple(False)
                    self.set_piezo(d)
                except Exception:
                    print(traceback.format_exc())

    def _step_edited(self, step):
        try:
            current = float(self.edit_piezo.text())
            current += step
            self.set_piezo(current)
        except Exception:
            print (traceback.format_exc())

    def updateSlot(self, status):
        self._auto = status["auto"]
        self.edit_piezo.setText(f'{status["piezo"]:4}')
        self.edit_piezo_widget.setText(f'{status["piezo"]:4}')
        self.edit_wl.setText(f'{status["target_wavelength"]: 3.1f}')
        self.label_actual.setText(f'{status["wavelength"]:3.1f} nm')
        self.label_actual_widget.setText(f'Peak center: {status["wavelength"]:3.1f} nm')

        self.plot_curve.setData(status["spectrum_x"], status["spectrum_y"], pen="b")



        