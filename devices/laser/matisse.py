# -*- coding: utf-8 -*-
"""

"""
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods

from PyQt5 import QtWidgets,QtCore
import pyvisa
import visa
import threading
from .wavemeter import Wavemeter
from time import sleep
from devices import H_C, N_AIR

default_req_port = 7163
default_pub_port = 7164


motor_controller_statuses = \
            {0x00: 'undefined status',
             0x01: 'system is initialized',
             0x02: 'system is waiting for command / button pressed',
             0x03: 'button is pressed',
             0x04: 'executing short move (manual control)',
             0x05: 'executing long move (manual control)',
             0x06: 'decelerating motor',
             0x07: 'finishing move',
             0x08: 'execute absolute move command',
             0x09: 'execute relative move command',
             0x10: 'calculating acceleration table'}
for i in range(0x0a, 0x0f+1):
    motor_controller_statuses[i] = 'executing origin search sequence'
    

class MatisseWorker(DeviceWorker):
    def __init__(self, req_port, pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.visa_lock = threading.Lock()

    def init_device(self):
        rm = visa.ResourceManager()
        self.visa_handle = rm.open_resource('USB::0x17E7::0x0101::11-12-21::INSTR')
        print('Current BiFi position: {}'.format(self.get_bifi_wavelength()))

    def _execute(self, message):
        with self.visa_lock:
            try:
                res = self.visa_handle.query(message)
                if message.endswith('?'):
                    res = res.split(' ',1)[1]
                return res
            except:
                pass
       
    def status(self):
        d = super().status()
        d["bifi_wavelength"] = self.get_bifi_wavelength()
        d["bifi_status"] = self.bifi_status()
        d["te_status"] = self.te_status()
        return d
        
    @remote
    def get_wavelength_from_wavemeter(self):
        return self.wavemeter.get_wavelength()
    
    @remote
    def get_bifi_position_steps(self):
        pos = int(self._execute('MOTBI:POS?'))
        return pos
    
    @remote
    def set_bifi_position_steps(self, pos):
        """ Move the Birefrigent Filter to a given position.
        This command does not wait for movement to complete"""
        self._execute('MOTBI:POS %d' % pos)
    
    def _motor_status(self, status_int):
        s = {'controller_status': motor_controller_statuses[status_int & 0x3f],
             'controller_error': bool(status_int & (1 << 7)),
             'motor_running': bool(status_int & (1 << 8)),
             'motor_off': bool(status_int & (1 << 9)),
             'invalid_motor_position': bool(status_int & (1 << 10)),
             'limit_switch1': bool(status_int & (1 << 11)),
             'limit_switch2': bool(status_int & (1 << 12)),
             'home_switch': bool(status_int & (1 << 13)),
             'manual_control': bool(status_int & (1 << 14))}
        return s
        
    @remote
    def bifi_status(self):
        status_int = int(self._execute('MOTBI:STA?'))
        return self._motor_status(status_int)
    
    @remote
    def bifi_home(self):
        """ Move the birefringent filter's stepper motor to its home position.
            Does not wait for completion of the motor movement. """
        self._execute('MOTBI:HOME')
    
    
    @remote
    def bifi_halt(self):
        """Stop a motion of the birefringent filter's stepper motor. 
        The command will use a smooth deceleration to maintain accurate step tracking. """
        self._execute('MOTBI:HALT')
    
    
    @remote
    def bifi_clear(self):
        """Clear pending errors at the birefringent filter motor controller."""
        self._execute('MOTBI:CL')
    
    
    @remote
    def set_bifi_wavelength(self, target):
        """ Move the birefringent filter to a wavelength position. 
        Does not wait for completion of the motor movement.
        
        target(float): desired wavelength in nm"""
        
        self._execute('MOTBI:WL {:.2f}'.format(target))
    
    
    @remote
    def get_bifi_wavelength(self):
        """Get the current position of the birefringent filter in terms of a wavelength. 
        The result is given in nanometers."""
        
        return float(self._execute('MOTBI:WL?'))

        
    @remote
    def get_wavelength(self):
        """ Returns the current position of the laser """
        return self.get_bifi_wavelength()
    
    @remote
    def get_te_position(self):
        """Get the current motor position of the thin etalon's stepper motor."""
        
        return int(self._execute('MOTTE:POS?'))
        
    
    @remote
    def set_te_position(self, pos):
        """ Move the stepper motor of the thin etalon to an absolute position. 
        The command does not wait for completion of the motor movement. """
        
        self._execute('MOTTE:POS {}'.format(pos))
    
    
    @remote
    def te_status(self):
        status_int = int(self._execute('MOTTE:STA?'))
        return self._motor_status(status_int)
    
    
    @remote
    def set_wavelength(self, target):
        self.set_bifi_wavelength(target)
        
    
    @remote
    def is_moving(self):
        return self.bifi_status()['motor_running'] or self.te_status()['motor_running']
    
    
    
    
@include_remote_methods(MatisseWorker)
class Matisse(DeviceOverZeroMQ):    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
                             
    def createDock(self, parentWidget, menu=None):
        """ Function for integration in GUI app. Implementation below 
        creates a button and a display """
        dock = QtWidgets.QDockWidget("Matisse laser", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        
        layout_form = QtWidgets.QFormLayout()
        self.display_nm = QtWidgets.QLineEdit()
        self.display_nm.setReadOnly(True)
        layout_form.addRow("Wavelength (air)", self.display_nm)
        self.display_energy = QtWidgets.QLineEdit()
        self.display_energy.setReadOnly(True)
        layout_form.addRow("Photon energy", self.display_energy)
        
        def on_click_nm(event):
            try:
                if event.button() == 1:
                    current = self.get_wavelength()
                    d, okPressed = QtWidgets.QInputDialog.getDouble(display, "Set wavelength", "Target wavelength (air) in nm:", current, 550, 1100)
                    if okPressed:
                        self.set_wavelength(d)
            except:
                pass
        self.display_nm.mousePressEvent  = on_click_nm
        
        def on_click_energy(event):
            try:
                if event.button() == 1:
                    current = H_C / self.get_wavelength()
                    d, okPressed = QtWidgets.QInputDialog.getDouble(display, "Set energy", "Target energy in meV:", current, 1000, 2500)
                    if okPressed:
                        self.set_wavelength(H_C / d)
            except:
                pass
        self.display_energy.mousePressEvent  = on_click_energy
        
        
        widget.setLayout(layout_form)
        
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
            
        # Following lines "turn on" the widget operation
        self.createListenerThread(self.updateSlot)

        
    def updateSlot(self, status):
        try:
            self.display_nm.setText("%.3f nm" % status["bifi_wavelength"])
            self.display_energy.setText("%.3f meV" % (H_C*N_AIR/status["bifi_wavelength"]))
        except:
            pass
