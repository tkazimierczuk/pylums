#!/usr/bin/python
import time, datetime
import paho.mqtt.client as mqtt
from subprocess import call
from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
from PyQt5 import QtCore, QtWidgets, QtGui
import pyvisa as visa
import io
import threading
import requests

sending_time = "1225"

default_req_port = 30005
default_pub_port = 30006

hook_test = "https://hooks.slack.com/services/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"
hook_lunch = "https://hooks.slack.com/services/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"
hook_main = hook_lunch


class LunchWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.date_last_sent = ""

    def init_device(self):
        print("Lunch bot has started")
        self.load_config()
        self.main_loop()

    def load_config(self):
        config_lines = io.open("config/lunch_bot.cfg", mode="r", encoding="utf-8").read().strip().split("\n")
        self.messages = {line.strip().split("#")[0].strip(): line.strip().split("#")[1].strip() for line in config_lines}

    def main_loop(self):
        while True:
            hour = datetime.datetime.fromtimestamp(time.time()).strftime("%H%M")
            date = datetime.datetime.fromtimestamp(time.time()).strftime("%y%m%d")
            if hour == sending_time and date != self.date_last_sent:
                self.load_config()
                self.date_last_sent = date
                weekday = datetime.datetime.fromtimestamp(time.time()).strftime("%a")
                message = ""
                if weekday in self.messages:
                    message = self.messages[weekday]
                if date in self.messages:
                    message = self.messages[date]
                self.send(message)
            time.sleep(5)

    def send(self, message):
        try:
            if message == "":
                return
            json = {'text':'%s' % (message,)}
            requests.post(url=hook_main, json=json)
        except Exception as e:
            print(e)

    def status(self):
        d = super().status()
        self.last_status = d
        return d
