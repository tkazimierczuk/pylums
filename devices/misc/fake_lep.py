from time import sleep
from time import perf_counter as clock
import serial
import serial.tools.list_ports
import logging
from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
import threading
import traceback
from PyQt5 import QtCore, QtWidgets, QtGui
import pyvisa as visa
import io
import datetime
import time
import re
from devices.motor_driver import MotorDriver


default_req_port = 19090
default_pub_port = 19091


class FakeLEPWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, address="COM30",
                 x_req_port = None, y_req_port = None, x_host="localhost", y_host="localhost", x_axis=0, y_axis=1, **kwargs):
        if not x_req_port or not y_req_port or not x_host or not y_host:
            print("Please specify x_req_port, y_req_port, x_host and y_host.")
            return
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.device_x = MotorDriver(req_port=x_req_port, host=x_host)
        self.device_y = MotorDriver(req_port=y_req_port, host=y_host)
        self.x = self.device_x.get_axis(int(x_axis))
        self.y = self.device_y.get_axis(int(y_axis))
        self.address = address
        self.LEPlastCommand = ''
        self.LEPcommands = {'STATUS': self.LEP_handle_status, \
                            'Panel': self.LEP_handle_OK, \
                            'A': self.LEP_handle_OK, \
                            'SPEED': self.LEP_handle_OK, \
                            'ACCEL': self.LEP_handle_OK, \
                            'WHERE': self.LEP_handle_where, \
                            'HALT': self.LEP_handle_OK, \
                            'JOYSTICK': self.LEP_handle_OK, \
                            'SPIN': self.LEP_handle_spin, \
                            'HERE': self.LEP_handle_here, \
                            'MOVE': self.LEP_handle_move
                            }
        self.sercom = None
        self.ScoordinX = 0  # standa coordins in mm zero correction
        self.ScoordinY = 0  # standa coordins in mm zero correction
        self.calibrating_in_progress = 0

        self.buffer = b""

    def __del__(self):
        try:
            self.sercom.close()
        except:
            pass

    def init_device(self):
        """
        Opens the communication
        """
        print("=========")
        print(str(datetime.datetime.now()))
        print("Communication at COM" + str(self.address[3:]) + " - initialisation..." )

        self.sercom = serial.Serial(port=self.address, baudrate=9600, bytesize=serial.EIGHTBITS, \
                                    parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_TWO, timeout=1)

        print("Serial communication with LEP protocol enabled!")

        self.thread = threading.Thread(target=self.run)
        self.thread.start()

    def status(self):
        d = super().status()
        return d

    def run(self):
        while True:
            self.LEPrecive()

    def LEPrecive(self):
        termination_chars='\r'
        answer_beg = b'\xff'
        answer_end = b'\r'
        try:
            mess = self.sercom.read_until(answer_end)
            if len(mess)>0 and not mess.endswith(answer_end):
              print("Possible error: ", str(mess))
              mess += self.sercom.read_until(answer_end)
              print("Fixed(?): ", str(mess))
        except:
            mess = ''
        #print("########### ", mess)
        lstr = str(mess)
        lstr = lstr[2:-1]
        lstr = lstr.replace('\\xff','.')
        lstr = lstr.replace('\\r','.')
        lstr = lstr.rsplit('.')
        str_list = list(filter(None,lstr))
        if len(str_list)>0:
            spl = str_list[-1].replace(' ','.')
            spl = spl.replace('=','.')
            spl = spl.split('.')
            command_class = spl[0]
            if command_class in self.LEPcommands:
                    self.LEPcommands[command_class](spl)
            else:
                print('Unknown command:',command_class)
                self.LEPcommands['HALT']()
            return str_list
        else:
            return #self.LEPrecive()

    def LEP_handle_OK(self, *args):
        self.sercom.write(b':A\n')

    def LEP_handle_move(self, *args):
        a = args[0]
        if len(a) >= 3:
            axis = a[1]
            target = int(a[2])
            try:
                if axis == 'X':
                    self.x.move_absolute(self.LEP2standaXunits(target))
                elif axis == 'Y':
                    self.y.move_absolute(self.LEP2standaXunits(target))
                else:
                    print("Zle miejsce XXX")
            except ConnectionError as err:
                print("Ignoring connection error")
        if len(a) >= 5:
            axis = a[3]
            target = int(a[4])
            try:
                if axis == 'X':
                    self.x.move_absolute(self.LEP2standaXunits(target))
                elif axis == 'Y':
                    self.y.move_absolute(self.LEP2standaXunits(target))
                else:
                    print("Zle miejsce XXX")
            except ConnectionError as err:
                print("Ignoring connection error")

        self.sercom.write(b':A\n')

    def LEP_handle_status(self, *args):
        try:
            Xmoving = not self.x.is_stopped()
            Ymoving = not self.y.is_stopped()
        except:
            print("Ignoring connection error")
            Xmoving = False
            Ymoving = False
        moving = Xmoving or Ymoving
        if moving:
            self.sercom.write(b'B\n')
            # busy
        else:
            if self.calibrating_in_progress:
                self.calibrating_in_progress -= 1
                time.sleep(1)
                self.sercom.write(b'B\n')
            else:
                self.sercom.write(b'N\n')

    def LEP_ignore(self, *args):
        self.sercom.write(b':A\n')
        print('ignored A sent ', args)

    def LEP_handle_spin(self, *args):
        self.calibrating_in_progress = 2
        a = args[0]
        try:
            if a[2] == '-400000':
                self.x.move_velocity(-2)
                self.y.move_velocity(-2)
            if a[2] == '400000':
                self.x.move_velocity(2)
                self.y.move_velocity(2)
        except:
            pass
        self.sercom.write(b':A\n')

    def LEP2standaXunits(self, x):
        '''standa mm from LEPsteps in Olympus '''
        return x / (100 * 400) + self.ScoordinX

    def LEP2standaYunits(self, y):
        '''standa mm from LEPsteps in Olympus '''
        return y / (100 * 400) + self.ScoordinY

    def LEPstandaX2LEP(self, x):
        '''standa mm to LEPsteps in Olympus in um'''
        return round((x - self.ScoordinX) * 100 * 400)

    def LEPstandaY2LEP(self, y):
        '''standa mm to LEPsteps in Olympus in um'''
        return round((y - self.ScoordinY) * 100 * 400)

    def LEP_handle_here(self, *args):
        self.ScoordinX = self.x.get_position()
        if abs(self.ScoordinX) < 0.5:  # mm
            self.ScoordinX = 0
        self.ScoordinY = self.y.get_position()
        if abs(self.ScoordinY) < 0.5:  # mm
            self.ScoordinY = 0
        self.sercom.write(b':A\n')

    def LEP_handle_where(self, *args):
        try:
            if args[0][1] == 'X':
                x = self.x.get_position()
                lx = self.LEPstandaX2LEP(x)
                ts = lx
            elif args[0][1] == 'Y':
                y = self.y.get_position()
                ly = self.LEPstandaY2LEP(y)
                ts = ly
            else:
                print('Unknown axis', args[0][1])
            reply = (':A %d\n' % (int(ts))).encode()
            self.sercom.write(reply)
            return
        except KeyError:
            pass
        except ConnectionError:
            print("!!!!! LEP Connection timeout !!!!!!!")
        self.sercom.write(b':N -1\n')  # if we fail reply something anyway


@include_remote_methods(FakeLEPWorker)
class FakeLEP(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def __del__(self):
        pass

    def createDock(self, parentWidget, menu=None):
        pass

    def updateSlot(self, status):
        pass
