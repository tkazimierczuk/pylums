#!/usr/bin/python
from time import sleep,time
import paho.mqtt.client as mqtt
from subprocess import call
from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
from PyQt5 import QtCore, QtWidgets, QtGui
import pyvisa as visa
import io
import requests

default_req_port = 30003
default_pub_port = 30004

timeout_helium = 6 * 3600
timeout_nitrogen = 2 * 3600
timeout_start = 0

hook_test = "https://hooks.slack.com/services/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"
hook_alerts = "https://hooks.slack.com/services/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"
hook_main = hook_alerts


class Alert():
    def __init__(self, topic, description, timeout, condition, hook, value_string=None):
        self.topic = topic
        self.description = description
        self.timeout = timeout
        self.condition = condition
        self.value = None
        self.error_sent = False
        self.last_time_bad_after_ok = 0
        self.last_was_ok = True
        self.hook = hook
        self.value_string = value_string
        self.last_ts = None

    def send(self):
        try:
            if self.timeout <= 0:
                timestr = ""
            elif self.timeout < 120 * 60:
                timestr = " for longer than %d minutes" % round(self.timeout / 60.)
            else:
                timestr = " for longer than %d hours" % round(self.timeout / 3600.)
            valstr = ""
            if self.value_string is not None:
                valstr = " (%s)" % self.value_string(self.value)
            message = "%s%s%s!" % (self.description, valstr, timestr)
            json = {'text': '%s' % (message,)}
            requests.post(url=hook_main, json=json)
        except Exception as e:
            print(e)


class AlertsWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

        self.alerts = [
                        Alert(topic="/magnet-vrm/helium", timeout=timeout_helium, condition=lambda x:x<5, hook=hook_main,
                              description="Helium level in VRM magnet is below 5 percent"),
                        Alert(topic="/magnet-vrm/nitrogen", timeout=timeout_nitrogen, condition=lambda x:x<5, hook=hook_main,
                              description="Nitrogen level in VRM magnet is below 5 percent"),
                        Alert(topic="/magnet-10t/helium", timeout=timeout_helium, condition=lambda x:x<5, hook=hook_main,
                              description="Helium level in 10T magnet is below 5 percent"),
                        Alert(topic="/magnet-10t/nitrogen", timeout=timeout_nitrogen, condition=lambda x:x<5, hook=hook_main,
                              description="Nitrogen level in 10T magnet is below 5 percent"),
                        Alert(topic="/magnet-11t/helium", timeout=timeout_helium, condition=lambda x: x < 5, hook=hook_main,
                              description="Helium level in 11T magnet is below 5 percent"),
                        Alert(topic="/magnet-11t/nitrogen", timeout=timeout_nitrogen, condition=lambda x: x < 5, hook=hook_main,
                              description="Nitrogen level in 11T magnet is below 5 percent"),
                        Alert(topic="/heliumpressure/presspump1", timeout=0, condition=lambda x: x > 1050, hook=hook_main,
                              description="Pressure at the output of 10T pump is higher than 1050hPa"),
                        Alert(topic="/heliumpressure/presspump2", timeout=0, condition=lambda x: x > 1050, hook=hook_main,
                              description="Pressure at the output of VRM pump is higher than 1050hPa"),
                        Alert(topic="/raspberry140/temphum", timeout=320, condition=lambda x: x[0] > 24, hook=hook_main,
                              description="Temperature in -1.40 is exceptionally high", value_string=lambda x: str(x[0]) + " deg. C"),
                        Alert(topic="/raspberry140/temphum", timeout=320, condition=lambda x: x[0] < 17, hook=hook_main,
                              description="Temperature in -1.40 is exceptionally low", value_string=lambda x: str(x[0]) + " deg. C")
                      ]
        self.connected = False

    def status(self):
        d = super().status()
        d["connected"] = self.connected
        self.last_status = d
        return d

    def on_log(self, client, obj, level, string):
        print(string)

    def init_device(self):
        while not self.connected:
            try:
                self.client = mqtt.Client("alerts_pylums2")
                self.client.on_connect = self.on_connect
                self.client.on_message = self.on_message
                #self.client.on_log = self.on_log
                self.client.username_pw_set('dms', 'cdmnte')
                self.client.connect("dms0.fuw.edu.pl", 1883)
                self.client.loop_start()
                sleep(2)
                if not self.connected:
                    print("reconnecting...")
                    sleep(8)
            except Exception:
                print(traceback.format_exc())


    def on_connect(self, client, userdata, flags, rc):
        for alert in self.alerts:
            client.subscribe(alert.topic)
        print("Connected with result code " + str(rc))
        self.connected = True

    def on_message(self, client, userdata, msg):
        try:
            val = float(msg.payload)
        except Exception:
            try:
                val = [float(x) for x in msg.payload.split()]
            except Exception as e:
                print(e)
        topic = msg.topic
        for alert in self.alerts:
            if alert.topic != topic:
                continue
            alert.value = val
            if alert.condition(val):
                if alert.last_was_ok:
                    alert.last_time_bad_after_ok = time()
                    alert.last_was_ok = False
            else:
                alert.last_was_ok = True
                alert.error_sent = False

        for alert in self.alerts:
            if alert.value is not None:
                if time() - alert.last_time_bad_after_ok > alert.timeout and alert.condition(alert.value) and not alert.error_sent:
                    alert.error_sent = True
                    alert.send()

