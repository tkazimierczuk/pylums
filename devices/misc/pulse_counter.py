# -*- coding: utf-8 -*-
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets,QtCore
from PyQt5.QtWidgets import (QPushButton, QMessageBox, QDialog)
from PyQt5.QtWidgets import (QApplication, QWidget, QMainWindow)
from PyQt5.QtWidgets import (QVBoxLayout, QHBoxLayout, QLineEdit)
from PyQt5.QtWidgets import (QLabel, QInputDialog)
from PyQt5.QtGui import (QFont, QColor)
from devices.lakeshore.temperature33x import my_LCD
import scipy.optimize
import numpy as np
import time

HIST_TIME = 1 # in seconds
HIST_FRESH = 0.02 # if record is not longer than 20ms in the past it is still fresh

class PulseCounterWorker(DeviceWorker):
    '''The class contains every methods needed to talk to the motor'''
       
    def __init__(self, port, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.port = port
        
    def init_device(self):
        from collections import deque
        self._hist = deque()
        
        import serial
        self._ser = serial.Serial(self.port, 115200, timeout=0.3)
        self._ser.flushInput()
        print("Start count:", self.get_current_count())
        
    def __del__(self):
        self._ser.close() #serial port close
        
    def status(self):
        d = super().status()
        d['total'] = self.get_current_count2()
        d['history'] = self.get_history_length()
        d['cps'] = self.counts_per_seconds()
        return d

    @remote
    def get_current_count(self):
        self._ser.write(b'c')
        r = self._ser.read(4)
        count = int.from_bytes(r, byteorder='little', signed=False)
        now = time.time()
        self._hist.append( (now,count) )
        while len(self._hist) > 1 and self._hist[1][0] < now-HIST_TIME:
            self._hist.popleft()
        return count
        
    @remote
    def get_current_count2(self):
        """ Returns the current total count. This version is designed
        for the monitoring purposes - it does not call the device if 
        the most recent readout is still fresh """
        now = time.time()
        if len(self._hist) == 0 or self._hist[-1][0] < now - HIST_FRESH:
            return self.get_current_count()
        else:
            return self._hist[-1][1]
        
    @remote
    def get_history_length(self):
        return len(self._hist)
        
    @remote
    def counts_per_seconds(self):
        now = time.time()
        if len(self._hist) == 0 or self._hist[-1][0] < now - HIST_FRESH:
            self.get_current_count()
            now = time.time()
        while len(self._hist) > 1 and self._hist[1][0] < now-HIST_TIME:
            self._hist.popleft()
        try:
            return (self._hist[-1][1]-self._hist[0][1])/(self._hist[-1][0]-self._hist[0][0])
        except ZeroDivisionError:
            return 0

      
    
@include_remote_methods(PulseCounterWorker)
class PulseCounter(DeviceOverZeroMQ):  
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
                             
    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("Pulse counter", parentWidget)
        
        self.display_cps = my_LCD(digit_count=7,decimals=0,dialog=False)
        
        dock.setWidget(self.display_cps)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
            
        # Following lines "turn on" the widget operation
        self.createListenerThread(self.updateSlot)

        
    def updateSlot(self, status):
        try:
            self.display_cps.display(status["cps"])
        except:
            pass

