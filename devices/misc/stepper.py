from time import sleep
from time import perf_counter as clock
import serial
import serial.tools.list_ports
import logging
from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
import threading
import traceback
from PyQt5 import QtCore, QtWidgets, QtGui
import pyvisa as visa
import io
import re
from devices.axis import Axis


default_req_port = 7040
default_pub_port = 7041


class StepperWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port,
                 id=0, current="", step="", velocity="", max_velocity="", hysteresis="", limit_type="", \
                 acceleration_time="", reversed="", homing_offset="", clone_axis="", axis_names="", emergency_button="", **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.position = 0
        def parse(string, type=float):
            if string != "":
                return [type(s.strip()) for s in string.split(',')]
            else:
                return []
        self.currents = parse(current)
        self.steps = parse(step)
        self.velocities = parse(velocity)
        self.max_velocities = parse(max_velocity)
        self.hystereses = parse(hysteresis)
        self.acceleration_times = parse(acceleration_time)
        self.limit_types = parse(limit_type, int)
        self.reversed = parse(reversed, int)
        self.homing_offsets = parse(homing_offset)
        self.clone_axis = clone_axis
        self.emergency_buttons = parse(emergency_button, int)
        self.axis_names = parse(axis_names, str)
        self.axis_by_name = {}
        self.id = int(id)
        self.read_buffer = ""
        self.last_axes_count = None

    def init_device(self):
        # ser = serial.serial_for_url('loop://', timeout=1)
        # self.serial = io.TextIOWrapper(io.BufferedRWPair(ser, ser), newline="\r")

        port_names = [port.device for port in serial.tools.list_ports.comports() \
                      if port.vid == 1155 and port.pid == 14155] # Nucleo ST-LINK

        self._address = None

        found = []
        for iteration in range(5):
            ports_to_retry = []
            for port in port_names:
                try:
                    self.serial = serial.Serial(port, 115200, timeout=1)
                    self.serial.Terminator = '\n'
                    device_id = self.get_id(allowed_retries=0)
                    if device_id > 0:
                        found.append(device_id)
                    if device_id == self.id:
                        self._address = port # found responding unit with correct id
                        break
                    sleep(0.1)
                except:
                    ports_to_retry.append(port)
            if self._address is not None:
                break
            try:
                self.serial.close()
            except:
                pass
            port_names = ports_to_retry
            sleep(0.9)

        if self._address is None:
            raise Exception(("Could not find unoccupied device with id %d. Found other unused drivers: " % (self.id,)) + str(found))

        #self.serial = serial.Serial(self._address, 115200, timeout=1)
        self.serial.Terminator = '\n'
        self.execute("?\r", reply_pattern=r"^\?$")  # clean the buffers of device and computer
        for i in range(len(self.axes())):
            self.axis_by_name[i] = i
            self.axis_by_name[self.get_name(i)] = i
        for i in range(len(self.currents)):
            self.set_current(i, self.currents[i])
        for i in range(len(self.steps)):
            self.set_step(i, self.steps[i])
        for i in range(len(self.velocities)):
            self.set_velocity(i, self.velocities[i])
        for i in range(len(self.max_velocities)):
            self.set_max_velocity(i, self.max_velocities[i])
        for i in range(len(self.hystereses)):
            self.set_hysteresis(i, self.hystereses[i])
        for i in range(len(self.acceleration_times)):
            self.set_acceleration(i, self.acceleration_times[i])
        for i in range(len(self.limit_types)):
            self.set_limit_type(i, self.limit_types[i])
        for i in range(len(self.reversed)):
            self.set_reversed(i, self.reversed[i])
        for i in range(len(self.homing_offsets)):
            self.set_homing_offset(i, self.homing_offsets[i])
        if self.clone_axis != "":
            if int(self.clone_axis) in self.axes():
                self.set_clone_axis(int(self.clone_axis))
        for i in range(len(self.emergency_buttons)):
            self.set_emergency_button(i, self.emergency_buttons[i])
        print("Driver no.", self.id, "- following axes are now enabled:", self.axes())
        self.status()

    def status(self):
        d = super().status()
        positions, modes = self.get_all()
        for i in self.axes():
            d["axis%d_pos" % i] = positions[i]
            d["axis%d_status" % i] = modes[i]
        self.last_status = d
        return d

    @remote
    def execute(self, message, reply_pattern=None, allowed_retries=5):
        data = bytes(message, 'utf-8') + b'\r'
        self.serial.write(data)
        res = self.serial.read_until(b'\r').decode('utf-8').strip()

        if reply_pattern:
            if not re.match(pattern=reply_pattern, string=res):
                if allowed_retries > 0:
                    print("Warning! Wrong reply (Q:%s, R:%s)" % (message, res))
                    self.serial.reset_input_buffer()
                    return self.execute(message, reply_pattern, allowed_retries - 1)
                else:
                    raise Exception("Wrong reply (Q:%s, R:%s)" % (message, res))
        else:
            print("Unverified reply (Q:%s, R:%s)" % (message, res))

        return res

    @remote
    def get_position_true(self, axis):
        try:
            s = self.execute('%dtp' % self.axis_by_name[axis], reply_pattern=r"^tp")[2:]
            position = float(s)
        except Exception as e:
            print(e)
            position = 0
        return position

    @remote
    def get_debug(self, axis):
        try:
            s = self.execute('%ddb' % axis, reply_pattern=r"^db")[2:]
        except Exception as e:
            print(e)
            s = ""
        return s

    @remote
    def get_status(self, axis):
        try:
            s = self.execute('%dts' % self.axis_by_name[axis], reply_pattern=r"^ts")[2:]
            status = int(s)
        except Exception as e:
            print(e)
            status = None
        return status

    @remote
    def is_stopped(self, axis):
        return self.get_status(axis) == 0

    @remote
    def get_all(self, allowed_retries=5):
        try:
            str = self.execute('ta', reply_pattern=r"^ta", allowed_retries=0)[2:]
            str_list = str.split(' ')
            positions = [float(str_list[i]) for i in self.axes()]
            modes = [{'0':'stopped', '1':'velocity', '2':'position', '3':'homing', '4':'emergency stop'}[x] \
                     for x in list(str_list[len(self.axes())].strip())]
            assert len(positions) == len(modes) == len(self.axes()), "Incomplete data from all axes status command!"
        except Exception as e:
            if allowed_retries > 0:
                #print(traceback.format_exc())
                print("Warning! Wrong Tell All reply.")
                return self.get_all(allowed_retries - 1)
            else:
                raise Exception("Wrong Tell All reply")
        return (positions, modes)

    @remote
    def get_id(self, allowed_retries=5):
        try:
            s = self.execute('id', reply_pattern=r"^id", allowed_retries=allowed_retries)[2:]
            res = int(s)
        except Exception as e:
            print(e)
            res = 0
        return res

    @remote
    def get_position(self, axis):
        return self.last_status["axis%d_pos" % self.axis_by_name[axis]]

    @remote
    def get_name(self, axis):
        try:
            return self.axis_names[self.axis_by_name[axis]]
        except:
            return str(axis)

    @remote
    def set_name(self, axis, name):
        self.axis_names[self.axis_by_name[axis]] = name

    @remote
    def axes(self):
        if self.last_axes_count is None:
            str = self.execute('ac', reply_pattern=r"^ac")[2:]
            self.last_axes_count = int(str)
        return list(range(self.last_axes_count))

    @remote
    def move_absolute(self, axis, position):
        self.execute('%dma%f' % (self.axis_by_name[axis], position), reply_pattern=r"^ma$")

    @remote
    def move_velocity(self, axis, velocity):
        self.execute('%dmv%f' % (self.axis_by_name[axis], velocity), reply_pattern=r"^mv$")

    @remote
    def set_emergency_button(self, axis, is_enabled):
        self.execute('%dse%d' % (self.axis_by_name[axis], is_enabled), reply_pattern=r"^se$")

    @remote
    def stop(self, axis):
        self.execute('%dmv' % (self.axis_by_name[axis],), reply_pattern=r"^mv$")

    @remote
    def stop_all(self):
        for a in self.axes():
            self.execute('%dmv' % (a,), reply_pattern=r"^mv$")

    @remote
    def move_relative(self, axis, distance):
        self.execute('%dmr%f' % (self.axis_by_name[axis], distance), reply_pattern=r"^mr$")

    @remote
    def home(self, axis):
        self.execute('%dhm' % (self.axis_by_name[axis]), reply_pattern=r"^hm$")

    @remote
    def set_clone_axis(self, axis):
        self.execute('%sca' % (self.axis_by_name[axis]), reply_pattern=r"^ca$")

    @remote
    def set_current(self, axis, val):
        self.execute('%dsc%d' % (int(self.axis_by_name[axis]), int(val)), reply_pattern=r"^sc$")

    @remote
    def set_velocity(self, axis, val): # velocity used in move to postion mode
        self.execute('%dsv%f' % (int(self.axis_by_name[axis]), float(val)), reply_pattern=r"^sv$")

    @remote
    def set_acceleration(self, axis, val): # seconds taken to accelerate to set_velocity speed
        self.execute('%dsa%f' % (int(self.axis_by_name[axis]), float(val)), reply_pattern=r"^sa$")

    @remote
    def set_max_velocity(self, axis, val):
        self.execute('%dsm%f' % (int(self.axis_by_name[axis]), float(val)), reply_pattern=r"^sm$")

    @remote
    def set_step(self, axis, val):
        self.execute('%dss%f' % (int(self.axis_by_name[axis]), float(val)), reply_pattern=r"^ss$")

    @remote
    def set_hysteresis(self, axis, val):
        self.execute('%dsh%f' % (int(self.axis_by_name[axis]), float(val)), reply_pattern=r"^sh$")

    @remote
    def set_limit_type(self, axis, val):
        self.execute('%dsl%d' % (int(self.axis_by_name[axis]), int(val)), reply_pattern=r"^sl$")

    @remote
    def set_homing_offset(self, axis, val):
        self.execute('%dso%f' % (int(self.axis_by_name[axis]), float(val)), reply_pattern=r"^so$")

    @remote
    def set_reversed(self, axis, val=1):
        self.execute('%dsr%d' % (int(self.axis_by_name[axis]), int(val)), reply_pattern=r"^sr$")


@include_remote_methods(StepperWorker)
class Stepper(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def __del__(self):
        pass

    def get_axis(self, axis):
        return Axis(self, axis)

    def createDock(self, parentWidget, menu=None):
        self.parentWidget = parentWidget
        dock = QtWidgets.QDockWidget("Stepper", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QVBoxLayout(widget)
        layout.setSpacing(2)
        widget.setLayout(layout)

        self.axis_widgets = {axis: self.createWidgetForAxis(layout, axis)
                             for axis in self.axes()}

        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

    def createWidgetForAxis(self, layout, axis):
        widget = QtWidgets.QWidget(layout.parent())
        hlayout = QtWidgets.QHBoxLayout(widget)
        hlayout.setContentsMargins(0, 0, 0, 0)
        label = QtWidgets.QLabel()
        name = self.get_name(axis)
        if name != "":
            label.setText(name)
        else:
            label.setText("Axis " + str(axis))


        hlayout.addWidget(label)
        display = QtWidgets.QLCDNumber()
        display.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        display.setDigitCount(9)
        display.decimals = 4
        policy = display.sizePolicy()
        policy.setHorizontalStretch(10)
        policy.setVerticalStretch(10)
        display.setSizePolicy(policy)
        hlayout.addWidget(display)
        home_button = QtWidgets.QPushButton("Home")
        home_button.setFixedWidth(50)
        home_button.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        #home_button.setFixedHeight(22)
        def home_clicked():
            clicked_button = QtGui.QMessageBox.question(self.parentWidget,'', "Are you sure you want to perform homing procedure?",
                                                   QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
            if clicked_button == QtGui.QMessageBox.Yes:
                self.home(axis)
        home_button.clicked.connect(home_clicked)
        hlayout.addWidget(home_button)
        hlayout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(widget)

        def on_click(event):
            if event.button() == 1:
                current = self.get_position(axis)
                d, okPressed = QtWidgets.QInputDialog.getDouble(display, "Go to", "Target:", current, decimals=4)
                if okPressed:
                    self.move_absolute(axis, d)
        display.mousePressEvent  = on_click
        return (display,)

    def connectToDevice(self):
        try:
            self.connect()
        except:
            self.disconnectFromDevice()

    def disconnectFromDevice(self):
        try:
            self.disconnect()
        except:
            pass

    def updateSlot(self, status):
        try:
            for axis in self.axis_widgets:
                self.axis_widgets[axis][0].display("%4.4f" % status["axis%d_pos" % axis])
                if status["axis%d_status" % axis] == "emergency stop":
                    self.axis_widgets[axis][0].display("---")
        except Exception as e:
            print(e)



