from time import sleep
from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
from devices.axis import Axis
from devices.misc.stepper import Stepper, StepperWorker

default_req_port = 17040
default_pub_port = 17041

class TilterWorker(StepperWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, id=0, acceleration_time=0.2,
                 hysteresis=0.01, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, step="0.00244141,0.00244141",
                         hysteresis=f"{hysteresis},{hysteresis}", axis_names="x,y", velocity="1,1", max_velocity="1,1",
                         id=id, acceleration_time=f"{acceleration_time},{acceleration_time}")

    def init_device(self):
        super().init_device()

Tilter = Stepper