import datetime
from time import sleep

import serial
from PyQt5 import QtWidgets, QtCore
from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods

default_req_port = 14020
default_pub_port = 14021


class KoradWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, port="COM22", **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

        self._port = port

    def init_device(self):
        """
        Opens the communication 
        """
        print("=========")
        print(str(datetime.datetime.now()))
        print("Korad PS: Initialization...")

        self.serial = serial.Serial(self._port, 9600, timeout=1.5)
        print(self._execute('*IDN?', replysize=16))


        status = bytes(self._execute('STATUS?', replysize=1), 'utf-8')
        # print (int(bytes(status, 'utf-8')))
        # print ('Channel 1 - ', end='')
        # print ('CC mode' if (bytes(status) & 1) else 'CV mode')
        # print ('Channel 2 -', ('CC mode' if (status & 2) True else 'CV mode'))
        # print ('Channel 2 -', ('CC mode' if (status & 12) else 'CV mode'))

        print("Korad PS: Success")

    @remote
    def _execute(self, message, replysize = 0):
        self.serial.reset_input_buffer()
        data = bytes(message, 'utf-8')
        self.serial.write(data)
        self.serial.flush()
        sleep(0.05)
        if replysize:
            res = self.serial.read(size=replysize).decode('utf-8').strip().rstrip("K")
            sleep(0.05) #necessery to wait for the remaing string to make sure the buffer is clear
            self.serial.reset_input_buffer() #if not applied then one may get a char 'K' from Power Supply's reply when asking for the current output current
            return res
        sleep(0.05)
        self.serial.reset_input_buffer()

    def status(self):
        d = super().status()
        d['out'] = self.get_output()
        d['vset'] = self.get_set_voltage()
        d['iset'] = self.get_set_current()
        d['vout'] = self.get_output_voltage()
        d['iout'] = self.get_output_current()
        return d


    @remote
    def get_set_current(self):
        return float(self._execute('ISET1?', replysize=5))

    @remote
    def get_set_voltage(self):
        return float(self._execute('VSET1?', replysize=5))

    @remote
    def get_output_current(self):
        return float(self._execute('IOUT1?', replysize=5))

    @remote
    def get_output_voltage(self):
        return float(self._execute('VOUT1?', replysize=5))

    @remote
    def set_current(self, value):
        if 0. <= value <= 5.1:
            self._execute(f'ISET1:{round(value, 3)}')
        else:
            return 'Value out of range, use 0-5.1A'

    @remote
    def set_voltage(self, value):
        if 0. <= value <= 31.:
            self._execute(f'VSET1:{round(value, 2)}')
        else:
            return 'Value out of range, use 0-31V'

    @remote
    def set_output(self, value):
        if value == 1:
            self._execute(f'OUT1')
        elif value == 0:
            self._execute(f'OUT0')
        else:
            return 'Invalid value. To reset use 0 or False; to set use 1 or True.'

    @remote
    def get_output(self):
        return bool(int.from_bytes(bytes(self._execute('STATUS?', replysize=1), 'utf-8'), byteorder='big', signed=False) & 64)

    @remote
    def toggle_output(self):
        self.set_output(not (self.get_output()))

    @remote
    def set_beep(self, value):
        if value == 1:
            self._execute(f'BEEP1')
        elif value == 0:
            self._execute(f'BEEP0')
        else:
            return 'Invalid value. To reset use 0 or False; to set use 1 or True.'

    @remote
    def set_ocp(self, value):
        if value == 1:
            self._execute(f'OCP1')
        elif value == 0:
            self._execute(f'OCP0')
        else:
            return 'Invalid value. To reset use 0 or False; to set use 1 or True.'

    @remote
    def set_ovp(self, value):
        if value == 1:
            self._execute(f'OVP1')
        elif value == 0:
            self._execute(f'OVP0')
        else:
            return 'Invalid value. To reset use 0 or False; to set use 1 or True.'


@include_remote_methods(KoradWorker)
class Korad(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("Korad PS", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QGridLayout(widget)
        layout.setSpacing(1)
        widget.setLayout(layout)

        self.button_output = QtWidgets.QPushButton("OUTPUT", parentWidget)
        self.button_output.clicked.connect(lambda: self.toggle_output())
        layout.addWidget(self.button_output, 0, 0, 1, 2)

        layout.addWidget(QtWidgets.QLabel("Set Volt. [V]"), 1, 0)
        self.display_set_voltage = QtWidgets.QLineEdit(parentWidget)
        self.display_set_voltage.setFixedWidth(63)

        def voltage_edit(event):
            if event.button() == 1:
                voltage = float(self.display_set_voltage.text())
                d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.display_set_voltage, "New value", "Input [V]:",
                                                                voltage, decimals=2)
                if ok_pressed:
                    self.set_voltage(d)

        self.display_set_voltage.mousePressEvent = voltage_edit
        layout.addWidget(self.display_set_voltage, 2, 0)

        layout.addWidget(QtWidgets.QLabel("Set Curr. [A]"), 3, 0)
        self.display_set_current = QtWidgets.QLineEdit(parentWidget)
        self.display_set_current.setFixedWidth(63)

        def current_edit(event):
            if event.button() == 1:
                current = float(self.display_set_current.text())
                d, okPressed = QtWidgets.QInputDialog.getDouble(self.display_set_current, "New value", "Input [A]:",
                                                                current, decimals=3)
                if okPressed:
                    self.set_current(d)

        self.display_set_current.mousePressEvent = current_edit
        layout.addWidget(self.display_set_current, 4, 0)

        layout.addWidget(QtWidgets.QLabel("Real Volt. [V]"), 1, 1)
        self.display_real_voltage = QtWidgets.QLineEdit(parentWidget)
        self.display_real_voltage.setFixedWidth(63)
        layout.addWidget(self.display_real_voltage, 2, 1)

        layout.addWidget(QtWidgets.QLabel("Real Curr. [A]"), 3, 1)
        self.display_real_current = QtWidgets.QLineEdit(parentWidget)
        self.display_real_current.setFixedWidth(63)
        layout.addWidget(self.display_real_current, 4, 1)

        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

    def updateSlot(self, status):
        if status["out"]:
            self.button_output.setStyleSheet("QPushButton {background-color: red;color: white;} ")
        else:
            self.button_output.setStyleSheet("QPushButton {} ")
        self.display_set_voltage.setText(f'{status["vset"]:.2f}')
        self.display_set_current.setText(f'{status["iset"]:.3f}')
        self.display_real_voltage.setText(f'{status["vout"]:.2f}')
        self.display_real_current.setText(f'{status["iout"]:.3f}')
