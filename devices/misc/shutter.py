# -*- coding: utf-8 -*-
"""
Created on Mon Nov 12 12:51:57 2018

@author: dms
"""
import serial
import time
from PyQt5 import QtWidgets,QtCore



from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods

class ShutterWorker(DeviceWorker):
    
    def __init__(self, *args, com="COM30",baud=9600, **kwargs):
        super().__init__(*args, **kwargs)
        self.baud = baud
        self.com = com
        
    def init_device(self):
        self.comp = serial.Serial(self.com,self.baud, timeout=2)
        print("Waiting 5s for arduino to reset")
        time.sleep(5)
        print("ok")
        print(self.comp.name)
        
         
    def status(self):
        d = super().status()
        for axis in range(1,4):
            d["open%d" % axis] = self.is_shutter_open(axis)
            d["enabled%d" % axis] = self.is_shutter_enabled(axis)               
        return d
      
    @remote
    def is_shutter_open(self, number):
        self.comp.write(b"gets %d\n" % number)
        odp = self.comp.readline()
        #print("Received: ", str(odp))
        odp_num = int(odp[0:1])
        if number != odp_num:
            raise Exception("Error in communication with device. Requested axis: %d, received axis: %d" % (number, odp_num))
        return odp[5:6] == b'1'
        
    @remote
    def is_shutter_enabled(self, number):
        self.comp.write(b"gets %d\n" % number)
        odp = self.comp.readline()
        #print("Received: ", str(odp))
        odp_num = int(odp[0:1])
        if number != odp_num:
            raise Exception("Error in communication with device. Requested axis: %d, received axis: %d" % (number, odp_num))
        return odp[3:4] == b'1'
    
    @remote
    def open_shutter(self,number):
        info="sets "+str(number)+" 1\n"
        
        self.comp.write(info.encode('ascii'))
        
    @remote    
    def close_shutter(self,number):
        info="sets "+str(number)+" 0\n"
        print(info)
        self.comp.write(info.encode('ascii'))
        

@include_remote_methods(ShutterWorker)
class Shutter(DeviceOverZeroMQ):
    """ Simple stub for the class to be accessed by the user """
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    def _generate_func(self, number):
        def change_shutter_state(on):
            if on:
                self.open_shutter(number)
            else:
                self.close_shutter(number)
        return change_shutter_state
        
    def createDock(self, parentWidget, menu=None):
        """ Function for integration in GUI app. Implementation below 
        creates a button and a display """
        dock = QtWidgets.QDockWidget("Beam shutter", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QHBoxLayout(parentWidget)
        widget.setLayout(layout)
               
        self.buttons = {}
        for axis in range(1,4):
            button = QtWidgets.QPushButton("%d" % axis)
            button.setFixedWidth(25)
            button.setCheckable(True)
            button.clicked.connect( self._generate_func(axis) )
            layout.addWidget(button)
            self.buttons[axis] = button
                    
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
            
        # Following lines "turn on" the widget operation
        self.createListenerThread(self.updateSlot)

        
    def updateSlot(self, status):
        """ This function receives periodic updates from the worker """
        for axis in self.buttons:
            try:
                self.buttons[axis].setChecked( status["open%d" % axis] )
                self.buttons[axis].setEnabled( status["enabled%d" % axis] )
            except KeyError:
                pass