# -*- coding: utf-8 -*-
"""
"""

from .spectrometer import *
from PyQt5 import QtWidgets,QtCore
import numpy as np

class AndorCom0ComWorker(SpectrometerWorker):
    
    def __init__(self, *args, port="COM8", baudrate=500000, **kwargs):
        super().__init__(*args, **kwargs)
        self.port = port
        self.baudrate = baudrate
        self.spectrum_started = False
        self.buffer = b''
        self.latest_data = None
        
    def init_device(self):
        import pyvisa
        rm = pyvisa.ResourceManager()
        self.ser = rm.open_resource(self.port)
        self.ser.baud_rate = 500000
        self.ser.read_termination=chr(7)
        try:
            self.send_handshake()
            self._read_actual_exposure_parameters()
        except Exception:
            print("Information: handshake was not received. Before starting your measurement make "\
                  "sure ListenForLabview script is running in Andor Solis.")
        self._continuous = False
   
    def send_handshake(self):
        self.ser.write_raw('helo\r')
        while True:
            ret = self.ser.read_raw(1000)
            if ret== b'ehlo\7':
                print("Handshake successful")
                return True
               
    def set_cooler(self, state):
        pass
    
    def get_cooler_state(self):
        return True
    
    def get_temperature(self):
        return np.nan
        
    def set_exposure(self, exp_time, number):
        self.ser.write_raw('setAccumulate\r')
        ret = self.ser.read_raw(5)
        self.ser.write_raw('%f\r' % exp_time)
        ret = self.ser.read_raw(5)
        self.ser.write_raw('%d\r' % number)
        ret = self.ser.read_raw(5)
        self._read_actual_exposure_parameters()
        
    def _read_actual_exposure_parameters(self):
        if not self.send_handshake():
           raise Exception("Handshake failed")
        self.ser.write_raw('getAccumulateParameters\r')
        ret = self.ser.read_raw(100)
        ret = ret.decode('ascii')[:-1] # remove trailing \0x07
        t,n = ret.split(' ')
        self._exp_time = float(t)
        self._exp_number = int(n)
                
        
    def start_single_acquisition(self):
        self.ser.write_raw('getM\r')
        self.spectrum_started = True
        self.buffer = b''
    
    def start_acquisition(self, continuous=False):
        self._read_actual_exposure_parameters()
        self._continuous = continuous
        self.start_single_acquisition()
        
    def abort_acquisition(self):
        self._continuous = False
    
    def parse_spectrum(self):
        data = [list(map(float, line.split())) for line in  self.buffer.splitlines()[:-1]]
        data = np.transpose(np.array(data, dtype=np.float32))
        if data.shape[1] == 2:
            ydata = data[:,1]
        else:
            ydata = np.transpose(data[:,1:])
        self.latest_data = (data[:,0], ydata)
        if self._continuous:
            self.start_single_acquisition()
        self.send_via_pubchannel(b'spectrum', self.latest_data)
    
    def check_if_spectrum_ready(self):
        if not self.spectrum_started:
            return
        while self.ser.bytes_in_buffer > 0:
            self.buffer += self.ser.read_raw(self.ser.bytes_in_buffer)
            if self.buffer[-1] == 7:
                self.spectrum_started = False
                return self.parse_spectrum()
                
    
    def acquisition_status(self):
        self.check_if_spectrum_ready()
        if self.spectrum_started:
            if self._continuous:
                return 'continuous'
            else:
                return 'running'
        else:
            return 'idle'

    def get_latest_data(self):
        self.check_if_spectrum_ready()
        return self.latest_data