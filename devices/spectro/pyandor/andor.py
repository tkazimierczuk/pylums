#   pyAndor - A Python wrapper for Andor's scientific cameras
#   Copyright (C) 2009  Hamid Ohadi
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   Code modified by Tomasz Kazimierczuk (2018)

import platform
from ctypes import WinDLL,cdll,byref,c_float,c_int,c_long,c_char_p,pointer
import sys
import numpy as np
import threading
import time
import weakref
from time import perf_counter as clock
"""Andor class which is meant to provide the Python version of the same
   functions that are defined in the Andor's SDK. Since Python does not
   have pass by reference for immutable variables, some of these variables
   are actually stored in the class instance. For example the temperature,
   gain, gainRange, status etc. are stored in the class. """

import os
os.environ["PATH"] = os.environ["PATH"] + os.pathsep + os.path.dirname(__file__)
   
class Andor:
    def __init__(self, serial=None):

        # Check operating system and load library
        # for Windows
        if platform.system() == "Windows":
            if platform.architecture()[0] == "32bit":
                self.dll = WinDLL("atmcd32d")
            else:
                self.dll = WinDLL("atmcd64d")
        # for Linux
        elif platform.system() == "Linux":
            dllname = "/usr/local/lib/libandor.so"
            self.dll = cdll.LoadLibrary(dllname)
        else:
            print("Cannot detect operating system, wil now stop")
            raise Exception()

        self.verbosity   = True
            
        if serial:
            # TODO: select camera based on given serial number
            pass
         
        self.Initialize()
        self.GetStatus()
        self.GetCameraSerialNumber() # sets self.serial
        self.CoolerON()

        cw = c_int()
        ch = c_int()
        self.dll.GetDetector(byref(cw), byref(ch))
          
    
        self.width       = cw.value
        self.height      = ch.value
        self.temperature = None
        self.set_T       = None
        self.gain        = None
        self.gainRange   = None
                
        self.preampgain  = None
        self.channel     = None
        self.outamp      = None
        self.hsspeed     = None
        self.vsspeed     = None
        self.exposure    = None
        self.accumulate  = None
        self.kinetic     = None
        self.ReadMode    = None
        self.AcquisitionMode = None
        self.scans       = 1
        self.hbin        = 1
        self.vbin        = 1
        self.hstart      = 1
        self.hend        = cw
        self.vstart      = 1
        self.vend        = ch
        self.cooler      = None
        self.hsspeed = None
        self.hsspeed_index = 0
        self.available_hsspeeds = None
        self.number_hsspeeds = None
        self.GetHSSpeed()
        self.hsspeed_index = 0
        self.hsspeed = self.available_hsspeeds[0]
                
        pw = c_float()
        ph = c_float()
        self.dll.GetPixelSize(byref(pw), byref(ph))
        self.pixel_size = (pw,ph)
        
        self.could_be_running = False
        self.last_image_index = 0
        
        #TODO: Turn on cosmic ray removal by SetFilterMode
        self.SetDemoReady()
        
    def __del__(self):
        self.CoolerOFF()
        error = self.dll.ShutDown()
        
    def install_callback(self, func):
        # installs a function that is called upon new data
        def poll():
            while True:
                self.dll.WaitForAcquisition()
                t0=clock()
                arr = self.GetNewDataAsArray()
                if arr is not None:
                    func(arr)
        self.callback = threading.Thread(target = poll)
        self.callback.daemon = True
        self.callback.start()
        
    def verbose(self, error, function=''):
        if self.verbosity is True:
            print("[%s]: %s" %(function, error))

    def SetVerbose(self, state=True):
        self.verbose = state

    def AbortAcquisition(self):
        error = self.dll.AbortAcquisition()
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def Initialize(self):
        if platform.system() == "Linux":
            tekst = c_char_p(b"/usr/local/etc/andor")
        else:
            tekst = c_char_p(b"")
        error = self.dll.Initialize(tekst)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]
        
    def ShutDown(self):
        error = self.dll.ShutDown()
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]
        
    def GetCameraSerialNumber(self):
        serial = c_int()
        error = self.dll.GetCameraSerialNumber(byref(serial))
        self.serial = serial.value
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    READMODE_FVB = 0
    READMODE_MULTITRACK = 1
    READMODE_RANDOMTRACK = 2
    READMODE_SINGLETRACK = 3
    READMODE_IMAGE = 4

    def SetReadMode(self, mode):
        #0: Full vertical binning
        #1: multi track
        #2: random track
        #3: single track
        #4: image
        error = self.dll.SetReadMode(mode)
        self.ReadMode = mode
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    ACQUISITIONMODE_SINGLESCAN = 1
    ACQUISITIONMODE_ACCUMULATE = 2
    ACQUISITIONMODE_KINETICS = 3
    ACQUISITIONMODE_FASTKINETICS = 4
    ACQUISITIONMODE_RUNTILLABORT = 5
    
    def SetAcquisitionMode(self, mode):
        #1: Single scan
        #3: Kinetic scan
        error = self.dll.SetAcquisitionMode(mode)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        self.AcquisitionMode = mode
        return ERROR_CODE[error]
        
    def SetNumberKinetics(self,numKin):
        error = self.dll.SetNumberKinetics(numKin)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        self.scans = numKin
        return ERROR_CODE[error]

    def SetNumberAccumulations(self,number):
        error = self.dll.SetNumberAccumulations(number)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def SetAccumulationCycleTime(self,time):
        error = self.dll.SetAccumulationCycleTime(c_float(time))
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def SetKineticCycleTime(self,time):
        error = self.dll.SetKineticCycleTime(c_float(time))
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def SetShutter(self,typ,mode,closingtime,openingtime):
        error = self.dll.SetShutter(typ,mode,closingtime,openingtime)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def SetImage(self,hbin,vbin,hstart,hend,vstart,vend):
        self.hbin = hbin
        self.vbin = vbin
        self.hstart = hstart
        self.hend = hend
        self.vstart = vstart
        self.vend = vend
        
        error = self.dll.SetImage(hbin,vbin,hstart,hend,vstart,vend)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def StartAcquisition(self, continuous=False):
        self._prepare_array()
        self.last_image_num = 0

        if self.could_be_running:
            error = self.dll.AbortAcquisition() # just in case

        if continuous:
            desired_mode = self.ACQUISITIONMODE_RUNTILLABORT
        else:
            desired_mode = self.ACQUISITIONMODE_SINGLESCAN
        
        if desired_mode != self.AcquisitionMode:
            self.SetAcquisitionMode(desired_mode)

        error = self.dll.StartAcquisition()
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        
        self.could_be_running = True
        self._prepare_array()
        self.last_image_index = 0
        
        return ERROR_CODE[error]

    def _prepare_array(self):
        shape = None
        if (self.ReadMode == self.READMODE_IMAGE):
            if (self.AcquisitionMode == self.ACQUISITIONMODE_KINETICS):
                shape = (self.width // self.hbin, self.height // self.vbin, self.scans)
            else:
                shape = (self.width // self.hbin, self.height // self.vbin)
                shape = (self.width // self.hbin, self.height // self.vbin)
        elif (self.ReadMode in [self.READMODE_MULTITRACK, self.READMODE_RANDOMTRACK]):
            if (self.AcquisitionMode == self.ACQUISITIONMODE_KINETICS):
                shape = (self.width // self.hbin, self.track_count, self.scans)
            else:
                shape = (self.width // self.hbin, self.track_count)
        elif (self.ReadMode in [self.READMODE_FVB, self.READMODE_SINGLETRACK]):
            if self.AcquisitionMode in (self.ACQUISITIONMODE_SINGLESCAN,
                                        self.ACQUISITIONMODE_RUNTILLABORT,
                                        self.ACQUISITIONMODE_ACCUMULATE):
                shape = (self.width,)
            elif (self.AcquisitionMode == self.ACQUISITIONMODE_KINETICS):
                shape = (self.width, self.scans)
        
        if shape is None:
            raise Exception("Invalid accumulation settings")

        self.cimage_dim = np.prod(shape)
        self.cimage_shape = shape
        cimageArray = c_int * self.cimage_dim
        self.cimage = cimageArray()

    def GetAcquiredDataAsArray(self):
        error = self.dll.GetAcquiredData(pointer(self.cimage), c_int(self.cimage_dim))
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        array = np.array(self.cimage)
        array.reshape(self.cimage_shape)
        return array
               
    def GetNewDataAsArray(self):
        index = c_int()
        error = self.dll.GetTotalNumberImagesAcquired(byref(index))
        index = index.value
        if index <= self.last_image_index:
            return None
        self.last_image_index = index

        error = self.dll.GetMostRecentImage(pointer(self.cimage), c_int(self.cimage_dim))
        array = np.array(self.cimage)
        array = array.reshape(list(reversed(self.cimage_shape)))
        return array

    def SetExposureTime(self, time):
        error = self.dll.SetExposureTime(c_float(time))
        self.exposure = time
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def SetFlip(self, hflip, vflip):
        error = self.dll.SetImageFlip(hflip, vflip)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def GetAcquisitionTimings(self):
        exposure   = c_float()
        accumulate = c_float()
        kinetic    = c_float()
        error = self.dll.GetAcquisitionTimings(byref(exposure),byref(accumulate),byref(kinetic))
        self.exposure = exposure.value
        self.accumulate = accumulate.value
        self.kinetic = kinetic.value
        #self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]
        
    def GetAcquisitionTimingsNorm(self):
        self.GetAcquisitionTimings()
        return self.exposure, self.accumulate

    def SetSingleScan(self):
        #self.SetReadMode(4)
        self.SetAcquisitionMode(1)
        #self.SetImage(1,1,1,self.width,1,self.height)

    def SetCoolerMode(self, mode):
        error = self.dll.SetCoolerMode(mode)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]
        
    def SetFanMode(self, mode):
        #0: fan on full
        #1: fan on low
        #2: fna off
        error = self.dll.SetFanMode(mode)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]


    def SaveAsTxt(self, path):
        file = open(path, 'w')

        for line in self.imageArray:
            file.write("%g\n" % line)

        file.close()

    def SetImageRotate(self, iRotate):
        error = self.dll.SetImageRotate(iRotate)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)

    def SaveAsFITS(self, filename, type):
        error = self.dll.SaveAsFITS(filename, type)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def CoolerON(self):
        error = self.dll.CoolerON()
        self.cooler = 1
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def CoolerOFF(self):
        error = self.dll.CoolerOFF()
        self.cooler = 0
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def IsCoolerOn(self):
        iCoolerStatus = c_int()
        self.cooler = iCoolerStatus
        error = self.dll.IsCoolerOn(byref(iCoolerStatus))
        #self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return iCoolerStatus.value

    def GetTemperature(self):
        ctemperature = c_int()
        error = self.dll.GetTemperature(byref(ctemperature))
        self.temperature = ctemperature.value
        #self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return self.temperature

    def SetTemperature(self,temperature):
        #ctemperature = c_int(temperature)
        #error = self.dll.SetTemperature(byref(ctemperature))
        error = self.dll.SetTemperature(temperature)
        self.set_T = temperature
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def GetEMCCDGain(self):
        gain = c_int()
        error = self.dll.GetEMCCDGain(byref(gain))
        self.gain = gain.value
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]
     
    def SetEMCCDGainMode(self, gainMode):
        error = self.dll.SetEMCCDGainMode(gainMode)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]   
        
    def SetEMCCDGain(self, gain):
        error = self.dll.SetEMCCDGain(gain)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]
        
    def SetEMAdvanced(self, gainAdvanced):
        error = self.dll.SetEMAdvanced(gainAdvanced)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def GetEMGainRange(self):
        low = c_int()
        high = c_int()
        error = self.dll.GetEMGainRange(byref(low),byref(high))
        self.gainRange = (low.value, high.value)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]
      
    def GetNumberADChannels(self):
        noADChannels = c_int()
        error = self.dll.GetNumberADChannels(byref(noADChannels))
        self.noADChannels = noADChannels.value
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def GetBitDepth(self):
        bitDepth = c_int()

        self.bitDepths = []

        for i in range(self.noADChannels):
            self.dll.GetBitDepth(i,byref(bitDepth))
            self.bitDepths.append(bitDepth.value)

    def SetADChannel(self, index):
        error = self.dll.SetADChannel(index)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        self.channel = index
        return ERROR_CODE[error]  
        
    def SetOutputAmplifier(self, index):
        error = self.dll.SetOutputAmplifier(index)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        self.outamp = index
        return ERROR_CODE[error]
        
    def GetNumberHSSpeeds(self):
        noHSSpeeds = c_int()
        error = self.dll.GetNumberHSSpeeds(self.channel, self.outamp, byref(noHSSpeeds))
        self.number_hsspeeds = noHSSpeeds.value
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def GetHSSpeed(self):
        HSSpeed = c_float()

        self.available_hsspeeds = []
        self.GetNumberHSSpeeds()

        for i in range(self.number_hsspeeds):
            self.dll.GetHSSpeed(self.channel, self.outamp, i, byref(HSSpeed))
            self.available_hsspeeds.append(HSSpeed.value)
            
    def SetHSSpeed(self, itype, index):
        error = self.dll.SetHSSpeed(itype, index)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        self.hsspeed_index = index
        self.hsspeed = self.available_hsspeeds[index]
        return ERROR_CODE[error]
        
    def GetNumberVSSpeeds(self):
        noVSSpeeds = c_int()
        error = self.dll.GetNumberVSSpeeds(byref(noVSSpeeds))
        self.number_hsspeeds = noVSSpeeds.value
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def GetVSSpeed(self):
        VSSpeed = c_float()

        self.VSSpeeds = []

        for i in range(self.noVSSpeeds):
            self.dll.GetVSSpeed(i,byref(VSSpeed))
            self.preVSpeeds.append(VSSpeed.value)

    def SetVSSpeed(self, index):
        error = self.dll.SetVSSpeed(index)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        self.vsspeed = index
        return ERROR_CODE[error] 
    
    def GetNumberPreAmpGains(self):
        noGains = c_int()
        error = self.dll.GetNumberPreAmpGains(byref(noGains))
        self.noGains = noGains.value
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def GetPreAmpGain(self):
        gain = c_float()

        self.preAmpGain = []

        for i in range(self.noGains):
            self.dll.GetPreAmpGain(i,byref(gain))
            self.preAmpGain.append(gain.value)

    def SetPreAmpGain(self, index):
        error = self.dll.SetPreAmpGain(index)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        self.preampgain = index
        return ERROR_CODE[error]

    def SetTriggerMode(self, mode):
        error = self.dll.SetTriggerMode(mode)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def GetStatus(self):
        status = c_int()
        error = self.dll.GetStatus(byref(status))
        self.status = ERROR_CODE[status.value]
        #self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return self.status

    def GetStatusNorm(self):
        """ This function returns status in more generic format """
        s = self.GetStatus()
        if s == "DRV_IDLE":
            return 'idle'
        elif s == "DRV_ACQUIRING":
            if self.AcquisitionMode == self.ACQUISITIONMODE_RUNTILLABORT:
                return 'continuous'
            else:
                return 'running'
        else:
            return 'error'
        
    def GetSeriesProgress(self):
        acc = c_long()
        series = c_long()
        error = self.dll.GetAcquisitionProgress(byref(acc),byref(series))
        if ERROR_CODE[error] == "DRV_SUCCESS":
             return series.value
        else:
             return None
             
    def GetAccumulationProgress(self):
        acc = c_long()
        series = c_long()
        error = self.dll.GetAcquisitionProgress(byref(acc),byref(series))
        if ERROR_CODE[error] == "DRV_SUCCESS":
            return acc.value
        else:
            return None
        
    def SetFrameTransferMode(self, frameTransfer):
        error = self.dll.SetFrameTransferMode(frameTransfer)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]
        
    def SetShutterEx(self, typ, mode, closingtime, openingtime, extmode):
        error = self.dll.SetShutterEx(typ, mode, closingtime, openingtime, extmode)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]
        
    def SetSpool(self, active, method, path, framebuffersize):
        error = self.dll.SetSpool(active, method, c_char_p(path), framebuffersize)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def SetSingleTrack(self, centre, height):
        error = self.dll.SetSingleTrack(centre, height)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]


    # def SetMultiTrack(self, centre, height):
    #     #  SetMultiTrack(noTracks,trackHeight,trackOffset,&bottom,&gap);
    #     bottom = c_long()
    #     gap = c_long()
    #     error = self.dll.SetMultiTrack(1, int(height), int(centre),  byref(bottom), byref(gap))
    #     print(f"{centre}, {height}, {bottom}, {gap}")
    #     self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
    #     return ERROR_CODE[error]

    def SetRandomTracks(self, pairs):
        #  SetMultiTrack(noTracks,trackHeight,trackOffset,&bottom,&gap);
        self.track_count = len(pairs)
        num_tracks = c_long(self.track_count)
        pairs_flat = list(np.ravel(np.array(pairs)))
        areas = (c_int * (self.track_count * 2))(*pairs_flat)
        error = self.dll.SetRandomTracks(num_tracks, areas)
        self.verbose(ERROR_CODE[error], sys._getframe().f_code.co_name)
        return ERROR_CODE[error]

    def SetDemoReady(self):
        error = self.SetSingleScan()
        error = self.SetTriggerMode(0)
        error = self.SetReadMode(self.READMODE_FVB)
        error = self.SetAcquisitionMode(self.ACQUISITIONMODE_SINGLESCAN)
        error = self.SetShutter(1,0,30,30)
        return error
    
    def SetBinning(self,binningmode):
        if (binningmode==1):
            self.SetImage(1,1,1,self.width,1,self.height)
        elif (binningmode==2):
            self.SetImage(2,2,1,self.width,1,self.height)
        elif (binningmode==4):
            self.SetImage(4,4,1,self.width,1,self.height)
        else:
            self.verbose("Binning mode not found")

ERROR_CODE = {
    20001: "DRV_ERROR_CODES",
    20002: "DRV_SUCCESS",
    20003: "DRV_VXNOTINSTALLED",
    20006: "DRV_ERROR_FILELOAD",
    20007: "DRV_ERROR_VXD_INIT",
    20010: "DRV_ERROR_PAGELOCK",
    20011: "DRV_ERROR_PAGE_UNLOCK",
    20013: "DRV_ERROR_ACK",
    20024: "DRV_NO_NEW_DATA",
    20026: "DRV_SPOOLERROR",
    20034: "DRV_TEMP_OFF",
    20035: "DRV_TEMP_NOT_STABILIZED",
    20036: "DRV_TEMP_STABILIZED",
    20037: "DRV_TEMP_NOT_REACHED",
    20038: "DRV_TEMP_OUT_RANGE",
    20039: "DRV_TEMP_NOT_SUPPORTED",
    20040: "DRV_TEMP_DRIFT",
    20050: "DRV_COF_NOTLOADED",
    20053: "DRV_FLEXERROR",
    20066: "DRV_P1INVALID",
    20067: "DRV_P2INVALID",
    20068: "DRV_P3INVALID",
    20069: "DRV_P4INVALID",
    20070: "DRV_INIERROR",
    20071: "DRV_COERROR",
    20072: "DRV_ACQUIRING",
    20073: "DRV_IDLE",
    20074: "DRV_TEMPCYCLE",
    20075: "DRV_NOT_INITIALIZED",
    20076: "DRV_P5INVALID",
    20077: "DRV_P6INVALID",
    20083: "P7_INVALID",
    20089: "DRV_USBERROR",
    20091: "DRV_NOT_SUPPORTED",
    20095: "DRV_INVALID_TRIGGER_MODE",
    20099: "DRV_BINNING_ERROR",
    20990: "DRV_NOCAMERA",
    20991: "DRV_NOT_SUPPORTED",
    20992: "DRV_NOT_AVAILABLE"
}
