# -*- coding: utf-8 -*-

""" Integration with LightField software.

"""


from .spectrometer import *
from PyQt5 import QtWidgets,QtCore
import numpy as np

import clr
import sys,os

sys.path.append(os.environ['LIGHTFIELD_ROOT'])
sys.path.append(os.environ['LIGHTFIELD_ROOT']+r'\AddInViews')

clr.AddReference('System.Collections')
clr.AddReference('PrincetonInstruments.LightFieldViewV5')
clr.AddReference('PrincetonInstruments.LightField.AutomationV5')
clr.AddReference('PrincetonInstruments.LightFieldAddInSupportServices')

from PrincetonInstruments.LightField.Automation import Automation
from PrincetonInstruments.LightField.AddIns import CameraSettings
from PrincetonInstruments.LightField.AddIns import DeviceType
from PrincetonInstruments.LightField.AddIns import ExperimentSettings
import System
from System.Collections.Generic import List
from System.Runtime.InteropServices import Marshal
from System.Runtime.InteropServices import GCHandle, GCHandleType
import numpy as np
import ctypes

DEFAULT_SPECTROMETER_REQ_PORT = 9874
DEFAULT_SPECTROMETER_PUB_PORT = 9875


class LightFieldWorker(SpectrometerWorker):
    _continuous = False
    
    def __init__(self, req_port=DEFAULT_SPECTROMETER_REQ_PORT, 
                       pub_port=DEFAULT_SPECTROMETER_PUB_PORT,
                       *args, **kwargs):
        super().__init__(req_port, pub_port, *args, **kwargs)
        
        
       
    def _new_data_callback(self, sender, event_args):
        try:
            dataSet = event_args.ImageDataSet
            xs = np.array( list( self._experiment.SystemColumnCalibration ) )
            
            num_rois = len(dataSet.Regions)           
            if num_rois == 1:
                frame = dataSet.GetFrame(0, 0)
                
                data = frame.GetData()
                
                dims = [frame.Width, frame.Height]
                
                xs = np.array( list( self._experiment.SystemColumnCalibration ) )
                ys = np.array( list( data )).reshape(dims)
                if ys.shape[1] == 1:
                    ys = ys[:,0]
                self.new_spectrum(xs, ys)
            else:
                all_ys = []
                for i in range(num_rois):
                    frame = dataSet.GetFrame(i, 0)
                
                    data = frame.GetData()
                
                    dims = [frame.Width, frame.Height]
                    ys = np.array( list( data )).reshape(dims)
                    if ys.shape[1] == 1:
                        ys = ys[:,0]
                    all_ys.append(ys)
                self.new_spectrum(xs, np.array(all_ys) )
                
        except:
            print("Callback failed")        
        
        
    def init_device(self):
        print("Starting...")
        self._auto = Automation(True, List[System.String]())
        self._experiment = self._auto.LightFieldApplication.Experiment
        self._ccd = None
        self._mono = None
        for dev in self._experiment.ExperimentDevices:
            if dev.Type == DeviceType.Camera:
                self._ccd = dev
                print("CCD found: " + self._ccd.get_Model())
            elif dev.Type == DeviceType.Spectrometer:
                self._mono = dev
                print("Monochromator found: " + self._mono.get_Model())
        if self._ccd is None and self._mono is None:
            print("Error: could not find all the devices")
            raise Exception("Could not find all the devices")
        
        self._experiment.ImageDataSetReceived += self._new_data_callback
        
                 
    def set_cooler(self, state):
        pass
    
    def get_cooler_state(self):
        return bool(self._experiment.GetValue(CameraSettings.SensorTemperatureStatus))
    
    def get_temperature(self):
        return self._experiment.GetValue(CameraSettings.SensorTemperatureReading)
        
    def set_exposure(self, exp_time, number):
        self._experiment.SetValue(CameraSettings.ShutterTimingExposureTime * 1000, exp_time)
        self._experiment.SetValue(ExperimentSettings.FrameSettingsFramesCombined, number)


    def get_exposure_time(self):
        """ [overloaded from superclass] Returns a tuple with exposure time (in seconds) and number of 
        accumulations """
        t = self._experiment.GetValue(CameraSettings.ShutterTimingExposureTime) / 1000
        n = self._experiment.GetValue(ExperimentSettings.FrameSettingsFramesCombined)
        return t, n
      
           
    def start_acquisition(self, continuous=False):
        self._continuous = continuous
        if not continuous:
            self._experiment.Acquire()
        else:
            self._experiment.Preview()
        
    def abort_acquisition(self):
        self._experiment.Stop()
       
    def acquisition_status(self):
        if self._experiment.IsRunning:
            if self._continuous:
                return 'continuous'
            else:
                return 'running'
        else:
            return 'idle'