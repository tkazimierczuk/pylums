# -*- coding: utf-8 -*-
"""
Created on Tue May 15 11:50:03 2018

@author: dms
"""

import sys
import clr
from os import system as System
import time
from .spectrometer import SpectrometerWorker
import numpy as np

assemblyPath = "C:\\Program Files\\S&I VistaControl"
if assemblyPath not in sys.path:
    sys.path.append(assemblyPath)

clr.AddReference("VistaControl")


from VistaControl.Remoting import Client,ICallback,Frame


class myCallback(ICallback):
    __namespace__ = "blabla"
    
    def __init__(self):
        pass
    
    def DataChanged(self,frame):
        pass


class TrivistaWorker(SpectrometerWorker):
    def __init__(self, *args, host="localhost", **kwargs):
        super().__init__(*args, **kwargs)
        # we do not call super().__init__() to avoid creating all the ZMQ sockets
        self.host = host
        self._anchor_points = []
        self._thread = {}
        
    
    def init_device(self):
        self.client = Client()
        self.framebuf = myCallback() #FrameBuffer()       
        if self.client.Connect(self.framebuf,self.host,8000,'VistaControl'):
            print("Trivista connected successfully")
        else:
            print("Error connecting to Trivista")
        self.pipe = self.client.pipeProxy
    
    def __del__(self):
        pass
          
    def get_latest_data(self):    
        frame = self.pipe.GetActualData()
        data = np.array(list(frame.xAxis.calibrationData)), np.array(list(frame.data))
        self.send_via_pubchannel(b'spectrum', data)
        return data
              
    def status(self):
        d = {}
        d["status"] = self.acquisition_status()
        lambda_center = self.get_wavelength()
        d["wavelength"] = lambda_center
        exposure,num = self.get_exposure_time()
        d["exposure"] = exposure
        d["number"] = num
        #optionally:
        #d["tracks"] = [ [0,0] ]
        #d["mode"] = "image" # "fvb" # "multitrack"
        return d
    
    def spectrometer_name(self):
        return "Trivista"
    
    def chip_size_px(self):
        """ Returns numbers of pixels in the chip. Mind that in the step&glue
        mode the length of returned array can exceed the chip size"""
        return 1024,1
    
    def set_cooler(self, state):
        """ state is True or False """
        pass
    
    def get_cooler_state(self):
        return True
    
    def get_temperature(self):
        return np.nan
        
    def set_exposure(self, exp_time, number):
        params = self.pipe.GetAcquisitionParameters()
        params.exposureTimeMS = System.Nullable[System.Double](exp_time * 1000)
        params.Accumulations.noOfAccumulations = System.Nullable[System.Int32](number)
        self.pipe.SetAcquisitionParameters(params)
    
    def get_exposure_time(self):
        """ Returns a tuple with exposure time (in seconds) and number of 
        accumulations """
        params = self.pipe.GetAcquisitionParameters()
        exp_time = params.exposureTimeMS / 1000
        num = params.Accumulations.noOfAccumulations
        return exp_time,num

    def get_measurement_time(self):
        """ Returns estimated time (in seconds) of the whole measurement.
        Usually it is close to the exposure time, but could be significantly
        longer in step&glue mode"""
        exp_time,num = self.get_measurement_time()
        return exp_time * num
    
    def start_acquisition(self, continuous=False):
        self.pipe.StopAcquisition()
        if continuous:
            self.pipe.StartContiniousAcquisition()
        else:
            self.pipe.StartSingleAcquisition()
    
    def abort_acquisition(self):
        self.pipe.StopAcquisition()
    
    def acquisition_status(self):
        """ Returns one of following strings: 'running', 'idle', 'continuous', 
        or 'error'"""
        state = self.pipe.GetAcquisitionState()
        if state == 1: #InProgress=1, NewFrameAvailable=2
            return 'running'
        elif state == 2:
            return 'continuous'
        else:
            return 'idle'
        
    def set_wavelength(self, wavelength):
        self.pipe.MoveToWavelength(wavelength)
    
    def get_wavelength(self):
        return self.pipe.GetCurrentWavelength()
        
    def get_wavelength_range(self):
        """ Returns a tuple (lambda_min, lambda_max)"""
        return 0, 0
    
    def reset_wavelength_correction(self):
        self._anchor_points.clear()
        
    def add_points_for_wavelength_correction(self, point_list):
        """ Takes from the user list of points: (apparent wavelength, *real* wavelength).
            The points are later used to improve the wavelength calibration """
        self._anchor_points.append(point_list)
        print("Current calibration points: ", self._anchor_points)
    
    def _apply_wavelength_correction(self, xs):
        return xs

        #if topic not in self._thread:
        #    thread = QtCore.QThread()
        #    if topic == 'status':
        #    listener = Trivista.StatusNotifier()
        #    listener.moveToThread(thread)
        #    thread.started.connect(zeromq_listener.loop)
        #    QtCore.QTimer.singleShot(0, thread.start)
        #    self._thread[topic] = (thread, zeromq_listener)
        #self._thread[topic][1].message.connect(updateSlot)