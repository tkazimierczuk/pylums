# -*- coding: utf-8 -*-
"""
"""

import devices.zeromq_device as zmqdev
from devices.spectro.spectrometer import *
from PyQt5 import QtWidgets,QtCore
import numpy as np
from numpy import sin,cos,arcsin
import ctypes as ct
from collections import namedtuple
import weakref

import os
os.environ["PATH"] = os.path.dirname(__file__)+ os.pathsep + os.environ["PATH"]

class Port:
    SIDE_ENTRANCE = 1
    FRONT_ENTRANCE = 2
    SIDE_EXIT = 3
    FRONT_EXIT = 4

class Acton(zmqdev.DeviceWorker):
    def __init__(self):
        super().__init__()
        self._opened = False
        
    def __del__(self):
        if self._opened:
            self.close_device()
        super().__del__()
        
    def init_device(self):
        self._opened = True
        self.dll = ct.windll.LoadLibrary("ARC_SpectraPro")
        num = ct.c_int32()
        self.dll.ARC_Search_For_Mono(ct.byref(num))
        if num.value == 0:
            raise Exception("No Acton devices found")
        
        self.handle = ct.c_int32()
        res = self.dll.ARC_Open_Mono(ct.c_int32(0),ct.byref(self.handle))
        if not res:
            raise Exception("Cannot open Acton device")
        
        buf = ct.create_string_buffer(256)
        self.dll.ARC_get_Mono_Model_CString(self.handle, buf)
        self.model = buf.value.decode('ascii')
        
        self.dll.ARC_get_Mono_Serial_CString(self.handle, buf)
        self.serial = buf.value.decode('ascii')
        
        print("Opened monochromator: %s (s/n: %s)" % (self.model,self.serial))
        
        self.geometry = {}
        dblVal = ct.c_double()
        self.dll.ARC_get_Mono_Focallength(self.handle, ct.byref(dblVal))
        self.geometry['focal_length'] = dblVal.value
        self.dll.ARC_get_Mono_HalfAngle(self.handle, ct.byref(dblVal))
        self.geometry['half_angle'] = dblVal.value
        self.dll.ARC_get_Mono_DetectorAngle(self.handle, ct.byref(dblVal))
        self.geometry['det_angle'] = dblVal.value
        
        intVal = ct.c_int32()
        self.dll.ARC_get_Mono_Turret(self.handle, ct.byref(intVal))
        self.turret = intVal.value
        
        self.gratings = {}
        Grating = namedtuple('Grating', ['density', 'blaze'])
        for i in range(1,4):
            self.dll.ARC_get_Mono_Grating_Blaze_CString(self.handle, ct.c_int32(i), buf)
            blaze = buf.value.decode('ascii')
            self.dll.ARC_get_Mono_Grating_Density(self.handle, ct.c_int32(i), ct.byref(intVal))
            density = intVal.value
            self.gratings[i] = Grating(density,blaze)
        
        
    def close_device(self):
        try:
            self.dll.ARC_Close_Mono(self.handle)
        except:
            pass
        
    @zmqdev.background_job
    def set_wavelength(self, wavelength_nm):
        self.dll.ARC_set_Mono_Wavelength_nm(self.handle, ct.c_double(wavelength_nm))

    #@zmqdev.buffered_when_busy        
    def get_wavelength(self):
        dblVal = ct.c_double()
        self.dll.ARC_get_Mono_Wavelength_nm(self.handle, ct.byref(dblVal))
        return dblVal.value
    
    @zmqdev.buffered_when_busy    
    def get_current_grating(self):
        intVal = ct.c_int32()
        self.dll.ARC_get_Mono_Grating(self.handle, ct.byref(intVal))
        return intVal.value
        
    def get_current_grating_density(self):
        return self.gratings[self.get_current_grating()].density
    
    @zmqdev.background_job
    def set_current_grating(self, index):
        intVal = ct.c_int32(int(index))
        self.dll.ARC_set_Mono_Grating(self.handle, intVal)
    
    @zmqdev.background_job
    def set_slit_width(self, width, port=Port.SIDE_ENTRANCE):
        if width > 3000:
            width = 3000
        if width < 10:
            width = 10
        mapping = {Port.SIDE_ENTRANCE: 1,
                   Port.FRONT_ENTRANCE: 2,
                   Port.FRONT_EXIT: 3,
                   Port.SIDE_EXIT: 4}
        directionInt = ct.c_int32( mapping[port] )
        widthInt = ct.c_int32(int(width))
        self.dll.ARC_set_Mono_Slit_Width(self.handle, directionInt, widthInt)
        
    @zmqdev.buffered_when_busy
    def get_slit_width(self, port=Port.SIDE_ENTRANCE):
        mapping = {Port.SIDE_ENTRANCE: 1,
                   Port.FRONT_ENTRANCE: 2,
                   Port.FRONT_EXIT: 3,
                   Port.SIDE_EXIT: 4}
        directionInt = ct.c_int32( mapping[port] )
        widthInt = ct.c_int32()
        self.dll.ARC_get_Mono_Slit_Width.restype = ct.c_bool
        result = self.dll.ARC_get_Mono_Slit_Width(self.handle, directionInt, ct.byref(widthInt))
        return widthInt.value if result else np.nan
    
    @zmqdev.background_job    
    def slit_home(self, port=Port.SIDE_ENTRANCE):
        mapping = {Port.SIDE_ENTRANCE: 1,
                   Port.FRONT_ENTRANCE: 2,
                   Port.FRONT_EXIT: 3,
                   Port.SIDE_EXIT: 4}
        directionInt = ct.c_int32( mapping[port] )
        self.dll.ARC_Mono_Slit_Home(self.handle, directionInt)
        
    @zmqdev.buffered_when_busy
    def get_entrance_port(self):
        directionInt = ct.c_int32( 1 ) # 1=entrance mirror
        posInt = ct.c_int32()
        self.dll.ARC_get_Mono_Diverter_Pos.restype = ct.c_bool
        result = self.dll.ARC_get_Mono_Diverter_Pos(self.handle, directionInt, ct.byref(posInt))
        if result and posInt.value==1:
            return 'side'
        elif result and posInt.value==2:
            return 'front'
        else:
            return 'n/a'
              
    @zmqdev.buffered_when_busy
    def get_exit_port(self):
        directionInt = ct.c_int32( 2 ) # 2=exit mirror
        posInt = ct.c_int32()
        self.dll.ARC_get_Mono_Diverter_Pos.restype = ct.c_bool
        result = self.dll.ARC_get_Mono_Diverter_Pos(self.handle, directionInt, ct.byref(posInt))
        if result and posInt.value==4:
            return 'side'
        elif result and posInt.value==3:
            return 'front'
        else:
            return 'n/a'

    @zmqdev.background_job
    def set_exit_port(self, port):
        """Set exit port to 'side' or 'front' """
        directionInt = ct.c_int32( 2 ) # 2=exit mirror
        posInt = ct.c_int32( 3 if port=='front' else 4 )
        result = self.dll.ARC_set_Mono_Diverter_Pos(self.handle, directionInt, posInt)
        
    @zmqdev.background_job
    def set_entrance_port(self, port):
        """Set exit port to 'side' or 'front' """
        directionInt = ct.c_int32( 1 ) # 1=entrance mirror
        posInt = ct.c_int32( 2 if port=='front' else 1 )
        result = self.dll.ARC_set_Mono_Diverter_Pos(self.handle, directionInt, posInt)


class SpectrometerWithCCDWorker(SpectrometerWorker):
    def __init__(self, ccd="andor", ccd_id=None, mono="acton",
                 mono_id=None, flip="false", *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.dev_params = {"ccd":ccd, "ccd_id":ccd_id, "mono":mono, "mono_id":mono_id}
        self.multitrack = [[1, 255]]
        self.readmode = 0 # READMODE_FVB
        self.scanning_speed_index_fvb = 0
        self.scanning_speed_index_mt = 0
        self.scanning_speed_index_image = 0
        self.number_accumulations = 1
        self.flip = True if flip.strip().lower() == "true" else False
        
    def init_device(self):
        if self.dev_params["ccd"] == "andor":
            from devices.spectro import pyandor
            self.ccd = pyandor.Andor(self.dev_params["ccd_id"])
            wref1 = weakref.WeakMethod(self.new_spectrum)
            wref2 = weakref.WeakMethod(self.wavelengths)
            def new_spectrum_cb(ydata):
                func = wref1()
                if func is not None:
                    func(wref2()(), ydata)
            self.ccd.install_callback(new_spectrum_cb)
            self.ccd.SetTemperature(-75)
            self.ccd.SetAcquisitionMode(self.ccd.ACQUISITIONMODE_SINGLESCAN) # accumulate not supported yet
            #self.ccd.SetNumberAccumulations(1)
            if self.flip:
                self.ccd.SetFlip(1, 0)
            self.scanning_speed_index_fvb = int(np.argmin(self.ccd.available_hsspeeds))
            self.scanning_speed_index_mt = int(np.argmin(self.ccd.available_hsspeeds))
            self.scanning_speed_index_image = int(np.argmax(self.ccd.available_hsspeeds))
            self.set_full_vertical_binning()
        else:
            raise Exception("Unsupported CCD type: %s" % self.dev_params["ccd"])
        
        if self.dev_params["mono"] == "acton":
            self.mono = Acton()
            self.mono.init_device()
        elif self.dev_params["mono"] == "shamrock":
            raise Exception("Unsupported monochromator type: %s" % self.dev_params["mono"])
        else:
            raise Exception("Unsupported monochromator type: %s" % self.dev_params["mono"])

   
    def spectrometer_name(self):
        print((self.ccd.serial, self.mono.model,self.mono.serial))
        return "Andor CCD (s/n: %d) at %s (s/n: %s)" % (self.ccd.serial, self.mono.model,self.mono.serial)
    
    def chip_size_px(self):
        return self.ccd.width, self.ccd.height
    
    def set_cooler(self, state):
        """ state is True or False """
        if state:
            self.ccd.CoolerON()
        else:
            self.ccd.CoolerOFF()
    
    def set_wavelength(self, nm):
        return self.mono.set_wavelength(nm)
        
    def get_wavelength(self):
        return self.mono.get_wavelength()
        
    def get_wavelength_range(self):
        """ Returns a tuple (lambda_min, lambda_max)"""
        ws = self.wavelengths()
        w_min, w_max = ws[0], ws[-1]
        if w_min > w_max:
            w_min, w_max = w_max, w_min
        return w_min,w_min
    
    def status(self):
        s = super().status()
        #s['grating'] = self.get_current_grating()
        modes = {self.ccd.READMODE_FVB: "fvb", 
                 self.ccd.READMODE_RANDOMTRACK: "multitrack",
                 self.ccd.READMODE_IMAGE: "image"}
        s['mode'] = modes[self.readmode]
        s['multitrack'] = self.multitrack
        s['scanning_speed'] = self.get_scanning_speed()
        return s
    
    def get_cooler_state(self):
        return bool(self.ccd.IsCoolerOn())
    
    def get_temperature(self):
        return self.ccd.GetTemperature()

    def get_target_temperature(self):
        return self.ccd.set_T
        
    def set_exposure(self, exp_time, number):
        self.number_accumulations = 1 # accumulate not yet supported. Reading architecture not really prepared?
        self.ccd.SetExposureTime(exp_time)
        #self.ccd.SetNumberAccumulations(self.number_accumulations)
    
    def get_exposure_time(self):
        """ Returns a tuple with exposure time (in seconds) and number of 
        accumulations """
        exposure, _ = self.ccd.GetAcquisitionTimingsNorm()
        return (exposure, self.number_accumulations)
    
    def start_acquisition(self, continuous=False):
        self.ccd.StartAcquisition(continuous)

    def abort_acquisition(self):
        self.ccd.AbortAcquisition()
    
    @remote
    def get_available_scanning_speeds(self):
        return self.ccd.available_hsspeeds

    @remote
    def get_scanning_speed(self):
        return self.ccd.hsspeed

    @remote
    def get_scanning_speed_index(self):
        return self.ccd.hsspeed_index

    @remote
    def set_scanning_speed_index(self, index):
        if self.readmode == self.ccd.READMODE_FVB:
            self.scanning_speed_index_fvb = index
        if self.readmode == self.ccd.READMODE_RANDOMTRACK:
            self.scanning_speed_index_mt = index
        if self.readmode == self.ccd.READMODE_IMAGE:
            self.scanning_speed_index_image = index
        return self.ccd.SetHSSpeed(0, index)

    @remote
    def set_multitrack(self, tracks=None):
        """accepts list of [begin, end] pairs"""
        if tracks is not None:
            self.multitrack = tracks
        self.readmode = self.ccd.READMODE_RANDOMTRACK
        self.ccd.SetReadMode(self.readmode)
        self.ccd.SetRandomTracks(self.multitrack)
        if self.ccd.hsspeed_index != self.scanning_speed_index_mt:
            self.set_scanning_speed_index(self.scanning_speed_index_mt)

    @remote
    def is_multitrack_supported(self):
        return True

    @remote
    def is_image_supported(self):
        return True

    @remote
    def set_full_vertical_binning(self):
        self.readmode = self.ccd.READMODE_FVB
        self.ccd.SetReadMode(self.readmode)
        if self.ccd.hsspeed_index != self.scanning_speed_index_fvb:
            self.set_scanning_speed_index(self.scanning_speed_index_fvb)

    @remote
    def set_image(self):
        self.readmode = self.ccd.READMODE_IMAGE
        self.ccd.SetReadMode(self.readmode)
        self.ccd.SetImage(1, 1, 1, self.ccd.width, 1, self.ccd.height)
        if self.ccd.hsspeed_index != self.scanning_speed_index_image:
            self.set_scanning_speed_index(self.scanning_speed_index_image)

    @remote
    def acquisition_status(self):
        s = self.ccd.GetStatusNorm()
        return s
    
    @remote
    def gratings(self):
        """ Return a list of installed gratings. Each grating consist of 
        a pair (density, blazing_info) """
        return self.mono.gratings

    def wavelengths(self):
        n = self.ccd.width
        xdata = np.arange(n)- 0.5*n
        xdata *= self.ccd.pixel_size[0]
        f = self.calibration_func()[0]
        return f(xdata)
   
    def calibration_func(self):
        """Returns function that wavelength(offset_um)"""
        w0 = self.mono.get_wavelength()
        k = self.mono.get_current_grating_density()
        ha = self.mono.geometry['half_angle']
        alpha = arcsin(1e-6*k*w0/(2*cos(ha)))-ha
        beta = alpha + 2*ha
        gamma = self.mono.geometry['det_angle']
        L = self.mono.geometry['focal_length']
        dispersion = 1e3 * cos(beta) * cos(gamma) / (k*L)
        distance2wavelength = lambda d: w0 + d*dispersion
        wavelength2distance = lambda w: (w-w0)/dispersion
        return distance2wavelength, wavelength2distance

    @remote
    def get_current_grating(self):
        """ Return information on current grating index """
        return self.mono.get_current_grating()
    
    @remote
    def set_current_grating(self, index):
        self.mono.set_current_grating(index)
    
    @remote
    def set_slit_width(self, width, port=Port.SIDE_ENTRANCE):
        self.mono.set_slit_width(width, port)
        
    @remote
    def get_slit_width(self, port=Port.SIDE_ENTRANCE):
        return self.mono.get_slit_width(port)
        
    @remote
    def slit_home(self, port=Port.SIDE_ENTRANCE):
        self.mono.slit_home(port)
        
    @remote
    def get_entrance_port(self):
        return self.mono.get_entrance_port(self)
              
    @remote
    def get_exit_port(self):
        return self.mono.get_exit_port()

    @remote
    def set_exit_port(self, port):
        """Set exit port to 'side' or 'front' """
        self.mono.set_exit_port(port)

    @remote
    def set_entrance_port(self, port):
        """Set entrance port to 'side' or 'front' """
        self.mono.set_entrance_port(port)

    

@include_remote_methods(SpectrometerWithCCDWorker)    
class CzernyTurnerSpectrometer(Spectrometer):
    pass