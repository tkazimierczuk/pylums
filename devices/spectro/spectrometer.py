# -*- coding: utf-8 -*-
"""
Spectrometer class represents a typical spectrometer, such as an OceanOptics
device, or a Czerny-Turner monochromator with a CCD.
"""

from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets,QtCore,QtGui
import numpy as np
import time
import traceback
import random

class SpectrometerWorker(DeviceWorker):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._anchor_points = []
        self._latest_data = (np.arange(1024), np.array([0]*1024)) #sample data
        self._exp_time = 0
        self._exp_number = 1
        
    def init_device(self):
        pass
        
    def status(self):
        d = super().status()
        d["cooler"] = self.get_cooler_state()
        d["temperature"] = self.get_temperature()
        d["status"] = self.acquisition_status()
        lambda_center = self.get_wavelength()
        d["wavelength"] = lambda_center
        exposure,num = self.get_exposure_time()
        d["exposure"] = exposure
        d["number"] = num
        #optionally:
        #d["tracks"] = [ [0,0] ]
        #d["mode"] = "image" # "fvb" # "multitrack"
        return d
    
    def new_spectrum(self, xs, ys):
        """ Can be used in subclasses to store a spectrum. The spectrum is 
        immediately broadcasted via PUB channel and stored for later queries
        by get_latest_data() method """
        self._latest_data = (xs, ys)
        self.send_via_pubchannel(b'spectrum', self._latest_data)
    
    @remote
    def spectrometer_name(self):
        return "Spectrometer"
    
    @remote
    def chip_size_px(self):
        """ Returns numbers of pixels in the chip. Mind that in the step&glue
        mode the length of returned array can exceed the chip size"""
        return 1024,1
    
    @remote
    def set_cooler(self, state):
        """ state is True or False """
        pass
    
    @remote
    def get_cooler_state(self):
        return True
    
    @remote
    def get_temperature(self):
        return np.nan
        
    @remote
    def set_exposure(self, exp_time, number):
        self._exp_time = exp_time
        self._exp_number = number
    
    @remote
    def get_exposure_time(self):
        """ Returns a tuple with exposure time (in seconds) and number of 
        accumulations """
        return self._exp_time, self._exp_number

    @remote
    def get_measurement_time(self):
        """ Returns estimated time (in seconds) of the whole measurement.
        Usually it is close to the exposure time, but could be significantly
        longer in step&glue mode"""
        return self._exp_number * self._exp_time
    
    @remote
    def start_acquisition(self, continuous=False):
        self.new_spectrum(list(range(1024)), [[100 * (random.random() - 0.5) * random.random() + 300 * (
                                                          np.sin(i / 277-0.5) ** 3) for i in range(1024)],
                                              [100 * (random.random() - 0.5) * random.random() + 300 * (
                                                          np.sin(i / 277) ** 3) for i in range(1024)],
                                              [100 * (random.random() - 0.5) * random.random() + 300 * (
                                                          np.sin(i / 277+0.5) ** 3) for i in range(1024)],
                          ])
    
    @remote
    def abort_acquisition(self):
        pass

    @remote
    def is_stopped(self):
        return self.acquisition_status() in ['idle', 'error']

    @remote
    def acquisition_status(self):
        """ Returns one of following strings: 'running', 'idle', 'continuous', 
        or 'error'"""
        return 'idle'
        
    @remote
    def get_latest_data(self):
        """ Returns the last measured data either as 1D or 2D array.
        The first dimension should be the same """
        return self._latest_data
    
    @remote
    def set_wavelength(self, wavelength):
        pass
    
    @remote
    def get_wavelength(self):
        return 0
        
    @remote
    def get_wavelength_range(self):
        """ Returns a tuple (lambda_min, lambda_max)"""
        return 0, 0
    
    @remote
    def reset_wavelength_correction(self):
        self._anchor_points.clear()
        
    @remote
    def add_points_for_wavelength_correction(self, point_list):
        """ Takes from the user list of points: (apparent wavelength, *real* wavelength).
            The points are later used to improve the wavelength calibration """
        self._anchor_points.append(point_list)
        print("Current calibration points: ", self._anchor_points)
    
    def _apply_wavelength_correction(self, xs):
        return xs

    @remote
    def set_full_vertical_binning(self, tracks=None):
        pass

    @remote
    def is_multitrack_supported(self):
        return False

    @remote
    def set_multitrack(self, tracks=None):
        pass

    @remote
    def is_image_supported(self):
        return False

    @remote
    def set_image(self):
        pass


    @remote
    def reset_wavelength_correction(self):
        pass
    
    
    
@include_remote_methods(SpectrometerWorker)    
class Spectrometer(DeviceOverZeroMQ):
  
    def __init__(self, *args, cosmic_ray_removal=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.cosmic_ray_removal = cosmic_ray_removal
	   
    def error_message(self, message):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(message)
        msg.exec_()
        print(message)
	
    def take_single_spectrum(self):
        self.start_acquisition()
        while not self.is_stopped():
            time.sleep(0.01)

        x_nm, data = self.get_latest_data()
        
        if self.cosmic_ray_removal:
            self.start_acquisition()
            while not self.is_stopped():
                time.sleep(0.01)
            x_nm, data2 = self.get_latest_data()
            data3 = np.where(abs(data - data2) > 100, np.minimum(data, data2), 0.5 * (data + data2))
            return x_nm, data3
        else:
            return x_nm, data
       
    def createDock(self, parentWidget, menu=None):
        """ Function for integration in GUI app. Implementation below 
        creates a button and a display """
        dock = QtWidgets.QDockWidget(self.spectrometer_name(), parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QVBoxLayout(widget)
        widget.setLayout(layout)
        widget1 = QtWidgets.QWidget(parentWidget)
        widget2 = QtWidgets.QWidget(parentWidget)
        widget3 = QtWidgets.QWidget(parentWidget)
        hlayout1 = QtWidgets.QHBoxLayout(widget1)
        hlayout2 = QtWidgets.QHBoxLayout(widget2)
        hlayout3 = QtWidgets.QHBoxLayout(widget3)
        hlayout1.setContentsMargins(0, 0, 0, 0)
        hlayout2.setContentsMargins(0, 0, 0, 0)
        hlayout3.setContentsMargins(0, 0, 0, 0)
       
        self.btn_run = QtWidgets.QToolButton()
        self.btn_run.setToolTip("Single spectrum")
        self.btn_run.setCheckable(True)
        self.btn_run.setIcon(QtGui.QIcon('img/spectro_single.svg'))
        hlayout1.addWidget(self.btn_run)
        def run_single_func(on):
            try:
                if on:
                    self.start_acquisition(False)
                else:
                    self.abort_acquisition()
            except:
                self.error_message(traceback.format_exc())
			
        self.btn_run.clicked.connect(run_single_func)
        
        self.btn_run_cnt = QtWidgets.QToolButton()
        self.btn_run_cnt.setToolTip("Continuous acquisition")
        self.btn_run_cnt.setCheckable(True)
        self.btn_run_cnt.setIcon(QtGui.QIcon('img/spectro_continuous.svg'))
        hlayout1.addWidget(self.btn_run_cnt)
        def run_continuous_func(on):
            try:
                if on:
                    self.start_acquisition(True)
                else:
                    self.abort_acquisition()
            except:
                self.error_message(traceback.format_exc())
        self.btn_run_cnt.clicked.connect(run_continuous_func)
        
        self.btn_stop = QtWidgets.QToolButton()
        self.btn_stop.setToolTip("Stop acquisition")
        self.btn_stop.setIcon(QtGui.QIcon('img/spectro_stop.svg'))
        hlayout1.addWidget(self.btn_stop)
        self.btn_stop.clicked.connect(lambda on: self.abort_acquisition())
        layout.addWidget(widget1)
        
        self._icon_image = QtGui.QIcon('img/spectro_image.svg')
        self._icon_fvb = QtGui.QIcon('img/spectro_fvb.svg')
        self._icon_mt = QtGui.QIcon('img/spectro_mt.svg')
        self.button_fvb = QtWidgets.QPushButton()
        self.button_fvb.setIcon(self._icon_fvb)
        self.button_mt = QtWidgets.QPushButton(self._icon_mt, "")
        self.button_image = QtWidgets.QPushButton(self._icon_image, "")
        self.button_fvb.setCheckable(True)
        self.button_mt.setCheckable(True)
        self.button_image.setCheckable(True)
        self.button_fvb.setToolTip("Change acquisition mode to Full Vertical Binning")
        self.button_mt.setToolTip("Change acquisition mode to MultiTrack")
        self.button_image.setToolTip("Change acquisition mode to Image")
        self.button_mt.setEnabled(self.is_multitrack_supported())
        self.button_image.setEnabled(self.is_image_supported())
        self.button_fvb.clicked.connect(self.try_fvb)
        self.button_mt.clicked.connect(self.try_mt)
        self.button_image.clicked.connect(self.try_image)
        hlayout2.addWidget(self.button_fvb)
        hlayout2.addWidget(self.button_mt)
        hlayout2.addWidget(self.button_image)
        layout.addWidget(widget2)

        self.button_timings = QtWidgets.QPushButton("Timing")
        self._chip_size = self.chip_size_px()
        self.button_timings.setMinimumWidth(60)
        self.button_timings.clicked.connect(lambda : self.show_exp_time_dialog(dock))
        
        self.button_wavelength = QtWidgets.QPushButton("Lambda")
        self.button_wavelength.setMinimumWidth(60)
        self.button_wavelength.clicked.connect(lambda : self.show_wavelength_dialog(dock))

        hlayout3.addWidget(self.button_wavelength)
        hlayout3.addWidget(self.button_timings)
        
        self.display_temp = QtWidgets.QCheckBox()
        self.display_temp.setMaximumWidth(80)
        self.display_temp.setText("???")
        # hlayout3.addWidget(self.display_temp)
        
        layout.addWidget(widget3)
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
            
        self.createListenerThread(self.updateSlot)

    def show_wavelength_dialog(self, parent):
        dialog = QtWidgets.QDialog(parent)
        dialog.setWindowTitle("Set wavelength")
        dialog.setModal(True)
        status = self.status()
        current_wl = status['wavelength']
        layout = QtWidgets.QFormLayout(dialog)

        edit_wl = QtWidgets.QLineEdit(f"{current_wl:.3f}")
        layout.addRow("Exposure time (s)", edit_wl)

        button_box = QtWidgets.QDialogButtonBox(dialog)
        button_box.setOrientation(QtCore.Qt.Horizontal)
        button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel)
        button_box.accepted.connect(dialog.accept)
        button_box.rejected.connect(dialog.reject)
        layout.addRow(button_box)
        result = dialog.exec_()
        if result:
            try:
                self.set_wavelength(float(edit_wl.text()))
            except:
                self.error_message(traceback.format_exc())

    def try_fvb(self):
        status = self.status_cached()
        if status["mode"] == "fvb":
            return
        self.abort_acquisition()
        self.set_full_vertical_binning()
        if status["status"] == "continuous":
            self.start_acquisition(True)

    def try_mt(self):
        status = self.status_cached()
        if status["mode"] == "multitrack":
            return
        self.abort_acquisition()
        self.set_multitrack()
        if status["status"] == "continuous":
            self.start_acquisition(True)

    def try_image(self):
        status = self.status_cached()
        if status["mode"] == "image":
            return
        self.abort_acquisition()
        self.set_image()
        if status["status"] == "continuous":
            self.start_acquisition(True)

    def show_exp_time_dialog(self, parent):
        dialog = QtWidgets.QDialog(parent)
        dialog.setWindowTitle("Set exposure time")
        dialog.setModal(True)            
        status = self.status()
        exp_time = status['exposure']
        num = status['number']
        layout = QtWidgets.QFormLayout(dialog)


        try:
            edit_speed = QtWidgets.QComboBox()
            speeds = self.get_available_scanning_speeds()
            for speed in speeds:
                edit_speed.addItem(f"{speed:.3f} MHz")
            edit_speed.setCurrentIndex(self.get_scanning_speed_index())
            layout.addRow("Readout speed", edit_speed)
        except:
            pass
        if status["mode"] == "multitrack":
            edit_mt = QtWidgets.QLineEdit()
            try:
                mt_text = ', '.join([f"{tr[0]}-{tr[1]}" for tr in status['multitrack']])
                edit_mt.setText(mt_text)
            except Exception:
                print(traceback.format_exc())
                edit_mt.setText("1-255")
            layout.addRow("Multitrack settings",edit_mt)

        edit_exp = QtWidgets.QLineEdit()
        layout.addRow("Exposure time (s)", edit_exp)
        edit_num = QtWidgets.QSpinBox()
        edit_num.setMinimum(1)
        edit_exp.setText(f"{exp_time:.6f}")
        edit_num.setValue(num)
        layout.addRow("Number of accumulations", edit_num)
        
        button_box = QtWidgets.QDialogButtonBox(dialog)
        button_box.setOrientation(QtCore.Qt.Horizontal)
        button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel)

        button_box.accepted.connect(dialog.accept)
        button_box.rejected.connect(dialog.reject)
        layout.addRow(button_box)
        
        result = dialog.exec_()
        if result:
            try:
                self.abort_acquisition()
            except Exception:
                pass
            try:
                self.set_exposure(float(edit_exp.text()), edit_num.value())    
            except Exception:
                self.error_message(traceback.format_exc())
            if status["mode"] == "multitrack":
                try:
                    mt = [[int(v) for v in p.strip().split("-")] for p in edit_mt.text().strip().split(",")]
                    self.set_multitrack(mt)
                except Exception:
                    print(traceback.format_exc())
            try:
                self.set_scanning_speed_index(edit_speed.currentIndex())
            except Exception:
                pass
            if status["status"] == "continuous":
                self.start_acquisition(True)
        
    def updateSlot(self, status):
        try:
            self.display_temp.setChecked(status["cooler"])
            self.display_temp.setText("%dK" % status["temperature"])
        except:
            pass
            
        if "status" in status:
            self.btn_run.setChecked(status["status"] == "running")
            self.btn_run_cnt.setChecked(status["status"] == "continuous")
        try:
            self.button_fvb.setChecked(status["mode"] == "fvb")
            self.button_image.setChecked(status["mode"] == "image")
            self.button_mt.setChecked(status["mode"] == "multitrack")
        except:
            pass
        if 'exposure' in status and 'number' in status:
            self.button_timings.setText(f"{status['number']}x{status['exposure']:.3f}s")
        if 'wavelength' in status:
            self.button_wavelength.setText(f"{status['wavelength']:.2f}nm")
