# -*- coding: utf-8 -*-
"""
Spectrometer class represents a typical spectrometer, such as an OceanOptics
device, or a Czerny-Turner monochromator with a CCD.
"""

from PyQt5 import QtWidgets,QtCore
import numpy as np

from .spectrometer import SpectrometerWorker
import seabreeze.spectrometers as sb
import time
import threading


class OceanWorker(SpectrometerWorker):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)       
        
    def init_device(self):
        devices = sb.list_devices()
        if not devices:
            print("Error! No devices detected!")
            raise Exception()
        print("Detected devices:")
        print(devices)
        self.spec = sb.Spectrometer(devices[0])
        self.device_desc = "%s (s/n: %s)" % (self.spec.model, self.spec.serial_number)
        print("Opened device ", self.device_desc)
        self._wavelengths = self.spec.wavelengths()
        self._last_spectrum_update = time.time()
        self._running = False
        self._continuous = False
        self.start_ocean_thread()
        
    def start_ocean_thread(self):
        def thread_func():
            exp_time = 0
            while True:
                time.sleep(0.050)
                with self.access_lock:
                    if exp_time != self._exp_time:
                        self.spec.integration_time_micros(self._exp_time*1e6)
                        exp_time = self._exp_time
                        self._cnt = 0
                ys = self.spec.intensities()
                with self.access_lock:
                    if not self._running:
                        continue
                    self._sum_spectrum += ys
                    self._cnt += 1
                    if self._cnt >= self._exp_number:
                        self._sum_spectrum /= (self._cnt * exp_time)
                        self._cnt = 0
                        self.new_spectrum(self._wavelengths, self._sum_spectrum)
                        self._sum_spectrum = 0 * self._wavelengths
                        if not self._continuous:
                            self._running = False
        self._ocean_thread = threading.Thread(target=thread_func)
        self._ocean_thread.start()
        self.set_exposure(0.1,1)
        
    def spectrometer_name(self):
        return "Ocean Optics " + self.device_desc
    
    def chip_size_px(self):
        return len(self._wavelengths),1
        
    def set_exposure(self, exp_time, number):
        # the library does not allow to cancel the acquisition so we
        # cap the exposure time at 200ms
        if exp_time > 0.2:
            exp_time = 0.2
        if number < 1:
            number = 1
        super().set_exposure(exp_time, number)

    def start_acquisition(self, continuous=False):
        self._running = True
        self._continuous = continuous
        self._cnt = 0 
        self._sum_spectrum = self._wavelengths * 0
    
    def abort_acquisition(self):
        self._running = False
    
    def acquisition_status(self):
        """ Returns one of following strings: 'running', 'idle', 'continuous', 
        or 'error'"""
        if not self._running:
            return 'idle'
        else:
            if self._continuous:
                return 'continuous'
            else:
                return 'running'
        
    def get_wavelength_range(self):
        """ Returns a tuple (lambda_min, lambda_max)"""
        return self._wavelengths[0], self._wavelengths[-1]