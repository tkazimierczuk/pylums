from time import sleep
import pyvisa
import numpy as np
from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
from PyQt5 import QtWidgets, QtCore, QtGui

#
#
#
#
#If there is any problem with communication then, please, provide each function def into statment checking for oscillioscope if it is busy
#
#
#
tektronix_device_name = 'TBS1202B'	#check the instrument name using NI MAX or Open Choice Desktop from Tektronix or choose one device from rm = pyvisa.ResourceManager() and print(rm.list_resources())
default_req_port = 14101
default_pub_port = 14102
#Allowed parameters for functions

#Two channel models
list_CH = [1, 2]
list_REF = ['A', 'B']

list_MEASUrement_IMMed_TYPes_source = ['CH1', 'CH2', 'MATH']
list_WFM = ['CH1', 'CH2', 'MATH', 'REFA', 'REFB'] #CH and REF has to be with a correct number
list_POSITION = ['POSITION1', 'POSITION2']
list_MEAS = ['MEAS1', 'MEAS2', 'MEAS3', 'MEAS4', 'MEAS5'] #A measurement specifier; <x> is 1-4 (TDS200) or 1-5 (TBS1000, TDS2000C, TDS1000C-EDU, TDS2000B, TDS1000B, TDS2000, TDS1000, TPS2000B, and TPS2000) or 1-6 (TBS1000B/EDU).

list_CHannel = ["BANdwidth", "COUPling", "CURRENTPRObe", "INVert", "POSition", "PRObe", "SCAle", "VOLts", "YUNit", "BAN", "COUP", "CURRENTPRO", "INV", "POS", "PRO", "SCA", "VOL", "YUN"]
list_CHannel_BANdwidth = ['ON', 'on', 'OFF', 'off']
list_CHannel_COUPling = ['AC', 'ac', 'DC', 'dc', 'GND', 'gnd']
list_CHannel_CURRENTPRObe = ['0.2', 0.2, '1', 1, '2', 2, '5', 5, '10', 10, '50', 50, '100', 100, '1000', 1000]
list_CHannel_INVert = ['ON', 'on', 'OFF', 'off']
list_CHannel_POSition = float
list_CHannel_PRObe = ['1', 1, '10', 10, '20', 20, '50', 50, '100', 100, '500', 500, '1000', 1000]
list_CHannel_SCAle = float
list_CHannel_VOLts = float
list_CHannel_YUNit = ['V', 'v', 'A', 'a']

list_DATa_encoding = ['ASCII', 'RIBINARY', 'RPBINARY', 'SRIBINARY', 'SRPBINARY']
list_MEASUrement_IMMed_TYPes = ['CRMs', 'FALL', 'FREQuency', 'MAXImum', 'MEAN', 'MINImum', 'NONe', 'NWIdth',
                                'PERIod', 'PK2pk', 'PWIdth', 'RISe', 'RMS', 'CURSORRms', 'PDUTy', 'PHAse',
                                'DELAYRR', 'DELAYRF', 'DELAYFR', 'DELAYFF', 'AMplitude', 'CMEAN', 'High',
                                'LOW', 'NDUty', 'POVERshoot', 'NOVERshoot', 'CURSORMean', 'BURSTWIDth',
                                'AREA', 'CAREA', 'PPULSECount', 'NPULSECount', 'REDGECount', 'FEDGECount',
                                'CRM', 'FALL', 'FREQ', 'MAXI', 'MEAN', 'MINI', 'NON', 'NWI',
                                'PERI', 'PK2', 'PWI', 'RIS', 'RMS', 'CURSORR', 'PDUT', 'PHA',
                                'DELAYRR', 'DELAYRF', 'DELAYFR', 'DELAYFF', 'CMEAN',
                                'LOW', 'NDU', 'POVER', 'NOVER', 'CURSORM', 'BURSTWID',
                                'AREA', 'CAREA', 'PPULSEC', 'NPULSEC', 'REDGEC', 'FEDGEC']
list_AUTORange_SETTings = ['HORizontal', 'HOR', 'VERTical', 'VERT', 'BOTH']

class TBS1000BWorker(DeviceWorker):

    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, address="COM1", isobus='', **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

        self._address = address.upper()

    def init_device(self):
        print('Device name: ' + tektronix_device_name + '\n Starting Initialization...')

        rm = pyvisa.ResourceManager()
        print(rm.list_resources())
        rm.open_resource(tektronix_device_name)
        print(self._execute('ID?', response=True))
        print(self._execute('*IDN?', response=True))

        print(tektronix_device_name + ' - OK')



    @remote
    def _execute(self, instruction, parameter='', response=None):

        if (parameter == '?'):
            response = True

        argument = ''
        if parameter=='': pass
        elif type(parameter)==float:
            argument = ' ' + str(parameter)
        elif type(parameter)==int:
            argument = ' ' + str(parameter)
        elif type(parameter)==str:
            argument = ' ' + parameter
        else:
            print('Argument is not a float or an integer or a string\n'+'got - '+str(parameter)+' known as - '+str(type(parameter)))
            return 'An error occured - take a look at Device Server window.'

        message = instruction + argument
        print('sending >>> ' + message)

        rm = pyvisa.ResourceManager()
        pyvisa_handle = rm.open_resource(tektronix_device_name)
        pyvisa_handle.write_termination = '\n'

        if response==True:
            pyvisa_handle.read_termination = '\n'
            print('Receiving string.')
            return pyvisa_handle.query(message)
        elif response=='binary_array' or response in ['RIBINARY', 'RPBINARY', 'SRIBINARY', 'SRPBINARY']:
            print('Receiving Binary array.')
            return np.array(pyvisa_handle.query_binary_values(message))
            ### probably it has to be changed does not work well for curve in binary
            # return pyvisa_handle.query_binary_values(argument, datatype='d', is_big_endian=True)
        elif response=='ASCII':
            print('Receiving ASCII array.')
            return pyvisa_handle.query_ascii_values(message)
        elif response==None or response==False:
            print('Not expecting reply.')
            pyvisa_handle.write(message)
        else:
            print('The message has not been send. Incorrect type of response.')
            print('Correct syntax - *._execute(#instruction, #parameter, #response)')
            return 'An error occured - take a look at Device Server window.'

    def status(self):
        d = super().status()
        return d

    def sratus(self):
        d['Ch1_bandwidth'] = self.get_channel(channel=1, request="BANdwidth")
        # d['Ch1_coupling'] = self.get_channel(channel=1, request="COUPling")
        # d['Ch1_currentprobe'] = self.get_channel(channel=1, request="CURRENTPRObe")
        # d['Ch1_invert'] = self.get_channel(channel=1, request="INVert")
        # d['Ch1_position'] = self.get_channel(channel=1, request="POSition")
        # d['Ch1_probe'] = self.get_channel(channel=1, request="PRObe")
        # d['Ch1_scale'] = self.get_channel(channel=1, request="SCAle")
        # d['Ch1_volts'] = self.get_channel(channel=1, request="VOLts")
        # d['Ch1_yunits'] = self.get_channel(channel=1, request="YUNit")
        # d['Ch2_bandwidth'] = self.get_channel(channel=2, request="BANdwidth")
        # d['Ch2_coupling'] = self.get_channel(channel=2, request="COUPling")
        # d['Ch2_currentprobe'] = self.get_channel(channel=2, request="CURRENTPRObe")
        # d['Ch2_invert'] = self.get_channel(channel=2, request="INVert")
        # d['Ch2_position'] = self.get_channel(channel=2, request="POSition")
        # d['Ch2_probe'] = self.get_channel(channel=2, request="PRObe")
        # d['Ch2_scale'] = self.get_channel(channel=2, request="SCAle")
        # d['Ch2_volts'] = self.get_channel(channel=2, request="VOLts")
        # d['Ch2_yunits'] = self.get_channel(channel=2, request="YUNit")
        d['Ch1_mean'] = self.get_measurement('MEAN', source1='CH1')
        d['Ch1_RMS'] = self.get_measurement('RMS', source1='CH1')
        d['Ch2_mean'] = self.get_measurement('MEAN', source1='CH2')
        d['Ch2_RMS'] = self.get_measurement('RMS', source1='CH2')
        d['autorange_settings_state'] = self.autorange()


    @remote
    def get_counterfreq(self):
        return self._execute('COUNTERFreq', '?')

    @remote
    def get_data_encoding(self):
        return self._execute('DATa:ENCdg?', response=True)

    @remote
    def get_curve(self, start=1, stop=2500, dataEncoding=None):
        ding_dong_you_are_wrong = 0
        if type(start)!=int or type(stop)!=int:
            print('Start and stop values both has to be integer.')
            ding_dong_you_are_wrong = 1
        if start not in range(1,2501):
            print('Start value not in range 1-2500')
            ding_dong_you_are_wrong = 1
        if start not in range(1, 2501):
            print('Stop value not in range 1-2500')
            ding_dong_you_are_wrong = 1
        if start > stop:
            print('The start value is larger than the stop value')
            ding_dong_you_are_wrong = 1

        if dataEncoding:
            if dataEncoding in list_DATa_encoding:
                self._execute('DATa:ENCdg', dataEncoding)
            else:
                print('Data encoding has to be - '+str(list_DATa_encoding)+'.')
                ding_dong_you_are_wrong = 1

        if ding_dong_you_are_wrong==1:
            print('Correct syntax - *.get_curve(#start, #stop, #dataEncoding)')
            return 'An error occured - take a look at Device Server window.'

        self._execute('DATa:STARt', start)
        self._execute('DATa:ENCdg', stop)
        responseType = self.get_data_encoding()
        return self._execute('CURVe?', response=responseType)



    @remote
    def get_measurement(self, argument = '?', source1=None, source2 = None):
        if source1!=None:
            if source1 in list_MEASUrement_IMMed_TYPes_source:
                self._execute('MEASUrement:IMMed:SOUrce1', source1)
            else:
                print('Provide correct source[1] input - '+ str(list_MEASUrement_IMMed_TYPes_source))
        if source2!=None:
            if source2 in list_MEASUrement_IMMed_TYPes_source:
                self._execute('MEASUrement:IMMed:SOUrce2', list_MEASUrement_IMMed_TYPes_source)
            else:
                print('Provide correct source[2] input - '+ str(list_CH))

        if argument=='?':
            return [self._execute('MEASUrement:IMMed:SOUrce1?', response=True),
                    self._execute('MEASUrement:IMMed:SOUrce2?', response=True),
                    self._execute('MEASUrement:IMMed:TYPe?', response=True)]
        elif argument in list_MEASUrement_IMMed_TYPes:
            self._execute('MEASUrement:IMMed:TYPe', argument)
            measurementType = self._execute('MEASUrement:IMMed:TYPe?', response=True)
            measurementValue = float(self._execute('MEASUrement:IMMed:VALue?', response=True))
            measurementUnits = self._execute('MEASUrement:IMMed:UNIts?', response=True)
            measurementUnits = measurementUnits[1:(len(measurementUnits)-1)]
            return [measurementType, measurementValue, measurementUnits]

        print('Choose one and use as an argument - ' + str(list_MEASUrement_IMMed_TYPes))
        print('Correct syntax - *.get_measurement(#argument, #source1, #source2')
        return 'An error occured - take a look at Device Server window.'



    @remote
    def set_channel(self, functionType=None, parameter='', channel=1, ):

        if(channel in list_CH):
            if(functionType in list_CHannel):
                if (functionType[0:3]=='BAN' and parameter not in list_CHannel_BANdwidth):
                    print('Argument for bandwidth has to be - '+str(list_CHannel_BANdwidth)+'\nbut got - '+str(parameter)+' known as - '+str(type(parameter)))
                elif (functionType[0:4]=='COUP' and parameter not in list_CHannel_COUPling):
                    print('Argument for coupling has to be - '+str(list_CHannel_COUPling)+'\nbut got - '+str(parameter)+' known as - '+str(type(parameter)))
                elif (functionType[0:10]=='CURRENTPRO' and parameter not in list_CHannel_CURRENTPRObe):
                    print('Argument for current probe has to be - '+str(list_CHannel_CURRENTPRObe)+'\nbut got - '+str(parameter)+' known as - '+str(type(parameter)))
                elif (functionType[0:3]=='INV' and parameter not in list_CHannel_INVert):
                    print('Argument for invert has to be - '+str(list_CHannel_INVert)+'\nbut got - '+str(parameter)+' known as - '+str(type(parameter)))
                elif (functionType[0:3]=='POS' and type(parameter) != list_CHannel_POSition):
                    print('Argument for position has to be - '+str(list_CHannel_COUPling)+'\nbut got - '+str(parameter)+' known as - '+str(type(parameter)))
                elif (functionType[0:3]=='PRO' and parameter not in list_CHannel_PRObe):
                    print('Argument for probe has to be - '+str(list_CHannel_PRObe)+'\nbut got - '+str(parameter)+' known as - '+str(type(parameter)))
                elif (functionType[0:3]=='SCA' and type(parameter) != list_CHannel_SCAle):
                    print('Argument for scale has to be - '+str(list_CHannel_SCAle)+'\nbut got - '+str(parameter)+' known as - '+str(type(parameter)))
                elif (functionType[0:3]=='VOL' and type(parameter) != list_CHannel_VOLts):
                    print('Argument for volts has to be - '+str(list_CHannel_VOLts)+'\nbut got - '+str(parameter)+' known as - '+str(type(parameter)))
                elif (functionType[0:3]=='YUN' and parameter not in list_CHannel_YUNit):
                    print('Argument for y-unit has to be - '+str(list_CHannel_YUNit)+'\nbut got - '+str(parameter)+' known as - '+str(type(parameter)))
                else:
                    if functionType[0:3]=='YUN':
                        parameter = "'"+parameter+"'"   #it has to be send in form of string #(command "A") or #(command 'v')
                    argument = 'CH' + str(channel) + ':' + functionType
                    self._execute(argument, parameter, response=False)
                    return
            else:
                print('Provide correct request argument from the list - ' + str(list_CHannel))
        else:
            print('Provide correct channel number as an argument. Allowed - ' + str(list_CH))
        print('Correct syntax - *.set_channel(#function_type, #parameters, #channel)')
        return 'An error occured - take a look at Device Server window.'



    @remote
    def get_channel(self, channel=1, request=None):
        try:
            request = str(request)
        except Exception:
            pass

        if channel in [1, 2, 3, 4]:
            if request in ["BANdwidth", "COUPling", "CURRENTPRObe", "INVert", "POSition", "PRObe", "SCAle", "VOLts", "YUNit"]:
                argument = 'CH' + str(channel) + ':' + request + '?'
                return self._execute(argument, response=True)
            elif request == None:
                argument = 'CH' + str(channel) + '?'
                return self._execute(argument, response=True)
            else:
                print('Provide correct request argument - None, "BANdwidth", "COUPling", "CURRENTPRObe", "INVert", "POSition", "PRObe", "SCAle", "VOLts", "YUNit"')
                return
        print('Provide correct channel number as an argument')
        print('Correct syntax - *.get_channel(#channel, #request)')
        return 'An error occured - take a look at Device Server window.'



    @remote
    def autoset(self):
        self._execute('AUTOSet:ENABLE', response=False)
        self._execute('AUTOSet EXECute', response=False)



    @remote
    def autorange(self, argument='?'):
        if argument=='?':
            return [self._execute('AUTORange:SETTings?', response=True), self._execute('AUTORange:STATE?', response=True)]
        elif argument in list_AUTORange_SETTings:
            self._execute('AUTORange:SETTings', argument, response=False)
        elif argument in ['ON', 'on', True, 'OFF', 'off', False]:
            if type(argument)==bool:
                argument = int(argument)
            self._execute('AUTORange:STATE', argument, response=False)
        else:
            print('Provide correct argument - '+str(list_AUTORange_SETTings)+'\n or '+str(['ON', 'on', True, 'OFF', 'off', False]))
            print('Correct syntax - *.autorange(#argument)')
            return 'An error occured - take a look at Device Server window.'



    @remote
    def get_busy(self):
        return bool(int((self._execute('BUSY?', response = True))[-1]))



    @remote
    def reset_factory_settings(self):
        return self._execute('FACtory')



@include_remote_methods(TBS1000BWorker)
class TBS1000B(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
