from time import sleep
import pyvisa
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5.QtWidgets import (QWidget, QPushButton, 
    QHBoxLayout, QVBoxLayout, QApplication)
from PyQt5 import QtWidgets, QtCore, QtGui
import threading

default_req_port = 6313
default_pub_port = 6314

class ElliptecWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, address="COM26", **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self._address = address
   
    
    def init_device(self):
        """
        Opens the communication 
        """
        self.visa_lock = threading.Lock()
        rm = pyvisa.ResourceManager()
       
        try:
            self.visa_handle = rm.open_resource("ASRL%s::INSTR" % self._address[3:])
        except:
            self.visa_handle = rm.open_resource("ASRLCOM%s::INSTR" % self._address[3:])
            
        self.visa_handle.set_visa_attribute(pyvisa.constants.VI_ATTR_ASRL_STOP_BITS,
                                            pyvisa.constants.VI_ASRL_STOP_ONE)
        self.visa_handle.set_visa_attribute(pyvisa.constants.VI_ATTR_ASRL_BAUD, 9600)

        self.visa_handle.write_termination = '\r\n'
        self.visa_handle.read_termination = '\r\n'
        self.motors = {}

        self.visa_handle.timeout = 300  # temporarily reduce timeout to make axis detection quicker
        for axis in range(0, 5):
            try:
                print(f"Trying axis {axis:d}...  ", end="")
                res = self._execute(axis, 'in')
                self.motors[str(axis)] = {'steps_per_unit': int(res['data'][22:30], 16),
                                          'homed': False
                                         }
                print("axis detected")
            except pyvisa.errors.VisaIOError:
                print("failed")
        self.visa_handle.timeout = 1000

        print(f"Connected with {len(self.motors)} available axes.")

    def _execute(self, axis, message):
        """ Send a message for given axis and return the response
            axis(char or int): 0-F """
        with self.visa_lock:
            self.visa_handle.write(str(axis) + message)
            res = self.visa_handle.read()
        return {'address': res[0],
                'header': res[1:3],
                'data': res[3:].strip()}

    @remote
    def read_axis_info(self, axis):
        """ axis (char or int) 0-F """
        res = self._execute(axis, 'in')
        return res
        
    def status(self):
        d = super().status()
        d["apt_devices"] = self.devices()
        for sn in self.motors:
            motor = self.motors[sn]
            d["apt_{0}".format(sn)] = \
                {"position": self.get_position(sn),
                 "stopped":  True,
                 "homed": motor['homed']}
        return d

    def _steps_per_unit(self, axis):
        return self.motors[str(axis)]['steps_per_unit'] / 360 

    @remote
    def move_absolute(self, axis, target):
        target_steps = int(target* self._steps_per_unit(axis))
        res = self._execute(axis, 'ma' + '{0:08x}'.format(target_steps).upper())

    @remote
    def devices(self):
        return list(self.motors.keys())

    @remote
    def axes(self):
        return self.devices()

    @remote
    def move_velocity(self, serial, velocity):
        pass
        #TODO

    @remote
    def stop(self, axis):
        self._execute(axis, 'ms')

    @remote
    def get_position(self, axis):
        res = self._execute(axis, 'gp')
        n = int(res['data'], 16)
        if n > 2**31:
            n -= 2**32
        return n / self._steps_per_unit(axis)

    @remote
    def is_stopped(self, serial):
        return True  # continuous rotation not supported
    
    @remote
    def homed(self, axis):
        return self.motors[str(axis)]['homed']
    
    @remote
    def home(self, axis):
        self._execute(axis, 'ho0')
        self.motors[str(axis)]['homed'] = True

    @remote
    def get_name(self, axis):
        return str(axis)
