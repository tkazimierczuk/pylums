# -*- coding: utf-8 -*-
"""
Created on Wed Sep 18 14:28:28 2019

@author: DMS
"""


import serial

from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods

from collections import namedtuple
from PyQt5 import QtCore, QtWidgets, QtGui
import time
import threading
import re

default_req_port = 8010
default_pub_port = 8011
default_serial='COM15'



class ThorlabsChopperWorker(DeviceWorker):
    _blades = ["MC1F2", "MC1F10", "MC1F15", "MC1F30", "MC1F60", "MC1F100", "MC1F10HP",'MC1F2P10','MC1F6P10','MC1F10A','MC2F330','MC2F47','MC2F57B','MC2F860','MC2F5360']
    _bladerange = namedtuple("bladerange", _blades)

    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, port=default_serial, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        
        
        self.ser = serial.Serial(port)
        self.ser.baudrate = 115200
        self.ser.timeout = 1
        self.ser.bytesize = serial.EIGHTBITS
        self.ser.parity = serial.PARITY_NONE
        self.ser.stopbits = serial.STOPBITS_ONE
        self.ser.xonxoff = 0
        self.ser.rtscts = 0
        self.ser.dsrdtr = 0
        #self.ser.write("\r")
        #self.ser.read(100)
        
        self._Bladerange = self._bladerange(MC1F2=(4.0, 200.0),MC1F10=(20.0,1000.0),
                                            MC1F15=(30.0,1500.0),MC1F30=(60.0,3000.0),
                                            MC1F60=(120.0,6000.0),
                                            MC1F100=(200.0,10000.0),
                                            MC1F10HP=(20.0,1000.0),
                                            MC1F2P10=(4.0,200.0),
                                            MC1F6P10=(12.0,600.0),
                                            MC1F10A=(20.0,1000.0),
                                            MC2F330=(6.0,300.0),
                                            MC2F47=(8.0,400.0),
                                            MC2F57B=(10.0,500.0),
                                            MC2F860=(16.0,800.0),
                                            MC2F5360=(160.0,5300.0))
        self.id=self.get_id()
        self._blade=self.get_blade
        self._intfreq=self.get_intfreq
        self._phase=self.get_phase
        self._enable=self.get_enable()
    
   
    
    
    
    
    
    def execute(self,command):
        for i in range(0,3):
            while True:
                try:    
                    self.ser.write(command.encode())
                    answer = self.ser.read_until(terminator=b' ')  
                    rlvalue = int(answer[len(command):-3])
                    
                    return rlvalue
                except ValueError:
                    self.ser.reset_input_buffer()
                    continue
                break
    
    
    @remote
    def status(self):
        d = super().status()
        d["int_freq"]=self.get_intfreq
        d["ext_freq"]=self.get_exfreq()
        d['phase']=self.get_phase
        d['enable']=bool(self.get_enable())
        d['blade']=self._blades[self.get_blade]
            
        return d
    
    @remote
    def get_id(self):
        command='id?\r'
        
        self.ser.write(command.encode())
        answer = self.ser.read_until(' ')  # adjust!
        rlvalue = answer[len(command):-3]
        return str(rlvalue)[2:-2]
    
    
    @property
    def get_intfreq(self):
        
        command = "freq?\r"
        value=self.execute(command)
        self._intfreq=value
        return value
            
    @get_intfreq.setter
    def set_intfreq(self, value):
        
        "set the internal frequency"
        #with self._chopper_mutex:
        if int(value) < self._Bladerange[self._blade][0] or int(value) > self._Bladerange[self._blade][1]:
            raise ValueError("{} is out of range!".format(str(value)))
        command = "freq={}\r".format(str(value))
        self.ser.write(command.encode())
        self._intfreq= value
        
    @property
    def get_blade(self):
        "get the current blade type"
        command = "blade?\r"
        #self.ser.reset_input_buffer()
        self.ser.write(command.encode())
        
        answer=self.ser.read_until(terminator=b' ')  # adjust!
        rlvalue = int(answer[len(command):-3])
        self._blade=rlvalue
        return rlvalue
    
    @get_blade.setter
    def set_blade(self, value, ):
        "set the blade type"
        
        if int(value) not in list(range(0,15)):
            raise ValueError("{} is out of range!".format(str(value)))
        command = "blade={}\r".format(str(value))
        self.ser.write(command.encode())
        
        self._blade=value
        
    @property
    def get_phase(self):
        "get the current phase"
        command = "phase?\r"
        value=self.execute(command)
        self._phase=value
        return value
    
    @get_phase.setter
    def set_phase(self,value):   
        
        if int(value) not in list(range(0,360)):
            raise ValueError("{} is out of range!".format(str(value)))
        command = "phase={}\r".format(str(value))
        self.ser.write(command.encode())
        self._phase=value
          
    @remote  
    def get_enable(self):
        "get current status (still or running)"
        command = "enable?\r"
        value=self.execute(command)
        self.enable = value
        return value
    
    @remote
    def get_exfreq(self):
        "get current external frequency"
        command = "refoutfreq?\r"
        value=self.execute(command)
        self.extfreq = value
        return value
    @remote
    def start(self):
        "send start signal"
        command = "enable=1\r"
        self.ser.write(command.encode())
        
        return
    @remote
    def stop(self):
        "send stop signal"
        command = "enable=0\r"
        self.ser.write(command.encode())
        
        return
    
    @remote
    def __del__(self):
        pass
        
@include_remote_methods(ThorlabsChopperWorker)
class ThorlabsChopper(DeviceOverZeroMQ):
    """ Simple stub for the class to be accessed by the user """
    _blades = ["MC1F2", "MC1F10", "MC1F15", "MC1F30", "MC1F60", "MC1F100", "MC1F10HP",'MC1F2P10','MC1F6P10','MC1F10A','MC2F330','MC2F47','MC2F57B','MC2F860','MC2F5360']
    _bladerange = namedtuple("bladerange", _blades)
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._Bladerange = self._bladerange(MC1F2=(4.0, 200.0),MC1F10=(20.0,1000.0),
                                            MC1F15=(30.0,1500.0),MC1F30=(60.0,3000.0),
                                            MC1F60=(120.0,6000.0),
                                            MC1F100=(200.0,10000.0),
                                            MC1F10HP=(20.0,1000.0),
                                            MC1F2P10=(4.0,200.0),
                                            MC1F6P10=(12.0,600.0),
                                            MC1F10A=(20.0,1000.0),
                                            MC2F330=(6.0,300.0),
                                            MC2F47=(8.0,400.0),
                                            MC2F57B=(10.0,500.0),
                                            MC2F860=(16.0,800.0),
                                            MC2F5360=(160.0,5300.0))
    def setFreqDialog(self,thrash):
        min_val= self._Bladerange[self._blades.index(self.status()['blade'])][0]
        max_val= self._Bladerange[self._blades.index(self.status()['blade'])][1]
        val,ok = QtWidgets.QInputDialog.getInt(None, "Set frequency", "New frequency:",self.status()['int_freq'],min_val,max_val,1)
        if ok:
                #QtWidgets.QApplication.processEvents()
                self.set_intfreq=val
                
    def createDock(self, parentWidget, menu=None):
        """ Function for integration in GUI app. Implementation below 
        creates a button and a display """
        dock = QtWidgets.QDockWidget("Chopper", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QGridLayout(parentWidget)
        widget.setLayout(layout)
        self.count_diplay_name=QtWidgets.QLabel('Int. Freq')  
        self.count_display = QtWidgets.QLineEdit()
        self.count_display.setReadOnly(True)
        
        self.external_count_display_name=QtWidgets.QLabel('Ext. Freq')
        self.external_count_display= QtWidgets.QLineEdit()
        self.external_count_display.setReadOnly(True)
        
        self.start_btn=QtWidgets.QPushButton('Start')
        self.stop_btn=QtWidgets.QPushButton('Stop')
        self.count_display.mousePressEvent=self.setFreqDialog
        self.start_btn.clicked.connect(lambda: self.start())
        self.stop_btn.clicked.connect(lambda: self.stop())
        
        layout.addWidget(self.count_diplay_name,0,0,1,1)
        layout.addWidget(self.count_display,0,1,1,2)
        layout.addWidget(self.external_count_display_name,1,0,1,1)
        layout.addWidget(self.external_count_display,1,1,1,2)
        layout.addWidget(self.start_btn,2,0,1,1)
        layout.addWidget(self.stop_btn,2,1,1,1)
        
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)
    
    

    
    def updateSlot(self, status):
            self.count_display.setText(str(status['int_freq']))
            self.external_count_display.setText(str(status['ext_freq']))