from datetime import datetime
import time
from time import sleep
from time import perf_counter as clock
import visa
import threading
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5.QtWidgets import (QWidget, QPushButton, 
    QHBoxLayout, QVBoxLayout, QApplication)
from PyQt5 import QtWidgets, QtCore, QtGui
import functools


default_req_port = 6000
default_pub_port = 6001

def when(func):
    def wrapper_when(*args, **kwargs):
        print(str(datetime.now()),func.__name__ )
        return func(*args, **kwargs)
        #return func(*args, **kwargs)
    return wrapper_when

class Temperature33xWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, address="COM1", isobus="None", **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

        self._address = address
        self._isobus = isobus

        if isobus != "None":
            self._isobus_header = '@%s' % self._isobus
        else:
            self._isobus_header = ''
        self.model_no = '' #'331' or '335'
        self.temperatureA =0
    
    def status(self):
        d = super().status()
        ta=self.temperatureK(probe='A')
        d["temperatureA"]=ta
        d["temperatureB"]=self.temperatureK(probe='B')
        d["setpoint"]=self.setpoint(loop=1)
        d["heater"]=self.heater()
        d["heater_range"]=self.heater_range()
        d["P"],d["I"],d["D"]=self.PID(loop=1)
        return d
    
    def init_device(self):
        """
        Opens the communication 
        """
        print("=========")
        print(str(datetime.now()))
        print("Lakeshore: Initialization...")
        self.visa_lock = threading.Lock()
        rm = visa.ResourceManager()
        termination_chars='\r\n'
        if self._address[0:3] == 'COM':
            print("Lakeshore: Communication at COM"+str(self._address[3:]))
            self.visa_handle = rm.open_resource("ASRL%s::INSTR" % self._address[3:])
            self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_STOP_BITS,
                                                visa.constants.VI_ASRL_STOP_ONE)
            self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_BAUD,57600)
            #newer model 335 has baud rate 57600
            self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_PARITY,visa.constants.VI_ASRL_PAR_ODD)
            self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_DATA_BITS, 7)
            self.visa_handle.read_termination = termination_chars
            mess=self._execute("*IDN?")
            if len(mess.strip()) != 0:
                    self.model_no = mess.strip()
                    self.model_no = self.model_no[10:13]
                    print("Lakeshore: Detected temperature controller model is "+self.model_no)
            else:
                print("Lakeshore: Changing baud rate from 57600 to 9600...")
                self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_BAUD,9600)
                # older model 331 has baud rate 9600
                self._execute('')
                mess=self._execute("*IDN?")
                if len(mess.strip()) == 0:
                    print("Lakeshore: Can't set up connection! Try again!")
                else:
                    #print(mess.strip())
                    self.model_no = mess.strip()
                    self.model_no = self.model_no[10:13]
                    print("Lakeshore: Detected temperature controller model is "+self.model_no)                     
        else:
            self.visa_handle = rm.open_resource("GPIB::%s" % self._address[4:])

        self.visa_handle.read_termination = '\r\n'      
        print("Lakeshore: End of initialization")
          

    def _read(self):
        # because protocol has no termination chars the read reads the number
        # of bytes in the buffer
        bytes_in_buffer = self.visa_handle.bytes_in_buffer
        # a workaround for a timeout error in the pyvsia read_raw() function
        with(self.visa_handle.ignore_warning(visa.constants.VI_SUCCESS_MAX_CNT)):
            mes = self.visa_handle.visalib.read(
                self.visa_handle.session, bytes_in_buffer)
        # cannot be done on same line for some reason
        mes = str(mes[0].decode())
        return mes

    def _execute(self, message):
        with self.visa_lock:
            self.visa_handle.write(self._isobus_header + message)
            sleep(70e-3)  # wait for the device to be able to respond
            result = self._read()
        if result.find('?') >= 0:
            print("Error: Command %s not recognized: %s" % (message, result))
            return None
        else:
            return result
    @when
    def close_device(self):
        self.continue_running = False    

    @remote
    def busy(self):
        return self._stage != None and self._stage < 7

    @remote
    def lock(self):
        '''To manually unlock press 'Enter' for 10s and write 123''' 
        self._execute("LOCK 1,123")
        
    @remote
    def unlock(self):
        '''To manually unlock press 'Enter' for 10s and write 123''' 
        self._execute("LOCK 0,123")
        
    @remote    
    def temperatureK(self,probe='A'):
        '''Get temperature in Kelvin from probe A''' 
        return float(self._execute("KRDG? "+probe))
    
    @remote        
    def temperatureC(self,probe='A'):
        '''Get temperature in Celcius from probe A''' 
        return float(self._execute("CRDG? "+probe))
    
    @remote        
    def heater(self):
        '''Get heater driving level in percents @ loop 1, \n for loop 2 see user manual for 331 model''' 
        return float(self._execute("HTR?"))
    
    @remote        
    def set_heater(self,heater_level=0.00,loop=1):
        '''Set heater driving level in percents for loop 1
        must be in open loop see CMOD command - not implemanted yet''' 
        self._execute("MOUT {},{}".format(loop,heater_level))
        
    @remote        
    def heater_range(self):
        '''Get heater range level  \n 0 = off, 1 = Low (0.5W), 2 = Medium (5W), 3 = High (50W)''' 
        return int(self._execute("RANGE?"))
    
    @remote
    #@when       
    def set_heater_range(self, h_range=0):
        '''h_range\n Set heater range level  \n 0 = off, 1 = Low (0.5W), 2 = Medium (5W), 3 = High (50W)''' 
        if isinstance(h_range,str):
            if h_range.upper()=="OFF":
                h_range=0
            elif h_range.upper()=="LOW":
                h_range=1
            elif h_range.upper()=="MEDIUM":
                h_range=2
            elif h_range.upper()=="HIGH":
                h_range=3
        self._execute("RANGE {}".format(int(h_range)))
        
    @remote        
    def set_setpoint(self, setpoint=4.2,loop=1):
        '''setpoint,loop\nSet setpoint in K for loop 1''' 
        self._execute("SETP {},{}".format(loop,setpoint))
        
    @remote 
    def setpoint(self,loop=1):
        '''Get setpoint  in K for loop 1''' 
        return float(self._execute("SETP? {}".format(loop)))
    
    @remote
    def PID(self, loop=1):
        '''loop\n  Get PID parameters for loop 1'''
        mess=self._execute("PID? {}".format(loop))
        mess=mess.split(',')
        return float(mess[0]),float(mess[1]),float(mess[2])

    @remote
    def set_PID(self,P=250.0,I=50.0,D=100.0, loop=1):
        '''P,I,D,loop \nSet PID parameters for loop 1 \n P frm 0.1 to 1000;\n I from 0.1 to 1000;\n D from 0 to 200'''
        #cant set D - I dont know why. 
        self._execute("PID {}, {}, {}, {}".format(loop,P,I,D))
        
    @remote
    def set_P(self,P=250.0, loop=1):
        '''P,I,D,loop \nSet PID parameters for loop 1 \n P frm 0.1 to 1000;\n I from 0.1 to 1000;\n D from 0 to 200'''
        #cant set D - I dont know why. 
        mess=self._execute("PID? {}".format(loop))
        mess=mess.split(',')
        Pold,Iold,Dold=float(mess[0]),float(mess[1]),float(mess[2])
        #print(Pold,Iold,Dold)
        self._execute("PID {}, {}, {}, {}".format(loop,P,Iold,Dold))
        
    @remote
    def set_I(self,I=50.0, loop=1):
        '''P,I,D,loop \nSet PID parameters for loop 1 \n P frm 0.1 to 1000;\n I from 0.1 to 1000;\n D from 0 to 200'''
        #cant set D - I dont know why. 
        mess=self._execute("PID? {}".format(loop))
        mess=mess.split(',')
        Pold,Iold,Dold=float(mess[0]),float(mess[1]),float(mess[2])
        self._execute("PID {}, {}, {}, {}".format(loop,Pold,I,Dold))
        
    @remote
    def set_D(self,D=100.0, loop=1):
        '''P,I,D,loop \nSet PID parameters for loop 1 \n P frm 0.1 to 1000;\n I from 0.1 to 1000;\n D from 0 to 200'''
        #cant set D - I dont know why. 
        mess=self._execute("PID? {}".format(loop))
        mess=mess.split(',')
        Pold,Iold,Dold=float(mess[0]),float(mess[1]),float(mess[2])
        self._execute("PID {}, {}, {}, {}".format(loop,Pold,Iold,D))
        
    @remote
    def ramp(self, loop=1):
        ''' loop\n Get ramp status and rate for loop 1 in K/min'''
        mess=self._execute("RAMP? {}".format(loop))
        mess=mess.split(',')
        return float(mess[0]),float(mess[1])
    
    @remote
    def set_ramp(self, ramp_stat=0,ramp_rate=10.0,loop=1):
        '''ramp_state,ramp_rate,loop\n Set ramp status and rate for loop 1 in K/min'''
        if isinstance(ramp_stat,str):
            if ramp_stat.upper()=="OFF":
                ramp_stat=0
            elif ramp_stat.upper()=="ON":
                ramp_stat=1
        self._execute("RAMP {},{},{}".format(loop,ramp_stat,ramp_rate))
        
    @remote
    def get_model(self):
        '''Returns model string '331' or '335' '''
        return self.model_no
    
    @remote
    def get_address(self):
        '''Returns COM port address '''
        return self._address
    
    @remote
    @when
    def heaters_off(self):
        '''Immediately turns off all heaters'''
        m=self.get_model()
        if m=="331":
            self._execute("RANGE 0")    
        elif m=="335":
            self._execute("RANGE 1,0")
            self._execute("RANGE 2,0")

@include_remote_methods(Temperature33xWorker)
class Temperature33x(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.setting_window=None
        self.LCD_unit="K"
        self.LCDtext="temperatureA"
        self.status_copy={}
        self.units={}
        self.units["temperatureA"] = "K"
        self.units["temperatureB"] = "K"
        self.units["setpoint"] = "K"
        self.units["heater"] = "%"
        self.units["heater_range"] = ""
        self.units["P"] = ""
        self.units["I"] = ""
        self.units["D"] = ""

    #def __del__(self):
    #    pass

    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("Lakeshore "+self.get_model(), parentWidget)
        widget1 = QtWidgets.QWidget(dock)
        layout = QtWidgets.QGridLayout(parentWidget)
             
        self.LCD = QtWidgets.QLCDNumber()
        self.LCD.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        self.LCD.setDigitCount(6)
        self.LCD.setToolTip(self.LCDtext)
        self.Units=QtWidgets.QLabel(self.units[self.LCDtext])     
        self.btn_settings = QtWidgets.QPushButton("Settings")
        self.btn_settings.setToolTip('Opens settings window where you can change temperature controller parameters')
        self.btn_settings.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
        QtWidgets.QSizePolicy.Fixed)
        self.btn_OFF = QtWidgets.QPushButton("heaters OFF!")
        self.btn_OFF.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
        QtWidgets.QSizePolicy.Expanding)
        #self.btn_OFF.setStyleSheet("background-color:rgb(255, 50, 50)")

        layout.addWidget(self.btn_settings, 0, 0, 1, 2)
        layout.addWidget(self.LCD, 1, 0, 1, 1)
        layout.setRowStretch(1, 1)
        layout.setColumnStretch(0, 1)
        layout.addWidget(self.Units, 1, 1, 1, 1)
        layout.addWidget(self.btn_OFF, 2, 0, 1, 2)
        
        widget1.setLayout(layout)
        dock.setWidget(widget1)
        
        self.btn_settings.clicked.connect(self.show_settings)
        self.btn_OFF.clicked.connect(self.emergency_off)

        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
        
        self.createListenerThread(self.updateSlot)
        
    def updateSlot(self,status):
        #print("slot updated")
        self.LCD.display(status[self.LCDtext])
        self.status_copy = status
                    
    def show_settings(self,status):
        #print("settings")
        self.btn_settings.setEnabled(0)
        if self.setting_window is not None:
            self.setting_window.show()
        else:
            sett_window=SettingsWindow(self)
            sett_window.show()
            def on_finished(code):
                self.btn_settings.setEnabled(True)
            sett_window.exec_()
            self.btn_settings.setEnabled(True)
    def emergency_off(self):
        self.heaters_off()
        print("Heaters OFF!")
        
#=============================================    
class my_LCD(QtWidgets.QLCDNumber):
    def __init__(self,lo_range=None,hi_range=None,setter=None,info_text='Enter double value:',digit_count=6,decimals=5,dialog=True):
        super().__init__()
        self.lo_range = lo_range
        self.hi_range = hi_range
        self.setter = setter
        self.info_text=info_text
        self.decimals=decimals
        self.setDigitCount(digit_count)
        self.dialog=dialog
        self.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
        QtWidgets.QSizePolicy.Expanding)
        
    def mousePressEvent(self,event):
        if self.dialog:
            try:
                liczba=0
                val, ok = QtWidgets.QInputDialog.getDouble(self, 'Title', 
                                                           self.info_text,liczba,self.lo_range,self.hi_range,self.decimals)
            
                if ok:
                    self.setter(val)
            except:
                pass
#==============================================   
class SettingsWindow(QtWidgets.QDialog):
    def __init__(self,object_inst):
        super().__init__()
        self.parent =object_inst
        self.initUI()
        #self.widget2=None
        timer = QtCore.QTimer(self)
        timer.setInterval(250)
        timer.timeout.connect(self.refresh_values)
        timer.start()
        
    def refresh_values(self):
        self.LCD1.display(self.parent.status_copy[self.parent.LCDtext])
        self.LCD2.display(self.parent.status_copy["temperatureA"])
        self.LCD3.display(self.parent.status_copy["temperatureB"])
        self.LCD4.display(self.parent.status_copy["setpoint"])
        self.LCD5.display(self.parent.status_copy["heater"])
        self.cmbx6.setCurrentIndex(self.parent.status_copy["heater_range"])
        self.LCD7.display(self.parent.status_copy["P"])
        self.LCD8.display(self.parent.status_copy["I"])
        self.LCD9.display(self.parent.status_copy["D"])
    
    def hide_settings(self):
        self.parent.btn_settings.setEnabled(True)
        print("hide")
        
    def set_from_combobox(self):
        val=self.parent.status_copy[self.comboBox.currentText()]
        t=self.comboBox.currentText()
        self.LCD1.display(val)
        self.parent.LCDtext = t
        print("current text"+t)
        self.unit1.setText(self.parent.units[t])
        self.parent.LCD_unit = self.parent.units[t]
        self.parent.Units.setText(self.parent.units[t])
        self.parent.LCD.setToolTip(t)
        
    def set_heater_combobox(self):
        self.parent.set_heater_range(int(self.cmbx6.currentIndex()))
    
    def show_dialog(self, ev):
        print("mouse pressed")
        #print(str(ev.sender()))
        val, ok = QtWidgets.QInputDialog.getDouble(self, '', 
            'Enter double value:')
        
        if ok:
            return val
    
    def closeEvent(self,event):
        #print("kakakakakakaka")
        #self.parent.btn_settings.setEnabled(True)
        #self.widget2.close()
        #not used for now
        pass
    def initUI(self):
        
        self.grid = QtWidgets.QGridLayout()
        #self.widget2=QtWidgets.QDialog()
        self.setWindowModality(True) #do not block other windows
        self.setWindowTitle("Lakeshore settings")
        self.setGeometry(300, 300, 280, 450) #x,y,w,h
        
        quit = QtWidgets.QAction("Quit", self)
        quit.triggered.connect(self.closeEvent)
        
        self.comboBox=QtWidgets.QComboBox()
        for label in self.parent.status_copy:
            self.comboBox.addItem(label)
        self.comboBox.setToolTip('Avaliable parameters for LCD display in small version of this window')
        self.comboBox.setCurrentText(self.parent.LCDtext)
        self.comboBox.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
        QtWidgets.QSizePolicy.Fixed)
        
        
        self.LCD1=QtWidgets.QLCDNumber()

        
        self.LCD1.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        self.LCD1.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
        QtWidgets.QSizePolicy.Expanding)
        self.unit1=QtWidgets.QLabel("K")
        
        #self.LCD2.append_to_layout(grid)
        
        self.Lbl2=QtWidgets.QLabel("Temperature @A :")
        self.LCD2=QtWidgets.QLCDNumber()
        self.LCD2.setDigitCount(6)
        self.LCD2.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        self.LCD2.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
        QtWidgets.QSizePolicy.Expanding)
        self.unit2=QtWidgets.QLabel("K")
        
        self.Lbl3=QtWidgets.QLabel("Temperature @B :")
        self.LCD3=QtWidgets.QLCDNumber()
        self.LCD3.setDigitCount(6)
        self.LCD3.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        self.LCD3.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
        QtWidgets.QSizePolicy.Expanding)
        self.unit3=QtWidgets.QLabel("K")
        
        self.Lbl4=QtWidgets.QLabel("Setpoint :")
        self.LCD4=my_LCD(1.2,300,self.parent.set_setpoint)
        self.unit4=QtWidgets.QLabel("K")
        
        self.Lbl5=QtWidgets.QLabel("Heater drive :")
        self.LCD5=my_LCD(0,100,self.parent.set_heater,dialog=False)
        self.unit5=QtWidgets.QLabel("%")
        
        self.Lbl6=QtWidgets.QLabel("Heater range :")
        self.cmbx6=QtWidgets.QComboBox()
        self.cmbx6.addItem("Off")
        self.cmbx6.addItem("Low")
        self.cmbx6.addItem("Medium")
        self.cmbx6.addItem("High")
        self.cmbx6.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
        QtWidgets.QSizePolicy.Expanding)
        self.unit6=QtWidgets.QLabel("")
        
        self.Lbl7=QtWidgets.QLabel("P :")
        self.LCD7=my_LCD(0.1,1000,self.parent.set_P)
        self.unit7=QtWidgets.QLabel("")
        
        self.Lbl8=QtWidgets.QLabel("I :")
        self.LCD8=my_LCD(0.1,1000,self.parent.set_I)
        self.unit8=QtWidgets.QLabel("")
        
        self.Lbl9=QtWidgets.QLabel("D :")
        self.LCD9=my_LCD(0,200,self.parent.set_D)
        self.unit9=QtWidgets.QLabel("")
    
        self.btn_hide = QtWidgets.QPushButton("hide")
        self.btn_hide.setToolTip('Apply changes and hide window')
        self.btn_hide.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
        QtWidgets.QSizePolicy.Fixed)
        
        
        self.grid.addWidget(self.comboBox,1,1)
        self.grid.addWidget(self.LCD1,1,2)
        self.grid.addWidget(self.unit1,1,3)
        self.grid.addWidget(self.Lbl2,2,1)
        self.grid.addWidget(self.LCD2,2,2)
        self.grid.addWidget(self.unit2,2,3)
        self.grid.addWidget(self.Lbl3,3,1)
        self.grid.addWidget(self.LCD3,3,2)
        self.grid.addWidget(self.unit3,3,3)
        self.grid.addWidget(self.Lbl4,4,1)
        self.grid.addWidget(self.LCD4,4,2)
        self.grid.addWidget(self.unit4,4,3)
        self.grid.addWidget(self.Lbl5,5,1)
        self.grid.addWidget(self.LCD5,5,2)
        self.grid.addWidget(self.unit5,5,3)
        self.grid.addWidget(self.Lbl6,6,1)
        self.grid.addWidget(self.cmbx6,6,2)
        self.grid.addWidget(self.unit6,6,3)
        self.grid.addWidget(self.Lbl7,7,1)
        self.grid.addWidget(self.LCD7,7,2)
        self.grid.addWidget(self.unit7,7,3)
        self.grid.addWidget(self.Lbl8,8,1)
        self.grid.addWidget(self.LCD8,8,2)
        self.grid.addWidget(self.unit8,8,3)
        self.grid.addWidget(self.Lbl9,9,1)
        self.grid.addWidget(self.LCD9,9,2)
        self.grid.addWidget(self.unit9,9,3)
        self.grid.addWidget(self.btn_hide,10,1)
        
        self.setLayout(self.grid)
        self.refresh_values()
        self.show()
        self.comboBox.currentIndexChanged.connect(self.set_from_combobox)
        self.cmbx6.activated.connect(self.set_heater_combobox)
        #self.LCD2.mousePressEvent = functools.partial(self.show_dialog, lo_range, hi_range, remote_function)                                                    setter=self.setTemp)
        self.btn_hide.clicked.connect(self.accept)
        
        #self.widget2.exec()
        
        #+++++++++++++++++++++++++++++++++++++++++++++++++++'''