# -*- coding: utf-8 -*-

from enum import Enum
from numpy import nan
import configparser

class Device:
    """ A prototype of a class representing a single device, 
    e.g. a positioner or a spectrometer """
    def __init__(self, **kwargs):
        self.priority = Device.Priority.Normal
        
    class Priority(Enum):
        Critical = 1 # Experiment is stopped if device fails
        Semicritical = 2 # Experiment is stopped if a request fails
        Normal = 3 # Experiment is resumed when communication is restored
        Optional = 4 # Experiment continoues even if device fails
    
    def name(self):
        return type(self).__name__
    
    def status(self):
        return {}
    
    def status_cached(self, timestamp=None, timeout=None, ask_if_not_available=True):
        """ Returns the last status if it is still fresh
        
            timestamp: if given status will not be older than this point
            timeout(seconds): if given status will not be older than this value
            ask_if_not_available(bool): determines what happens when no cached
                                     status is available. If True then status()
                                     is invoked. If False then {} is returned.
        """
        # default implementation: no caching implemented
        if ask_if_not_available:
            return self.status()
        else:
            return {}
    


class Parameter:
    """ A prototype of a class representing a single parameter in the experiment.
    This class is introduced to provide a uniform interface for control widgets.
    E.g., a single multi-axis controller Device can provide several Parameters
    representing single axes."""
    def name(self):
        raise NotImplementedError
    
    def value(self):
        raise NotImplementedError
    
    def move_to_target(self, target):
        raise NotImplementedError
    
    def move_continuous(self, value):
        raise NotImplementedError
    
    def is_moving(self):
        raise NotImplementedError


""" A dictionary of loaded devices. Key is the interpreter name of the object """
active_devices = {}


import importlib
import sys,traceback




def load_devices(use_gui=False, parent=None):
    config = configparser.ConfigParser()
    config.read('devices.ini')
    sections = [config[name] for name in config.sections()]
    for section in sections:
        try:
            if section['enabled'].lower() == 'true':
                section.enabled = True
            else:
                section.enabled = False
        except:
            section.enabled = True
           
    if use_gui:
        from PyQt5 import QtWidgets
        dialog = QtWidgets.QDialog(parent)
        dialog.setWindowTitle("Select devices")
        layout = QtWidgets.QVBoxLayout()
        dialog.setLayout(layout)
        for section in sections:
            checkbox = QtWidgets.QCheckBox(section.name)
            checkbox.setChecked(section.enabled)
            if section.name in active_devices:
                checkbox.setEnabled(False)
            section.checkbox = checkbox
            layout.addWidget(checkbox)
        buttonbox = QtWidgets.QDialogButtonBox()
        buttonbox.addButton("Start", QtWidgets.QDialogButtonBox.AcceptRole)
        buttonbox.accepted.connect(dialog.accept)
        layout.addWidget(buttonbox)
        result = dialog.exec_()
        changed = False
        for section in sections:
            oldstate = section.enabled
            section.enabled = section.checkbox.isChecked()
            if oldstate ^ section.enabled:
                changed = True
            section['enabled'] = str(section.enabled).lower()
        if changed:
            with open('devices.ini', 'w') as configfile:
                config.write(configfile)
    
    new_devs_cnt = 0
    for section in config.sections():
        if section in active_devices:
            continue
        try:
            items = dict(config.items(section))
            if 'enabled' in items:
                if items['enabled'].lower() == 'true':
                    del(items['enabled'])
                else:
                    continue
            module_name, class_name = items['class'].rsplit(".", 1)
            kwargs = items.copy()
            kwargs.pop('class')
            DeviceClass = getattr(importlib.import_module('devices.'+module_name), class_name)
            instance = DeviceClass(**kwargs)
            active_devices[section] = instance
            new_devs_cnt += 1
        except Exception as e:
            print("Loading device %s failed." % section)
            traceback.print_exc(file=sys.stdout)
            
    return new_devs_cnt

    
def load_worker_class(class_str):
    module_name, class_name = class_str.rsplit(".", 1)
    DeviceClass = getattr(importlib.import_module('devices.'+module_name), class_name)
    return DeviceClass

def load_workers():
    config = configparser.ConfigParser()
    config.read('local_devices.ini')
    workers = []
    for section in config.sections():
        try:
            items = dict(config.items(section))
            if 'enabled' in items:
                if items['enabled'].lower() == 'true':
                    del(items['enabled'])
                else:
                    continue
            DeviceClass = load_worker_class(items['class'])
            name = items['name']
            kwargs = items.copy()
            kwargs.pop('class')
            kwargs.pop('name')
            workers.append( (name, DeviceClass, kwargs) )
        except Exception as e:
            print("Loading device %s failed." % section)
            traceback.print_exc(file=sys.stdout)
    return workers

def set_worker_autostart(worker_name, state):
    config = configparser.ConfigParser()
    config.read('local_devices.ini')
    changed = False
    for section in config:
        try:
            if config[section]['name'] == worker_name:
                new_value = str(state).lower()
                if 'pylums_autostart' not in config[section] or config[section]['pylums_autostart'] != new_value:
                    changed = True
                config[section]['pylums_autostart'] = new_value
        except KeyError:
            pass
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
    if changed:
        with open('local_devices.ini', 'w') as configfile:
            config.write(configfile)