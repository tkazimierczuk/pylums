# -*- coding: utf-8 -*-

import ctypes as ct
from mss import mss
import sys
from PyQt5 import QtWidgets, QtCore, QtGui
import time
import threading
from devices.device import Device
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods

import os
os.environ["PATH"] = os.path.dirname(__file__)+ os.pathsep + os.environ["PATH"]

default_req_port = 12050
default_pub_port = 12051


class SuperKWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        try:
            self.dll = ct.CDLL("PE_Filter_SDK.dll")
        except OSError:
            self.dll = ct.CDLL("PE_Filter_SDK_32.dll")
        except:
            print("Loading library failed")

    def status(self):
        d = super().status()
        return d

    def init_device(self):
        print("Connecting to LLTF Contrast device...")
        self.handle_lltf = ct.c_void_p()

        filename = ct.c_char_p(b"C:\\Program Files\\Photon etc\\PHySpecV2\\Devices\\M000010395.xml")
        systemName=(ct.c_char*256)()
        status = self.dll.PE_Create(filename,ct.addressof(self.handle_lltf))
        if status != 0:
            raise(Exception("Could not initialize LLTF library. Status code: " + str(status) +
                            ". Please refer to LLTF Contrast-SDK-User_Manual.pdf"))
        
        self.dll.PE_GetSystemName(self.handle_lltf, 0, systemName, ct.sizeof(systemName))
        status = self.dll.PE_Open(self.handle_lltf, systemName)
        if status != 0:
            raise(Exception("Could not connect to LLTF device. Status code: " + str(status) +
                            ". Check if device is not claimed by other software. " +
                            "Please refer to LLTF Contrast-SDK-User_Manual.pdf for erroc code"))
        print("Connected to %s.\nCurrent wavelength: %f nm." % (systemName.value.decode("utf-8"),
                                                           self.get_wavelength()))

    @remote
    def get_wavelength(self):
        wavelength = ct.c_double()
        self.dll.PE_GetWavelength(self.handle_lltf, ct.addressof(wavelength))
        return float(wavelength.value)

    @remote
    def set_wavelength(self, wavelength):
        wl = ct.c_double(wavelength)
        self.dll.PE_SetWavelength(self.handle_lltf, wl)

    def status(self):
        d={}
        d["wavelength"] = self.get_wavelength()
        return d

@include_remote_methods(SuperKWorker)
class SuperK(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def createDock(self, parentWidget, menu = None):
        dock = QtWidgets.QDockWidget("Supercontinuum LLTF", parentWidget)
        widget = QtWidgets.QWidget(dock)
        layout = QtWidgets.QVBoxLayout(parentWidget)

        self.LCD = QtWidgets.QLCDNumber()
        self.LCD.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        self.LCD.setDigitCount(7)
        self.LCD.setToolTip("Wavelength Setpoint")
        self.createListenerThread(self.updateSlot)

        def setTemperature(this):
            wl, ok = QtWidgets.QInputDialog.getDouble(None, "Set wavelength", "New wavelength:", 500, 400, 1200, 3)
            if ok:
                QtWidgets.QApplication.processEvents()
                self.set_wavelength(wl)

        self.LCD.mousePressEvent = setTemperature
        layout.addWidget(self.LCD)

        widget.setLayout(layout)
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)

    def updateSlot(self, status):
        self.LCD.display("%4.03f" % status["wavelength"])

   
        
    
   
    
    
   

    

