# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 16:32:31 2018

@author: AleksanderB
"""

import os
os.environ["PATH"] = os.path.dirname(__file__)+ os.pathsep + os.environ["PATH"]
import ctypes as ct
import math
import numpy as np
import re

from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
import devices.standa.pyximc as pyxilab

#from devices import Parameter
from PyQt5 import QtWidgets, QtCore, QtGui
import time
import datetime

import serial
import threading
import sys,traceback


default_req_port = 1166
default_pub_port = 1167
#address =  COM30
'''
user friendly name: Axis X, Serial numer:12511
user friendly name: Axis Y, Serial numer:12512
user friendly name: Axis LZ, Serial numer:12502
user friendly name: Axis RZ, Serial numer:12425
user friendly name: Axis R, Serial numer:12126 
'''
'''
class StandaParameter(Parameter):
    def __init__(self, standa, motor_number):
        self.standa = standa
        self.motor_serial = motor_number
        
    def name(self):
        return 'Standa motor, s/n: %d' % self.motor_serial
    
    def value(self):
        return self.standa.position(self.motor_serial)
    
    def move_to_target(self, target):
        self.standa.moveTo(self.motor_serial, target)
    
    def move_continuous(self, rate):
        self.standa.moveVelocity(self.motor_serial, rate)
    
    def is_moving(self):
        return not self.standa.isStopped()
'''    

def when(func):
    def wrapper_when(*args, **kwargs):
        print(str(datetime.datetime.now()),func.__name__ )
        return func(*args, **kwargs)
        #return func(*args, **kwargs)
    return wrapper_when


class StandaWorker(DeviceWorker):
    """ Class managing all Standa XILab motor controllers """
    @when
    def __init__(self, req_port = default_req_port, pub_port = default_pub_port, lep_address="COM30", emulate_lep=False, **kwargs):
        super().__init__(req_port = req_port, pub_port = pub_port, **kwargs)
        self._address = lep_address
        self.motors = {} #dictionary keys are user frienly names, values are indexes
        self.devenum = None
        self.dev_count = None
        self.dll=None
        self.maxspeeds = {} #keys are friendly names , these are nominal speeds, for joystic use 15% of it
        self.ishomed = {}
        self.units ={}
        self.stepsPerUnit = {} #{'Axis R':100,'Axis X':400, 'Axis Y':400,\
        #                    'Axis LZ':400,'Axis RZ':200} #steps per unit, keys are device_id
        self.emulate_lep = emulate_lep
        self.lep_obj = None
        
        
    def init_device(self):
        try:
            self.dll= ct.windll.LoadLibrary("libximc.dll")
            print("Standa: loading of libximc.dll")
            sbuf = pyxilab.create_string_buffer(64)
            self.dll.ximc_version(sbuf)
            print("Standa: libximc library version: " + sbuf.raw.decode().rstrip("\0"))
        except:
            print("Standa: cant't load libximc.dll")
            
        print("Standa: Looking for devices... it may take a few seconds...")
        # This is device search and enumeration with probing. It gives more information about devices.
        probe_flags = pyxilab.EnumerateFlags.ENUMERATE_ALL_COM + pyxilab.EnumerateFlags.ENUMERATE_PROBE #+ pyxilab.EnumerateFlags.ENUMERATE_NETWORK+
        enum_hints = b""
        # enum_hints = b"addr=" # Use this hint string for broadcast enumerate
        self.devenum = self.dll.enumerate_devices(probe_flags, enum_hints)
        print(f"Standa: Device enum handle: {self.devenum}")
        self.dev_count = self.dll.get_device_count(self.devenum)
        print(f"Standa: Device count: {self.dev_count}")
        
        if self.dev_count ==0:
            print('Standa: No devices detected!!!')
            return
        controller_name = pyxilab.controller_name_t()
        for dev_ind in range(0, self.dev_count):
            enum_name = self.dll.get_device_name(self.devenum, dev_ind)
            result = self.dll.get_enumerate_device_controller_name(self.devenum, dev_ind, ct.byref(controller_name))
            if result == pyxilab.Result.Ok:
                
                try:
                    open_name = self.dll.get_device_name(self.devenum, dev_ind)
                    if type(open_name) is str:
                        open_name = open_name.encode()
                    device_id = self.dll.open_device(open_name)
                    #print("Standa: Device id " + repr(device_id)+' ; '+repr(controller_name.ControllerName))
                    print("Standa: Device index {} ; id #{} ; Friendly name: ".format(dev_ind,device_id) + repr(controller_name.ControllerName) + " ; port name: "+ repr(enum_name) + ".")
                    self.motors[controller_name.ControllerName.decode('ascii')]=device_id
                    self.maxspeeds[self.dev_name(device_id)] = self.getMaxSpeed(device_id)
                    self.units[self.dev_name(device_id)] = self.get_unit(device_id)
                    self.stepsPerUnit[self.dev_name(device_id)] = self.get_stepsPerUnit(device_id)
                    self.stopMovement(device_id)
                    self.ishomed[self.dev_name(device_id)]=self.get_homed_flag(self.dev_name(device_id))
                except:
                    print("Standa: An error occured during device opening")
        
        if self.emulate_lep:
            try:
                print('Standa: LEP emulation enabled')
                self.lep_obj = FakeLEP()
                self.lep_obj.init_serial_communication(self._address)
                self.lep_thread = threading.Thread(target = self.lep_obj.run)
                self.lep_thread.start()
            except:
                print("Starting LEP thread failed")
                traceback.print_exc(file=sys.stdout)
    '''
    def __del__(self):
        #self.close_devices()
        pass
    '''
        
    def no_of_commands(binary_str,binary_expr=b'\xff'):
        n=0
        a = binary_str
        c = binary_expr
        if len(a)>=len(c):
            for i in range(0,len(a)):  
                k = i+(len(c))
                if k<len(a)+1:
                    if a[i:k] == c:
                        n+=1
        return n
        #b'\xffAPanel S -\r'
        #b'STATUS\r'
        #b'\xffASTATUS\r'
        
    
    @remote
    def getFakeLEP(self):
        a =self.lep_obj
        return a 
    
    @remote
    def close_devices(self):
        try: 
            if self.dev_count > 0:
                print("Standa: Closing devices...")
                for key,device_id in self.motors.items():
                    self.dll.close_device(ct.byref(ct.cast(device_id, ct.POINTER(ct.c_int))))
                    print("Standa: Device id: #" + repr(device_id) + " "+ str(key) +" Closed")
        except ValueError:
            print("Standa: An error occured during closing devices!")
            
            
    def status(self):
        d = super().status()
        d["standa_devices"] = self.motors
        number_of_retries = 5
        for key in self.motors:
            motor_id = self.get_id(key)
            pos = pyxilab.get_position_t()
            
            for retry in range(number_of_retries):
                result = self.dll.get_position(motor_id, ct.byref(pos))
                if result == pyxilab.Result.Ok:
                    break
                else:
                    time.sleep(0.001)
                
            d["standa_"+key] = {}
            if result == pyxilab.Result.Ok:
                #print("Standa: "+' id #' +str(motor_id)+' '+str(key)+ " Position: {0} steps, {1} microsteps".format(pos.Position, pos.uPosition))
                pos,upos=pos.Position, pos.uPosition
                d["standa_"+key]["steps"] = pos
                d["standa_"+key]["usteps"] = upos
                d["standa_"+key]["pos"] = self.s2u(key,pos,upos)
                d["standa_"+key]["unit"] = self.units[self.dev_name(motor_id)]
            d["standa_"+key]["ishomed"] = self.ishomed[self.dev_name(motor_id)]
        return d
    
    @remote
    def get_unit(self, device_id_or_frname ):
        '''returns string with units of particular motor'''
        dev_id = self.get_id(device_id_or_frname)
        s = pyxilab.stage_settings_t()
        result = self.dll.get_stage_settings(dev_id, ct.byref(s))
        if result == pyxilab.Result.Ok:
            return str(s.Units.decode('utf-8'))
        
    @remote
    def get_pitch(self, device_id_or_frname ):
        '''returns pitch of screw needed for conversion to units'''
        dev_id = self.get_id(device_id_or_frname)
        s = pyxilab.stage_settings_t()
        result = self.dll.get_stage_settings(dev_id, ct.byref(s))
        if result == pyxilab.Result.Ok:
            return float(s.LeadScrewPitch)
        
    @remote
    def get_stepsPerRevolution(self, device_id_or_frname ):
        '''returns number of steps per revolution \n
        remember about gearReductionIn factor'''
        dev_id = self.get_id(device_id_or_frname)
        s = pyxilab.engine_settings_t()
        result = self.dll.get_engine_settings(dev_id, ct.byref(s))
        if result == pyxilab.Result.Ok:
            return s.StepsPerRev
    @remote    
    def get_stepsPerUnit(self, device_id_or_frname):
        '''returns number of steps per one unit'''
        dev_id = self.get_id(device_id_or_frname)
        p = self.get_pitch(dev_id)
        spr = self.get_stepsPerRevolution(device_id_or_frname)
        g = self.get_gearReductionIn(device_id_or_frname)
        r = spr/p * g
        return r
    
    @remote    
    def get_gearReductionIn(self, device_id_or_frname):
        '''returns gear ration needed for calculation of position in units'''
        dev_id = self.get_id(device_id_or_frname)
        s = pyxilab.gear_settings_t()
        result = self.dll.get_gear_settings(dev_id, ct.byref(s))
        if result == pyxilab.Result.Ok:
            return s.ReductionIn
    
    @remote
    def s2u(self,device_id_or_frname,*InputStepFormat):
        '''steps to units input steps usteps or steps float \n
        returns float in units'''
        dev_id = self.get_id(device_id_or_frname)
        spu = self.stepsPerUnit[self.dev_name(dev_id)]
        f_steps = self.steps2float(InputStepFormat)
        in_units = f_steps/spu
        return in_units
    
    @remote
    def u2s(self,device_id_or_frname, InputInUnits):
        '''unit to steps input float , returns steps in float representation'''
        dev_id = self.get_id(device_id_or_frname)
        spu = self.stepsPerUnit[self.dev_name(dev_id)]
        f_steps = InputInUnits * spu
        return f_steps
        
    @remote
    def devices(self):
        return [key for key in self.motors]
    
    @remote        
    def dev_name(self,device_id):
        '''returns friendly name of motor'''
        device_name=list(self.motors.keys())[list(self.motors.values()).index(device_id)]
        return device_name
    
    @remote        
    def toStepsFormat(self, *stepsInput):
        ''' valid input: 2500.01953125 or (2500,5) or [2500,5] or 2500,5 \n
        returns two signed ints: steps, usteps'''
        def foo(t):
            if type(t) == tuple and len(t)==2:
                return t
            elif type(t) == tuple:
                t = foo(t[0])
                return t
            elif not isinstance(t,tuple):
                return t
            else:
                return t
        stepsInput = foo(stepsInput)
        if isinstance(stepsInput,float) or isinstance(stepsInput,int):
            m = math.modf(stepsInput)
            steps = int(m[1])
            usteps = (int(round(m[0] * 256,0))) #czy abs? 
            if usteps == 256:
                steps += 1
                usteps = 0
            elif usteps == -256:
                steps -= 1
                usteps = 0
        elif (isinstance(stepsInput,list) or isinstance(stepsInput,tuple)) and \
        len(stepsInput) == 2 and isinstance(stepsInput[0],int) and isinstance(stepsInput[1],int)\
        and (stepsInput[1] >= -255) and (stepsInput[1] <= 255):
            steps = stepsInput[0]
            usteps = stepsInput[1]
        else:

            raise ValueError('Invalid input for toStepsFormat function in Standa 1',stepsInput)
        return steps, usteps
    
    @remote
    def steps2float(self,*stepsInput):
        '''gets steps and usteps and returns steps as float representation'''
        s,us = self.toStepsFormat(stepsInput)
        r = float(s+us/256.)
        return r
            
    
    def test_get_position(self,device_id):
        motor_n = self.dev_name(device_id)
        x_pos = pyxilab.get_position_t()
        result = self.dll.get_position(device_id, ct.byref(x_pos))
        if result == pyxilab.Result.Ok:
            print("Standa: "+str(motor_n)+ " Position: {0} steps, {1} microsteps".format(x_pos.Position, x_pos.uPosition))
        return x_pos.Position, x_pos.uPosition
    
    
    
    @remote
    def position(self,device_id_or_frname):
        ''' returns position in steps and usteps \n
        use functions s2u and u2s to convert to units'''
        dev_id = self.get_id(device_id_or_frname)
        pos = pyxilab.get_position_t()
        result = self.dll.get_position(dev_id, ct.byref(pos))
        if result == pyxilab.Result.Ok:
            return float(pos.Position + pos.uPosition/255.)
    
    @remote
    def get_homed_flag(self,device_id_or_frname):
        dev_id = self.get_id(device_id_or_frname)
        home_set = pyxilab.status_t()
        result = self.dll.get_status(dev_id,ct.byref(home_set))
        if result == pyxilab.Result.Ok:
            print("GPIOFlags value",home_set.Flags)
        return bool(home_set.Flags & pyxilab.StateFlags.STATE_IS_HOMED)
    
    @remote
    def is_moving(self,device_id_or_frname):
        dev_id = self.get_id(device_id_or_frname)
        move_set = pyxilab.status_t()
        result = self.dll.get_status(dev_id,ct.byref(move_set))
        if result == pyxilab.Result.Ok:
            pass
            #print("MvCmdSts value",bool(move_set.MvCmdSts & pyxilab.MvcmdStatus.MVCMD_RUNNING))
        return bool(move_set.MvCmdSts & pyxilab.MvcmdStatus.MVCMD_RUNNING)
    
    @remote
    def is_stopped(self, device_id_or_frname):
        return not self.is_moving(device_id_or_frname)
    
    @remote
    def home(self,device_id_or_frname):
        def home_in_another_thread():
            print("Standa: Homing procedure will start in 5 seconds...")
            #time.sleep(5)
            dev_id=self.get_id(device_id_or_frname)
            self.dll.command_homezero(dev_id)
        self._home_thread = threading.Thread(target=home_in_another_thread)
        self._home_thread.daemon = True
        self._home_thread.start()
    '''
    @remote
    def homeOlympus(self):
        self.moveAbsolute( 'Axis RZ', self.u2s(23.0))
        self.moveAbsolute( 'Axis LZ', self.u2s(0.0))
        self.home('Axis X')
        self.home('Axis Y')
    '''
        
    @remote
    def home_all(self):
        print("Standa: Homing ALL procedure will start in 5 seconds...")
        time.sleep(5)
        for device_id_or_frname in self.motors:
            def home_in_another_thread():
                dev_id=self.get_id(device_id_or_frname)
                self.dll.command_homezero(dev_id)
            self._home_thread = threading.Thread(target=home_in_another_thread)
            self._home_thread.daemon = True
            self._home_thread.start()
        
    
    @remote
    def get_id(self,device_id_or_frname):
        ''' returns valid id 1,2,..., inputs: device_id or string with friendly name or byte with friendly name'''
        if isinstance(device_id_or_frname,int):
            return device_id_or_frname
        elif isinstance(device_id_or_frname,str):
            return self.motors[device_id_or_frname]
        else:
            try:
                a=self.motors[device_id_or_frname.decode('ascii')]
                return a
            except:
                print("Standa: Can't find id based on device_id_or_frname" )
                
    @remote 
    def getMaxSpeed(self, device_id_frname):
        '''returns max nondamaging speed in steps/s \n
        don't use it! smaller speeds e.g. 40% are recommended '''
        dev_id=self.get_id(device_id_frname)
        motor_n = self.dev_name(dev_id)
        con_set = pyxilab.engine_settings_t()
        result = self.dll.get_engine_settings(dev_id,ct.byref(con_set))
        if result == pyxilab.Result.Ok:
            #print("Standa: "+str(motor_n)+ " MaxSpeed: ", con_set.NomSpeed)
            pass
        return con_set.NomSpeed
    
    @remote
    def calcStepVelocity(self,device_id_frname,velocity,Fast = False):
        '''velocity should be from -1 to 1
        returns velocity in step and ustep velocity, 15% of max speed '''
        dev_id=self.get_id(device_id_frname)
        max_vel = 0.15 * self.maxspeeds[self.dev_name(dev_id)] # 15% of max speed
        if Fast:
            max_vel = 0.5 * self.maxspeeds[self.dev_name(dev_id)] # 50% of max speed
        velocity = max_vel * velocity
        sign = lambda x: (1, -1)[x < 0] 
        velocity_direction = sign(velocity)
        if abs(velocity)>max_vel: velocity = max_vel
        velocity_full = abs(int(round(math.modf(velocity)[1],0)))
        velocity_frac = abs(int(round(math.modf(velocity)[0]*255,0)))
        return velocity_full,velocity_frac,velocity_direction
        
    @remote
    def calcStepVelocityUnits(self,device_id_frname,velocity,Fast = False):
        '''velocity should be from -1 to 1
        returns velocity in step and ustep velocity, 15% of max speed '''
        dev_id=self.get_id(device_id_frname)
        max_vel = 0.15 * self.maxspeeds[self.dev_name(dev_id)] # 15% of max speed
        if Fast:
            max_vel = 0.5 * self.maxspeeds[self.dev_name(dev_id)] # 50% of max speed
        sign = lambda x: (1, -1)[x < 0] 
        velocity_direction = sign(velocity)
        if abs(velocity)>max_vel: velocity = max_vel
        velocity_full = abs(int(round(math.modf(velocity)[1],0)))
        velocity_frac = abs(int(round(math.modf(velocity)[0]*255,0)))
        return velocity_full,velocity_frac,velocity_direction
    
    @remote    
    def spinFast(self,device_id_frname, velocity):
        '''velocity should be from -1 to 1 \n
        1 is actually set velocity'''
        dev_id=self.get_id(device_id_frname)
        if velocity==0:
            self.dll.command_sstp(dev_id)
            return       
        mov_set = pyxilab.move_settings_t()
        result = self.dll.get_move_settings(dev_id, ct.byref(mov_set))
        vel_st, vel_frac, vel_dir = self.calcStepVelocity(dev_id,velocity,Fast = True)
        if result == pyxilab.Result.Ok:
            
            mov_set.uSpeed = vel_frac
            mov_set.Speed = vel_st
            #print('current speed is', mov_set.Speed)
            #print('current speed is', mov_set.uSpeed)
            self.dll.set_move_settings(dev_id, ct.byref(mov_set))
            
        if vel_dir >0:
            self.dll.command_right(dev_id)
            #print('moving right')
        else:
            self.dll.command_left(dev_id)
            #print('moving left')
    
    @remote
    def moveVelocity(self,device_id_frname, velocity):
        '''velocity should be from -1 to 1 \n
        1 is actually set velocity'''
        dev_id=self.get_id(device_id_frname)
        if velocity==0:
            self.dll.command_sstp(dev_id)
            return       
        mov_set = pyxilab.move_settings_t()
        result = self.dll.get_move_settings(dev_id, ct.byref(mov_set))
        vel_st, vel_frac, vel_dir = self.calcStepVelocity(dev_id,velocity)
        if result == pyxilab.Result.Ok:
            
            mov_set.uSpeed = vel_frac
            mov_set.Speed = vel_st
            #print('current speed is', mov_set.Speed)
            #print('current speed is', mov_set.uSpeed)
            self.dll.set_move_settings(dev_id, ct.byref(mov_set))
            
        if vel_dir >0:
            self.dll.command_right(dev_id)
            #print('moving right')
        else:
            self.dll.command_left(dev_id)
            #print('moving left')
            
    
    @remote
    def moveVelocityApproach(self,device_id_frname,speed_um_s):
        print(speed_um_s,speed_um_s/1000.,self.u2s(device_id_frname,speed_um_s/1000.))
        velocity = self.u2s(device_id_frname,speed_um_s/1000.) #steps/s
        dev_id=self.get_id(device_id_frname)
        if velocity==0:
            self.dll.command_sstp(dev_id)
            return
        mov_set = pyxilab.move_settings_t()
        result = self.dll.get_move_settings(dev_id, ct.byref(mov_set))
        vel_st, vel_frac, vel_dir = self.calcStepVelocityUnits(dev_id,velocity)
        print(vel_st,vel_frac,vel_dir)
        if result == pyxilab.Result.Ok:
            
            mov_set.uSpeed = vel_frac
            mov_set.Speed = vel_st
            print('current speed is', mov_set.Speed)
            print('current speed is', mov_set.uSpeed)
            self.dll.set_move_settings(dev_id, ct.byref(mov_set))
            
        if vel_dir >0:
            self.dll.command_right(dev_id)
            #print('moving right')
        else:
            self.dll.command_left(dev_id)
            #print('moving left')
        
    @remote
    def stopAll(self):
        for key in self.motors:
            self.stopMovement(key)
    
    @remote
    def stopMovement(self,device_id_frname ):
        '''soft stop'''
        dev_id = self.get_id(device_id_frname)
        self.dll.command_sstp(dev_id)    
        
    @remote
    def move_absolute(self, device_id_frname, target):
        ''' compatible with Thorlabs APT move_absolute '''
        self.moveAbsolute(device_id_frname , self.u2s(target))
        
        
    @remote
    def moveAbsolute(self, device_id_frname, *stepsInput):
        '''moves to target in steps and usteps
        convert to units with s2u and u2s functions'''
        self.setFast(device_id_frname)
        dev_id = self.get_id(device_id_frname)
        steps,usteps = self.toStepsFormat(stepsInput[0])
        self.dll.command_move(dev_id, steps, usteps)
        #result = pyxilab.Result.Error
        #n=0
        #while result == pyxilab.Result.Error:
        #    result = self.dll.command_wait_for_stop(dev_id, 10)
        #    n+=1
        #if result == pyxilab.Result.Ok:
        #    pass
        #else:
        #    print("Standa: moveAbsolute failed! axis:", self.dev_name(dev_id) )
            
    @remote
    def setFast(self,device_id_frname):
        ''' sets speed to 40% of damaging speed'''
        dev_id=self.get_id(device_id_frname)       
        mov_set = pyxilab.move_settings_t()
        result = self.dll.get_move_settings(dev_id, ct.byref(mov_set))
        vmax = self.getMaxSpeed(dev_id)
        vel_st = abs(int(0.4*vmax)) #approx 2000
        vel_frac = 255
        if result == pyxilab.Result.Ok:
            mov_set.uSpeed = vel_frac
            mov_set.Speed = vel_st
            self.dll.set_move_settings(dev_id, ct.byref(mov_set))
            #print("Standa: fast mode for device id:",dev_id)
            
    @remote
    def setSlow(self,device_id_frname):
        ''' sets speed to 5% of damaging speed'''
        dev_id=self.get_id(device_id_frname)       
        mov_set = pyxilab.move_settings_t()
        result = self.dll.get_move_settings(dev_id, ct.byref(mov_set))
        vmax = self.getMaxSpeed(dev_id)
        vel_st = abs(int(0.05*vmax)) #approx 2000
        vel_frac = 255
        if result == pyxilab.Result.Ok:
            mov_set.uSpeed = vel_frac
            mov_set.Speed = vel_st
            self.dll.set_move_settings(dev_id, ct.byref(mov_set))
            #print("Standa: slow mode for device id:",dev_id)
            
    @remote
    def moveSteps(self, device_id_frname,*stepsInput):
        '''function used by joystick control \n
        moves by desired nubmer of steps and usteps
        convert to units with s2u and u2s functions'''
        self.setFast(device_id_frname)
        if  isinstance(*stepsInput,float):
         stepsInput=stepsInput[0]
         stepsInput*=0.1
         self.moveRelative(device_id_frname,stepsInput)
        else:
         self.moveRelative(device_id_frname,*stepsInput)
        

    @remote
    def moveRelative(self, device_id_frname, *stepsInput):
        ''' moves by desired nubmer of steps and usteps
        convert to units with s2u and u2s functions'''
        self.setFast(device_id_frname)
        dev_id = self.get_id(device_id_frname)
        steps,usteps = self.toStepsFormat(stepsInput)
        self.dll.command_movr(dev_id, steps, usteps)
        #result = pyxilab.Result.Error
        #n=0
        #while result == pyxilab.Result.Error:
        #    result = self.dll.command_wait_for_stop(dev_id, 10)
        #    n+=1
        #if result == pyxilab.Result.Ok:
        #    pass
        #else:
        #    print("Standa: moveRelative failed! axis:", self.dev_name(dev_id) )
    @remote
    def get_move_settings(self,device_id_frname):
        dev_id=self.get_id(device_id_frname)
        move_settings = pyxilab.move_settings_t()
        result = self.dll.get_move_settings(dev_id, ct.byref(move_settings))
        if result == pyxilab.Result.Ok:
            print("Speed", move_settings.Speed)
            print("uSpeed", move_settings.uSpeed)
            print("Accel", move_settings.Accel)
            print("Decel", move_settings.Decel)
            print("AntiplaySpeed", move_settings.AntiplaySpeed)
            print("uAntiplaySpeed", move_settings.uAntiplaySpeed)
            return move_settings
     
    def test_info(self, device_id):
        print("\nGet device info")
        x_device_information = pyxilab.device_information_t()
        result = self.dll.get_device_information(device_id, ct.byref(x_device_information))
        print("Result: " + repr(result))
        if result == pyxilab.Result.Ok:
            print("Device information:")
            print(" Manufacturer: " +
                    repr(ct.string_at(x_device_information.Manufacturer).decode()))
            print(" ManufacturerId: " +
                    repr(ct.string_at(x_device_information.ManufacturerId).decode()))
            print(" ProductDescription: " +
                    repr(ct.string_at(x_device_information.ProductDescription).decode()))
            print(" Major: " + repr(x_device_information.Major))
            print(" Minor: " + repr(x_device_information.Minor))
            print(" Release: " + repr(x_device_information.Release))
            

@include_remote_methods(StandaWorker)
class FakeLEP(DeviceOverZeroMQ):
    def __init__(self, req_port = default_req_port, pub_port = default_pub_port, **kwargs):
        super().__init__(req_port = req_port, pub_port = pub_port, **kwargs)
        
        self.LEPlastCommand = ''
        self.LEPcommands ={'ASTATUS':self.LEP_handle_status,\
                           'STATUS':self.LEP_handle_status, \
                           'APanel': self.LEP_handle_OK,\
                           'Panel': self.LEP_handle_OK,\
                           'A': self.LEP_handle_OK,\
                           'SPEED':self.LEP_handle_OK,\
                           'ACCEL':self.LEP_handle_OK,\
                           'WHERE':self.LEP_handle_where,\
                           'HALT':self.LEP_handle_OK,\
                           'JOYSTICK':self.LEP_handle_OK,\
                           'SPIN':self.LEP_handle_spin,\
                           'HERE':self.LEP_handle_here,\
                           'MOVE':self.LEP_handle_move
                           }
        self.sercom = None
        self.ScoordinX = 0 #standa coordins in mm zero correction
        self.ScoordinY = 0 #standa coordins in mm zero correction
        self.calibrating_in_progress = 0
        print("Standa: FakeLEP object created")
    
    def __del__(self):
        try:
            self.sercom.close()
            print("Standa: Serial communication closed")
        except ValueError:
            print("Standa: An error occured during closing serial communication!")
    
    def init_serial_communication(self, address):
        """
        Opens the communication 
        """
        print("=========")
        print(str(datetime.datetime.now()))
        print("Standa: serial communication initialization...")
        if self._address[0:3] == 'COM':
            print("Standa: Communication at COM"+str(self._address[3:]))
            try:
                self.sercom = serial.Serial(self._address, 9600, serial.EIGHTBITS, serial.PARITY_NONE,\
                                            serial.STOPBITS_TWO , 1 '''second timeout''')
            except:
                print("Standa: can't establish serial connection!")
                
        print("Standa: serial communication with LEP protocole enabled!")

    def LEPrecive(self):
        termination_chars='\r'
        answer_beg = b'\xff'
        answer_end = b'\r'
        try:
            mess = self.sercom.read_until(answer_end)
        except:
            mess = ''
        lstr = str(mess)
        lstr = lstr[2:-1]
        lstr = lstr.replace('\\xff','.')
        lstr = lstr.replace('\\r','.')
        lstr = lstr.rsplit('.')
        str_list = list(filter(None,lstr))
        if len(str_list)>0:
            spl = str_list[-1].replace(' ','.')
            spl = spl.replace('=','.')
            spl = spl.split('.')
            command_class = spl[0]
            if command_class in self.LEPcommands:
                    self.LEPcommands[command_class](spl)
            else:
                print('Unknown command:',command_class)
                self.LEPcommands['HALT']()
            return str_list
        else:
            return #self.LEPrecive()

            
    def LEP_handle_OK(self,*args):
        self.sercom.write(b':A\n')
        
        
        

        
    def LEP_handle_move(self,*args):
        a = args[0]
        if len(a) >= 3:
            axis= a[1]
            target = int(a[2])
            if axis=='X':
                frname = 'Axis X'
                standa_target = self.LEP2standaXunits(target)
            elif axis == 'Y':
                frname = 'Axis Y'
                standa_target = self.LEP2standaYunits(target)
            else:
                print("Zle miejsce XXX")
            self.moveAbsolute(frname,self.u2s(frname,standa_target))      
        if len(a) >= 5:
            axis= a[3]
            target = int(a[4])
            if axis=='X':
                frname = 'Axis X'
                standa_target = self.LEP2standaXunits(target)
            elif axis == 'Y':
                frname = 'Axis Y'
                standa_target = self.LEP2standaYunits(target)
            else:
                print("Zle miejsce ZZZ")
            self.moveAbsolute(frname,self.u2s(frname,standa_target))      
        
        self.sercom.write(b':A\n')
        
        
        
    def LEP_handle_status(self,*args):
        Xmoving = self.is_moving('Axis X')
        Ymoving = self.is_moving('Axis Y')
        moving = Xmoving or Ymoving
        if moving:
            self.sercom.write(b'B')
            #busy
        else:
            if self.calibrating_in_progress:
                self.calibrating_in_progress -= 1
                time.sleep(1)
                self.sercom.write(b'B')
            else:
                self.sercom.write(b'N')
            
        
    def LEP_ignore(self,*args):
        self.sercom.write(b':A\n')
        print('ignored A sent ',args)
        
    def LEP_handle_spin(self,*args):
        self.calibrating_in_progress = 2
        a = args[0]
        if a[2]=='-400000':
            self.spinFast('Axis X',-1)
            self.spinFast('Axis Y',-1)
        if a[2]=='400000':
            self.spinFast('Axis X',1)
            self.spinFast('Axis Y',1)
        self.sercom.write(b':A\n')
        
    def LEP2standaXunits(self,x):
        '''standa mm from LEPsteps in Olympus '''
        return x/(1000*40)+self.ScoordinX
    
    def LEP2standaYunits(self,y):
        '''standa mm from LEPsteps in Olympus '''
        return y/(1000*40)+self.ScoordinY
    
    def LEPstandaX2LEP(self,x):
        '''standa mm to LEPsteps in Olympus in um'''
        return round((x-self.ScoordinX)*1000*40)
    
    def LEPstandaY2LEP(self,y):
        '''standa mm to LEPsteps in Olympus in um'''
        return round((y-self.ScoordinY)*1000*40)
    
            
    def LEP_handle_here(self,*args):
        d = self.status()
        self.ScoordinX = d['standa_Axis X']['pos']
        if abs(self.ScoordinX) < 0.5: #mm
            self.ScoordinX  = 0
        self.ScoordinY = d['standa_Axis Y']['pos']
        if abs(self.ScoordinY) < 0.5: #mm
            self.ScoordinY  = 0
        self.sercom.write(b':A\n')

    
    def LEP_handle_where(self,*args):
        try:
            d=self.status()
            x = d['standa_Axis X']['pos']
            y = d['standa_Axis Y']['pos']
            lx = self.LEPstandaX2LEP(x)
            ly = self.LEPstandaY2LEP(y)
            if args[0][1] == 'X':
                ts = lx

            elif args[0][1] == 'Y':
                ts = ly

            else:
                print('Unknown axis',args[0][1])
            self.sercom.write(':A {}\n'.format(int(ts)).encode())
            return
        except KeyError:
            pass
        self.sercom.write(b':N -1\n') # if we fail reply something anyway
        
        
    def run(self):
        while True:
            self.LEPrecive()
    



@include_remote_methods(StandaWorker)
class Standa(DeviceOverZeroMQ):
    def __init__(self, req_port = default_req_port, pub_port = default_pub_port, **kwargs):
        super().__init__(req_port = req_port, pub_port = pub_port, **kwargs)
        # custom initialization here
        self.widgets = {}

    def createDock(self, parentWidget, menu = None):
        """ Function for integration in GUI app.  """
        dock = QtWidgets.QDockWidget("Standa", parentWidget)
        dock.setMinimumWidth(700)
        dock.setMinimumHeight(400)
        widget = QtWidgets.QWidget(parentWidget)
        self.layout = QtWidgets.QVBoxLayout(parentWidget)
        widget.setLayout(self.layout)
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)


    def appendRow(self, dev_id):
        hlayout = QtWidgets.QHBoxLayout()
        name = self.dev_name(dev_id)
        label = QtWidgets.QLabel("%s" % name)
        
        hlayout.addWidget(label)
        display = QtWidgets.QLCDNumber()
        display.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        #display.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
        #QtWidgets.QSizePolicy.Expanding)
        policy = display.sizePolicy()
        policy.setHorizontalStretch(10)
        display.setSizePolicy(policy)
        display.setDigitCount(12)
        display.decimals = 9
        display.display("0.0")

        hlayout.addWidget(display)
        #hlayout.addStretch(3)
        self.layout.addLayout(hlayout)
        self.widgets[dev_id] = (display,)

        def on_click(event):
            if event.button() == 1:
                current = self.s2u(dev_id,self.position(dev_id))
                d, okPressed = QtWidgets.QInputDialog.getDouble(display, "Go to", "Target:", current,decimals = 9)
                if okPressed:
                    print(d)
                    self.moveAbsolute(dev_id, self.u2s(dev_id,d))
        display.mousePressEvent  = on_click            
        
        
    def updateSlot(self, status):
        for key in status["standa_devices"]:
            dev_id = self.get_id(key)
            if dev_id not in self.widgets:
                self.appendRow(dev_id)
            motor_status = status["standa_%s" % key]
            try:
                s = motor_status["pos"]
                self.widgets[dev_id][0].display("%.5f" % s)
            except:
                print(str(motor_status))
                print("Can't update status! ")
            
            

            


#s=StandaWorker()
#d=s.status()
#while True:
#    print(s.LEPrecive())
#s.close_devices()
#Standa: b'Axis R' Position: -36250 steps, 0 microsteps
#u = s.get_unit('Axis X')
#p = s.get_pitch('Axis X')
#spr = s.get_stepsPerRevolution('Axis X')
#print(" steps per unit",s.stepsPerUnit)