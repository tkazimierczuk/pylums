# -*- coding: utf-8 -*-
# Support for Oxford Instruments level meter ILM200 
#
# Adapted from MIT licensed code by
# Pieter Eendebak <pieter.eendebak@tno.nl>, 2017
# Takafumi Fujita <t.fujita@tudelft.nl>, 2016
# Guenevere Prawiroatmodjo <guen@vvtp.tudelft.nl>, 2009
# Pieter de Groot <pieterdegroot@gmail.com>, 2009


from time import time,sleep
import zmq
import logging
import threading
from devices.zeromq_device import array_object_hook
import paho.mqtt.client as mqtt
import json

class MQTTSender:
    def __init__(self, system_name,magnet_pub_addr=None,
                 levelmeter_pub_addr=None,temp_pub_addr=None, **kwargs):
        self._name = system_name
        self.addresses = {}
        self.addresses["magnet"] = magnet_pub_addr
        self.addresses["ilm"] = levelmeter_pub_addr
        self.addresses["itc"] = temp_pub_addr

    def init_device(self):
        self.last_reported = 0
        self.statuses = {}
        self.zmq_connect()
        if not self.sockets:
            raise Exception("No connections established")
            
    def start_server(self, blocking=False):
        self.mqtt_connect()
        self.mqtt_client.loop_start()
        #self.mqtt_thread = threading.Thread(target=self._loop_mqtt)
        #self.mqtt_thread.daemon = True
        #self.mqtt_thread.start()
        if blocking:
            self._loop_zmq()
        else:
            self.zmq_thread = threading.Thread(target=self._loop_zmq)
            self.zmq_thread.daemon = True
            self.zmq_thread.start()
        
                
    def zmq_connect(self):
        context = zmq.Context()
        self.sockets = {}
        for name,addr in self.addresses.items():
            try:
                full_addr = "tcp://" + addr
                socket = context.socket(zmq.SUB)
                socket.setsockopt(zmq.SUBSCRIBE, b'status')
                socket.connect(full_addr)
                print("Connecting to magnet at %s" % full_addr)
                self.sockets[name] = socket
            except:
                pass 
            

    def mqtt_connect(self):
        print("Connecting to mqtt server...")
        self.mqtt_client = mqtt.Client("pymonitor" + self._name)
        self.mqtt_client.username_pw_set('dms', 'cdmnte')
        self.mqtt_client.will_set('/sensors/' + self._name, 'offline', 0, True)
        self.mqtt_client.connect("dms0.fuw.edu.pl", 1883, 90)
        self.mqtt_client.publish('/sensors/' + self._name, 'online', 0, True)
        print("Connected")

    def report_now(self):
        if time() - self.last_reported > 30:
            self.last_reported = time()
            try:
                t0,status = self.statuses['ilm']
                if time()-t0 > 100:
                    raise TimeoutError()
                helium = status["helium"]
                nitrogen = status["nitrogen"]
                self.mqtt_client.publish('/' + self._name + '/helium', str(helium), 1, True)
                self.mqtt_client.publish('/' + self._name + '/nitrogen', str(nitrogen), 1, True)
                self.mqtt_client.publish('/sensors/' + self._name, 'online', 0, True)
            except (TimeoutError,KeyError):
                pass
            except Exception as e:
                print(e)
            
            try:
                t0,status = self.statuses['itc']
                if time()-t0 > 100:
                    raise TimeoutError()
                temperature = status["temperature"]
                
                self.mqtt_client.publish('/' + self._name + '/temperature', str(temperature), 1, True)
            except (TimeoutError,KeyError):
                pass
            except Exception as e:
                print(e)
            
    def _loop_zmq(self):
        # Initialize poll set
        poller = zmq.Poller()
        for name,socket in self.sockets.items():
            poller.register(socket, zmq.POLLIN)
        
        while True:
            socks = dict(poller.poll())
            
            for name,sock in self.sockets.items():
                if sock in socks:
                    full_msg = sock.recv_multipart()
                    msg = full_msg[1].decode('ascii')
                    try:
                        self.statuses[name] = (time(), json.loads(msg, object_hook=array_object_hook))
                    except:
                        print("Status: ", full_msg)
                        raise
            self.report_now()