# -*- coding: utf-8 -*-
# Support for Oxford Instruments level meter ILM200 
#
# Adapted from MIT licensed code by
# Pieter Eendebak <pieter.eendebak@tno.nl>, 2017
# Takafumi Fujita <t.fujita@tudelft.nl>, 2016
# Guenevere Prawiroatmodjo <guen@vvtp.tudelft.nl>, 2009
# Pieter de Groot <pieterdegroot@gmail.com>, 2009


from time import time,sleep
from .ips import OxfordWorker
import logging
import threading
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets, QtCore, QtGui
import paho.mqtt.client as mqtt

default_req_port = 36715
default_pub_port = 36716

class ILMWorker(OxfordWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port,
                 **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def init_device(self):
        super().init_device()

    def status(self):
        d = super().status()
        if not self.is_offline:
            d["helium"] = self.get_helium()
            d["nitrogen"] = self.get_nitrogen()
        return d

    @remote
    def get_helium(self):
        return self.get_r_parameter(1) / 10
    
    @remote
    def get_nitrogen(self):
        return self.get_r_parameter(2) / 10
    
    @remote
    def get_rate(self):
        rate = {
            1: "fast",
            0: "slow"
        }
        result = self.execute('X', reply_pattern= r"^X[01239]{3}S[0-9A-F]{6}R[0-9A-F]{2}$")
        return rate.get(int(format(int(result[5:7]), '08b')[6]), "Unknown")

    def close_device(self):
        self.visa_handle.close()

    def get_version(self):
        return self.execute('V')

    @remote
    def set_local(self):
        self.execute('C2', reply_pattern=r"^C$") # local, unlocked

    @remote
    def set_remote(self):
        self.execute('C3', reply_pattern=r"^C$") # remote, unlocked

    @remote
    def set_slow(self):
        self.set_remote()
        self.execute('S1', reply_pattern=r"^S$")
        self.set_local()

    @remote
    def set_fast(self):
        self.set_remote()
        self.execute('T1', reply_pattern=r"^T$")
        self.set_local()
        
@include_remote_methods(ILMWorker)
class ILM(DeviceOverZeroMQ):
    pass