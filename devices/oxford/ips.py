from datetime import datetime
from time import sleep
from time import perf_counter as clock
import pyvisa as visa
import threading
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets, QtCore, QtGui
import warnings
import re
import numpy as np

devices_locks = {}

numeric_pattern = r'[-+]?(?:(?:\d*\.\d+)|(?:\d+\.?))(?:[Ee][+-]?\d+)?'
re_numeric = re.compile(numeric_pattern, re.VERBOSE)

class OxfordWorker(DeviceWorker):
    """ Class implementing communication with Oxford Instrument Devices: ILM, IPS, ITC """
    def __init__(self, req_port, pub_port, address="COM1", isobus='', **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

        self._address = address.upper()

        try:
            if not isobus.isdigit():
                raise
            self.isobus_header = '@%s' % str(isobus)
        except:
            self.isobus_header = ''

        try:
            self.visa_lock = devices_locks[address.upper()]
        except KeyError:
            self.visa_lock = threading.Lock()
            devices_locks[address.upper()] = self.visa_lock
        self.is_offline = False

    IGPIB = 0
    IRS232 = 1
    IETHERNET = 2
    
    def _connect(self):
        print("Connecting...")
        rm = visa.ResourceManager()
                
        if self._address[0:3] == 'COM':
            self._interface = OxfordWorker.IRS232
            self.visa_handle = rm.open_resource("ASRL%s::INSTR" % self._address[3:])
            self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_STOP_BITS,
                                                visa.constants.VI_ASRL_STOP_TWO)
        elif self._address[0:4] == 'GPIB':
            self._interface = OxfordWorker.IGPIB
            if "::" not in self._address:
                self.visa_handle = rm.open_resource("GPIB::%s" % self._address[4:])
            else:
                self.visa_handle = rm.open_resource(self._address)
            self.visa_handle.clear()
            sleep(0.2)
        else:
            self._interface = OxfordWorker.IETHERNET
            self.visa_handle = rm.open_resource("TCPIP::%s::7020::SOCKET" % self._address)
            self.visa_handle.write_termination = '\r'
            
        self.visa_handle.read_termination = '\r'
        self.visa_handle.timeout = 1000
        
        
    def init_device(self):
        self._connect()
        self.execute("X", reply_pattern=r"^X") #check the communication

    @remote
    def execute(self, message, reply_pattern=None, allowed_retries=5):
        try:
            with self.visa_lock:
                #res = self.visa_handle.query(self.isobus_header + message)
                if self._interface == OxfordWorker.IGPIB and self.visa_handle.read_stb() & 0x10: # checking MAV bit
                    self.visa_handle.clear()
                    print("Clearing line")
                self.visa_handle.write(self.isobus_header + message)
                if self._interface == OxfordWorker.IGPIB :
                    while not (self.visa_handle.read_stb() & 0x10): # checking MAV bit
                        sleep(0.01)
                res = self.visa_handle.read_raw().decode('ascii')
                self.is_offline = False
        except visa.VisaIOError:
            if allowed_retries > 0:
                with self.visa_lock:
                    self._connect()
                return self.execute(message, reply_pattern, allowed_retries-1)
            else:
                self.is_offline = True
                res = ''
                warnings.warn("Communication error occured on message: %s" % message, ResourceWarning)
                    
        res = res.strip()
        if reply_pattern:
            if not re.match(pattern=reply_pattern, string=res):
                if allowed_retries > 0:
                    print("Warning! Wrong reply (Q:%s, R:%s)" % (message, res))
                    return self.execute(message, reply_pattern, allowed_retries-1)
                else:
                    raise Exception("Wrong reply (Q:%s, R:%s)" % (message, res))
        else:
            print("Unverified reply (Q:%s, R:%s)" % (message, res))
                    
        return res
    
    
    _r_pattern = re.compile(r"^R[-+]?" + numeric_pattern + r"|^\?")
    
    
    @remote
    def get_r_parameter(self, n):
        # the device will respond e.g. R12.5 or ?R2; the latter means that the parameter is undefined
        res = self.execute('R%d' % n, reply_pattern=OxfordWorker._r_pattern)
        if res.startswith('?'):
            return np.nan
        else:
            return float(re_numeric.match(res[1:]).group())    

default_req_port = 7031
default_pub_port = 7032

class IPSWorker(OxfordWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, 
                 **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

        self._target_field = 0
        self._persistent_after = False
        self._stage = None
        self._goal_time = clock()
        self.debug = False
        self.procedure_lock = threading.Lock()

    def status(self):
        d = super().status()
        if not self.is_offline:
            d["busy"] = self.busy()
            d["field"] = self.field()
            d["setpoint"] = self.setpoint()
            d["power_supply_field"] = self.power_supply_field()
        return d

    def init_device(self):
        super().init_device()
        self.set_remote()
        self.execute("M9", reply_pattern=r"^M$") # set display to tesla
        self.continue_running = True
        self.ips_thread = threading.Thread(target=self._procedure_loop)
        self.ips_thread.start()
        print("Success")

    def close_device(self):
        self.continue_running = False
        self.ips_thread.join()

    def _procedure_loop(self):
        while self.continue_running:
            if not self.is_offline:
                self._procedure_iteration()
            sleep(0.1)

    def _procedure_iteration(self):
        with self.procedure_lock:
            if self._stage == 0: # wait for heater state to settle (if procedure was aborted)
                if clock() >= self._goal_time:
                    self._stage = 1
            if self._stage == 1: # set power supply output to field inside magnet, if necessary
                self.set_remote()
                magnet_field = self.field()
                if magnet_field == self.power_supply_field():
                    self._stage = 3
                else:
                    self.set_setpoint(magnet_field)
                    self.go_to_setpoint(set_remote=False)
                    self._stage = 2
            if self._stage == 2: # wait for power supply output to reach field inside magnet
                if self.power_supply_field() == self.setpoint():
                    self._stage = 3
            if self._stage == 3: # turn on heater, if necessary
                if self.heater():
                    self._stage = 5
                else:
                    self._goal_time = clock() + 5
                    self._stage = 3.5
            if self._stage == 3.5: # wait for current to settle before turning on the heater
                if clock() >= self._goal_time:               
                    self.set_heater(True, set_remote=False)
                    self._goal_time = clock() + 20
                    self._stage = 4
            if self._stage == 4: # wait 20s for heater to turn on
                if clock() >= self._goal_time:
                    self._stage = 5
            if self._stage == 5: # go to target field
                self.set_setpoint(self._target_field, set_remote=False)
                self.go_to_setpoint()
                self._stage = 6
            if self._stage == 6: # waiting for field to reach target, heater off if set persistent
                if self.power_supply_field() == self.setpoint():
                    if self._persistent_after:
                        self.set_heater(False, set_remote=False)
                        self._goal_time = clock() + 20
                        self._stage = 7
                    else:
                        self._stage = None
            if self._stage == 7: # wait 20s for heater to turn off, then go to zero
                if clock() >= self._goal_time:
                    self.go_to_zero(set_remote=False)
                    self._stage = None


    @remote
    def busy(self):
        return self._stage != None and self._stage < 7
        
    @remote
    def is_stopped(self):
        return self._stage == None or self._stage >= 7

    @remote
    def stop(self, set_remote=True):
        with self.procedure_lock:
            self._stage = None
            self.set_hold(set_remote=set_remote)

    @remote
    def set_remote(self):
        self.execute("C3", reply_pattern=r"^C$")

    @remote
    def field(self):
        if self.heater():
            return self.power_supply_field()
        else:
            return self.persistent_field()

    @remote
    def set_field(self, target_field=0, persistent_after=False):
        self.set_remote()
        with self.procedure_lock:
            self._persistent_after = persistent_after
            self._target_field = target_field
            self._stage = 0

    @remote
    def setpoint(self):
        return self.get_r_parameter(8)

    def set_setpoint(self, field, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute("J{}".format(round(field, 5)), reply_pattern=r"^J$")

    @remote
    def power_supply_field(self):
        return self.get_r_parameter(7)

    @remote
    def persistent_field(self):
        return self.get_r_parameter(18)

    @remote
    def field_sweep_rate(self):
        return self.get_r_parameter(9)

    @remote
    def set_field_sweep_rate(self, rate, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute("T{}".format(round(rate, 5)), reply_pattern=r"^T")

    def go_to_zero(self, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute("A2", reply_pattern=r"^A$")

    def go_to_setpoint(self, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute("A1", reply_pattern=r"^A$")

    def set_hold(self, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute("A0", reply_pattern=r"^A$")

    @remote
    def heater(self):
        status = self.execute("X", reply_pattern=r"^X")
        if status != None:
            if len(status) >= 15:
                return status[8] == "1"
                
    def set_heater(self, state=1, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute("H{}".format(int(state)), reply_pattern=r"^H$")
        
    @remote
    def set_slow_mode(self, run_slow):
        """ run_slow: bool """
        # TODO
        pass

@include_remote_methods(IPSWorker)
class IPS(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.request_timeout = 10000 # timeout extended to 10s

    def __del__(self):
        pass

    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("Magnet IPS120", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QGridLayout(parentWidget)
        layout.setSpacing(2)
        widget.setLayout(layout)
        self.label_field = QtWidgets.QLineEdit()
        self.label_power_supply_field = QtWidgets.QLineEdit()
        self.label_setpoint = QtWidgets.QLineEdit()
        for label in [self.label_field, self.label_power_supply_field, self.label_setpoint]:
            label.setFixedWidth(62)
            label.setEnabled(False)
        self.check_persistent = QtWidgets.QCheckBox("Set persistent")
        self.edit_field = QtWidgets.QLineEdit()
        self.edit_field.setValidator(QtGui.QDoubleValidator())
        self.edit_field.returnPressed.connect(self.set_field_clicked)
        self.button_set_field = QtWidgets.QPushButton("Set Field")
        self.button_set_field.clicked.connect(self.set_field_clicked)

        layout.addWidget(QtWidgets.QLabel("Field:"), 1, 1)
        layout.addWidget(QtWidgets.QLabel("P.S. out:"), 2, 1)
        layout.addWidget(QtWidgets.QLabel("Setpoint:"), 3, 1)
        layout.addWidget(self.label_field, 1, 2)
        layout.addWidget(self.label_power_supply_field, 2, 2)
        layout.addWidget(self.label_setpoint, 3, 2)
        layout.addWidget(self.check_persistent, 1, 3)
        layout.addWidget(self.edit_field, 2, 3)
        layout.addWidget(self.button_set_field, 3, 3)

        dock.setWidget(widget)

        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

    def set_field_clicked(self):
        try:
            target = float(self.edit_field.text())
            self.set_field(target, self.check_persistent.isChecked())
        except Exception:
            print("invalid target field value")

    def updateSlot(self, status):
        self.label_field.setText("%2.4f T" % status["field"])
        self.label_power_supply_field.setText("%2.4f T" % status["power_supply_field"])
        self.label_setpoint.setText("%2.4f T" % status["setpoint"])

