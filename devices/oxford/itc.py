# OxfordInstruments_ILM200.py class, to perform the communication between the Wrapper and the device
# Guenevere Prawiroatmodjo <guen@vvtp.tudelft.nl>, 2009
# Pieter de Groot <pieterdegroot@gmail.com>, 2009
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import pyvisa as visa
import logging
from .ips import OxfordWorker
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets, QtCore, QtGui
import traceback

default_req_port = 7035
default_pub_port = 7036


class ITCWorker(OxfordWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port,
                 **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def init_device(self):
        super().init_device()

        self.set_remote()
        self.get_all()
        print("Success")

    def status(self):
        d = super().status()
        if not self.is_offline:
            d["temperature"] = self.get_T()
            d["setpoint"] = self.get_T_setpoint()
            d["heater"] = self.get_heater()
            d["power"] = self.get_heater_setpoint()
        return d

    def get_all(self):
        self.get_T1()
        self.get_T2()
        self.get_T3()
        self.get_p()
        self.get_i()
        self.get_d()
        self.get_T_setpoint()
        self.get_heater()
        self.get_remote_status()

    @remote
    def identify(self):
        return self.execute('V',  reply_pattern="^V$")

    @remote
    def set_remote(self):
        self.set_remote_status(3)

    @remote
    def set_local(self):
        self.set_remote_status(2)

    def set_remote_status(self, mode):
        status = {
        0: "Local and locked",
        1: "Remote and locked",
        2: "Local and unlocked",
        3: "Remote and unlocked",
        }
        self.execute('C%s' %mode, reply_pattern=r"^C$")
    
    @remote
    def set_temperature(self, target_temperature):
        self.set_heater_auto(target_temperature)
    
    @remote
    def set_heater_auto(self, target_temperature=None, set_remote=True):
        '''Set heater mode to auto. Warning: could change gas valve state.
            Input:
                target_temperature (float or None) '''
        if set_remote:
            self.set_remote()
        if target_temperature is not None:
            self.set_T_setpoint(target_temperature)
        self.set_heater(1)
    
    @remote
    def set_heater_manual(self, heater_level=None, set_remote=True):
        if set_remote:
            self.set_remote()
        self.set_heater(0)
        if heater_level is not None:
            self.set_heater_setpoint(heater_level)
        

    @remote
    def set_heater(self, mode, set_remote=True):
        if set_remote:
            self.set_remote()
        '''
        Set the mode of the temperature controller:
            mode (int) :
            0 : "Heater Manual, Gas manual",
            1 : "heater auto, gas manual",
            2 : "heater manual, gas auto",
            3 : "heater auto, gas auto"

        Output:
            None
        '''
        status = {
        0: "Heater Manual, Gas manual",
        1: "heater auto, gas manual",
        2: "heater manual, gas auto",
        3: "heater auto, gas auto"
        }
        if status.__contains__(mode):
            logging.info(__name__ + ' : Setting heater to %s' % status.get(mode, "Unknown"))
            self.execute('A%s' % mode, reply_pattern=r"^A$")
        else:
            print('Invalid mode inserted.')

    @remote
    def set_p(self, p, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute('P%s' % p, reply_pattern=r"^P$")

    @remote
    def set_i(self, i, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute('I%s' % i, reply_pattern=r"^I$")

    @remote
    def set_d(self, d, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute('D%s' % d, reply_pattern=r"^D$")

    @remote
    def set_T_setpoint(self, T, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute('T%s' % T, reply_pattern=r"^T$")

    @remote
    def set_heater_setpoint(self, T, set_remote=True):
        if set_remote:
            self.set_remote()
        self.execute('O%s' % T, reply_pattern=r"^O$")

    @remote
    def get_p(self):
        return self.get_r_parameter(8)

    @remote
    def get_i(self):
        return self.get_r_parameter(9)

    @remote
    def get_d(self):
        return self.get_r_parameter(10)

    @remote
    def get_T_setpoint(self):
        return self.get_r_parameter(0)

    @remote
    def get_heater_setpoint(self):
        return self.get_r_parameter(5)

    @remote
    def get_heater(self):
        '''
        Get the heater status.
        '''
        result = self.execute('X', reply_pattern="^X\dA\dC\dS\d\dH\dL\d$")
        return bool(result[3])

    @remote
    def get_T(self):
        return self.get_T1()

    @remote
    def get_T1(self):
        '''
        Get temperature on channel 1.
        Input:
            None

        Output:
            result (float) : Temperature
        '''
        return self.get_r_parameter(1)

    @remote
    def get_T2(self):
        '''
        Get temperature on channel 2.
        Input:
            None

        Output:
            result (float) : Temperature
        '''
        return self.get_r_parameter(2)

    @remote
    def get_T3(self):
        '''
        Get temperature on channel 3.
        Input:
            None

        Output:
            result (float) : Temperature
        '''
        return self.get_r_parameter(3)

    @remote
    def get_remote_status(self):
        '''
        Get remote control status

        Input:
            None

        Output:
            result(str) :
            "Local & locked",
            "Remote & locked",
            "Local & unlocked",
            "Remote & unlocked",
        '''
        result = self.execute('X', reply_pattern="^X\dA\dC\dS\d\dH\dL\d$")
        return int(result[5])
        

class MyBar(QtWidgets.QWidget):
    """ Creates custom 'vertical progress bar'"""

    def __init__(self, parent=None):
        super(MyBar, self).__init__(parent)
        self.text = ""
        self.maximumValue = 100
        self.currentValue = 0
        self.setFixedWidth(18)

    def setValue(self, currentValue):
        if self.currentValue != currentValue:
            self.currentValue = currentValue
            self.text = "H: %.3f %%" % (currentValue,)
            self.repaint()

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.translate(0, self.height() - 1)
        painter.rotate(-90)

        painter.setPen(QtGui.QColor(140, 138, 135))
        painter.drawRoundedRect(QtCore.QRectF(0, 0, self.height() - 1, self.width() - 1), 4, 4)

        painter.setPen(QtGui.QColor(201, 199, 197))
        path = QtGui.QPainterPath()
        path.addRoundedRect(QtCore.QRectF(1, 1, self.height() - 3, self.width() - 3), 3, 3)

        painter.fillPath(path, QtGui.QColor(214, 212, 210))
        painter.drawPath(path)

        path = QtGui.QPainterPath()
        path.addRoundedRect(QtCore.QRectF(1, 1, (self.height() - 3) * self.currentValue / self.maximumValue, self.width() - 3), 2, 2)

        painter.fillPath(path, QtGui.QColor(255, 0, 0))
        painter.drawPath(path)

        painter.setPen(QtCore.Qt.black)
        # print text centered
        painter.drawText(self.height() / 2 - 20, self.width() / 2 + 5, self.text)
        painter.end()


@include_remote_methods(ITCWorker)
class ITC(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.heating_scheduled = False
        self.heating_setpoint = None
        self.heating_time = None

    def createDock(self, parentWidget, menu=None):
        self.dock = QtWidgets.QDockWidget("Temperature Controller ITC503S", parentWidget)
        widget = QtWidgets.QWidget(self.dock)
        layout = QtWidgets.QGridLayout(parentWidget)
             
        self.LCD = QtWidgets.QLCDNumber()
        self.LCD.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        self.LCD.setDigitCount(8)
        self.LCD.setToolTip("Current temperature")
        self.LCDtext = "temperature"
        
        self.LCD2 = QtWidgets.QLCDNumber()
        self.LCD2.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        self.LCD2.setDigitCount(6)
        self.LCD2.setToolTip("Temperature Setpoint")
        self.LCD2text = "setpoint"
        
        self.heat_btn = QtWidgets.QPushButton("Schedule heating")
        self.heat_btn.clicked.connect(self.schedule_heating)
        self.heat_btn.setToolTip("Set automatic heating")

        self.bar_heater = MyBar()
        
        font = QtGui.QFont()
        font.setPointSize(18)
        layout.addWidget(QtWidgets.QLabel("T:"), 0, 0)
        layout.addWidget(self.LCD, 0, 1)
        layout.addWidget(QtWidgets.QLabel("S:"), 1, 0)
        layout.addWidget(self.LCD2, 1, 1)
        layout.addWidget(self.bar_heater, 0, 2, 3, 1)
        
        layout.addWidget(self.heat_btn, 2, 0, 1, 2)
        widget.setLayout(layout)
        self.dock.setWidget(widget)
        self.dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.dock)

        def set_temperature(this):
            temp, ok = QtWidgets.QInputDialog.getDouble(None, "Set temperature setpoint",
                                                        "New position:", 299, 0, 300, 3)
            if ok:
                QtWidgets.QApplication.processEvents()
                self.set_remote()
                self.set_heater_auto(temp)

        def set_heater(this):
            value, ok = QtWidgets.QInputDialog.getDouble(None, "Set heater level",
                                                        "Fix heater power at (%):", 0, 0, 100, 1)
            if ok:
                QtWidgets.QApplication.processEvents()
                self.set_remote()
                self.set_heater_manual(value)

        self.LCD2.mousePressEvent = set_temperature
        self.bar_heater.mousePressEvent = set_heater
        
        if menu:
            menu.addAction(self.dock.toggleViewAction())
    
        self.createListenerThread(self.updateSlot)    

    def updateSlot(self, status):
        self.bar_heater.setValue(status["power"])
        self.LCD.display("%.3f" % status["temperature"])
        self.LCD2.display(status["setpoint"])
        if self.heating_scheduled:
            remaining_time = QtCore.QDateTime.currentDateTime().secsTo(self.heating_time)
            if remaining_time < 0:
                try:
                    self.set_remote()
                    self.set_heater_auto()
                    self.set_temperature(self.heating_setpoint)
                    self.heat_btn.setText("Schedule Heating")
                    self.heating_scheduled = False
                except Exception:
                    pass
            else:
                self.heat_btn.setText(f"Heating to {self.heating_setpoint:3.1f} K\n in "
                                      f"{remaining_time // 3600}h {remaining_time // 60 % 60:02d}m "
                                      f"{remaining_time % 60:02d}s")

    def schedule_heating(self):
        if self.heating_scheduled:
            self.heating_scheduled = False
            self.heat_btn.setText("Schedule Heating")
            return

        dialog = QtWidgets.QDialog(self.dock)
        dialog.setWindowTitle("Schedule heating")
        dialog.setModal(True)

        layout = QtWidgets.QFormLayout(dialog)

        edit_time = QtWidgets.QDateTimeEdit()
        edit_time.setFixedWidth(125)
        edit_time.setToolTip("Start heating at:")
        edit_time.setCalendarPopup(True)
        edit_time.setDateTime(QtCore.QDateTime.currentDateTime())
        layout.addRow("Start heating at:", edit_time)

        edit_temp = QtWidgets.QLineEdit("299")
        layout.addRow("Target temperature (K):", edit_temp)

        button_box = QtWidgets.QDialogButtonBox(dialog)
        button_box.setOrientation(QtCore.Qt.Horizontal)
        button_box.setStandardButtons(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel)

        button_box.accepted.connect(dialog.accept)
        button_box.rejected.connect(dialog.reject)
        layout.addRow(button_box)

        result = dialog.exec_()
        if result:
            try:
                now = QtCore.QDateTime.currentDateTime()
                target_time = edit_time.dateTime()
                target_temperature = float(edit_temp.text())

                if target_time < now:
                    self.error_message("Cannot schedule heating in the past!")
                    return

                if target_temperature < 0:
                    self.error_message("Invalid temperature!")
                    return

                elif target_temperature > 300:
                    self.error_message("Setting temperature higher than 300K is forbidden!")
                    return

                try:
                    self.get_remote_status()
                except ConnectionError:
                    self.error_message("Connection error - could not establish connection with device worker!")
                    return

                self.heating_setpoint = target_temperature
                self.heating_time = target_time
                self.heating_scheduled = True
            except Exception:
                self.error_message(traceback.format_exc())

    def error_message(self, message):
        msg = QtWidgets.QMessageBox(self.dock)
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText(message)
        msg.exec_()
        print(message)
