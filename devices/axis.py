class Axis():
    def __init__(self, device, axis):
        self.device = device
        self.axis = axis
        if self.device is not None and self.axis is not None:
            functions_param = ["move_absolute", "move_relative", "move_velocity", "set_velocity", "set_name"]
            functions_noparam = ["get_position", "stop", "home", "is_stopped", "is_homed", "get_name"]
            for name in functions_param:
                try:
                    setattr(self, name, lambda val, name=name: getattr(self.device, name)(self.axis, val))
                except:
                    pass
            for name in functions_noparam:
                try:
                    setattr(self, name, lambda name=name: getattr(self.device, name)(self.axis))
                except:
                    pass
