from .device import *

N_AIR = 1.00028  # refraction index of the air
H_C = 1239.84197 # Planck constant * speed of light in eV * nm