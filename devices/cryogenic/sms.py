from datetime import datetime
from time import sleep
from time import time as clock
import pyvisa as visa
import threading
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5 import QtWidgets, QtCore, QtGui
import warnings
import re

default_req_port = 7031
default_pub_port = 7032

TESLA_EPSILON = 0.1


STAGE_MATCH_CURRENT = 0
STAGE_WAIT_BEFORE_OPENING = 0.5
STAGE_WAIT_OPENING = 1
STAGE_RAMP = 2
STAGE_WAIT_BEFORE_CLOSING = 2.5
STAGE_WAIT_CLOSING = 3
STAGE_SWEEP_TO_ZERO_SLOWLY = 4
STAGE_IDLE = -1


class SMSWorker(DeviceWorker):
    """ Device driver for Cryogenic SMS power supply. User API is 
    consistent with IPS120 driver, allowing to use the same GUI """
    
    
    class Switch:
        DELAY = {True: 60, # 60 s for switch to open
                 False: 120 # 120 s for switch to close
                }
        
        def __init__(self, initially_open):
            initially_open = bool(initially_open)
            self.is_open = initially_open
            self._last_change = clock()            
            
        def set(self, new_state):
            new_state = bool(new_state)
            if new_state != self.is_open:
                self.is_open = new_state
                self._last_change = clock()
            
        def is_ready(self):
            return clock() - self._last_change > self.DELAY[self.is_open]
    
    
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, 
                 address="COM1", **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self._address = address
        self._target_field = 0
        self._persistent_after = False
        self._stage = STAGE_IDLE
        self._last_ramp = clock()
        self.debug = False
        self.procedure_lock = threading.Lock()

    FIELD_CURRENT_RATIO = 17 / 133.58  # T/Amps
    
    MAX_RATE_PTS = [-17, -16, -8, 8, 16, 17] # values in T where max rate is changed
    MAX_RATE_VALS = [0.010, 0.025, 0.050, 0.025, 0.010]


    @remote
    def set_slow_mode(self, run_slow):
        """ run_slow: bool """
        if run_slow:
            self.MAX_RATE_PTS = [-17, -16, -8, 8, 16, 17] # values in T where max rate is changed
            self.MAX_RATE_VALS = [0.005, 0.013, 0.025, 0.013, 0.005]
        else:
            self.MAX_RATE_PTS = [-17, -16, -8, 8, 16, 17] # values in T where max rate is changed
            self.MAX_RATE_VALS = [0.010, 0.025, 0.050, 0.025, 0.010]


    def _limit_target(self, desired_target):
        """ Check allowed sweep rate.
        desired_target - target field in tesla
    
        Return: target, rate
        target - either the desired target or the nearest threshold where the rate is changed
        rate - maximum allowed rage """
        
        now = self.field()
        l = zip(self.MAX_RATE_PTS[:-1], self.MAX_RATE_PTS[1:], self.MAX_RATE_VALS)
        
        if desired_target > now:
            now += TESLA_EPSILON
        else:
            now -= TESLA_EPSILON
        
        for left,right,rate in l:
            if left <= now <= right:
                if desired_target > right:
                    return right, rate
                elif desired_target < left:
                    return left, rate
                else:
                    return desired_target, rate

    def status(self):
        d = super().status()
        
        d["busy"] = self.busy()
        d["setpoint"] = self.setpoint()
        current, voltage = self.get_output_raw()
        d["power_supply_current"] = current
        d["power_supply_voltage"] = voltage
        d["power_supply_field"] = self.power_supply_field(current)
        heater = self.heater()
        d["switch_heater"] = heater
        if heater:
            d["field"] = d["power_supply_field"]
        else:
            d["field"] = self.persistent_field()
        return d

    def init_device(self):
        super().init_device()
        
        self._comm_lock = threading.Lock()
        
        rm = visa.ResourceManager()
        self.visa_handle = rm.open_resource("ASRL%s::INSTR" % self._address)
        self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_STOP_BITS,
                                                visa.constants.VI_ASRL_STOP_ONE)
        self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_BAUD, 9600)
        self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_DATA_BITS, 8)
        self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_PARITY,
                                            visa.constants.Parity.odd)
        self.visa_handle.read_termination = '}'
        self.visa_handle.write_termination = '\r\n'
        self.visa_handle.timeout = 1000
        self.visa_handle.clear()
        self.visa_handle.flush(visa.constants.VI_READ_BUF_DISCARD)

        try:
            for line in self._execute('U'):
                print(line)
        except:
            print("No reply. Make sure the device is connected and in Remote mode")
            raise Exception()

        self._switch_state = SMSWorker.Switch(False)
        self.heater() # updates _switch_state

        self.continue_running = True
        def func():
            while self.continue_running:
                self._procedure_iteration()
                sleep(1)
        self.sms_thread = threading.Thread(target=func)
        self.sms_thread.start()
        

    def close_device(self):
        self.continue_running = False
        self.sms_thread.join()
        
        
    def _execute_single(self, cmd):
        with self._comm_lock:
            self.visa_handle.write(cmd)
            resp = ''
            while not resp.endswith('}'):
                resp += self.visa_handle.read_raw().replace(b'\x08\x20\x08\x7d',b'').decode('ascii')
            lines = resp.split('\r\n')
            if len(lines) < 2 or lines[0] != cmd or not lines[-1].endswith('}'):
                raise Exception(f"Error in communication with device (Q: {cmd}, R: {resp})")
            return lines[1:-1]
    
    def _execute(self, cmd):
        for i in range(3):
            try:
                return self._execute_single(cmd)
            except:
                print("Error in execute cmd: ", cmd, ", retrying...")
        raise Exception("Persistent error in communications")
    
    
    GET_OUTPUT_PATTERN = re.compile('.{9}OUTPUT: (.*) AMPS AT (.*) VOLTS')
    
    @remote
    def get_output_raw(self):
        """ Returns current (in amps) and voltage (in volts) """
        ret = self._execute('G O')[0]
        m = self.GET_OUTPUT_PATTERN.match(ret)
        if m:
            return float(m.group(1)), float(m.group(2))
        else:
            raise Exception(f"Wrong instrument reply! R:{ret}")
        
    @remote
    def power_supply_field(self, current=None):
        """ Returns field (in tesla) and voltage (in volts) """
        if current is None:
            current, _ = self.get_output_raw()
        return current * self.FIELD_CURRENT_RATIO
        
    
    GET_PERSISTENT_PATTERN = re.compile('.{9} (.*) AMPS')
        
    @remote
    def persistent_field(self):
        ret = self._execute('G P')
        if not ret:
            return 0.
        else:
            m = self.GET_OUTPUT_PATTERN.match(ret[0])
            return float(m.group(1)) * self.FIELD_CURRENT_RATIO
        
        
    GET_RATE_PATTERN = re.compile('.*RAMP RATE: (.*) A/SEC')
    
    @remote
    def get_rate_raw(self):
        """ Returns ramp rate in amp/s """
        ret = self._execute('G R')[0]
        m = self.GET_RATE_PATTERN.match(ret)
        if m:
            return float(m.group(1))
        else:
            raise Exception(f"Wrong instrument reply! R:{ret}")
    
    @remote
    def field_sweep_rate(self):
        """ Returns ramp rate in T/min """
        return self.get_rate_raw() * 60 * self.FIELD_CURRENT_RATIO
    
    
    GET_SETPOINT_PATTERN = re.compile('.*MID SETTING: (.*) AMPS')
    
    @remote
    def get_setpoint_raw(self):
        """ Returns (mid) setpoint in A """
        ret = self._execute('G %')[0]
        m = self.GET_SETPOINT_PATTERN.match(ret)
        if m:
            return float(m.group(1))
        else:
            raise Exception(f"Wrong instrument reply! R:{ret}")
            
    @remote
    def setpoint(self):
        """ Returns (mid) setpoint in T """
        return self.get_setpoint_raw() * self.FIELD_CURRENT_RATIO
    
    
    GET_HEATER_PATTERN = re.compile('.{9}HEATER STATUS: (ON|OFF)')
    
    @remote
    def heater(self):
        """ Returns True if switch heater is on """
        ret = self._execute('H')[0]
        m = self.GET_HEATER_PATTERN.match(ret)
        if m:
            ret = (m.group(1)=='ON')
            self._switch_state.set(ret)
            return ret
        else:
            raise Exception(f"Wrong instrument reply! R:{ret}")
    
    
    GET_RAMP_PATTERN = re.compile('.{9}RAMP STATUS: (HOLDING|RAMPING) .*')
    
    @remote
    def get_ramp_status(self):
        """ Returns True if magnet is ramping """
        ret = self._execute('R S')[0]
        m = self.GET_RAMP_PATTERN.match(ret)
        if m:
            ret =  (m.group(1)=='RAMPING')
            if ret:
                self._last_ramp = clock()
            return ret
        else:
            raise Exception(f"Wrong instrument reply! R:{ret}")
    

    def _procedure_iteration(self):
        self.status()
        with self.procedure_lock:
            if self._stage == STAGE_MATCH_CURRENT:
                if self._switch_state.is_open:
                    self._stage = STAGE_WAIT_OPENING
                else:
                    if not self.get_ramp_status() and abs( self.power_supply_field() - self.persistent_field() ) < TESLA_EPSILON:
                        self._stage = STAGE_WAIT_BEFORE_OPENING
                    else:
                        self.set_setpoint(self.persistent_field)
                        self.go_to_setpoint()
            if self._stage == STAGE_WAIT_BEFORE_OPENING:
                if clock() - self._last_ramp > 60:
                    self.set_heater(True)
                    self._stage = STAGE_WAIT_OPENING
            if self._stage == STAGE_WAIT_OPENING:
                if self._switch_state.is_ready():
                    self._stage = STAGE_RAMP
                    self.pause()
                    target, rate = self._limit_target(self._target_field)
                    self.set_field_sweep_rate_raw(rate)
                    self.set_setpoint(target)
                    self.go_to_setpoint()
                    self.resume()
            if self._stage == STAGE_RAMP:
                if not self.get_ramp_status(): 
                    if abs(self.field() - self._target_field) < TESLA_EPSILON:
                        self._stage = STAGE_WAIT_BEFORE_CLOSING
                    else:
                        target, rate = self._limit_target(self._target_field)
                        self.set_field_sweep_rate_raw(rate)
                        self.set_setpoint(target)
                        self.go_to_setpoint()
            if self._stage == STAGE_WAIT_BEFORE_CLOSING:
                if not self._persistent_after:
                    self._stage = STAGE_IDLE
                elif clock() - self._last_ramp > 60:
                    self.set_heater(False)
                    self._stage = STAGE_WAIT_CLOSING
            if self._stage == STAGE_WAIT_CLOSING:
                if self._switch_state.is_ready():
                    self._stage = STAGE_SWEEP_TO_ZERO_SLOWLY
                    self.go_to_zero()
            if self._stage == STAGE_SWEEP_TO_ZERO_SLOWLY:
                self._stage = STAGE_IDLE

    @remote
    def busy(self):
        return self._stage != STAGE_IDLE
        
    @remote
    def is_stopped(self):
        return not self.busy()

    @remote
    def stop(self, set_remote=True):
        self.pause()
        self._stage = STAGE_IDLE
        
    def pause(self):
        self._execute('P 1')

    def resume(self):
        self._execute('P 0')

    @remote
    def field(self):
        if self.heater():
            return self.power_supply_field()
        else:
            return self.persistent_field()

    @remote
    def set_field(self, target_field=0, persistent_after=False):
        with self.procedure_lock:
            self._stage = STAGE_MATCH_CURRENT
            self._persistent_after = persistent_after
            self._target_field = target_field


    def set_field_sweep_rate_raw(self, rate, set_remote=True):
        """ rate - desired rate in Amps/s """
        # TODO: implement safety
        self._execute("S R {}".format(round(rate, 5)))

    def set_setpoint(self, field):
        self._execute("S % {}".format(round(field / self.FIELD_CURRENT_RATIO, 5)))

    def go_to_zero(self, set_remote=True):
        self._execute("R 0")

    def go_to_setpoint(self, set_remote=True):
        self._execute("R %")

    def set_hold(self, set_remote=True):
        #self.execute("A0", reply_pattern=r"^A$")
        raise Exception("Not implemented")
                
    def set_heater(self, state=1, set_remote=True):
        self._switch_state.set(state)
        self._execute(f"H {int(state)}")