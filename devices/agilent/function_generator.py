from datetime import datetime
import time
from time import sleep
from time import perf_counter as clock
import pyvisa as visa
import threading
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5.QtWidgets import (QWidget, QPushButton, 
    QHBoxLayout, QVBoxLayout, QApplication)
from PyQt5 import QtWidgets, QtCore, QtGui
import functools


default_req_port = 6780
default_pub_port = 6781

class FunctionGeneratorWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, address="COM1", **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self._address = address
    
    def status(self):
        d = super().status()
        d['output_state'] = self.get_output_state()
        d['frequency'] = self.get_frequency()
        d['function'] = self.get_function()
        return d
    
    def init_device(self):
        """
        Opens the communication 
        """
        print("=========")
        print(str(datetime.now()))
        print("Agilent 33250A: Initialization...")
        self.visa_lock = threading.Lock()
        rm = visa.ResourceManager()
        termination_chars='\r\n'
        if self._address[0:3] == 'COM':
            print("Lakeshore: Communication at COM"+str(self._address[3:]))
            self.visa_handle = rm.open_resource("ASRL%s::INSTR" % self._address[3:])
            self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_STOP_BITS,
                                                visa.constants.VI_ASRL_STOP_ONE)
            self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_BAUD,57600)
            #newer model 335 has baud rate 57600
            #self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_PARITY,visa.constants.VI_ASRL_PAR_ODD)
            #self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_DATA_BITS, 8)
            self.visa_handle.read_termination = termination_chars
            mess=self._execute("*IDN?")
            if len(mess.strip()) != 0:
                    print(mess.strip())
            else:
                print("Lakeshore: Changing baud rate from 57600 to 9600...")
                self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_BAUD,9600)
                # older model 331 has baud rate 9600
                self._execute('')
                info_str = self._execute("*IDN?").strip()
                if len(info_str) == 0:
                    print("Agilent: Can't set up connection! Try again!")
                else:
                    tokens = info_str.split(',')
                    self.model_name = tokens[0]
                    self.model_number = tokens[1]
                    print(info_str)
        else:
            self.visa_handle = rm.open_resource("GPIB::%s" % self._address[4:])

        self.visa_handle.read_termination = '\r\n'
        print("Agilent: End of initialization")
          

    def _read(self):
        # because protocol has no termination chars the read reads the number
        # of bytes in the buffer
        bytes_in_buffer = self.visa_handle.bytes_in_buffer
        # a workaround for a timeout error in the pyvsia read_raw() function
        with(self.visa_handle.ignore_warning(visa.constants.VI_SUCCESS_MAX_CNT)):
            mes = self.visa_handle.visalib.read(
                self.visa_handle.session, bytes_in_buffer)
        # cannot be done on same line for some reason
        mes = str(mes[0].decode())
        return mes

    def _execute(self, message):
        with self.visa_lock:
            self.visa_handle.write(message)
            sleep(70e-3)  # wait for the device to be able to respond
            result = self._read()
        if result.find('?') >= 0:
            print("Error: Command %s not recognized: %s" % (message, result))
            return None
        else:
            return result
    
    def close_device(self):
        self.continue_running = False    

    @remote
    def busy(self):
        return self._stage != None and self._stage < 7

    @remote
    def get_output_state(self):
        return int(self._execute("OUTPUT:STATE?"))
        
    @remote
    def set_output_state(self, state):
        """ Turn output on/off """
        if state:
            state_str = 'ON'
        else:
            state_str = 'OFF'
        self._execute("OUTPUT:STATE %s" % state_str)
        
    @remote
    def get_frequency(self):
        '''Read current frequency in Hz from the device''' 
        return float(self._execute("FREQUENCY?"))
    
    @remote
    def set_frequency(self, freq):
        '''Set the signal frequency''' 
        self._execute("FREQUENCY %f" % freq)
    
    @remote
    def set_voltage(self, amplitude=None, offset=None, high=None, low=None):
        '''Set signal voltage. Require keyword arguments: amplitude, offset, low or high''' 
        if amplitude is not None:
            self._execute("VOLT:AMPL %f" % amplitude)
        if offset is not None:
            self._execute("VOLT:OFFSET %f" % offset)
        if low is not None:
            self._execute("VOLT:LOW %f" % low)
        if high is not None:
            self._execute("VOLT:HIGH %f" % high)
    
    @remote
    def get_voltage_low_high(self):
        """Read current voltage settings from the device"""
        low = float(self._execute("VOLT:LOW?"))
        high = float(self._execute("VOLT:HIGH?"))
        return low,high
        
    @remote
    def get_voltage_amplitude_offset(self):
        """Read current voltage settings from the device"""
        amplitude = float(self._execute("VOLT:AMPLITUDE?"))
        offset = float(self._execute("VOLT:OFFSET?"))
        return amplitude,offset
    
    
    @remote        
    def set_function(self,shape):
        '''Set function.
        shape should be one of following: 'sine', 'square', 'ramp', 'pulse', 'dc', 'user' ''' 
        allowed_shapes = ("SIN", "SQU", "RAMP", "PULS", "NOIS", "DC", "USER")
        for token in allowed_shapes:
            if shape.lower().startswith(token.lower()):
                self._execute("FUNC:SHAP {}".format(token))
                return
        print("Error: shape '{}' not recognized".format(shape))
        
    @remote
    def get_function(self):
        return self._execute("FUNC:SHAP?").strip()
        
        
       
    @remote
    def upload_waveform(self, waveform):
        #TODO
        raise NotImplementedError
        #FORM:BORD
        #DATA:DAC VOLATILE
        #FORM:BORD SWAP # little-endian
        
@include_remote_methods(FunctionGeneratorWorker)
class FunctionGenerator(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        

    #def __del__(self):
    #    pass

    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("Agilent 33250A", parentWidget)
        widget1 = QtWidgets.QWidget(dock)
        layout = QtWidgets.QHBoxLayout(parentWidget)
        """     
        self.LCD = QtWidgets.QLCDNumber()
        self.LCD.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        self.LCD.setDigitCount(6)
        self.LCD.setToolTip(self.LCDtext)
        self.Units=QtWidgets.QLabel(self.units[self.LCDtext])     
        self.btn_settings = QtWidgets.QPushButton("Settings")
        self.btn_settings.setToolTip('Opens settings window where you can change temperature controller parameters')
        self.btn_settings.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
        QtWidgets.QSizePolicy.Fixed)
        self.btn_OFF = QtWidgets.QPushButton("heaters OFF!")
        self.btn_OFF.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
        QtWidgets.QSizePolicy.Expanding)
        font=QtGui.QFont()
        font.setPointSize(18)
        self.btn_OFF.setFont(font)
        #self.btn_OFF.setStyleSheet("background-color:rgb(255, 50, 50)")

        layout.addWidget(self.btn_settings)
        layout.addWidget(self.LCD)
        layout.addWidget(self.Units)
        layout.addWidget(self.btn_OFF)
        
        widget1.setLayout(layout)
        dock.setWidget(widget1)
        
        self.btn_settings.clicked.connect(self.show_settings)
        self.btn_OFF.clicked.connect(self.emergency_off)

        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
        
        self.createListenerThread(self.updateSlot)
        """
        
    def updateSlot(self,status):
        #print("slot updated")
        self.LCD.display(status[self.LCDtext])
        self.status_copy = status
                    
    def show_settings(self,status):
        #print("settings")
        self.btn_settings.setEnabled(0)
        if self.setting_window is not None:
            self.setting_window.show()
        else:
            sett_window=SettingsWindow(self)
            sett_window.show()
            def on_finished(code):
                self.btn_settings.setEnabled(True)
            sett_window.exec_()
            self.btn_settings.setEnabled(True)