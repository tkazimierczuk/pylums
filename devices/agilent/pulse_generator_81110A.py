from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
from PyQt5 import QtWidgets, QtCore, QtGui
import pyvisa as visa
from time import sleep
import time
import inspect
from datetime import datetime
import threading
import warnings
import re

default_req_port = 3456
default_pub_port = 4567
max_pattern_bits = 16384


class PulseGenerator81110AWorker(DeviceWorker):

    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, address='GPIB10', **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self._address = address
        self._pattern = [None] * 4

    IGPIB = 0
    IRS232 = 1
    IETHERNET = 2

    def init_device(self):
        print(str(datetime.now()))
        print('Initialization...')
        self._visa_lock = threading.Lock()
        self._connect()
        print('Communication handler created')
        self.clear_status()
        print('Status cleared')
        m = str(self._execute('*IDN?', True))
        print(m)
        print('Connection established')
        if 'HP81110A' in m:
            print('TWO channel pulse-pattern generator HP81110A detected')
            self._pattern[1] = self._pattern_str_to_array(self._get_pattern(1))
            self._pattern[2] = self._pattern_str_to_array(self._get_pattern(2))

        else:
            print('Wrong Type Of Generator')
            raise TypeError('generator is not HP81110A')
        print('End of initialization')

    def _connect(self):
        print('Connecting...')
        rm = visa.ResourceManager()
        if self._address[0:3] == 'COM':
            self._interface = PulseGenerator81110AWorker.IRS232
            self.visa_handle = rm.open_resource('ASRL%s::INSTR' % self._address[3:])
            self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_STOP_BITS,
                                                visa.constants.VI_ASRL_STOP_TWO)
        elif self._address[0:4] == 'GPIB':
            self._interface = PulseGenerator81110AWorker.IGPIB
            self.visa_handle = rm.open_resource('GPIB::%s' % self._address[4:])
            self.visa_handle.clear()
            sleep(0.2)
        else:
            self._interface = PulseGenerator81110AWorker.IETHERNET
            self.visa_handle = rm.open_resource('TCPIP::%s::7020::SOCKET' % self._address)
            self.visa_handle.write_termination = '\n'

        self.visa_handle.read_termination = '\n'
        self.visa_handle.timeout = 3000

    def status(self):
        d = super().status()
        for i in (1, 2):
            d['output' + str(i)] = self.get_output(i)
            d['voltageLOW' + str(i)] = self.get_voltageLOW(i)
            d['voltageHIGH' + str(i)] = self.get_voltageHIGH(i)
            d['polarity' + str(i)] = self.get_polarity(i)

        err = self.check_error()
        while 'No error' not in err:
            print(err)
            err = self.check_error()
        return d

    @remote
    def _execute(self, message, response_expected=False, reply_pattern=None, allowed_retries=5):
        try:
            with self._visa_lock:
                self.visa_handle.write(message)
                if not response_expected:
                    return None
                t0 = time.time()
                res = self.visa_handle.read_raw().decode('ascii')
                t1 = time.time()
                if t1 - t0 > 0.5:
                    print('Command %s takes %f s' % (message, t1 - t0))
                self.is_offline = False
        except visa.VisaIOError:
            if allowed_retries > 0:
                with self._visa_lock:
                    self._connect()
                return self._execute(message, reply_pattern, allowed_retries - 1)
            else:
                self.is_offline = True
                res = ''
                warnings.warn('Communication error occured on message: %s' % message, ResourceWarning)

        res = res.strip()
        if reply_pattern:
            if not re.match(pattern=reply_pattern, string=res):
                if allowed_retries > 0:
                    print('Warning! Wrong reply (Q:%s, R:%s)' % (message, res))
                    return self._execute(message, reply_pattern, allowed_retries - 1)
                else:
                    raise Exception('Wrong reply (Q:%s, R:%s)' % (message, res))

        return res

    @remote
    def ok(self):
        print('Communication ok')
        return self._execute('*OPC?', True)

    @remote
    def reset(self):
        self._execute('*RST')

    @remote
    def clear_status(self):
        self._execute('*CLS')

    @remote
    def check_error(self):
        return self._execute(':SYST:ERR?', True)

    @remote
    def set_double_pulsed(self, channel, value):
        if value in (0, 1) and channel in (1, 2):
            self._execute(f':PULS:DOUB{channel} {"ON" if value == 1 else "OFF"}')
        else:
            print(f'{inspect.stack()[0][3]}: Invalid channel value, use 1|2 or value use 0|1')

    @remote
    def get_double_pulsed(self, channel):
        if channel in (1, 2):
            self._execute(f':PULS:DOUB{channel}?', True)
        else:
            print(f'{inspect.stack()[0][3]}: Invalid channel value, use 1|2')

    @remote
    def set_continuous(self):
        self._execute(':ARM:SOUR IMM')

    @remote
    def set_external_trigger(self):
        self._execute(':ARM:SOUR EXT')

    @remote
    def set_frequency(self, value):
        if 0.000001 <= value <= 50.:
            self._execute(f':FREQ{value}MHz')
        else:
            print(f'{inspect.stack()[0][3]}: Provided value is out of range.')

    @remote
    def get_frequency(self):
        return self._execute(':FREQ?', True)

    @remote
    def set_period(self, value):
        if 0.02 <= value <= 999500000.:
            self._execute(f':PULS:PER {value}US')
        else:
            print('Provided value is out of range')

    @remote
    def get_period(self):
        return self._execute(':PULS:PER?', True)

    @remote
    def set_output(self, channel, value):
        if value in (1, 0) and channel in (1, 2):
            self._execute(f':OUTP{channel} {value}')
        else:
            print(f'{inspect.stack()[0][3]}: Invalid output value: use 1|0, or channel: use 1|2')

    @remote
    def get_output(self, channel):
        return self._execute(f':OUTP{channel}:STAT?', True)

    @remote
    def toggle_output(self, channel):
        self.set_output(channel, 1 - int(self.get_output(channel)))

    @remote
    def set_width(self, channel, value):
        if 0.01 <= value < self.get_period() * 1e6 - 0.02:
            self._execute(f':PULS:WIDT{channel} {value}US')
        else:
            print('Provided value is out of range')

    @remote
    def get_width(self, channel):
        return self._execute(f':PULS:WIDT{channel}?', True)

    @remote
    def set_delay(self, channel, value):
        if 0. <= value < 999000000 and value <= self.get_period() * 1e6:
            self._execute(f':PULSe:DELay{channel} {value}US')
        else:
            print('Provided value is out of range')

    @remote
    def get_delay(self, channel):
        return self._execute(f':PULS:DEL{channel}?', True)

    @remote
    def set_polarity(self, channel, value):
        if value in ('NORM', 'INV'):
            self._execute(f':OUTP{channel}:POL {value}')
        else:
            print('Invalid polarity value, use NORM|INV')

    @remote
    def get_polarity(self, channel):
        return self._execute(f':OUTP{channel}:POL?', True)

    @remote
    def set_voltageLOW(self, channel, value):
        if -10.0 <= value <= 9.9:
            self._execute(':HOLD VOLT')
            self._execute(f':VOLT{channel}:LOW {value}')
        else:
            print(f'{inspect.stack()[0][3]}: Invalid value or channel: use 1|2')

    @remote
    def get_voltageLOW(self, channel):
        if channel in (1, 2):
            m = self._execute(f':VOLT{channel}:LOW?', True)
            return float(m)
        else:
            print(f'{inspect.stack()[0][3]}: Invalid channel: use 1|2')

    @remote
    def set_voltageHIGH(self, channel, value):
        if -10.0 <= value <= 9.9:
            self._execute(':HOLD VOLT')
            self._execute(f':VOLT{channel}:HIGH {value}')
        else:
            print(f'{inspect.stack()[0][3]}: Invalid value or channel: use 1|2')

    @remote
    def get_voltageHIGH(self, channel):
        if channel in (1, 2):
            m = self._execute(':VOLT' + str(channel) + ':HIGH?', True)
            return float(m)
        else:
            print(f'{inspect.stack()[0][3]}: Invalid channel: use 1|2')

    @remote
    def clear_pattern(self, channel):
        self._execute(f':DIG:PATT:PRES{channel} 0,{max_pattern_bits}')

    # Pattern parameters as delay and two lists consisting of pulse and separation widths in us.
    # Both lists need to have the same length (seperation after the entire pattern must be present
    # but is taken care of)
    @remote
    def alt_set_pulse_sequence(self, channel, pattern_array):
        print("Changing pulse sequence (alt)")
        new_period = self._calculate_new_period(channel, pattern_array)

        new_pattern, adjusted_pattern = self._calculate_patterns(channel, pattern_array, new_period)

        if max(len(new_pattern), len(adjusted_pattern)) > max_pattern_bits:
            print('Patterns too long')
            return

        new_pattern_str = ''.join(new_pattern)
        adjusted_pattern_str = ''.join(adjusted_pattern)

        for i in [1, 2, 3]:
            self.clear_pattern(i)

        self.set_period(new_period)
        self.set_pattern(channel, new_pattern_str)
        self.set_pattern(3 - channel, adjusted_pattern_str)
        self.set_pattern(3, new_pattern_str)

        self._pattern[channel] = self._pattern_str_to_array(new_pattern_str)
        self._pattern[3 - channel] = self._pattern_str_to_array(adjusted_pattern_str)

    def _calculate_patterns(self, channel, pattern_array, new_period):
        old_period = float(self.get_period()) * 1e6
        result = (list(), list())

        for i, val in enumerate(pattern_array):
            n = int(round(val / new_period))
            result[0].extend(['0' if i % 2 == 0 else '1' for j in range(n)])
        result[0].append('0')

        for i, val in enumerate(self._pattern[3 - channel]):
            n = int(round(val * old_period / new_period))
            result[1].extend(['0' if i % 2 == 0 else '1' for j in range(n)])
        result[1].append('0')

        idx = 0 if len(result[0]) < len(result[1]) else 1
        result[idx].extend(['0' for i in range(len(result[1 - idx]) - len(result[idx]))])

        return result

    def _calculate_new_period(self, channel, pattern_array):
        old_period = float(self.get_period()) * 1e6

        def myround(x, base):
            return base * round(x / base)

        old_period = myround(old_period * 1000 + 50, 100) / 1000.

        new_len = sum(pattern_array)
        other_len = sum(self._pattern[3 - channel]) * old_period

        period = float(max(new_len, other_len)) / float(max_pattern_bits)
        return myround(period * 1000 + 50, 100) / 1000.

    def _pattern_str_to_array(self, pattern_str):
        result = [0]
        idx = 0

        if pattern_str[0] == '1':
            result.append(0)
            idx += 1

        for i in range(len(pattern_str) - 1):
            result[idx] += 1
            if pattern_str[i] != pattern_str[i + 1]:
                result.append(0)
                idx += 1

        if len(result) % 2 != 0:
            result.pop()

        return result

    @remote
    def set_pattern(self, channel, pattern_str='01010110011100011110'):
        self._execute('DIG:PATT:DATA' + str(channel) + ' #' + str(len(str(len(pattern_str)))) + str(
            len(pattern_str)) + pattern_str)
        self._execute(':DIGital:PATTern ON')

        self._pattern[channel] = self._pattern_str_to_array(pattern_str)

        print('Pattern set')

    @remote
    def set_pulse_sequence(self, channel, no_of_pulses, sequence_delay_us, width_us, pulses_separation_us, keep_period):
        pattern_array = [sequence_delay_us]
        for i in range(no_of_pulses):
            pattern_array.append(width)
            pattern_array.append(pulses_separation_us)

        pattern_array.pop()
        self.alt_set_pulse_sequence(channel, pattern_array)

    def _get_pattern(self, channel):
        pat = self._execute(':DIG:PATT:DATA' + str(channel) + '?', True)
        oldlen = int(pat[2:int(pat[1]) + 2])
        pat = ['1' if p == '\x01' else '0' for p in pat[-oldlen:]]
        return "".join(pat)

    @remote
    def get_pattern(self, channel):
        return self._pattern[channel]


@include_remote_methods(PulseGenerator81110AWorker)
class PulseGenerator81110A(DeviceOverZeroMQ):

    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget('Pulse Signal Generator', parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QVBoxLayout(parentWidget)

        # sub_layout = QtWidgets.QHBoxLayout(parentWidget)
        #
        # sub_layout.addWidget(QtWidgets.QLabel('Frequency [MHz]'))
        # self.FrequencyDisplay = QtWidgets.QLineEdit(parentWidget)
        #
        # def frequency_edit(event):
        #     if event.button() == 1:
        #         current = float(self.FrequencyDisplay.text())
        #         d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.FrequencyDisplay, 'New value', 'Input [GHz]:',
        #                                                         current, decimals=9)
        #         if ok_pressed:
        #             self.set_frequency(d)
        #
        # self.FrequencyDisplay.mousePressEvent = frequency_edit
        # sub_layout.addWidget(self.FrequencyDisplay)
        #
        # sub_layout.addWidget(QtWidgets.QLabel('Period [μs]'))
        # self.PeriodDisplay = QtWidgets.QLineEdit(parentWidget)
        #
        # def period_edit(event):
        #     if event.button() == 1:
        #         current = float(self.PeriodDisplay.text())
        #         d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.FrequencyDisplay, 'New value', 'Input [μs]:',
        #                                                         current, decimals=3)
        #         if ok_pressed:
        #             self.set_period(d)
        #
        # self.PeriodDisplay.mousePressEvent = period_edit
        # sub_layout.addWidget(self.PeriodDisplay)
        #
        # layout.addLayout(sub_layout)
        sub_layout = QtWidgets.QHBoxLayout(parentWidget)

        self.OutputButton = [None, QtWidgets.QPushButton(parentWidget), QtWidgets.QPushButton(parentWidget)]
        # self.WidthDisplay = [None, QtWidgets.QLineEdit(parentWidget), QtWidgets.QLineEdit(parentWidget)]
        # self.DelayDisplay = [None, QtWidgets.QLineEdit(parentWidget), QtWidgets.QLineEdit(parentWidget)]
        self.PolarityDisplay = [None, QtWidgets.QLineEdit(parentWidget), QtWidgets.QLineEdit(parentWidget)]
        self.voltageLOWDisplay = [None, QtWidgets.QLineEdit(parentWidget), QtWidgets.QLineEdit(parentWidget)]
        self.voltageHIGHDisplay = [None, QtWidgets.QLineEdit(parentWidget), QtWidgets.QLineEdit(parentWidget)]
        self.PatternButton = [None, QtWidgets.QPushButton(parentWidget), QtWidgets.QPushButton(parentWidget)]

        # def width_edit1(event):
        #     if event.button() == 1:
        #         current = float(self.WidthDisplay[1].text())
        #         d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.WidthDisplay[1], 'New value', 'Input [μs]:',
        #                                                         current, decimals=3)
        #         if ok_pressed:
        #             self.set_width(1, d)
        #
        # self.WidthDisplay[1].mousePressEvent = width_edit1
        #
        # def delay_edit1(event):
        #     if event.button() == 1:
        #         current = float(self.DelayDisplay[1].text())
        #         d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.DelayDisplay[1], 'New value', 'Input [μs]:',
        #                                                         current, decimals=3)
        #         if ok_pressed:
        #             self.set_delay(1, d)
        #
        # self.DelayDisplay[1].mousePressEvent = delay_edit1

        def polarity_edit1(event):
            items = ('Normal', 'Inverted')
            current = 0 if self.PolarityDisplay[1].text() == 'Normal' else 1
            if event.button() == 1:
                item, ok_pressed = QtWidgets.QInputDialog.getItem(self.PolarityDisplay[1], 'hange polarity', 'Input:',
                                                                  items, current, False)
                if ok_pressed:
                    self.set_polarity(1, 'NORM' if item == 'Normal' else 'INV')

        self.PolarityDisplay[1].mousePressEvent = polarity_edit1

        def vlow_edit1(event):
            if event.button() == 1:
                current = float(self.voltageLOWDisplay[1].text())
                d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.voltageLOWDisplay[1], 'New value', 'Input [V]:',
                                                                 current, decimals=3)
                if ok_pressed:
                    self.set_voltageLOW(1, d)

        self.voltageLOWDisplay[1].mousePressEvent = vlow_edit1

        def vhigh_edit1(event):
            if event.button() == 1:
                current = float(self.voltageHIGHDisplay[1].text())
                d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.voltageHIGHDisplay[1], 'New value', 'Input [V]:',
                                                                 current, decimals=3)
                if ok_pressed:
                    self.set_voltageHIGH(1, d)

        self.voltageHIGHDisplay[1].mousePressEvent = vhigh_edit1

        # class PatternDialog(QtWidgets.QDialog):
        #     def __init__(self, parent=None):
        #         super().__init__(parent=parent)
        #
        #         self.setWindowTitle('HELLO!')
        #
        #         QBtn = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        #
        #         self.buttonBox = QDialogButtonBox(QBtn)
        #         self.buttonBox.accepted.connect(self.accept)
        #         self.buttonBox.rejected.connect(self.reject)
        #
        #         self.layout = QVBoxLayout()
        #         message = QLabel('Something happened, is that OK?')
        #         self.layout.addWidget(message)
        #         self.layout.addWidget(self.buttonBox)
        #         self.setLayout(self.layout)

        def pattern_edit2(event):
            pass

        self.PatternButton[2].clicked.connect(pattern_edit2)

        # def width_edit2(event):
        #     if event.button() == 1:
        #         current = float(self.WidthDisplay[2].text())
        #         d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.WidthDisplay[2], 'New value', 'Input [μs]:',
        #                                                         current, decimals=3)
        #         if ok_pressed:
        #             self.set_width(2, d)
        #
        # self.WidthDisplay[2].mousePressEvent = width_edit2
        #
        # def delay_edit2(event):
        #     if event.button() == 1:
        #         current = float(self.DelayDisplay[2].text())
        #         d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.DelayDisplay[2], 'New value' 'Input [μs]:',
        #                                                         current, decimals=3)
        #         if ok_pressed:
        #             self.set_delay(2, d)

        # self.DelayDisplay[2].mousePressEvent = delay_edit2

        def polarity_edit2(event):
            items = ('Normal', 'Inverted')
            current = 0 if self.PolarityDisplay[2].text() == 'Normal' else 1
            if event.button() == 1:
                item, ok_pressed = QtWidgets.QInputDialog.getItem(self.PolarityDisplay[2], 'Change polarity', 'Input:',
                                                                  items, current, False)
                if ok_pressed:
                    self.set_polarity(2, 'NORM' if item == 'Normal' else 'INV')

        self.PolarityDisplay[2].mousePressEvent = polarity_edit2

        def vlow_edit2(event):
            if event.button() == 1:
                current = float(self.voltageLOWDisplay[2].text())
                d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.voltageLOWDisplay[2], 'New value', 'Input [V]:',
                                                                 current, decimals=3)
                if ok_pressed:
                    self.set_voltageLOW(2, d)

        self.voltageLOWDisplay[2].mousePressEvent = vlow_edit2

        def vhigh_edit2(event):
            if event.button() == 1:
                current = float(self.voltageHIGHDisplay[2].text())
                d, ok_pressed = QtWidgets.QInputDialog.getDouble(self.voltageHIGHDisplay[2], 'New value', 'Input [V]:',
                                                                 current, decimals=3)
                if ok_pressed:
                    self.set_voltageHIGH(2, d)

        self.voltageHIGHDisplay[2].mousePressEvent = vhigh_edit2

        def pattern_edit2():
            pass

        self.PatternButton[2].clicked.connect(pattern_edit2)

        self.OutputButton[1].clicked.connect(lambda: self.toggle_output(1))
        self.OutputButton[2].clicked.connect(lambda: self.toggle_output(2))

        for i in (1, 2):
            channel_layout = QtWidgets.QVBoxLayout(parentWidget)
            sub_channel_layout = QtWidgets.QHBoxLayout(parentWidget)

            label_button_layout = QtWidgets.QVBoxLayout(parentWidget)

            output_label = QtWidgets.QLabel('Output ' + str(i))
            output_font = QtGui.QFont()
            output_font.setBold(True)
            output_label.setFont(output_font)

            label_button_layout.addWidget(output_label)

            self.OutputButton[i].setCheckable(True)
            label_button_layout.addWidget(self.OutputButton[i])

            sub_channel_layout.addLayout(label_button_layout)
            label_button_layout = QtWidgets.QVBoxLayout(parentWidget)

            label_button_layout.addWidget(QtWidgets.QLabel('Polarity ' + str(i)))
            label_button_layout.addWidget(self.PolarityDisplay[i])

            sub_channel_layout.addLayout(label_button_layout)
            label_button_layout = QtWidgets.QVBoxLayout(parentWidget)

            channel_layout.addLayout(sub_channel_layout)
            sub_channel_layout = QtWidgets.QHBoxLayout(parentWidget)

            label_button_layout.addWidget(QtWidgets.QLabel('LOW ' + str(i) + ' [V]'))
            label_button_layout.addWidget(self.voltageLOWDisplay[i])

            sub_channel_layout.addLayout(label_button_layout)
            label_button_layout = QtWidgets.QVBoxLayout(parentWidget)

            label_button_layout.addWidget(QtWidgets.QLabel('HIGH ' + str(i) + ' [V]'))
            label_button_layout.addWidget(self.voltageHIGHDisplay[i])

            sub_channel_layout.addLayout(label_button_layout)

            channel_layout.addLayout(sub_channel_layout)
            sub_channel_layout = QtWidgets.QHBoxLayout(parentWidget)

            # channel_layout.addWidget(QtWidgets.QLabel('Width ' + str(i) + ' [μs]'))
            # channel_layout.addWidget(self.WidthDisplay[i])
            #
            # channel_layout.addWidget(QtWidgets.QLabel('Delay ' + str(i) + ' [μs]'))
            # channel_layout.addWidget(self.DelayDisplay[i])

            pattern_label = QtWidgets.QLabel('Pattern ' + str(i))
            sub_channel_layout.addWidget(pattern_label)

            sub_channel_layout.addWidget(self.PatternButton[i])

            channel_layout.addLayout(sub_channel_layout)
            sub_layout.addLayout(channel_layout)

        layout.addLayout(sub_layout)

        widget.setLayout(layout)
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

    def updateSlot(self, status):
        # self.FrequencyDisplay.setText(('%.6f' % (float(status['frequency']) * 10 ** -6,)).rstrip('0').rstrip('.'))
        # self.PeriodDisplay.setText(('%.4f' % (float(status['period']) * 10 ** 6,)).rstrip('0').rstrip('.'))
        for i in (1, 2):
            self.OutputButton[i].setChecked(status['output' + str(i)] == '1')
            # self.WidthDisplay[i].setText(
            #     ('%.4f' % (float(status['width' + str(i)]) * 10 ** 6,)).rstrip('0').rstrip('.'))
            # self.DelayDisplay[i].setText(
            #     ('%.4f' % (float(status['delay' + str(i)]) * 10 ** 6,)).rstrip('0').rstrip('.'))
            self.PolarityDisplay[i].setText('Normal' if status['polarity' + str(i)] == 'NORM' else 'Inverted')
            self.voltageLOWDisplay[i].setText(str(status['voltageLOW' + str(i)]).rstrip('0').rstrip('.'))
            self.voltageHIGHDisplay[i].setText(str(status['voltageHIGH' + str(i)]).rstrip('0').rstrip('.'))
