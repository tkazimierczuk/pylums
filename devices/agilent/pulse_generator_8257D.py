from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
from PyQt5 import QtWidgets, QtCore, QtGui
import pyvisa as visa
from time import sleep
from datetime import datetime
import threading
import traceback

default_req_port = 1234
default_pub_port = 2345

# TODO non-fixed units
time_unit_prefixes = ("N", "U", "M", "")

#def convert_value(val, n):
#    exp = int(val[val.find('E')+1:])
#    exp += n
#    return float(val[:val.find('E')+1] + f'{exp:+}')

class PulseGenerator8257DWorker(DeviceWorker):

    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, address="GPIB19", **kwargs):
        super().__init__(req_port = req_port, pub_port = pub_port, **kwargs)
        self._address = address
        
    IGPIB = 0
    IRS232 = 1
    IETHERNET = 2
    
    
    
    def init_device(self):
        print(str(datetime.now()))
        print("Initialization...")
        self._visa_lock = threading.Lock()
        self._connect()
        print("Communication handler created")
        self.clear_status()
        print("Status cleared")
        
        print("End of initialization")
       
    def _connect(self):
        print("Connecting...")
        rm = visa.ResourceManager()
        if self._address[0:3] == 'COM':
            self._interface = PulseGenerator8257DWorker.IRS232
            self.visa_handle = rm.open_resource("ASRL%s::INSTR" % self._address[3:])
            self.visa_handle.set_visa_attribute(visa.constants.VI_ATTR_ASRL_STOP_BITS,
                                                visa.constants.VI_ASRL_STOP_TWO)
        elif self._address[0:4] == 'GPIB':
            self._interface = PulseGenerator8257DWorker.IGPIB
            self.visa_handle = rm.open_resource("GPIB::%s" % self._address[4:])
            self.visa_handle.clear()
            sleep(0.2)
        else:
            self._interface = PulseGenerator8257DWorker.IETHERNET
            self.visa_handle = rm.open_resource("TCPIP::%s::7020::SOCKET" % self._address)
            self.visa_handle.write_termination = '\n'
            
        self.visa_handle.read_termination = '\n'
        self.visa_handle.timeout = 1000
        
        
    @remote
    def status(self):
        d = super().status()
        
        d["output"] = self.get_output()
        d["pulse"] = self.get_pulse()
        d["modulation"] = self.get_modulation()
        d["frequency"] = self.get_frequency()
        d["amplitude"] = self.get_amplitude()
        d["polarity"] = self.get_polarity()
        d["period"] = self.get_period()
        d["width"] = self.get_width()
        d["delay"] = self.get_delay()
        d["delay_step"] = self.get_delay_step()
        err = self.check_error()
        self.local()
        while "No error" not in err:
            print(err)
            err = self.check_error()
        return d
        
    @remote
    def _execute(self, message, responseExpected = False, reply_pattern=None, allowed_retries=5):
        try:
            with self._visa_lock:
                self.visa_handle.write(message)
                if not responseExpected:
                    return None
                res = self.visa_handle.read_raw().decode('ascii')
                self.is_offline = False
        except visa.VisaIOError:
            print(traceback.format_exc())
            if allowed_retries > 0:
                with self._visa_lock:
                    self._connect()
                return self._execute(message, reply_pattern, allowed_retries-1)
            else:
                self.is_offline = True
                res = ''
                warnings.warn("Communication error occured on message: %s" % message, ResourceWarning)
                    
        res = res.strip()
        if reply_pattern:
            if not re.match(pattern=reply_pattern, string=res):
                if allowed_retries > 0:
                    print("Warning! Wrong reply (Q:%s, R:%s)" % (message, res))
                    return self._execute(message, reply_pattern, allowed_retries-1)
                else:
                    raise Exception("Wrong reply (Q:%s, R:%s)" % (message, res))
                    
        return res

    @remote
    def reset(self):
        self._execute("*RST")

    @remote
    def clear_status(self):
        self._execute("*CLS")
        
    @remote
    def local(self):
        self._execute(':SYSTem:COMMunicate:GTLocal')

    @remote
    def set_output(self, value):
        if value in (1, 0):
            self._execute(":OUTPut:STATe " + str(value))
        else:
            print("Invalid output value, use 1|0")

    @remote
    def get_output(self):
        return self._execute(":OUTPut:STATe?", True)
        
    @remote
    def toggle_output(self):
        self.set_output(1 - int(self.get_output()))
        
    @remote
    def set_pulse(self, value):
        if value in (1, 0):
            self._execute(":PULM:STATe " + str(value))
        else:
            print("Invalid output value, use 1|0")

    @remote
    def get_pulse(self):
        return self._execute(":PULM:STATe?", True)
        
    @remote
    def toggle_pulse(self):
        self.set_pulse(1 - int(self.get_pulse()))
        
    @remote
    def set_modulation(self, value):
        if value in (1, 0):
            self._execute(":OUTP:MOD " + str(value))
        else:
            print("Invalid output value, use 1|0")
    
    @remote
    def get_modulation(self):
        return self._execute(":OUTP:MOD?", True)
        
    @remote
    def toggle_modulation(self):
        self.set_modulation(1 - int(self.get_modulation()))
        
#    [:SOURce]:PULM:SOURce:INTernal SQUare|FRUN|TRIGgered|DOUBlet|GATEd
#[:SOURce]:PULM:SOURce:INTernal?
#MWG._execute(':PULM:SOURce:INTernal?')


#[:SOURce]:PULM:SOURce INTernal|EXTernal|SCALar
#[:SOURce]:PULM:SOURce?

    @remote
    def set_frequency(self, value, unit="G"):
        value = float(value)
        if 0.00025 <= value and value <= 67.:
            self._execute(":FREQuency " + str(value) + unit + "HZ")
        else:
            print("Provided value %f is out of range." % (value,))

    @remote
    def get_frequency(self):
        return float(self._execute(":FREQuency?", True))
        
    @remote
    def set_amplitude(self, value):
        value = float(value)
        if -110 <= value:
            self._execute(":POWer:AMPLitude " + str(value) + "DBM")
        else:
            print("Provided value %f is not a number or out of range." % (value,))

    @remote
    def get_amplitude(self):
        return float(self._execute(":POWer:AMPLitude?", True))

    @remote
    def set_polarity(self, value):
        value = "NORM" if (value == 1) else value
        value = "INV" if (value == -1) else value
        if value in ("NORM", "INV"):
            self._execute(":PULM:EXTernal:POLarity  " + str(value))
        else:
            print("Invalid polarity value, use NORM|INV|1|-1")

    @remote
    def get_polarity(self):
        m = self._execute(":PULM:EXTernal:POLarity?", True)
        return 1 if m == 'NORM' else -1 

    @remote
    def set_period(self, value, unit="U"):
        value = float(value)
        if self.get_width() < value and 0.07 <= value and value <= 42000000. and unit in time_unit_prefixes:
            self._execute(":PULM:INTernal:PERiod " + str(value) + unit + "S")
        else:
            print("set_period:Provided value is out of range")

    @remote
    def get_period(self):
        return float(self._execute(":PULM:INTernal:PERiod?", True))

    @remote
    def set_width(self, value, unit="U"):
        value = float(value)
        if True or 0.01 <= value and value < self.get_period()*1e6 - 0.02 and unit in time_unit_prefixes:
            self._execute(":PULM:INTernal:WIDTh " + str(value) + unit + "S")
        else:
            print("set_width:Provided value is out of range")

    @remote
    def get_width(self):
        return float(self._execute(":PULM:INTernal:WIDTh?", True))

    @remote
    def set_delay(self, value, unit="U"):
        value = float(value)
        if 0.07 <= value and value <= self.get_period()*1e6 - self.get_width()*1e6 and unit in time_unit_prefixes:
            self._execute(":PULM:INTernal:DELay " + str(value) + unit + "S")
        else:
            print("Provided value %f is out of range." % (value,))

    @remote
    def get_delay(self):
        return float(self._execute(":PULM:INTernal:DELay?", True))

    @remote
    def set_delay_step(self, value, unit="U"):
        value = float(value)
        if 0.01 <= value and value <= self.get_period() * 1e6 - 0.02 and unit in time_unit_prefixes:
            self._execute(":PULM:INTernal:DELay:STEP " + str(value) + unit + "S")
        else:
            print("Provided value %f is out of range." % (value,))

    @remote
    def get_delay_step(self):
        return float(self._execute(":PULM:INTernal:DELay:STEP?", True))

    @remote
    def inc_delay(self):
        self._execute(":PULM:INTernal:DELay:STEP UP")

    @remote
    def dec_delay(self):
        self._execute(":PULM:INTernal:DELay:STEP DOWN")

    @remote
    def check_error(self):
        return self._execute(":SYSTem:ERRor?", True)


@include_remote_methods(PulseGenerator8257DWorker)
class PulseGenerator8257D(DeviceOverZeroMQ):

    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("MW Signal Generator", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QHBoxLayout(parentWidget)

        sub_layout = QtWidgets.QVBoxLayout(parentWidget)
        
        output_label = QtWidgets.QLabel("Output")
        output_font = QtGui.QFont()
        output_font.setBold(True)
        output_label.setFont(output_font)
        
        sub_layout.addWidget(output_label)
        self.OutputButton = QtWidgets.QPushButton(parentWidget)
        self.OutputButton.setCheckable(True)
        sub_layout.addWidget(self.OutputButton)
        
        sub_layout.addWidget(QtWidgets.QLabel("Pulse"))
        self.PulseButton = QtWidgets.QPushButton(parentWidget)
        self.PulseButton.setCheckable(True)
        sub_layout.addWidget(self.PulseButton)
        
        sub_layout.addWidget(QtWidgets.QLabel("Modulation"))
        self.ModulationButton = QtWidgets.QPushButton(parentWidget)
        self.ModulationButton.setCheckable(True)
        sub_layout.addWidget(self.ModulationButton)

        layout.addLayout(sub_layout)
        sub_layout = QtWidgets.QVBoxLayout(parentWidget)

        sub_layout.addWidget(QtWidgets.QLabel("Frequency [GHz]"))
        self.FrequencyDisplay = QtWidgets.QLineEdit(parentWidget)
        
        def frequency_edit(event):
            if event.button() == 1:
                current = float(self.FrequencyDisplay.text())
                d, okPressed = QtWidgets.QInputDialog.getDouble(self.FrequencyDisplay, "New value", "Input [GHz]:", current, decimals=9)
                if okPressed:
                    self.set_frequency(d)
        self.FrequencyDisplay.mousePressEvent = frequency_edit
        sub_layout.addWidget(self.FrequencyDisplay)

        sub_layout.addWidget(QtWidgets.QLabel("Amplitude [dBm]"))
        self.AmplitudeDisplay = QtWidgets.QLineEdit(parentWidget)
        def amplitude_edit(event):
            if event.button() == 1:
                current = float(self.AmplitudeDisplay.text())
                d, okPressed = QtWidgets.QInputDialog.getDouble(self.AmplitudeDisplay, "New value", "Input [dBm]:", current, decimals=2)
                if okPressed:
                    self.set_amplitude(d)
        self.AmplitudeDisplay.mousePressEvent = amplitude_edit
        sub_layout.addWidget(self.AmplitudeDisplay)
        
        sub_layout.addWidget(QtWidgets.QLabel("Polarity"))
        self.PolarityDisplay = QtWidgets.QLineEdit(parentWidget)
        
        def polarity_edit(event):
            items = ("Normal", "Inverted")
            current = 0 if self.PolarityDisplay.text() == "Normal" else 1
            if event.button() == 1:
                item, okPressed = QtWidgets.QInputDialog.getItem(self.PolarityDisplay, "Change polarity", "Input:", items, current, False)
                if okPressed:
                    self.set_polarity("NORM" if item == "Normal" else "INV")
        self.PolarityDisplay.mousePressEvent = polarity_edit
        sub_layout.addWidget(self.PolarityDisplay)

        sub_layout.setSpacing(0)
        layout.addLayout(sub_layout)
        sub_layout = QtWidgets.QVBoxLayout(parentWidget)

        sub_layout.addWidget(QtWidgets.QLabel("Period [μs]"))
        self.PeriodDisplay = QtWidgets.QLineEdit(parentWidget)
        
        def period_edit(event):
            if event.button() == 1:
                current = float(self.PeriodDisplay.text())
                d, okPressed = QtWidgets.QInputDialog.getDouble(self.FrequencyDisplay, "New value", "Input [μs]:", current, decimals=3)
                if okPressed:
                    self.set_period(d)
        self.PeriodDisplay.mousePressEvent = period_edit        
        sub_layout.addWidget(self.PeriodDisplay)

        sub_layout.addWidget(QtWidgets.QLabel("Width [μs]"))
        self.WidthDisplay = QtWidgets.QLineEdit(parentWidget)
        
        def width_edit(event):
            if event.button() == 1:
                current = float(self.WidthDisplay.text())
                d, okPressed = QtWidgets.QInputDialog.getDouble(self.WidthDisplay, "New value", "Input [μs]:", current, decimals=3)
                if okPressed:
                    self.set_width(d)
        self.WidthDisplay.mousePressEvent = width_edit 
        sub_layout.addWidget(self.WidthDisplay)

        sub_layout.addWidget(QtWidgets.QLabel("Delay [μs]"))
        self.DelayDisplay = QtWidgets.QLineEdit(parentWidget)

        def delay_edit(event):
            if event.button() == 1:
                current = float(self.DelayDisplay.text())
                d, okPressed = QtWidgets.QInputDialog.getDouble(self.DelayDisplay, "New value", "Input [μs]:", current, decimals=3)
                if okPressed:
                    self.set_delay(d)
        self.DelayDisplay.mousePressEvent = delay_edit 
        sub_layout.addWidget(self.DelayDisplay)

        sub_layout.setSpacing(0)
        layout.addLayout(sub_layout)

        widget.setLayout(layout)
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.OutputButton.clicked.connect(lambda: self.toggle_output())
        self.PulseButton.clicked.connect(lambda: self.toggle_pulse())
        self.ModulationButton.clicked.connect(lambda: self.toggle_modulation())
        self.createListenerThread(self.updateSlot)

    def updateSlot(self, status):
        self.OutputButton.setChecked(status["output"] == '1')
        self.PulseButton.setChecked(status["pulse"] == '1')
        self.ModulationButton.setChecked(status["modulation"] == '1')
        self.PolarityDisplay.setText("Normal" if status["polarity"] == 1 else "Inverted")
        self.FrequencyDisplay.setText(('%.9f' % (status["frequency"] * 10**-9,)).rstrip('0').rstrip('.'))
        self.AmplitudeDisplay.setText(('%.4f' % (status["amplitude"] ,)).rstrip('0').rstrip('.'))
        self.PeriodDisplay.setText(('%.4f' % (status["period"]* 10**6,)).rstrip('0').rstrip('.'))
        self.WidthDisplay.setText(('%.4f' % (status["width"]* 10**6,)).rstrip('0').rstrip('.'))
        self.DelayDisplay.setText(('%.4f' % (status["delay"]* 10**6,)).rstrip('0').rstrip('.'))