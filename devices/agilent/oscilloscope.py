from datetime import datetime
import time
from time import sleep
from time import perf_counter as clock
import pyvisa as visa
import threading
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from PyQt5.QtWidgets import (QWidget, QPushButton, 
    QHBoxLayout, QVBoxLayout, QApplication)
from PyQt5 import QtWidgets, QtCore, QtGui
import functools
import numpy as np


default_req_port = 6780
default_pub_port = 6781

class OscilloscopeWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, address="COM1", **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self._address = address
    
    def status(self):
        d = super().status()
        return d
    
    def init_device(self):
        """
        Opens the communication 
        """
        print("=========")
        print(str(datetime.now()))
        print("Agilent DSO: Initialization...")
        self.visa_lock = threading.Lock()
        rm = visa.ResourceManager()
        self.visa_handle = rm.open_resource(self._address)
        self.visa_handle.read_termination = '\r\n'
        mess=self._execute("*IDN?")
        if len(mess.strip()) != 0:
            print(mess.strip())
        print("Agilent DSO: End of initialization")
          
    def _read(self):
        mes = self.visa_handle.read_raw()
        # cannot be done on same line for some reason
        return mes.decode()

    @remote
    def _execute(self, message):
        with self.visa_lock:
            print("Writing: ", str(message))
            self.visa_handle.write(message)
            sleep(70e-3)  # wait for the device to be able to respond
            if message.find('*IDN')<0 and message.find('?')<0:
                return
            print("Reading: ")
            result = self._read()
        if result.find('?') >= 0:
            print("Error: Command %s not recognized: %s" % (message, result))
            return None
        else:
            return result
    
    def close_device(self):
        self.continue_running = False    

    @remote
    def busy(self):
        return self._stage != None and self._stage < 7

    @remote
    def get_waveform(self, channel):
        self._execute("WAV:FORM ASCII")
        data = self._execute("WAV:DATA? CHAN%d" % int(channel))
        data = data[data.find(' '):]
        ys =  np.array(list(map(float, data.split(','))))
        step = float(self._execute(":WAVeform:XINC?  CHAN%d" % int(channel)))
        xs = np.arange(len(ys))* step
        return np.transpose(np.array([xs,ys]))
        
        
    @remote
    def set_time_scale(self, scale):
        self._execute("TIM:MAIN:SCAL %f" % float(scale))
        
    @remote
    def get_time_scale(self):
        return float(self._execute("TIM:MAIN:SCAL?"))
        
    @remote
    def get_voltage_scale(self, channel):
        return float(self._execute("CHAN%d:SCAL?" % int(channel)))
        
    @remote
    def set_voltage_scale(self, channel, scale):
        self._execute("CHAN%d:SCAL %f" % (int(channel), float(scale)))
        
    @remote
    def do_autoscale(self):
        self._execute("AUT")
        
@include_remote_methods(OscilloscopeWorker)
class Oscilloscope(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        

    #def __del__(self):
    #    pass

    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("Agilent DSO", parentWidget)
        widget1 = QtWidgets.QWidget(dock)
        layout = QtWidgets.QHBoxLayout(parentWidget)
        """     
        self.LCD = QtWidgets.QLCDNumber()
        self.LCD.setSegmentStyle(QtWidgets.QLCDNumber.Flat)
        self.LCD.setDigitCount(6)
        self.LCD.setToolTip(self.LCDtext)
        self.Units=QtWidgets.QLabel(self.units[self.LCDtext])     
        self.btn_settings = QtWidgets.QPushButton("Settings")
        self.btn_settings.setToolTip('Opens settings window where you can change temperature controller parameters')
        self.btn_settings.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
        QtWidgets.QSizePolicy.Fixed)
        self.btn_OFF = QtWidgets.QPushButton("heaters OFF!")
        self.btn_OFF.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
        QtWidgets.QSizePolicy.Expanding)
        font=QtGui.QFont()
        font.setPointSize(18)
        self.btn_OFF.setFont(font)
        #self.btn_OFF.setStyleSheet("background-color:rgb(255, 50, 50)")

        layout.addWidget(self.btn_settings)
        layout.addWidget(self.LCD)
        layout.addWidget(self.Units)
        layout.addWidget(self.btn_OFF)
        
        widget1.setLayout(layout)
        dock.setWidget(widget1)
        
        self.btn_settings.clicked.connect(self.show_settings)
        self.btn_OFF.clicked.connect(self.emergency_off)

        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
        
        self.createListenerThread(self.updateSlot)
        """
        
    def updateSlot(self,status):
        #print("slot updated")
        self.LCD.display(status[self.LCDtext])
        self.status_copy = status
                    
    def show_settings(self,status):
        #print("settings")
        self.btn_settings.setEnabled(0)
        if self.setting_window is not None:
            self.setting_window.show()
        else:
            sett_window=SettingsWindow(self)
            sett_window.show()
            def on_finished(code):
                self.btn_settings.setEnabled(True)
            sett_window.exec_()
            self.btn_settings.setEnabled(True)