
import math
import scipy
from PyQt5 import QtWidgets, QtGui, QtCore
import pyqtgraph as pg
import devices
from devices.spectro.spectrometer import Spectrometer
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
import numpy as np
import re
import os
import jsonpickle
import time
import threading
import traceback

default_req_port = 11010
default_pub_port = 11011

class MapWidgetWorker(DeviceWorker):
    _device_name = "PowerTabWorker"
    def __init__(self, parent, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.parent = parent

    def init_device(self):
        return

    def status(self):
        d = super().status()
        return d

    @remote
    def _set_power(self, p, tolerance):
        self.parent.start_set_power_remote(p, tolerance)

    @remote
    def get_points(self):
        return self.parent.generate_map_points_list()

    @remote
    def _get_axis_info(self, name):
        return self.parent.axis_info[self.parent.combos[name].currentIndex() - 1]

    @remote
    def _get_spectrometer_info(self):
        return self.parent.spectrometer_info[self.parent.combos["spectrometer"].currentIndex() - 1]

@include_remote_methods(MapWidgetWorker)
class MapWidgetInterface(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def get_axis(self, name):
        if name not in ["x", "y", "z"]:
            raise Exception("Invalid axis name")
        ip, port, module_name, class_name, axis_name = self._get_axis_info(name)
        exec("from %s import %s" % (module_name, class_name), globals())
        exec('axis = %s(host="%s", req_port=%s).get_axis("%s")' % (class_name, ip, port, axis_name), globals())
        return axis

    def get_x_axis(self):
        return self.get_axis("x")

    def get_y_axis(self):
        return self.get_axis("y")

    def get_z_axis(self):
        return self.get_axis("z")

    def get_spectrometer(self):
        ip, port, module_name, class_name = self._get_spectrometer_info()
        exec("from %s import %s" % (module_name, class_name), globals())
        exec('spectrometer = %s(host="%s", req_port=%s)' % (class_name, ip, port), globals())
        return spectrometer

    def createDock(self, parentWidget, menu=None):
        pass

    def updateSlot(self,status):
        pass

