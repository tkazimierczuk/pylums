# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 18:23:21 2018

@author: tkaz
"""
import math
import scipy
from PyQt5 import QtWidgets, QtGui, QtCore
import pyqtgraph as pg
import devices
from devices.spectro.spectrometer import Spectrometer
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
import numpy as np
import re
import os
import jsonpickle
import time
from numpy import arccos, arctan2, cosh, exp, sqrt, arccosh, arctanh, degrees, log2, radians, tan, arcsin, ceil, e, \
    tanh, arcsinh, log, sin, nan, arctan, cos, floor, log10, pi, sinh
from numpy import pi as Pi
from numpy import pi as PI
from scipy.special import gamma, erf, erfc
import threading
import traceback
from src.widgets import VerticalScrollArea
from queue import Queue

default_req_port = 11000
default_pub_port = 0

class SpectrumTabWorker(DeviceWorker, QtCore.QObject):
    _device_name = "SpectrumTabWorker"
    remote_fit_signal = QtCore.pyqtSignal()
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, parent=None, **kwargs):
        QtCore.QObject.__init__(self)
        DeviceWorker.__init__(self, req_port=req_port, pub_port=pub_port)
        self.timestamp = 0
        self.parameters = {}
        self.lock = threading.Lock()
        self.track = 0
        self.values_dict = {}
        self.fit_result_ready = False
        self.data_ref = None
        self.data_bg = None

    def init_device(self):
        return

    def status(self):
        d = {}
        return d

    @remote
    def fit(self, track=0, init_values_dict={}):
        self.track = track
        self.init_values_dict = init_values_dict
        self.fit_result_ready = False
        self.queue = Queue()
        self.remote_fit_signal.emit()
        if not self.queue.get(block=True, timeout=1.5):
            return ({}, None)
        return (self.parameters, self.pcov)

    @remote
    def get_background(self):
        with self.lock:
            return self.data_bg

    @remote
    def get_reference(self):
        with self.lock:
            return self.data_ref

    @remote
    def get_parameters(self):
        with self.lock:
            return self.parameters

    @remote
    def get_timestamp(self):
        with self.lock:
            return self.timestamp


@include_remote_methods(SpectrumTabWorker)
class SpectrumTabInterface(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def createDock(self, parentWidget, menu=None):
        pass

    def updateSlot(self,status):
        pass

    def wait_for_fit_younger_than(self, seconds):
        while self.get_timestamp() < time.time() - seconds:
            time.sleep(0.01)