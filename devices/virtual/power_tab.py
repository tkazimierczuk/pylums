
import math
import scipy
from PyQt5 import QtWidgets, QtGui, QtCore
import pyqtgraph as pg
import devices
from devices.spectro.spectrometer import Spectrometer
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
import numpy as np
import re
import os
import jsonpickle
import time
import threading
import traceback

default_req_port = 11010
default_pub_port = 11011

class PowerTabWorker(DeviceWorker):
    _device_name = "PowerTabWorker"
    def __init__(self, parent, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.parent = parent

    def init_device(self):
        return

    def status(self):
        d = super().status()
        return d

    @remote
    def _set_power(self, p, tolerance):
        self.parent.start_set_power_remote(p, tolerance)

    @remote
    def get_power(self):
        return self.parent.get_power(ignore_error_negative=True)

    @remote
    def get_power_uw(self):
        return self.parent.get_power(ignore_error_negative=True) * 1000000.

    @remote
    def get_powers_acumulations(self):
        return self.parent.get_points()

    @remote
    def is_stopped(self):
        return self.parent.is_stopped()


@include_remote_methods(PowerTabWorker)
class PowerTabInterface(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def set_power_uw(self, power, tolerance=None, wait = False):
        self.set_power(power / 1000000, tolerance, wait)

    def set_power(self, power, tolerance=None, wait = False):
        self._set_power(power, tolerance)
        if wait:
            while not self.is_stopped():
                time.sleep(0.1)

    def createDock(self, parentWidget, menu=None):
        pass

    def updateSlot(self,status):
        pass

