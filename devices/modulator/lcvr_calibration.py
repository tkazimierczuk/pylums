import numpy as np

#paste calibration data below
calibration_string = """
-0.8333	0
-0.83249	0.25
-0.82634	0.5
-0.8204	0.75
-0.8109	1
-0.76918	1.25
-0.59395	1.5
-0.04879	1.75
0.64301	2
1.30657	2.25
1.88129	2.5
2.36902	2.75
2.76346	3
3.08767	3.25
3.35091	3.5
3.56559	3.75
3.73974	4
3.88184	4.25
4.00197	4.5
4.09924	4.75
4.18859	5
4.26257	5.25
4.32482	5.5
4.38316	5.75
4.43173	6
4.47489	6.25
4.51381	6.5
4.55135	6.75
4.58446	7
4.61632	7.25
4.64468	7.5
4.66964	7.75
4.69228	8
4.71521	8.25
4.73488	8.5
4.75953	8.75
4.77732	9
4.79057	9.25
4.80868	9.5
4.8234	9.75
"""

_calib_data = np.array(list(map(float, calibration_string.split())))
_calib_data = _calib_data.reshape(len(_calib_data)//2, 2)
_voltages = _calib_data[:,1]
_phases = _calib_data[:,0]


def phase2voltage(phase):
    phase = phase % (2*np.pi)
    if phase > max(_phases):
        phase -= np.pi
    return np.interp(phase, _phases, _voltages)
    
def voltage2phase(voltage):
    return np.interp(voltage, _voltages, _phases)