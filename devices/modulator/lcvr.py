# -*- coding: utf-8 -*-
"""
Support for LCVR - Liquid Crystal Variable Retarder from Meadowlarks
"""

from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from .lcvr_calibration import phase2voltage,voltage2phase
from PyQt5 import QtWidgets,QtCore
import threading
import time
import numpy as np

NOT_RUNNING = 0
RUNNING = 1
STOPPING = 2

class LCVRWorker(DeviceWorker):
    """ Worker class for Liquid Crystal Variable Retarder """
    
    
    class WaveformSender:
        """ Since LCVR does not have programmable waveform memory,
        the only way is to continuously update single voltage value.
        This class monitors the serial port and sends consecutive
        voltage values as soon as possible. """
        
        BYTE_TIME = 234.37e-6 # time to transmit a single byte with 38400 baud rate
                
        
        def __init__(self, ser, com_lock):
            self.control = threading.Condition()
            self.state = NOT_RUNNING
            self._ser = ser
            self._com_lock = com_lock
            self.func = lambda t: t//1, 0
            self._prev = 0
                       
        def run(self):
            while True:
                self.control.acquire()
                if self.state == STOPPING or self.state == NOT_RUNNING:
                    self.state = NOT_RUNNING
                    self.control.wait()
                self.control.release()
                while self.state == RUNNING:
                    out_waiting = self.ser.out_waiting
                    if out_waiting > 1:
                        time.sleep(BYTE_TIME*(out_waiting-1))
                    else:
                        self._update_voltage()
                    
        def _update_voltage(self):
            t = time.time() + self.BYTE_TIME * 8 # new voltage will take effect only after ~ 8 bytes
            n,d = self.func(t)
            if n != self._prev:
                with self._com_lock:
                    self.ser.write(b'sout:\r')
                self._prev = n
                return
                
            d = round(v * 6553.5)
            if d < 0:
                d = 0
            if d > 65535:
                d = 65535
            with self._com_lock:
                self.ser.write(b'ld:1,%d\r' % d )
            
        def start_sending(self):
            self.control.acquire()
            self.state = RUNNING
            self.control.notifyAll()
            self.control.release()
            
        def stop_sending(self):
            self.control.acquire()
            self.state = STOPPING
            self.control.release()
    
    
    
    def __init__(self, comport=None, *args, **kwargs):
        """ comport: COM1, COM2, ... """
        super().__init__(*args, **kwargs)
        self._comport = comport
        
            
    def init_device(self):
        """ Initializes connection with the device """
        import serial
        self.ser = serial.Serial(self._comport, 38400, timeout=0.5)
        self._com_lock = threading.Lock()
        self._voltage = self.read_voltage()
        self._wthread = self.WaveformSender(self.ser, self._com_lock)
        self._wthread.daemon = True
        self._wthread.start()
        
    def _send_msg(self, msg, read_reply=False):
        with self._com_lock:
            if read_reply:
                self.ser.reset_input_buffer()
            self.ser.write(msg.encode('ascii')+'\r')
            if read_reply:
                buf = b''
                try:
                    c = self.ser.read()
                    while c != b'\r' and len(buf) < 100:
                        buf += c
                        c = self.ser.read()
                    return buf
                except:
                    pass
        
    def status(self):
        d = super().status()
        d["voltage"] = self.voltage()
        d["phase"] = voltage2phase(d["voltage"])
        return d
    
    @remote
    def set_voltage(self, v):
        d = round(v * 6553.5)
        if d < 0:
            d = 0
        if d > 65535:
            d = 65535
        self._send_msg('ld:1,%d' % d )
        self._voltage = self.read_voltage()
    
    @remote
    def set_phase(self, p):
        self.set_voltage(phase2voltage(p))
        
    @remote
    def read_voltage(self):
        """ This function reads the voltage from the device. Usually you
        should use voltage() method which uses cached values"""
        try:
            v =  int(self._send_msg('ld:1?', read_reply=True)) / 6553.5
            self._voltage = v
            return v
        except:
            return np.nan
        
    @remote
    def voltage(self):
        return self._voltage
    
    @remote
    def start_modulation(self):
        self._wthread.start_sending()
    
    @remote
    def stop_modulation(self):
        self._wthread.stop_sending()
    
    
@include_remote_methods(LCVRWorker)
class LCVR(DeviceOverZeroMQ):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        
    def createDock(self, parentWidget, menu=None):
        """ Function for integration in GUI app. Implementation below 
        creates a button and a display """
        widget = QtWidgets.QWidget(parentWidget)
        dock = QtWidgets.QDockWidget("LCVR", parentWidget)
        layout = QtWidgets.QHBoxLayout(parentWidget)
        widget.setLayout(layout)
        
        self.voltageDisplay = QtWidgets.QDoubleSpinBox(parentWidget)
        layout.addWidget(self.voltageDisplay)
        
        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())
            
        self.createListenerThread(self.updateSlot)

        
    def updateSlot(self, status):
        """ This function receives periodic updates from the worker """
        try:
            self.voltageDisplay.setValue(status["voltage"])
        except:
            print("Error while updating LCVR GUI")