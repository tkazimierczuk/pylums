# -*- coding: utf-8 -*-

from PyQt5 import QtCore
import zmq
import json
from . import device
import numpy as np
import base64
import threading
import weakref
import time
import sys
import traceback

""" Constants describing status of a device in the experiment. If the connection fails, query for the crucial device
will be repeated until the answer is received. Optional devices will simply issue ConnectionError exception.

see also: DeviceOverZeroMQ.failure_policy
"""
RETRY_ON_FAILURE_EXCEPT_STATUS = 0
RETRY_ON_FAILURE = 1
EXCEPTION_ON_FAILURE = 2



def remote(func):
    """ Decorator for methods that should be available over the Ethernet
    
    The function is not changed immediately, but only marked for later use.
    """
    func.accessible_remotely = True
    return func


def _makeFun(method_name):
    """ Creates a delegated function. """
   
    def single_call(self, *args, **kwargs):
        obj = (method_name, args, kwargs)
        msg = json.dumps(obj, cls=ArrayEncoder).encode('ascii')
        with self.req_lock:
            try:
                self._client.send(msg)
            except zmq.ZMQError: # we try to reconnect, but only once
                # Timeout.
                # Socket is confused. Close and reopen to reset its state.
                try:
                    self._client.setsockopt(zmq.LINGER, 0)
                except:
                    pass
                self._client.close()
                self.poll.unregister(self._client)
                self._client = context.socket(zmq.REQ)
                self._client.connect(self._channel)
                self._poll.register(self._client, zmq.POLLIN)
                self._client.send(msg)
                print("Reconnecting in zeromq_device worked! Remove this message and commit to the repository")

            socks = dict(self._poll.poll(self.request_timeout))
            if socks.get(self._client) == zmq.POLLIN:
                return self._client.recv_json(object_hook=array_object_hook)
            else:
                # Timeout.
                # Socket is confused. Close and reopen to reset its state.
                self._client.setsockopt(zmq.LINGER, 0)
                self._client.close()
                self._poll.unregister(self._client)
                self._client = context.socket(zmq.REQ)
                self._client.connect(self._channel)
                self._poll.register(self._client, zmq.POLLIN)
                raise ConnectionError
                
    def fun(self, *args, **kwargs):
        while True:
            try:
                return single_call(self, *args, **kwargs)
            except ConnectionError:
                if self.failure_policy == RETRY_ON_FAILURE \
                   or (self.failure_policy == RETRY_ON_FAILURE_EXCEPT_STATUS \
                       and method_name != 'status'):
                    print("Problem with connection, retrying...")
                else:
                    raise
                
    return fun

def _makeGetter(param_name):
    fun = _makeFun('__getattribute__')
    def getter(self):
        return fun(self, param_name)
    return getter
    
def _makeSetter(param_name):
    fun = _makeFun('__setattr__')
    def setter(self, value):
        return fun(self, param_name, value)
    return setter
    
    
def include_remote_methods(worker_class):
    """ Decorator for class to import all methods with @remote decorator """
    def decorator(frontend_class):
        for name in dir(worker_class):
            try:
                local_func = getattr(worker_class,name)
                if type(local_func) == property:
                    fun = property()
                    if local_func.fget is not None:
                        fun = fun.getter(_makeGetter(name))
                    if local_func.fset is not None:
                        fun = fun.setter(_makeSetter(name))                           
                    setattr(frontend_class, name, fun)
                try:
                    if local_func.accessible_remotely:
                        fun = _makeFun(name)
                        fun.__doc__ = local_func.__doc__
                        setattr(frontend_class, name, fun)
                except:
                    pass
            except IOError:
                pass
        return frontend_class
    return decorator

context = zmq.Context()

class ArrayEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return {"dtype": obj.dtype.str,
                    "shape": obj.shape,
                    "data": base64.b64encode(obj.tostring()).decode('ascii')}
        elif np.issubdtype(type(obj), np.integer):
            return int(obj)
        elif np.issubdtype(type(obj), np.floating):
            return float(obj)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)

def array_object_hook(d):
    try:
        a = np.frombuffer(base64.standard_b64decode(d["data"]),
                          dtype=d["dtype"])
        a = a.reshape(d["shape"])
        return a
    except:
        return d
         
from functools import wraps

def buffered_when_busy(func):
    @wraps(func)
    def buf_func(self, *args, **kwargs):
        if self._is_busy():
            try:
                return func._last_result
            except AttributeError:
                return None
        else:
            result = func(self, *args, **kwargs)
            func._last_result = result
            return result
    return buf_func

def background_job(func,policy='normal'):
    @wraps(func)
    def bg_func(self, *args, **kwargs):
        if not hasattr(self, '_background_jobs_list'):
            self._start_background_thread()
        with self._background_cond:
            self._background_jobs_list.append( (func, (self,)+args, kwargs) )
            self._background_cond.notify()
    return bg_func


class DeviceWorker:
    def __init__(self, req_port=0, pub_port=None, refresh_rate=0.2):
        if not req_port or pub_port is None:
            print("Ports not specified for class: %s" % self.__class__.__name__)
        self.REQchannel = "tcp://*:" + str(req_port)
        self.use_PUBchannel = (pub_port != 0)
        self.PUBchannel = "tcp://*:" + str(pub_port)
        self.REPchannel = "tcp://localhost:"+str(req_port)
        self.refresh_rate = float(refresh_rate)
        super().__init__()
        self.access_lock = threading.Lock() # general lock for hardware exclusiveness
        self.notifier_lock = threading.Lock() # obtain this lock before sending via pub_channel
        self._background_running = False
               
        
    def _start_background_thread(self):
        self._background_jobs_list = []
        self._background_cond = threading.Condition()
        
        cond = self._background_cond
        job_list = self._background_jobs_list
        selfref = weakref.ref(self)
        def worker():
            while True:
                with cond:
                    while len(selfref()._background_jobs_list) == 0:
                        cond.wait()
                    func, args, kwargs = job_list.pop(0)
                    self = selfref()
                    if self is None:
                        return
                    self._background_running = True
                if func is None:
                    print("Background exiting")
                    return
                print("Background job starting")
                func(*args, **kwargs)
                print("Background job done")
                with self._background_cond:
                    self._background_running = False
                del(func, args, kwargs, self)
        _background_thread = threading.Thread(target=worker)
        _background_thread.daemon = True
        _background_thread.start()
        
    def _stop_background_thread(self):
        try:
            with self._background_cond:
                self._background_jobs_list.append( (None, None, None) )
                self._background_cond.notify()
        except:
            pass
        
    def __del__(self):
        self._stop_background_thread()

    @remote
    def status(self):
        return {}

    def init_device(self):
        pass
    
    def close_device(self):
        pass
    
    def _is_busy(self):
        """Returns true if background thread is busy. Please note the difference
        between the background thread being busy and the physical device 
        being busy."""
        return self._background_running

    def send_via_pubchannel(self, topic, obj, extra_raw_data=None):
        """Sends data over the PUB channel.
           topic (bytes) - the topic identifying the data
           obj - any JSON-encodable Python object
           extra_raw_data (bytes) - optional data that will be sent without JSON encoding"""
        msg = json.dumps(obj, cls=ArrayEncoder).encode('ascii')
        try:
            with self.notifier_lock:
                if extra_raw_data is not None:
                    self.notifier.send_multipart([topic, msg, extra_raw_data])
                else:
                    self.notifier.send_multipart([topic, msg])
        except AttributeError: # occurs if server is not started yet
            pass
        

    def start_server(self, blocking=False):
        """ Starts to listen for requests over TCP/IP connection. Also starts
        broadcasting the status in the regular intervals.
        
        Caution: once the server has started you need to be careful if you
        continue to access the object directly. To stay on the safe side
        use the following idiom for direct access:
        >>> with device_worker.access_lock:
                result = device_worker.method()
        """
        context = zmq.Context(1)

        if self.use_PUBchannel:
            self.notifier = context.socket(zmq.PUB)
            self.notifier.bind(self.PUBchannel)
        
        def reminder_func():
            reminder = context.socket(zmq.REQ)
            reminder.connect(self.REPchannel)
            try:
                while True:
                    reminder.send_json( ("status", None, None))
                    reminder.recv_json()
                    time.sleep(self.refresh_rate)
            except zmq.error.ContextTerminated:
                print("Safely closing")
                reminder.close()
                pass
            except:
                reminder.close()
                raise
        
        self.polling_thread = threading.Thread(target=reminder_func)
        self.polling_thread.daemon = True
        self.polling_thread.start()
        
        def listen_func():
            server = context.socket(zmq.REP)
            server.bind(self.REQchannel)
            while True:
                request = server.recv_json()
                with self.access_lock:
                    if request[0] == '_kill_worker':
                        break
                    if request[0] == "status":
                        msg = json.dumps(self.status(), cls=ArrayEncoder).encode('ascii')
                        server.send(msg)
                        with self.notifier_lock:
                            if self.use_PUBchannel:
                                self.notifier.send_multipart([b"status", msg])
                        continue            

                    try:
                        f = getattr(self, request[0])
                        args = request[1]
                        kwargs = request[2]
                        result = f(*args, **kwargs)
                        msg = json.dumps(result, cls=ArrayEncoder).encode('ascii')
                        server.send(msg)
                    except SystemExit:
                        print("Terminating!")
                        break
                    except Exception as e:
                        print("Exception: ", traceback.format_exc())
                        server.send_json("ERROR processing request")
            server.close()
            self.notifier.close()
            context.term()        
        
        if not blocking:
            self.listening_thread = threading.Thread(target=listen_func)
            self.listening_thread.daemon = True
            self.listening_thread.start()
        else:
            listen_func()
                        
                        
class DeviceWorkerCollection:
    """ A collection of worker devices which have to operate in a single process,
    e.g. because they access the same serial port on Windows.
    Example entry configuration file:
    [oxford_collection]
    class=zeromq_device.DeviceWorkerCollection
    name="Oxford Instruments Devices"
    devices=itc,ilm
    itc_class=...
    itc_pub_port=...
    ...
    ilm_class=...
    ilm_....
    """
    
    def __init__(self, devices, **kwargs):
        """ Input:
            devices (string): comma separated list of device names
            kwargs: arguments that should be passed on to the devices
                    Each argument should begin with XXX_, where XXX
                    is a name of one of the devices """
        
        device_list = devices.strip().split(',')
        if not device_list:
            print("Device list cannot be empty", file=sys.stderr)
            raise ValueError
        self.devices = {}
        for dev in device_list:
            try:
                DeviceClass = device.load_worker_class(kwargs[dev+'_class'])
                self.devices[dev] = (DeviceClass, {})
            except KeyError:
                print("No class specified for device: %s" % dev, file=sys.stderr)
                raise
        
        for arg,value in kwargs.items():
            try:
                dev,real_arg = arg.split('_', 1)
                if real_arg == "class" or real_arg == "name":
                    continue
                if dev in self.devices:
                    self.devices[dev][1][real_arg] = value
                else:
                    raise ValueError()
            except ValueError:
                print("Configuration error in argument: %s" % arg, file=sys.stderr)
                raise
        
    def init_device(self):
        self.active_devices = [DeviceClass(**kwargs) for DeviceClass,kwargs in self.devices.values()]
        for dev in self.active_devices[:]:  # we iterate over copy ([:]) since the list is modified inside
            try:
                dev.init_device()
            except Exception:
                print("Device %s failed to start" % str(dev))
                self.active_devices.remove(dev)
        
    def start_server(self, blocking=False):
        for dev in self.active_devices[:-1]:
            dev.start_server(blocking=False)
        self.active_devices[-1].start_server(blocking=blocking)




@include_remote_methods(DeviceWorker)
class DeviceOverZeroMQ(device.Device):
    def __init__(self, req_port, pub_port=None, host="localhost"):           
        self._thread = {}
        self._channel = "tcp://"+host+":"+str(req_port)
        self._client = context.socket(zmq.REQ)
        self._client.connect(self._channel)
        self._poll = zmq.Poller()
        self._poll.register(self._client, zmq.POLLIN)
        self.request_timeout = 2000 # 2s in milliseconds
        if pub_port:
            pub_channel = "tcp://"+host+":"+str(pub_port)
            self._pub_thread = DeviceOverZeroMQ.ListenerThread(pub_channel)
            self._pub_thread.start()
        else:
            self._pub_thread = None
        self.req_lock = threading.Lock()
        self._req_port = req_port
        self._pub_port = pub_port
    
    
    def __del__(self):
        # try to stop the PUB thread (if exists)
        try:
            self._pub_thread.socket.close()
        except:
            pass
    
    failure_policy = EXCEPTION_ON_FAILURE # determines what should happen if the remote worker fails
    
    class ListenerThread(QtCore.QThread):
        """ A thread that will receive messages broadcasted over PUB channel. """
        def __init__(self, pub_channel, parent=None):
            QtCore.QThread.__init__(self, parent)
            self.socket = context.socket(zmq.SUB)
            self.socket.connect(pub_channel)
            self.socket.setsockopt(zmq.SUBSCRIBE, b'quit')
            self.socket.setsockopt(zmq.SUBSCRIBE, b'status')
            
            self.msg_signals = {}
            
        def run(self):
            while True:
                try:
                    full_msg = self.socket.recv_multipart()
                except zmq.error.ZMQError:
                    return

                topic = full_msg[0]
                if topic==b'quit':
                    print("Quitting")
                    return

                msg = full_msg[1].decode('ascii')
                try:
                    payload = json.loads(msg, object_hook=array_object_hook)
                except:
                    print("Status: ", full_msg)
                    raise
                
                if topic==b'status':
                    self.last_status = (time.time(), payload)
                    
                if len(full_msg) > 2:
                    payload = (payload, full_msg[2])
                
                if topic in self.msg_signals:
                    self.msg_signals[topic].signal.emit(payload)
                
        def connect_slot(self, topic, slot):
            if topic not in self.msg_signals:
                emitter = DeviceOverZeroMQ.Emitter(self)
                emitter.moveToThread(self)
                self.msg_signals[topic] = emitter
                self.socket.setsockopt(zmq.SUBSCRIBE, topic)
            self.msg_signals[topic].signal.connect(slot)


    def status_cached(self, timestamp=None, timeout=None, ask_if_not_available=True):
        """ Returns the last status if it is still fresh
        
            timestamp: if given status will not be older than this point
            timeout(seconds): if given status will not be older than this value
            ask_if_not_available(bool): determines what happens when no cached
                                     status is available. If True then status()
                                     is invoked. If False then {} is returned.
        """
        try:
            t0,cached = self._pub_thread.last_status
            ok = True
            if timestamp is not None and timestamp < t0:
                ok = False
            if timeout is not None and time.time()-t0 > timeout:
                ok = False
            if ok:
                return cached
        except:
            pass
        
        if ask_if_not_available:
            return self.status()
        else:
            return {}
            
    class Emitter(QtCore.QObject):
        signal = QtCore.pyqtSignal(object)
                         
            
    def createListenerThread(self, updateSlot, topic=b'status'):
        try:
            self._pub_thread.connect_slot(topic, updateSlot)
        except:
            print("Error: cannot connect listener slot")

    def _kill_worker(self):
        """Stops the Worker."""
        obj = ("_kill_worker", (), {})
        msg = json.dumps(obj).encode('ascii')
        with self.req_lock:
            self._client.send(msg)