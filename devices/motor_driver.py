from time import sleep
from time import perf_counter as clock
import serial
import serial.tools.list_ports
import logging
from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
import threading
import traceback
from PyQt5 import QtCore, QtWidgets, QtGui
import pyvisa as visa
import io
import re
from devices.axis import Axis


default_req_port = 19040
default_pub_port = 19041


class MotorDriverWorker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def init_device(self):
        pass

    def status(self):
        d = super().status()
        return d

    @remote
    def is_stopped(self, axis):
        pass

    @remote
    def get_position(self, axis):
        pass

    @remote
    def axes(self):
        pass

    @remote
    def move_absolute(self, axis, position):
        pass

    @remote
    def move_velocity(self, axis, velocity):
        pass

    @remote
    def stop(self, axis):
        pass

    @remote
    def stop_all(self):
        pass

    @remote
    def move_relative(self, axis, distance):
        pass

    @remote
    def home(self, axis):
        pass

    @remote
    def set_velocity(self, axis, val): # velocity used in move to postion mode
        pass


@include_remote_methods(MotorDriverWorker)
class MotorDriver(DeviceOverZeroMQ):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def __del__(self):
        pass

    def get_axis(self, axis):
        return Axis(self, axis)

    def createDock(self, parentWidget, menu=None):
        self.parentWidget = parentWidget
        dock = QtWidgets.QDockWidget("Motor", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QVBoxLayout(parentWidget)
        layout.setSpacing(2)
        widget.setLayout(layout)

        self.axis_widgets = {axis: self.createWidgetForAxis(layout, axis)
                             for axis in self.axes()}

        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

    def createWidgetForAxis(self, layout, axis):
        hlayout = QtWidgets.QHBoxLayout(layout.parent())
        label = QtWidgets.QLabel("Axis " + str(axis))
        hlayout.addWidget(label)
        lineedit = QtWidgets.QLineEdit()
        hlayout.addWidget(lineedit)
        home_button = QtWidgets.QPushButton("Home")
        home_button.setFixedWidth(50)
        home_button.setFixedHeight(22)
        def home_clicked():
            clicked_button = QtGui.QMessageBox.question(self.parentWidget,'', "Are you sure you want to perform homing procedure?",
                                                   QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
            if clicked_button == QtGui.QMessageBox.Yes:
                self.home(axis)
        home_button.clicked.connect(home_clicked)
        hlayout.addWidget(home_button)
        hlayout.setContentsMargins(0, 0, 0, 0)
        layout.addLayout(hlayout)

        def on_click(event):
            if event.button() == 1:
                current = self.get_position(axis)
                d, okPressed = QtWidgets.QInputDialog.getDouble(lineedit, "Go to", "Target:", current, decimals=4)
                if okPressed:
                    self.move_absolute(axis, d)
        lineedit.mousePressEvent  = on_click
        return (lineedit,)

    def updateSlot(self, status):
        try:
            for axis in self.axis_widgets:
                self.axis_widgets[axis][0].setText("%4.4f" % status["axis%d_pos" % axis])
        except Exception as e:
            print(e)



