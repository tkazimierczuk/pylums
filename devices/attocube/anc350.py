from PyQt5 import QtWidgets,QtCore

import ctypes as ct
from devices.zeromq_device import DeviceWorker,DeviceOverZeroMQ,remote,include_remote_methods
from devices import Parameter
from PyQt5 import QtWidgets, QtCore, QtGui
import time
from devices.axis import Axis
from collections import defaultdict

import os
import pathlib
try:
    os.add_dll_directory(pathlib.Path().absolute().as_posix() + "/devices/attocube/") # Python 3.8+
except AttributeError:
    os.environ["PATH"] = pathlib.Path().absolute().as_posix() + "/devices/attocube/" + os.pathsep + os.environ["PATH"]


default_req_port = 7006
default_pub_port = 7007


class ANC350Worker(DeviceWorker):
    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.connected = False
        self.axesList = []
        self.axisDirection = {}
        self.default_velocity = 500
        self.velocity = defaultdict(lambda: int(self.default_velocity))

    def status(self):
        d = super().status()
        d["connected"] = self.connected
        for i in self.axes():
            d["axis%d_pos" % i] = self.get_position(i)
        return d

    def init_device(self):
        self.dll = ct.windll.LoadLibrary("hvpositionerv2.dll")
        if self.connected:
            return
        class positionerinfo(ct.Structure):
            _fields_ = [("id", ct.c_int32), ("locked", ct.c_bool)]
        mem = ct.POINTER(ct.POINTER(positionerinfo))()
        count = self.dll.PositionerCheck(ct.byref(mem))
        if count < 1:
            return Exception("No Attocube controllers found", \
                                "No ANC350 controllers found")
        self.handle = ct.c_int32()
        ret = self.dll.PositionerConnect(0, ct.byref(self.handle))
        if ret != 0:
            return Exception("Attocube:PositionerConnect", "Function failed")
        self.connected = True

        for axis in range(9):
            self.dll.PositionerStopDetection(self.handle, ct.c_int32(axis), \
                                             ct.c_bool(False))
        time.sleep(0.5)
        for axis in range(9):
            if self.get_position(axis) != 0:
                self.axisDirection[axis] = 0
                self.axesList.append(axis)
        print("Following axes are now enabled: ", self.axes())

    @remote
    def axes(self):
        return self.axesList

    @remote
    def disconnect(self):
        if not self.connected:
            return
        for i in range(3):
            self.disable_axis(i)
        self.dll.PositionerClose(self.handle)
        self.connected = False

    @remote
    def enable_axis(self, axis):
        self.dll.PositionerSetOutput(self.handle, ct.c_int32(axis), True)

    @remote
    def disable_axis(self, axis):
        self.dll.PositionerSetOutput(self.handle, ct.c_int32(axis), False)

    @remote
    def move_relative(self, axis, steps):
        """Number of steps can be positive or negative"""
        d = ct.c_int32(0 if steps > 0 else 1)
        for i in range(abs(round(steps))):
            self.dll.PositionerMoveSingleStep(self.handle, ct.c_int32(axis), d)
            time.sleep(0.002)

    @remote
    def move_absolute(self, axis, target, wait=False):
        self.set_frequency(axis, self.velocity[axis])
        self.dll.PositionerMoveAbsolute(self.handle, ct.c_int32(axis), \
                                        ct.c_int32(int(target*1000)), ct.c_int32(0))

    @remote
    def set_velocity(self, axis, velocity):
        self.velocity[axis] = velocity

    @remote
    def stop(self, axis):
        self.move_relative(axis, 1)  # Hack. Stop function from dll didn't seem to work.
        self.move_relative(axis, -1)
        
    @remote
    def move_velocity(self, axis, frequency):
        if frequency == 0:
            self.stop(axis)
            self.axisDirection[axis] = 0
            return
        if frequency < 0:
            frequency = -frequency
            dir = 1
        else:
            dir = 0
        self.dll.PositionerFrequency(self.handle, ct.c_int32(axis), ct.c_int32(round(frequency)))
        if 1 - 2 * dir != self.axisDirection[axis]:
            self.dll.PositionerMoveContinuous(self.handle, ct.c_int32(axis), ct.c_int32(dir))
        self.axisDirection[axis] = 1 - 2 * dir

    @remote
    def set_frequency(self, axis, frequency):
        self.dll.PositionerFrequency(self.handle, ct.c_int32(axis), ct.c_int32(round(frequency)))

    @remote
    def move_continous(self, axis, dir):
        self.dll.PositionerMoveContinuous(self.handle, ct.c_int32(axis), ct.c_int32(dir))

    @remote
    def get_position(self, axis):
        if not self.connected:
            return 0
        pos = ct.c_int32()
        self.dll.PositionerGetPosition(self.handle, ct.c_int32(axis), \
                                      ct.byref(pos))
        return pos.value / 1000.

    @remote
    def get_status(self, axis):
        if not self.connected:
            return 0
        status = ct.c_int32()
        self.dll.PositionerGetStatus(self.handle, ct.c_int32(axis), \
                                       ct.byref(status))
        return status.value

    @remote
    def is_stopped(self, axis):
        raise NotImplementedError

    @remote
    def get_name(self, axis): # TODO: read axis names from config file like in stepper
        try:
            return self.axis_names[self.axis_by_name[axis]]
        except:
            return str(axis)

@include_remote_methods(ANC350Worker)
class ANC350(DeviceOverZeroMQ):

    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

    def __del__(self):
        pass

    def get_axis(self, axis):
        return Axis(self, axis)

    def createDock(self, parentWidget, menu=None):
        dock = QtWidgets.QDockWidget("Attocube ANC350", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QVBoxLayout(widget)
        layout.setSpacing(2)
        widget.setLayout(layout)
        self.axis_widgets = {axis: self.createWidgetForAxis(layout, axis)
                        for axis in self.axes()}

        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

    def createWidgetForAxis(self, layout, axis):
        widget = QtWidgets.QWidget(layout.parent())
        hLayout = QtWidgets.QHBoxLayout(widget)
        hLayout.setContentsMargins(0, 0, 0, 0)
        label = QtWidgets.QLabel("Axis " + str(axis))
        hLayout.addWidget(label)
        lineedit = QtWidgets.QLineEdit()
        lineedit.setFixedWidth(52)
        hLayout.addWidget(lineedit)
        layout.addWidget(widget)
        return (lineedit,)

    def updateSlot(self, status):
        for axis in self.axis_widgets:
            self.axis_widgets[axis][0].setText("%4.2f" % status["axis%d_pos" % axis])

            
            
        
if __name__ == '__main__':
    import sys
    def run_app():
        app = QtWidgets.QApplication(sys.argv)
        window = QtWidgets.QMainWindow()
        window.show()
        app.exec_()
    run_app()
