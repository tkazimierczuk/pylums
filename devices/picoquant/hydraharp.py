# -*- coding: utf-8 -*-
"""
Support for PicoQuant HydraHarp 400 via HHLIB.DLL v 3.0.
Based on Keno Goertz, PicoQuant GmbH, February 2018
"""

import time
import ctypes as ct
from ctypes import byref
import os
import sys

# From hhdefin.h
LIB_VERSION = "3.0"
MAXDEVNUM = 8
MODE_HIST = 0
MAXLENCODE = 6
HHMAXINPCHAN = 8
MAXHISTLEN = 65536
FLAG_OVERFLOW = 0x001

SYNCDIVMIN = 1
SYNCDIVMAX = 16

ZCMIN = 0  # mV
ZCMAX = 40  # mV
DISCRMIN = 0  # mV
DISCRMAX = 1000  # mV

CHANOFFSMIN = -99999  # ps
CHANOFFSMAX = 99999  # ps

OFFSETMIN = 0  # ps
OFFSETMAX = 500000  # ns
ACQTMIN = 1  # ms
ACQTMAX = 360000000  # ms  (100*60*60*1000ms = 100h)

STOPCNTMIN = 1
STOPCNTMAX = 4294967295  # //32 bit is mem max

# new from v1.2: after getting the count rates you can check for warnings
# tryfunc(hhlib.HH_GetWarnings(ct.c_int(dev[0]), byref(warnings)), "GetWarnings")
# if warnings.value != 0:
#    hhlib.HH_GetWarningsText(ct.c_int(dev[0]), warningstext, warnings)
#    print("\n\n%s" % warningstext.value.decode("utf-8"))

# tryfunc(hhlib.HH_SetStopOverflow(ct.c_int(dev[0]), ct.c_int(0), ct.c_int(10000)),\
#        "SetStopOverflow") # for example only

#    tryfunc(hhlib.HH_ClearHistMem(ct.c_int(dev[0])), "ClearHistMem")

# here you could check for warnings again


# ctcstatus = ct.c_int(0)
# while ctcstatus.value == 0:
#    tryfunc(hhlib.HH_CTCStatus(ct.c_int(dev[0]), byref(ctcstatus)),\
#            "CTCStatus")

#
#        integralCount = 0
#        for j in range(0, histLen.value):
#            integralCount += counts[i][j]
#
#        print("  Integralcount[%1d]=%1.0lf" % (i,integralCount))
#
#    tryfunc(hhlib.HH_GetFlags(ct.c_int(dev[0]), byref(flags)), "GetFlags")
#
#    if flags.value & FLAG_OVERFLOW > 0:
#        print("  Overflow.")
#
# for j in range(0, histLen.value):
#    for i in range(0, numChannels.value):
#        outputfile.write("%5d " % counts[i][j])
#    outputfile.write("\n")


from devices.zeromq_device import DeviceWorker, DeviceOverZeroMQ, remote, include_remote_methods
from PyQt5 import QtWidgets, QtCore
import numpy as np

default_req_port = 8886
default_pub_port = 8887


class HydraHarpWorker(DeviceWorker):
    """ Simple stub for the class directly communicating with the hardware """

    def __init__(self, req_port=default_req_port, pub_port=default_pub_port,
                 **kwargs):

        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)

        # Variables to store information read from DLLs
        self._mem_counts = [(ct.c_uint * MAXHISTLEN)() for i in range(0, HHMAXINPCHAN)]
        self._mem_histLen = ct.c_int()
        self._mem_syncRate = ct.c_int()
        self._mem_countRate = ct.c_int()
        self._mem_flags = ct.c_int()
        self._mem_warnings = ct.c_int()
        self._mem_warningstext = ct.create_string_buffer(b"", 16384)

    def init_device(self):
        """ This function will be called once upon starting the process """
        if os.name == "nt":
            self.hhlib = ct.WinDLL("hhlib.dll")
        else:
            self.hhlib = ct.CDLL("libhh400.so")

        libVersion = ct.create_string_buffer(b"", 8)
        self.hhlib.HH_GetLibraryVersion(libVersion)
        print("Library version is %s" % libVersion.value.decode("utf-8"))
        if libVersion.value.decode("utf-8") != LIB_VERSION:
            print("Warning: The application was built for version %s" % LIB_VERSION)

        print("\nSearching for HydraHarp devices...")
        print("Devidx     Status")

        hwSerial = ct.create_string_buffer(b"", 8)

        self._dev = None
        for i in range(0, MAXDEVNUM):
            retcode = self.hhlib.HH_OpenDevice(ct.c_int(i), hwSerial)
            if retcode == 0:
                print("  %1d        S/N %s" % (i, hwSerial.value.decode("utf-8")))
                self._dev = ct.c_int(i)

        # We have only a single HydraHarp device in the lab so selection is not necessary

        if self._dev is None:
            print("No device available.")
            raise Exception("No device available")

        print("\nInitializing the device...")

        # Histo mode with internal clock
        self._check_error(self.hhlib.HH_Initialize(self._dev, ct.c_int(MODE_HIST), ct.c_int(0)), \
                          "Initialize")

        hwPartno = ct.create_string_buffer(b"", 8)
        hwVersion = ct.create_string_buffer(b"", 8)
        hwModel = ct.create_string_buffer(b"", 16)
        # Only for information
        self._check_error(self.hhlib.HH_GetHardwareInfo(self._dev, hwModel, hwPartno, hwVersion), \
                          "GetHardwareInfo")
        print("Found Model %s Part no %s Version %s" % (hwModel.value.decode("utf-8"), \
                                                        hwPartno.value.decode("utf-8"),
                                                        hwVersion.value.decode("utf-8")))

        numChannels = ct.c_int()
        self._check_error(self.hhlib.HH_GetNumOfInputChannels(self._dev, byref(numChannels)), \
                          "GetNumOfInputChannels")
        print("Device has %i input channels." % numChannels.value)
        self._num_channels = numChannels.value

        print("\nCalibrating...")
        self._check_error(self.hhlib.HH_Calibrate(self._dev), "Calibrate")

        ret = self.hhlib.HH_SetHistoLen(self._dev, ct.c_int(MAXLENCODE), byref(self._mem_histLen))
        self._check_error(ret, "SetHistoLen")

        # following fields have to be created manually; others will be created by the setters
        self._sync_cfd = 50
        self._sync_zerocross = 10

        self._binning = 0
        self._offset = 0
        self._tacq = 1000  # Measurement time in millisec
        self._sync_div = 1
        self._sync_zero_cross = 10  # in mV
        self._sync_cfd = 50  # in mV
        self._sync_offset = -5000  # in ps, like a cable delay

        self._resolution = 512  # in ps
        self._channel_cfd = []  # in mVs
        self._channel_zero_cross = []  # in mVs
        self._channel_offset = []
        for i in range(0, self._num_channels):
            self._channel_cfd.append(160)  # in mVs
            self._channel_zero_cross.append(10)  # in mVs
            self._channel_offset.append(0)  # in ps

    @property
    def num_channels(self):
        return self._num_channels

    @property
    def binning(self):
        return self._binning

    @binning.setter
    def binning(self, value):
        ret = self.hhlib.HH_SetBinning(self._dev, ct.c_int(value))
        if self._check_error(ret, 'SetBinning'):
            self._binning = value
        resolution = ct.c_double()
        ret = self.hhlib.HH_GetResolution(self._dev, byref(resolution))
        if self._check_error(ret, "GetResolution"):
            self._resolution = resolution.value

    @property
    def resolution(self):
        return self._resolution

    @property
    def offset(self):
        return self._offset

    @offset.setter
    def offset(self, value):
        if value > OFFSETMAX:
            value = OFFSETMAX
        if value < OFFSETMIN:
            value = OFFSETMIN
        ret = self.hhlib.HH_SetOffset(self._dev, ct.c_int(value))
        if self._check_error(ret, 'SetOffset'):
            self._offset = value

    @property
    def sync_divider(self):
        return self._sync_div

    @sync_divider.setter
    def sync_divider(self, value):
        if value > SYNCDIVMAX:
            value = SYNCDIVMAX
        if value < SYNCDIVMIN:
            value = SYNCDIVMIN
        ret = self.hhlib.HH_SetSyncDiv(self._dev, ct.c_int(value))
        if self._check_error(ret, 'SetSyncDiv'):
            self._sync_div = value

    @property
    def sync_cfd(self):
        return self._sync_cfd

    @sync_cfd.setter
    def sync_cfd(self, value):
        if value > DISCRMAX:
            value = DISCRMAX
        if value < DISCRMIN:
            value = DISCRMIN
        ret = self.hhlib.HH_SetSyncCFD(self._dev, ct.c_int(value), ct.c_int(self._sync_zero_cross))
        if self._check_error(ret, 'SetSyncCFD'):
            self._sync_cfd = value

    @property
    def sync_zero_crossing(self):
        return self._sync_zero_cross

    @sync_zero_crossing.setter
    def sync_zero_crossing(self, value):
        if value > ZCMAX:
            value = ZCMAX
        if value < ZCMIN:
            value = ZCMIN
        ret = self.hhlib.HH_SetSyncCFD(self._dev, ct.c_int(self._sync_cfd), ct.c_int(value))
        if self._check_error(ret, 'SetSyncZeroCross'):
            self._sync_zero_cross = value

    @property
    def sync_offset(self):
        return self._sync_offset

    @sync_offset.setter
    def sync_offset(self, value):
        if value > CHANOFFSMAX:
            value = CHANOFFSMAX
        if value < CHANOFFSMIN:
            value = CHANOFFSMIN
        ret = self.hhlib.HH_SetSyncChannelOffset(self._dev, ct.c_int(value))
        if self._check_error(ret, 'SetSyncChannelOffset'):
            self._sync_offset = value

    @property
    def acquisition_time(self):
        return self._tacq

    @acquisition_time.setter
    def acquisition_time(self, value):
        self._tacq = value

    @property
    def channel_cfd(self):
        return self._channel_cfd

    @channel_cfd.setter
    def channel_cfd(self, value):

        for i in range(0, self._num_channels):
            if value[i] > DISCRMAX:
                value[i] = DISCRMAX
            if value[i] < DISCRMIN:
                value[i] = DISCRMIN
            ret = self.hhlib.HH_SetInputCFD(self._dev, ct.c_int(i), ct.c_int(value[i]),
                                            ct.c_int(self._channel_zero_cross[i]))
            if not self._check_error(ret, 'SetInputCFD'):
                return
        self._channel_cfd = value

    @property
    def channel_zero_crossing(self):
        return self._channel_zero_cross

    @channel_zero_crossing.setter
    def channel_zero_crossing(self, value):
        for i in range(0, self._num_channels):
            if value[i] > ZCMAX:
                value[i] = ZCMAX
            if value[i] < ZCMIN:
                value[i] = ZCMIN

            ret = self.hhlib.HH_SetInputCFD(self._dev, ct.c_int(i), ct.c_int(self._channel_cfd[i]), ct.c_int(value[i]))
            if not self._check_error(ret, 'SetInputZeroCross'):
                return
        self._channel_zero_cross = value

    @property
    def channel_offset(self):
        return self._channel_offset

    @channel_offset.setter
    def channel_offset(self, value):
        for i in range(0, self._num_channels):
            if value[i] > CHANOFFSMAX:
                value[i] = CHANOFFSMAX
            if value[i] < CHANOFFSMIN:
                value[i] = CHANOFFSMIN

            ret = self.hhlib.HH_SetInputChannelOffset(self._dev, ct.c_int(i), ct.c_int(value[i]))
            if not self._check_error(ret, 'SetInputChannelOffset'):
                return
        self._channel_offset = value

    def close_device(self):
        try:
            super().close_device()
        except:
            pass

        try:
            self.hhlib.HH_CloseDevice(self._dev)
            self._dev = None
        except:
            pass

    def _check_error(self, retcode, funcName):
        if retcode == 0:
            return True
        elif retcode < 0:
            errorString = ct.create_string_buffer(b"", 40)
            self.hhlib.HH_GetErrorString(errorString, ct.c_int(retcode))
            print("HH_%s error %d (%s)." % (funcName, retcode, \
                                            errorString.value.decode("utf-8")))
            return False

    def status(self):
        """ This function will be called periodically to monitor the state
        of the device. It should return a dictionary describing the current
        state of the device. This dictionary will be delivered to the
        front-end class."""
        d = super().status()
        d["resolution"] = self._resolution
        d["acquisition_time"] = self.acquisition_time
        d["sync_divider"] = self._sync_div
        d["sync_cfd"] = self._sync_cfd
        d["sync_zero_crossing"] = self.sync_zero_crossing
        d["channel_cfd"] = self._channel_cfd
        d["channel_zero_crossing"] = self.channel_zero_crossing
        d["sync_offset"] = self._sync_offset
        d["channel_offset"] = self.channel_offset
        d["offset"] = self._offset
        d["sync_rate"] = self.get_sync_rate()
        d["channel_rates"] = self.get_channels_rate()
        return d

    @remote
    def get_sync_rate(self):
        ret = self.hhlib.HH_GetSyncRate(self._dev, byref(self._mem_syncRate))
        self._check_error(ret, "GetSyncRate")
        return self._mem_syncRate.value

    @remote
    def get_channels_rate(self):
        rates = []
        for i in range(0, self.num_channels):
            self.hhlib.HH_GetCountRate(self._dev, ct.c_int(i), byref(self._mem_countRate))
            rates.append(self._mem_countRate.value)
        return np.array(rates)

    @remote
    def start_measurement(self):
        # TODO stop previous measurement if necessary
        ret = self.hhlib.HH_ClearHistMem(self._dev)
        self._check_error(ret, "ClearHistMem")
        ret = self.hhlib.HH_SetHistoLen(self._dev, ct.c_int(MAXLENCODE), byref(self._mem_histLen))
        self._check_error(ret, "SetHistoLen")
        ret = self.hhlib.HH_StartMeas(self._dev, ct.c_int(round(self.acquisition_time)))
        self._check_error(ret, "StartMeas")

    @remote
    def stop_measurement(self):
        ret = self.hhlib.HH_StopMeas(self._dev)
        self._check_error(ret, "StopMeas")

    @remote
    def is_running(self):
        """ Returns true if acquisition is in progress """
        ctcstatus = ct.c_int(0)
        self.hhlib.HH_CTCStatus(self._dev, byref(ctcstatus))
        return (ctcstatus.value == 0)

    @remote
    def get_histograms(self, clear=False):
        if clear:
            clear_flag = ct.c_int(1)
        else:
            clear_flag = ct.c_int(0)
        for i in range(self.num_channels):
            ret = self.hhlib.HH_GetHistogram(self._dev, byref(self._mem_counts[i]),
                                             ct.c_int(i), clear_flag)
            self._check_error(ret, 'GetHistogram')
        return np.array(self._mem_counts)  # TODO: crop on histlen


@include_remote_methods(HydraHarpWorker)
class HydraHarp(DeviceOverZeroMQ):
    """ Simple stub for the class to be accessed by the user """

    def __init__(self, req_port=default_req_port, pub_port=default_pub_port, **kwargs):
        super().__init__(req_port=req_port, pub_port=pub_port, **kwargs)
        self.setting_window = None
        self.status_copy = {}

    def createDock(self, parentWidget, menu=None):

        dock = QtWidgets.QDockWidget("HydraHarp", parentWidget)
        widget = QtWidgets.QWidget(parentWidget)
        layout = QtWidgets.QGridLayout(parentWidget)
        widget.setLayout(layout)

        self.sync_label = QtWidgets.QLabel('Sync rate')
        self.sync_display = QtWidgets.QLineEdit()

        self.channel_label = QtWidgets.QLabel('Channel rate')
        self.channel_display = QtWidgets.QLineEdit()
        self.selector = QtWidgets.QSpinBox()
        self.selector.setRange(1, self.num_channels)

        self.btn_settings = QtWidgets.QPushButton("Settings")
        self.btn_settings.clicked.connect(self.show_settings)

        layout.addWidget(self.sync_label, 0, 0)
        layout.addWidget(self.sync_display, 0, 1)
        layout.addWidget(self.btn_settings, 0, 3)

        layout.addWidget(self.channel_label, 1, 0)
        layout.addWidget(self.channel_display, 1, 1)
        layout.addWidget(self.selector, 1, 3)

        dock.setWidget(widget)
        dock.setAllowedAreas(QtCore.Qt.TopDockWidgetArea | QtCore.Qt.BottomDockWidgetArea)
        parentWidget.addDockWidget(QtCore.Qt.TopDockWidgetArea, dock)
        if menu:
            menu.addAction(dock.toggleViewAction())

        self.createListenerThread(self.updateSlot)

    def show_settings(self, status):

        print("settings")
        self.btn_settings.setEnabled(0)
        if self.setting_window is not None:
            self.setting_window.show()
        else:
            sett_window = SettingsWindow(self)
            sett_window.show()

            def on_finished(code):
                self.btn_settings.setEnabled(True)

            sett_window.exec_()
            self.btn_settings.setEnabled(True)

    def updateSlot(self, status):
        self.sync_display.setText('%d' % status['sync_rate'])
        self.channel_display.setText('%d' % status['channel_rates'][self.selector.value() - 1])
        self.status_copy = status


# ==============================================================================

class SettingsSubWindow(QtWidgets.QWidget):
    def __init__(self, obj_inst, title):
        super().__init__()
        self.parent = obj_inst
        self.title = title
        self.cfd_val = 0
        self.zerocq_val = 0
        self.offset_val = 0
        self.initUI()
        self.updateDisplay()

    def initUI(self):
        self.layout = QtWidgets.QFormLayout()

        self._title = QtWidgets.QLabel()
        self._title.setText(self.title)
        self._cfd = QtWidgets.QLineEdit()
        self._zerocq = QtWidgets.QLineEdit()
        self._offset = QtWidgets.QLineEdit()

        self.layout.addRow('', self._title)
        self.layout.addRow('Discr. mV', self._cfd)
        self.layout.addRow('Zero cr. mV', self._zerocq)
        self.layout.addRow('Offset ps', self._offset)

        self.setLayout(self.layout)

    def updateDisplay(self):
        self._cfd.setText(str(self.cfd_val))
        self._zerocq.setText(str(self.zerocq_val))
        self._offset.setText(str(self.offset_val))


class SettingsWindow(QtWidgets.QDialog):
    def __init__(self, object_inst):
        super().__init__()
        self.parent = object_inst
        self.initUI()

    def initUI(self):

        self.sync_ui = SettingsSubWindow(self, 'Sync')
        self.sync_ui.cfd_val = self.parent.status_copy['sync_cfd']
        self.sync_ui.zerocq_val = self.parent.status_copy['sync_zero_crossing']
        self.sync_ui.offset_val = self.parent.status_copy['sync_offset']
        self.sync_ui.updateDisplay()
        self.channels = {}
        for x in range(0, 8):
            self.channels["channel_{0}".format(x + 1)] = SettingsSubWindow(self, "Channel {0}".format(x + 1))
            self.channels["channel_{0}".format(x + 1)].cfd_val = self.parent.status_copy['channel_cfd'][x]
            self.channels["channel_{0}".format(x + 1)].zerocq_val = self.parent.status_copy['channel_zero_crossing'][x]
            self.channels["channel_{0}".format(x + 1)].offset_val = self.parent.status_copy['channel_offset'][x]

            self.channels["channel_{0}".format(x + 1)].updateDisplay()

        self.grid = QtWidgets.QGridLayout()
        # self.widget2=QtWidgets.QDialog()
        self.setWindowModality(True)  # do not block other windows
        self.setWindowTitle("HydraHarp")
        # self.setGeometry(300, 300, 280, 450) #x,y,w,h

        self.accept_changes = QtWidgets.QPushButton('Accept')
        self.accept_changes.clicked.connect(self.updateChannels)
        self.grid.addWidget(self.sync_ui, 0, 0)
        for i in range(0, 8):
            if i < 4:
                self.grid.addWidget(self.channels.get("channel_{0}".format(i + 1)), 0, (i + 1) * 5)
            else:
                self.grid.addWidget(self.channels.get("channel_{0}".format(i + 1)), 1, (i - 4) * 5)
        self.grid.addWidget(self.accept_changes, 3, 0)

        self.setLayout(self.grid)

    def updateChannels(self):

        self.sync_ui.cfd_val = int(self.sync_ui._cfd.text())
        self.sync_ui.zerocq_val = int(self.sync_ui._zerocq.text())
        self.sync_ui.offset_val = int(self.sync_ui._offset.text())

        self.parent.sync_cfd = self.sync_ui.cfd_val
        self.parent.sync_zero_crossing = self.sync_ui.zerocq_val
        self.parent.sync_offset = self.sync_ui.offset_val

        self.cfd_list = []
        self.zerocq_list = []
        self.offset_list = []
        for x in range(0, 8):
            self.channels["channel_{0}".format(x + 1)].cfd_val = int(
                self.channels["channel_{0}".format(x + 1)]._cfd.text())
            self.channels["channel_{0}".format(x + 1)].zerocq_val = int(
                self.channels["channel_{0}".format(x + 1)]._zerocq.text())
            self.channels["channel_{0}".format(x + 1)].offset_val = int(
                self.channels["channel_{0}".format(x + 1)]._offset.text())
            self.cfd_list.append(self.channels["channel_{0}".format(x + 1)].cfd_val)
            self.zerocq_list.append(self.channels["channel_{0}".format(x + 1)].zerocq_val)
            self.offset_list.append(self.channels["channel_{0}".format(x + 1)].offset_val)
        self.parent.channel_cfd = self.cfd_list
        self.parent.channel_zero_crossing = self.zerocq_list
        self.parent.channel_offset = self.offset_list