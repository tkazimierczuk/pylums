#use_autocreated_file:#2#locked:#1#
# Example sets power at each step using Power Tab. Remember to enable Wirtual Device Worker in Power Tab.
# Wavelength calibration is presented as an example. You should perform your calibration using calibration script. 
# This calibration was performed for Ti:Sapp laser on 2019-11-19 and it does not cover full spectral range.

from devices.virtual.power_tab import PowerTabInterface
power_tab = PowerTabInterface(req_port=11010)

# you have to add stepper at host=10.96.1.77, req_port = 7040, pub_port = 7041 to devices.ini
axis = stepper_lasers.get_axis(1) # axis 0: rhodamine, axis 1: titan - sapphire 

#calibration data - lists of wavelengths and corresponding motor positions 
xs = [705.1236094,707.7221207,710.2282492,712.7564349,715.2051951,717.7538669,720.2365529,722.7535293,725.1987197,727.5917351,730.0089792,732.4758409,734.929508,737.2015679,739.6372733,741.9627672,744.3336348,746.6620641,748.8637548,751.1646532,753.3291292,755.5655941,757.7207024,759.7968536,761.9923137,763.9918786,766.1474937,768.1442411,770.0309535,772.0532812,773.9261267,775.8702944,777.7079647,779.4218164,781.2532965,782.9180573]
ys = [4.9,5,5.1,5.2,5.3,5.4,5.5,5.6,5.7,5.8,5.9,6,6.1,6.2,6.3,6.4,6.5,6.6,6.7,6.8,6.9,7,7.1,7.2,7.3,7.4,7.5,7.6,7.7,7.8,7.9,8,8.1,8.2,8.3,8.4]

def set_wavelength(wavelength, wait=True):
    axis.move_absolute(numpy.interp(wavelength + 0.8, xs, ys))
    while wait and not axis.is_stopped():
        sleep(0.1)