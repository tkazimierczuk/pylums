#use_autocreated_file:#0#locked:#1#
# Calibration script for tuned lasers (rhodamine and Ti:Sapphire)
# Please make sure, that laser peak is the strongest peak visible on spectrometer for whole spectral range.
# Resulting calibration data can be trimmed manually afterwards.

# you have to add stepper at host=10.96.1.77, req_port = 7040, pub_port = 7041 to devices.ini
axis = stepper_lasers.get_axis(1) # axis 0: rhodamine, axis 1: titan - sapphire 

def take_spectrum():
    spectrometer.start_acquisition()
    while spectrometer.acquisition_status() == 'running':
        sleep(0.1)
    return spectrometer.get_latest_data()

positions = list(numpy.arange(4.9, 8.401, 0.1))
wavelengths = []
for i in range(len(positions)):
    #axis.move_absolute(positions[i])
    while not axis.is_stopped():
        sleep(0.1)
    x, y = take_spectrum()
    # We need to find maximum of the peak. Peak will be smoothed out by convolution with triangle function with FWHM equal to 'width'
    width = 12
    triangle = numpy.convolve(numpy.ones(width), numpy.ones(width))
    smoothed = numpy.convolve(y, triangle, 'same')
    maximum = numpy.argmax(smoothed)
    wavelengths.append(x[maximum])
    progress(i / len(positions))
    
str1 = "xs = [" + ", ".join([str(round(v, 9)) for v in wavelengths]) + "]"
str2 = "ys = [" + ", ".join([str(round(v, 9)) for v in positions]) + "]"
print(str1)
print(str2)
 
  