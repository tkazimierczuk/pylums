#use_autocreated_file:#2#locked:#1#
# Example measurement of PLE using rhodamine or Ti:Sapphire (default) laser.

include "(predefined) PLE - common functions"
   
wavelengths_exc = list(numpy.arange(736,756.01,0.5))
i = 0
for wl in wavelengths_exc:
    set_wavelength(wl)
    power_tab.set_power_uw(10)
    wavelengths, data = spectrometer.take_single_spectrum()
    file.save_snapshot(x=wavelengths, y=wl, z=data)
    print("%f" % (wl,))
    i += 1
    progress(i/len(wavelengths_exc))
file.close()