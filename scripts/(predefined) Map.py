#use_autocreated_file:#2#locked:#1#
# Map widget remote use example. Worker must be enabled in remote operation panel in map widget.
from devices.virtual.map_widget import MapWidgetInterface
map_widget = MapWidgetInterface(req_port=11020)

points = map_widget.get_points()
ax = map_widget.get_x_axis() # or map_widget.get_axis("x") 
ay = map_widget.get_y_axis() # or map_widget.get_axis("y") 
spectrometer = map_widget.get_spectrometer()

# Main file is created automatically by measurement tab. Creating log files:
file_log1 = open(file.filename + ".log1", "a")
file_log2 = open(file.filename + ".log2", "a")

for i in range(len(points)):
    x, y = points[i]
    ax.move_absolute(x)
    ay.move_absolute(y)
    
    # Wait for axes to stop. Map widget normally uses fixed time delay, as it is the only way for attocube
    while not ax.is_stopped() or not ay.is_stopped():
        sleep(0.02)
    
    spectrometer.start_acquisition()
    while spectrometer.acquisition_status() != 'idle':
        sleep(0.02)
    
    real_x = ax.get_position()
    real_y = ay.get_position()
    
    data_x, data_z = spectrometer.get_latest_data()
    file.save_snapshot(x=data_x, y=i+1, z=data_z)
    file_log1.write(str(real_x) + "\n")
    file_log2.write(str(real_y) + "\n")

    progress((i + 1) / len(points))
    
file_log1.close()
file_log2.close()