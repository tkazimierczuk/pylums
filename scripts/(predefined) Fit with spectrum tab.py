#use_autocreated_file:#0#locked:#1#
from devices.virtual.spectrum_tab import SpectrumTabInterface
tab = SpectrumTabInterface(req_port=11030)

for i in range(2):
    x0 = 600 - i * 150
    bestfit, pcov = tab.fit(track=i, init_values_dict={'x0':x0})
    print(bestfit)