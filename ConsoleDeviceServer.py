# -*- coding: utf-8 -*-
"""
Created on Sat May 26 09:54:02 2018

@author: tkaz
"""

import devices
import sys,traceback
import multiprocessing
   
class CustomPrint():
    def __init__(self, info):
        self.old_stdout=sys.stdout
        self.info = info

    def write(self, text):
        text = text.rstrip()
        if len(text) == 0: 
            return
        self.old_stdout.write(self.info + text + '\n')
    
    def flush(self):
        pass
   
def process_for_device(name, WorkerClass, kwargs): #note: double star (**) is not missing!        
    sys.stdout = CustomPrint('['+ name + '|out]')
    sys.stderr = CustomPrint('['+ name + '|err]')
    w = WorkerClass(**kwargs)
    w.init_device()
    w.start_server(blocking=True)

if __name__ == '__main__':
    child_processes = []
    
    import sys
    workers_desc = devices.load_workers()
    
    for name,cls,kwargs in workers_desc:
        try:
            try:
                autostart = kwargs['pylums_autostart'].lower() in ['true', '1', 't', 'y', 'yes']
                del(kwargs['pylums_autostart'])
            except KeyError:
                autostart = False
            
            if not autostart:
                print("Device %s has no autostart flag set" % name)
                continue
                
            print("Starting device %s" % name)            
            
            process = multiprocessing.Process(None, process_for_device, 
                    'pyLUMS - %s' % name,
                    (name, cls, kwargs))
            process.daemon = True
            process.start()
            
            child_processes.append(process)                
                
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            
    for p in child_processes:
        p.join()